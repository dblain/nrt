# Change Log
All notable changes to this project will be documented in this file.
All changes listed below the 3.4.0 version were never released here and are
given for information.

The format is based on [Keep a Changelog](http://keepachangelog.com)
and this project adheres to [Semantic Versioning](http://semver.org).

## [7.0.0] - 2018-12-21
### Added
- Module ```Astra``` for planet and star objects.
- Class ```Target```, used to store physical (mass, radii, light source, ...) and geometric parameters (emission/incidence angles, latitudes/longitudes, ...).
- Class ```InstrumentDataFile``` and other minor stuffs to support instrument LSF varying with wavenumber.
- Function ```get_pixel_geometry``` to return Spice geometry without searching full intercept.
- [Fortran] Convolution by an instrument LSF varying with wavenumber.

### Changed
- VMR profiles directory moved into the atmospheres directory.
- Absorption cross sections directory moved into molecules directory.
- Doppler shift is now calculated relative to the surface point rather than relative to the barycenter of the main body.
- Cloud reflectance works now as a maximum, sum with transmittance cannot exceed 1.
- Renamed some radiance conversion functions to be more generic.
- Renamed functions ```from_dat``` into ```from_data```.
- Renamed parameter ```n_process``` into ```number_processes```.
- Renamed parameter ```path_attenuation``` into ```path_absorption_cross_sections```.
- [Fortran] "Solar" temperature, distance and radius are now input parameters.
- [Fortran] Atmospheric mass is now calculated level-by-level.
- [Fortran] Surface gravity is now calculated from the target mass and its equatorial and polar radii.
- [Fortran] Cross sections format changed to pseudo HITRAN .xsc.
- [Fortran] H2 and He are now treated as normal molecules.

### Removed
- Now useless ```optical_depths``` directory.
- Deprecated ```DerivativeCloudTransmittance``` plot function.
- Deprecated ```merge_spec``` function.

### Fixed
- ```spice_kernels``` functions misbehaviour. 
- Attribute ```EXT``` of some high-level ```Files``` not set as there parents.
- Attribute ```PATH``` of some high-level ```Files``` not set correctly.
- ```Noise``` is not loaded before calculating chi2 after retrievals.
- Crash when writing a ```DataFile``` from floats instead of iterables.
- ```download_from``` function always checking for a valid certificate (some time this is not appropriate)
- [Fortran] Crash when writing VMR profiles with extreme values.
- [Fortran] Gaussian function not working properly.
- [Fortran] Function ```mean_restep``` not working properly.
- [Fortran] Standard MPI output custom functions not synchronized.

## [6.1.2] - 2018-11-19
### Added
- [Fortran]Info about optimal synthetic resolution.

### Fixed
- [Fortran]"Mean restep" of spectrum and derivatives not working properly on multiple processes.


## [6.1.1] - 2018-11-15
### Fixed
- Mean Volume Mixing Ratios not being calculated properly.

## [6.1.0] - 2018-11-14
### Added
- Saturation profiles limitations in retrieving.
- Functions to generate Lines from wavenumbers and isotopic mixing.
- Function to generate VMR from scale height.
- [Fortran]Convolution wavenumbers to speed up retrieval, at the cost of a (minor) loss in accuracy.
- More comments !

### Changed
- NRT default compiler changed from mpif90 to mpifort.
- Default number of processes lowered from 8 to 4.
- [Fortran]Updated to OpenMPIv4.0.0 and now using mpi_f08.
- [Fortran]Revised inputs code architecture to make it clearer.
- [Fortran]Changed a few variable names to make them clearer.

### Fixed
- VMR profile interpolation not working.
- Crash when launching NRT without instrument and without convolving.
- [Fortran]Some memory management minor issues.

## [6.0.0] - 2018-10-09
### Added
- Handling of arrays of latitudes and longitudes.
- Handling of noise-per-pixel.
- Function ```get_mean_from_spectra``` to ```SpectralCube```.
- [Fortran]Handling of NaN in observed spectrum and noise.
- [Fortran]Check of emission and incidence angles.
- More comments !

### Changed
- Replaced "psf" in variable names to the more accurate "lsf" (for Light Spread Function).
- ```Spectrum``` are no longer automatically loaded when initialized.

### Fixed
- Instruments not loaded before writing of the input file.
- Functions ```Spectrum.errorbar()``` not displaying VMR profiles.
- [Fortran]Useless warnings if the instrument LSF is larger than the synthetic spectrum.

### Removed
- Argument ```emission_angle_max``` of maps functions.
- [Fortran]Useless sharing of the observed spectra, should improve performances.
- [Fortran]Useless extra radiance calculation during retrieval, should improve performances.
- [Fortran]Forward model error calculation during retrieval

## [5.1.0] - 2018-09-21
### Added
- Argument ```reject_partial_intercept``` to ```spice_kernels.get_pixel_boresight_intercept_geometry()```.
- Argument ```radiance_per_wavenumber``` to ```SpectralSlice.from_radiance_file()```.
- Info message when rejecting a ```SpectralSlice``` because of undesired target.
- More comments !

### Changed
- Renamed ```VMRProfile``` parameter ```pressure``` to ```pressures```.
- Lines are now automatically updated after H2-He broadening calculations.

### Fixed
- Non-systematic display of NaN encountered error when reading attenuation cross-section file.
- Over-calculation of H2-He HWHM broadening temperature coefficients.
- Crash when reading a ```Lines``` file without integer to describe upper J.
- ```SpectralSlice``` auto-name not using ISO 8601 format.

## [5.0.0] - 2018-09-07
### Added
- Cartopy support.
- Hotfix file for hexbin cartopy support.
- Functions ```load()``` and ```load_all()``` to ```Molecule```.
- Attribute ```DEFAULT_SIZE``` to ```Figure```.
- Functions ```get_figsize_from_width()```, ```get_figsize_from_resolution()```, ```resize_labels()``` and ```save_fig()``` to ```Figure```.
- More comments !

### Changed
- ```Molecule``` sub-objects and ```TemperatureProfile``` no longer automatically load when find.
- Renamed ```TemperatureProfile``` parameter ```pressure``` to ```pressures```.
- Renamed ```TemperatureProfile``` parameter ```temperature``` to ```temperatures```.
- Renamed arguments of function ```velocity2doppler()```.
- Functions ```interpol_*``` of ```Atmosphere``` are now "hidden" (added ```_``` at the beginning of their name).
- Updated example scripts.
- Default figure font size from 12 to 18.
- Standardized code writing a bit.

### Fixed
- Multiple cloud total transmittance calculations giving wrong results.
- Retrieval unstability if the kernel contain values near or equal to 0.
- Font size of colorbar in ```DerivativeVMR``` function ```plot``` not behaving properly.
- Lack of comments and poor semantic in some of the oldest functions.

### Removed
- Useless argument ```file``` remaining in some ```read()``` and ```write()``` functions.
- Unused and buggy GUI.
- Unused vibrational quantum numbers identification from GEISA parameter E1.
- Useless variable in retrieval code.
- Outdated Basemap support.

## [4.10.1] - 2018-05-04
### Changed
- Renamed ```Spectrum``` parameter ```XUNITS``` to ```X_UNITS```.
- Renamed ```Spectrum``` parameter ```YUNITS``` to ```RADIANCE_UNITS```.

### Fixed
- Function ```plot_projected_feature_map``` not working.
- ```SpectralCube``` function ```quick_stats``` not behaving properly.
- ```Spectrum``` function ```errorbar``` not behaving properly.
- ```ObjList``` function ```call``` not behaving properly.
- Clarified error in ```SpectralCube``` function ```get_from_spectra```.

### Removed
- Parameter ```zero_to_mask``` in some functions.

## [4.10.0] - 2018-04-25
### Added
- Directory ".inputs/optical_depths/column_attenuation_coefficients" to store H2-He continuum files.

### Changed
- Source directory "mpi" renamed "Fortran".
- Input directory "opacities" renamed "optical_depths".
- Input directory "MPI" renamed "attenuation_cross_sections".
- Input directory "abundance_profiles" renamed "vmr_profiles".
- Output directory "abundance_profiles" renamed "vmr_profiles".
- Replaced "abundance" in variables, functions and classes name by "VMR".

## [4.9.1] - 2018-04-23
### Fixed
- Function ```get_from_spectra``` of ```SpectralCube``` object not behaving properly.

## [4.9.0] - 2018-04-21
### Added
- BibTex support in module ```files```.
- Cell sheet support in module ```files```.
- Function to convert a string into either a boolean, an integer or a float.

## [4.8.1] - 2018-04-21
### Changed
- More Fortran code clarifications.

### Fixed
- Bug in VMR output introduced by previous version.
- ```SpectralCube``` function ```plot_map``` not working.

## [4.8.0] - 2018-04-18
### Added
- (WIP) SPICE support using [spiceypy](https://github.com/AndrewAnnex/SpiceyPy).
- Module ```spice_kernels``` for SPICE kernels object. 
- Ephemeris time conversion functions  to J2000.
- File function to download files from a url and a regex pattern.
- Discrimination between spectra and images during ```SpectralSlice``` loading from radiance file (works well with JIRAM data).
- Attribute ```metakernel``` to ```SpectralCube``` object, to store SPICE metakernels.
- Attribute ```array_shape``` to ```Instrument``` object.
- Attributes ```start_time``` and ```stop_time``` to ```Spectrum``` object.
- Attribute ```is_continuum``` to ```Molecule``` object.
- Method to correct the Doppler shift of a ```Spectrum```.

### Changed
- Output radiance unit is now W.m-2.sr-1/cm-1 instead of W.cm-2.sr-1/cm-1.
- File "module_nrt_retrieving" renamed "module_nrt_retrieval".
- ```SpectralCube``` method ```velocity2doppler()``` moved into module ```math```.
- ```Atmosphere``` attribute ```zenith_angle``` renamed ```incidence_angle```.
- ```Molecule``` attribute ```qv``` renamed ```n_vibrational_band```.
- ```Molecule``` attribute ```v_nu``` renamed ```vibrational_bands_wavenumber```.
- ```Molecule``` attribute ```v_d``` renamed ```vibrational_bands_degeneracy```.
- ```run_nrt``` parameter ```write_mpi_absorption_files``` renamed ```write_absorption_coefficients_files```.
- ```run_nrt``` parameter ```path_observed_spectrum``` renamed ```path_retrieved_spectrum```.
- Function ```linarange``` moved from module ```math_``` to module ```utils```.
- Function ```stack_maps``` moved from module ```math_``` to module ```utils``` and renamed ```stack_arrays```.
- Functions ```*subattr``` moved into object ```ObjList```.
- Clarified radiance conversion functions.
- Python PEP cleaning.
- Performances of matrix inversion enhanced.
- Clarified Fortran code.
- Cleaned Fortran code a bit (H2-He continuum function is still _terra incognita_).
- Example scripts archive is lighter.

### Removed
- Hard limit on number of molecule in atmosphere.
- Hard limit on number of cloud in atmosphere.
- Function ```merge_spec_maps``` from object ```SpectralCube```.
- Attribute ```time``` from object ```Spectrum```.
- Temporary removed vibrational partition function recalculation.

### Fixed
- Function ```from_pkl``` of object ```SpectralCube``` not behaving properly.
- Docstring in absorption coefficients calculation code.
- Radiance conversion functions.

## [4.7.0] - 2018-03-09
### Added
- Object SpectralSlice, designed to read JIRAM data.
- Atmosphere function to calculate the angle between two coordinates.
- File functions to update attribute "file" from attributes "name", "path" and "ext", and reciprocally.
- File FortranUnformattedFile, to read Fortran unformatted binary files.
- File PDS3LabelFile, to read PDS3 label files.
- Numerous SpectralCube metadata attributes.
- SpectralCube function to calculate the wavenumber Doppler shift at a given velocity.
- Quick initialisation file.
- Some radiance units conversion function.
- Some docstrings.

### Changed
- Directory "inputs/atmospheres_ap" renamed "atmospheres".
- Directory "inputs/abundance_profiles" moved to "inputs/molecules/abundance_profiles".
- Directory "inputs/lines" moved to "inputs/molecules/lines".
- Directory "inputs/mol_parameters" moved to "inputs/molecules/parameters".
- Module "math" renamed "math_" to avoid conflict with other library modules.
- Added "s" to the name of array-like attributes.
- Atmosphere attribute "viewing_angle" renamed "emission_angle".
- Atmosphere attribute "sol_angle" renamed "zenith_angle".
- Atmosphere function "airmass2angle" now always output an angle in degrees.
- Atmosphere function "write_all" renamed "init_all".
- Dates now follow ISO 8601 format.
- Reworked SpectralCube attributes to avoid redundancy with Spectrum metadata attributes and to add useful informations.
- Renamed some SpectralCube functions.
- Function "mpicompile_nrt" now can take compiler and linker flags as arguments.
- SpectralCube class method "from_sav" is now faster and easier to understand.
- Updated some of the oldest methods.
- Updated some docstrings.
- File method "search" is now a class method.
- Molecule lines files are now checked when auto loading a Molecule.
- Molecules parameters are now auto-loaded only if molar mass or rotational partition exponent are set to 0.

### Removed
- Atmosphere attribute "airmass".
- Atmosphere attribute "OBJECT".
- Some spectral cubes attributes, like "latitude_map".
- Deprecated SpectralCube functions "airmass_fx" and "sav2pkl".
- Deprecated Atmosphere function "use_molecules_as_default".

### Fixed
- Error when executing the Fortran code on non-avx processors.
- Error when encountering unknown instrument function shape not stopping execution.
- File initialisation flags "exists" and "find" not behaving properly.
- File initialisation without "file" not behaving properly.
- Info message not using the utils.info function.
- Some typos.

## [4.6.1] - 2018-02-02
### Fixed
- Crash of Spectrum function location2str if location is of size 1.
- Typos in the changelog.

## [4.6.0] - 2018-02-01
### Added
- Reflectance support in Cloud object and cloud model.

### Changed
- Some arguments name of SpectralCube function retrieval_accurate.

## [4.5.0] - 2018-01-31
### Known issues
- The current cloud model uses a hardcoded cloud reflectance, fixed at 0.15; the user wanting to change this have to modify line 1430 of file module_nrt_physics.

### Added
- Possibility to plot multiple features on basemap plots.
- SpectralCube feature longitudinal averaging function.
- Option in Spectrum errorbar to set the number of points to use to draw the sigma lines of the residuals plot.
- Twin axes for some graphs with two units on the same axis.

### Changed
- Default spectral cube accurate retrieval final retrieval weight of a priori set to 1E-1 instead of 1E-2.

### Removed
- Some deprecated functions.

### Fixed
- Line reader crashing on truncated lines files.
- Line reader crashing when no transition quantum identifications given.
- Crash of SpectralCube ite_stats function.
- Wrong parallel annotations in basemap figures.
- DerivativeVMR plot function (quick update).

## [4.4.1] - 2018-01-08
### Fixed
- Synthetic spectrum interval not trimmed correctly in some cases.
- Cloud top pressure not consistent with cloud base pressure during spectral cube retrieval.

## [4.4.0] - 2017-12-19
### Added
- Default atmosphere and function Atmosphere.use_as_default().
- Function Atmosphere.write_all() to write quickly all the files needed by an Atmosphere.

### Changed
- Function run() renamed run_nrt().
- Function Spectrum.errorbar_spec_and_abundances() renamed Spectrum.errorbar().
- Parameter "wavenumbers" of function Spectrum.calculate_radiances() renamed "wavenumber_lim".

### Removed
- Useless parameter "file" of function Molecule.save_parameters().
- Default molecules.
- Parameter "write_all" of Atmosphere().

### Fixed
- Synthetic spectrum interval not trimmed correctly if the observed spectrum had exactly the same wavenumbers than the 
synthetic spectrum.
- Number of lines indicated in outputted spectra when doing a convolution.
- Crash of function Molecule.auto_load().
- Crash when calculating radiances without an Instrument.
- Crash when reading a DerivativeVMR object.
- Crash when retrieving a Spectrum without a Noise object attached.

## [4.3.0] - 2017-12-07
### Added
- Function to change an equirectangular-projected map into an orthographic-projected map.
- Function to retrieve latitude and longitude of the pixels of a disk.
- Function to perform the inverse orthographic projection.

### Removed
- Deprecated graphic to centric option in function plot_projected_feature_map().

## [4.2.0] - 2017-12-01
### Added
- Module for image processing.
- Function to change a cylindrical-projected map into a equirectangular-projected map.
- Function to change an equirectangular-projected map into a Mollweide-projected map.

### Changed
- SpectralCube function graphic2centric now take the equator-polar ratio in argument.
- Voigt function variables are now all in double precision.

### Fixed
- Function plot_projected_map() not working when using the imshow plot function. 

## [4.1.1] - 2017-11-23
### Fixed
- Some docstrings and comments in the fortran code.

## [4.1.0] - 2017-11-22
### Added
- SpectralCube and VMRProfile functions to convert VMR to ppmv and reciprocally.
- Docstrings to some functions.

### Changed
- Module _runs renamed _scripts.

### Fixed
- SpectralCube pressure-latitude slice figures not working.

## [4.0.0] - 2017-11-16
### Added
- Math functions put in a separate module.
- SpectralCube put in a separate module.
- DataFile File object.
- Module file to store all File objects.
- NameListFile File object.
- PickleFile File object.
- Function retrieve to retrieve spectra.
- Functions retrieve and retrieve_accurate to retrieve SpectralCubes.
- Class method function calculate_radiance to calculate the radiance of a spectrum at given wavenumbers.
- Class method function from_spectrum to replace function auto_name for Spectrum.
- Class method function from_physics to replace function auto_name for Spectrum.
- Verbose option in spectral cube retrieval functions.
- Standard Spectrum units and labels for easier plots.
- Plot options in SpectralCube retrieval.

### Changed
- Complete rework of runs: they are now functions called from Spectrum and SpectralCube objects.
- Atmosphere objects put in a separated module.
- Atmosphere objects (Molecule, Sky, ...) are now outside of object Atmosphere.
- Ephemeris objects put in a separated module.
- Spectrum plots functions moved into Spectrum.
- Module nrt renamed interface.
- Module utils now store all generic functions and objects.
- Module runs renamed _runs. Now it only serves to store the most specific functions.
- File plots renamed figures.
- Objects NRTFile and NRTFigure renamed respectively File and Figure.
- Object StdErr renamed Noise.
- Object NMLLine renamed NameListLine.
- Object SpecCube renamed SpectraCube.
- Clarification of a lot of parameter names.
- When no Noise file or Sky file is specified for a run, 'None' is writen instead of 'F'.
- Switched default basemap map projection from Mollweide to Eckert IV since the latter is more accurate ([source](https://arxiv.org/abs/astro-ph/0608501)).
- Derivatives files are now File objects.

### Removed
- Module figures.

### Fixed
- Bug in molecule related files name when an absorption coefficient file were missing.
- Abundance profiles uncertainty factor not saved in SpectralCubes.
- Crash when plotting figures due to latest version of matplotlib (solution not satisfactory).
- A priori profile not showing up in abundance profile figures.
- Figure not sent to the right directory.

## [3.20.0] - 2017-10-02
### Added
- Ephemeris class, using jdcal package.
- Overwrite option in run_nrt: checks if the outputted spectrum already exists.
- Possibility to select the figure output format in errorbar_spec_and_abdps().
- Function to convert radiance in W.cm-2.sr-1/cm-1 into W.cm-2.sr-1.um-1.
- Function to convert radiance in W.cm-2.sr-1/cm-1 into W.m-2.arcsec-2.um-1.
- Function pressure_latitude_slice() to generate a slice of a 2D feature (e.g. VMR profile) along a longitude.
- More informative error in plot_lat_abundance_profile().

### Changed
- Word "deriv" in some function names renamed "derivative".
- Spectrum parameter "reference" renamed "_reference".
- Convolution of derivatives is done only on retrieved parameters or if derivatives files are outputted.
- Using western longitude in plot_projected_feature_map() now reverse the maps so that West is left.
- Generalised plot_lat_abundance_profile() a bit.

### Fixed
- Stack overflow in mpi_fftconvolve.
- Wrong number of wavenumber in derivative file.
- Parameters using the word "convoluted" now use "convolved" instead.
- Varying output type of function stack_maps().
- Zero-values of latitudes and longitudes maps not systematically masked.
- Wrong label in the outputted figure of run_nrt().
- Unnecessary abundance profile file reading in errorbar_spec_and_abdps().
- Spectrum parameter "reference" not saved.
- Spectrum parameter "time' not appearing in auto name.
- Deleted unused variables declarations in fortran code.

## [3.19.1] - 2017-08-01
### Fixed
- Unnecessary warning messages when initialising an atmosphere with write_all.
- Truncated y-axis ticks in some spectrum figures.

## [3.19.0] - 2017-07-31
### Added
- [Basemap](https://matplotlib.org/basemap) support for spectral cube maps.
- Wavenumber step argument in run_nrt().
- Possibility to automatically save molecule param. when initialising an atm.
- Atmosphere function to load current molecules by default.
- Function to stack maps of uneven shape together.
- Planetographic to planetocentric latitudes conversion function.

### Removed
- All the make arguments of run_nrt().
- Deprecated Atmosphere function write_mol_params().

### Fixed
- Molecular absorption coefficient files being written while explicitly demanded not to.
- Crash when plotting molecule maps at the end of spectral cube retrieval.
- Bad file name when loading molecular parameters.
- Useless warnings when loading molecules.
- Setup file not installing the Fortran retrieving module.

## [3.18.1] - 2017-07-21
### Fixed
- Molecular absorption coefficient files not being written while explicitly demanded to.

## [3.18.0] - 2017-07-21
### Added
- Lacking comments in some Python functions.
- Optional arguments in SpecCube map plotting functions.

### Changed
- Moved *subattr functions from "utils" to "objects".
- Renamed parameter "name" of map plotting functions  into "fig_name".
- Argument "molecules" in map plot. func. no longer needs to be a [Molecule].

### Removed
- Some useless Python functions.

### Fixed
- Crash when trying to plot a spectrum figure at the end of some runs.
- Ugly latitude and longitude overlay around longitude 0°.
- All PEP8 violations, mainly uppercase function arguments.

## [3.17.0] - 2017-07-18
### Added
- Molecule function "vmr2abundance_profile".
- Molecule functions for loading and saving parameters.
- Latitude and longitude overlay on maps.
- Attribute "atmosphere" on spectral cubes.
- Attribute "latitude_map" on spectral cubes.
- Attribute "longitude_map" on spectral cubes.
- Some Fortran functions.

### Changed
- The atm. mean molar mass is calculated from the mean VMR of all the species.
- Molecules parameters are loaded from parameter files instead of hardcoded.
- All Molecules have an abundance profile.
- Molecule parameters files are now Fortran name lists.
- Molecule attribute "t_exp" renamed "rotational_partition_exp".
- Molecule attribute "M" renamed "molar_mass".
- Atmosphere attribute "PLANET" renamed "OBJECT".
- Maps plotting spectral cube functions are no longer static.
- Auto-named figure directory spectral cube functions are no longer static.
- "Inversion" renamed "retrieving".
- Numerous F90 function rename and rework.

### Removed
- Molecule parameter "vmr".

### Fixed
- New abundance profile parameters erased if its file already exists.
- Instrument not saved in retrieved spectral cubes.
- Spectral cubes latitude in radian instead of degree.
- H2-He continuum file not saving number of level and H2 and He VMR.

## [3.16.0] - 2017-07-04
### Added
- Spectral cube loader from pkl.
- Auto-named spectral cube figure directory.
- Figure directory can be specified in spectral cube maps plotting function. 

### Fixed
- Crash in run_nrt if the observed spectra files does not exists.
- Crash when splitting a file name with no extension.
- Crash due to spectrum doppler shift set to None by default.
- Sky not written during spectral cube extraction.

## [3.15.0] - 2017-06-28
### Added
- Possibility to select which molecules to retrieve in spectral cube retrieval.
- Possibility to select viewing angle max in cloud only spec. cube retrieval.
- Auto-named directories for figures.

### Changed
- Auto-named spectral cubes now integrate hour time in their name.
- Auto-named maps in runs now integrate spectral cube metadata.
- More default arguments in plot maps functions.

## [3.14.2] - 2017-06-26
### Changed
- Spectral cube extracting progression bar no longer in ascii.

### Fixed
- Doppler shift not taken into account in skies.
- Skies cancelling convolution padding.
- Difference plotted in run_nrt with only one spectrum passed in the plot func.
- Atmosphere parameter in run_nrt being ignored.
- Conversion from location to string not behaving properly.
- Crash when trying to find molecule data.

## [3.14.1] - 2017-06-22
### Fixed
- Sky being shifted in wavenumber when extracting spec. cube with doppler cor.
- Crash when plotting all maps.
- Retrieval run not extracting molecule abundance maps.
- Extra '_' in map names.


## [3.14.0] - 2017-06-20
### Added
- Function for spectral cube to quickly display some basic stats.

### Fixed
- Crash when auto initialising spectrum label

## [3.13.1] - 2017-06-19
### Fixed
- Atmosphere in some runs not initialising properly.
- Crash when plotting all maps if no molecule in atmosphere.
- Crash when plotting all maps with the default scale boundaries.
- Typos in auto-initialised spectrum labels.
- Extra '_' at the end of some names.

## [3.13.0] - 2017-06-16
### Added
- Lots of metadata attributes for spectral cubes.
- Auto-rename function for spectral cubes.
- Run to retrieve only the cloud trans. 1st, then the cloud and the molecules.
- Option to plot sky in mol-by-mol graphs.

### Changed
- Auto names are a bit standardised.
- .sav spectral cubes are now loaded with a separate loader.
- "iD" parameter of spectrum is now called "reference".
- Plotting all maps is now more flexible.

### Removed
- Plot rad map option when extracting spectral cubes.
- Various debug or useless attributes of spectral cubes.

### Fixed
- Observed spectrum not taken into account in run_nrt.
- Convoluted parameter for skies not initialising properly.
- Filter for spectra with viewing angle < 80 not working properly.
- Auto named spectra instrument not initialising properly.
- Figure name of derivative files not initialising properly.
- Missing "." in derivative file extension.

## [3.12.3] - 2017-06-14
### Changed
- Figures could probably get more pretty, but it would be hard...

### Fixed
- Crash of runs without observed spectrum.
- Crash if wrong syntax in header of data files.
- Molecules reset to None when loading an Atmosphere.
- Function air2H2He launching even when H2 and He are not present in atm.
- Wrong temperature profile interpolated when launching the function to do so.
- Molecule parameters files not writing properly.
- Function check_dirs_from_dict not behaving properly with too many dirs levels.
- Plotting differences even if only one file is to be plotted.
- Nicknames still having an extra '_'.

## [3.12.2] - 2017-06-09
### Fixed
- Imports failure from distribution.

## [3.12.1] - 2017-06-09
### Fixed
- Continuum file checking is now also based on wavenumber range.
- Sky and error files not extracting properly from .sav spectral cubes.
- Auto-named spectra not having their instrument.

## [3.12.0] - 2017-06-08
### Added
- Comments here and there.

### Changed
- Using run_nrt is more simple.
- Renamed some files.
- Emplacement of some functions.

### Removed
- Multiple deprecated functions and code lines.

### Fixed
- Various minor bugs.

## [3.11.0] - 2017-06-02
### Added
- Spectral cube retrieval function for cloud only.

### Changed
- Extracting .sav spectral cubes now make spectral maps automatically.

### Fixed
- Imports for distributed module.
- Bug in relation with the size of the instrument function.
- NoneType object error when trying to calculate a spectra without retrieving.
- Sky wavenumber writing precision too high causing failure during interp.
- Fortran code will no longer be installed in the python libraries directory.

## [3.10.0] - 2017-05-31
### Added
- Software installer.

## [3.9.0] - 2017-05-29
### Added
- Spectrum chunk comparison maps.
- Numerous comments.

### Changed
- Writing format of abundance and temp. profiles have now a precision of 2.

### Removed
- Multiple deprecated functions, classes and lines.

### Fixed
- Writing format of abundance profile could not handle values >= 1E+100.
- Chi2 map not saving properly.
- File _find method not behaving properly.
- Extra "_" in maps nicknames.

## [3.8.0] - 2017-05-15
### Added
- Function to plot all maps easily.
- Comments here and there.

### Changed
- Figures are (even more) prettier.
- Profiles are now evenly log spaced.

### Removed
- Some unused function parameters.

### Fixed
- Interpolation (fortran) and extrapolation (python) functions.
- Abundance profile figures legend and title.

## [3.7.3] - 2017-05-10
### Changed
- Figures are prettier.

### Fixed
- Added forgotten factor 1/mu in derivatives over abundance calculations.
- Cloud reflection derivative, which were applied on levels below the cloud.
- Minor errors in derivatives over cloud transmittance.
- Various minor bugs.

## [3.7.2] - 2017-04-07
### Changed
- Figures are prettier.

### Fixed
- Crashes due to some objects set to None.

## [3.7.1] - 2017-04-03
### Changed
- Instrument resolution power replaced by fwhm.

### Removed
- Function res2fwhm.

### Fixed
- Sky transmittance during execution of fts98 data reduction was not reset.
- Some radiances could be negative, fixed but source not identified.

## [3.7.0] - 2017-03-31
### Added
- Chi2 functions.
- Signal to noise ratio of spec cubes.

### Changed
- Updated some old functions.
- Renamed fortran function "interp1d" to "interp".
- Temperature references in module physical parameters are now double precision.

### Fixed
- Spectrum atmospheres were shared instead of copied.
- Pressure on tops of cloud could be higher than pressure at the bases.
- Tqdm messages not showing up properly.
- Crash and NaN when there were no H2-He continuum in atmosphere.
- Bugs when inverse is set to false or when no observed spectrum are specified.
- Various warnings.

## [3.6.0] - 2017-03-24
### Added
- Spectral cubes output for function nrt_map.

### Fixed
- Retrieved abundance profiles were not in figures.
- Lines were stacked at each read.

### Removed
- Useless prints.

## [3.5.1] - 2017-03-24
### Fixed
- Abundance profiles could not be retrieved from the Python code.
- Some console outputs in the Fortran code.

## [3.5.0] - 2017-03-23
### Changed
- Moved LineFile object into Atmosphere.Molecule and renamed it Lines.
- Reworked texes_extract.py.
- Minloc functions in code now return a scalar instead of a rank-one array.
- H2 and He are now considered as normal molecules.
- Raised number of molecule limit to 16 and length of molecule name to 16.

## [3.4.1] - 2017-03-17
### Changed
- Python source code moved from Python/nrt to Python/
- Figures directory moved into parent directory.

## [3.4.0] - 2017-03-17
First git release.
### Added
- Error file for the observed spectrum.
- Error bars in figures.
- Forward model error.
- Solar incident light and viewing angle.
- Atmosphere and Instrument objects.
- Cloud transmittance map output.

### Changed
- Inputs/Outputs uniformisation.

## [3.3.0] - 2017-02-03
### Added
- Reflected and thermal cloud contribution.
- Various adds and tweaks in the Python code.
- Abundance and chi2 map output.
- Windows compatibility.

### Changed
- Various restructurations and cleaning.
- Clarifying of some functions.

### Fixed
- Freezing bug which could occurs during the calculation of the continuum file.

## [3.2.0] - 2016-11-14
### Added
- Various adds and tweaks in the Python code.

### Changed
- H2-He continuum calculation subroutine updated from Fortran77 to Fortran90.

### Fixed
- Removed array temporary creation in function voigt induced by a reshape.

## [3.1.0] - 2016-10-13
### Added
- Parallel matrix inversion.
- Sky correction.
- Cloud inversion.
- Various adds and tweaks in the Python code.

### Fixed
- Retrieved profile bug: weight of a priori was set to 3 instead of 0.1.

## [3.0.0] - 2016-09-09
### Added
- Parallelisation using [OpenMPI](https://www.open-mpi.org).
- Python "shell" to manage the code runs and to exploit the outputs.
- Inversion method and linked derivatives calculations.
- Cloud contribution.
- Sequential LU matrix inversion.
- Spectra convolution by FFT.
- Interpolation subroutine.
- Random gaussian noise generator.
- Basic "hot spot" auto-detection and extraction from spectral cubes.
- Output of retrieved abundance profiles.
- Output of Averaging kernels.
- Automatic naming of output files.
- [WIP] Graphical User Interface.

### Changed
- It is now possible to specify directly which molecule to inverse.

### Removed
- IDL "shell"
- Unused function EINT() for limb observations.

### Fixed
- "Stair effect" bug in the interpolation of the H2-He continuum.

## [2.0.0] - 2016-01-28
### Added
- IDL "shell" to manage the code runs and to exploit the outputs.
- Spectral cubes management.
- Voigt function.

### Changed
- Converted from Fortran77 to Fortran90 (basic conversion made with [f2f.pl](https://bitbucket.org/lemonlab/f2f/src)).
- Units in profiles changed form erg to Joules and mbar to bar.
- Input file is now a namelist.
- Absorption coefficient calculation subroutine updated.
- Various optimisations.

## [1.0.0] - 1997-02-14
Legacy program by Pierre Drossart and Emmanuel Lellouch.
