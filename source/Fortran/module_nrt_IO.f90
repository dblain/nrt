module nrt_IO
    ! """
    ! Contains all the parameters, functions and subroutines to manage the inputs and outputs of program NRT.
    ! """
    use mpi_utils

    implicit none

    save

    contains
        subroutine copy_file(file, output_file)
            ! """
            ! Copy the content of a file into another file.
            !
            ! inputs:
            !   file: file to copy
            !   output_file: copy of the file
            ! """
            use nrt_parameters, only: file_name_size

            implicit none

            character(len=file_name_size), intent(in):: file
            character(len=file_name_size), intent(in):: output_file

            character(len=120) :: &
                line  ! to read lines in files

            integer ::&
                io  ! input/output status


            open(999, file=output_file)
            open(1,file=file)

            do
                read(1,'(A)',iostat=io) line
                if (io < 0) exit
                write (999,*) line
            end do

            close(1)
            close(999)
        end subroutine copy_file


        subroutine get_molecular_cross_sections()
            ! """
            ! Get the molecular absorption cross sections; they are needed to calculate the optical depths.
            ! Try to read the corresponding files. If they do not exsists or do not have the right set of parameters,
            ! the absorption cross sections are calculated using molecular parameters and line parameters and saved
            ! in new files.
            ! absorption cross sections are used to calculate the molecular contribution to opacity at each wavelength
            ! and each level for all of the molecules.
            ! """
            use atmosphere, only: n_level, pressures, temperatures, absorption_cross_sections
            use molecules, only: molecules_name, n_mol, vmr_profiles
            use nrt_parameters, only: calculate_cross_sections, write_cross_sections, absorption_cross_sections_files
            use nrt_physics, only: calculate_absorption_cross_sections
            use spectrum, only: n_wvn__, wavenumber_max, wavenumber_min, wavenumber_step

            implicit none

            logical :: &
                present,                &  ! test for presence of a file in the indicated directory
                wrong_file = .False.,   &  ! test if the readed file is conform
                call_calculate_cross_sections = .False.  ! if True, call the absorption cross sections routine

            character(len=3) :: &
                broadener

            character(len=5) :: &
                generation_method

            character(len=20) :: &
                molecule

            integer :: &
                j,                      &  ! index (wavenumber)
                k,                      &  ! index (atmospheric level)
                l,                      &  ! index (molecule)
                ll,                     &  ! index (molecule)
                nwvn_file,              &  ! number of wavenumbers__ for the current process given by the file
                ref

            double precision :: &
                temperature_file,       &
                pressure_file,          &
                vmr_file,               &
                vmr_broadener_file,     &
                wvn_max_file,           &  ! last wavenumber of the whole spectra
                wvn_min_file,           &  ! first wavenumber of the whole spectra
                wvn_step_file              ! wavenumber step given by the file

            double precision, dimension(n_level) :: &
                vmr_broadener              !

            ! Check if all files are presents
            do l = 1,n_mol
                inquire(file=absorption_cross_sections_files(l),exist=present)
                if (.not. present) then
                    write_cross_sections = .True.
                    exit
                end if
            end do

            call MPI_ALLREDUCE(write_cross_sections, call_calculate_cross_sections, 1, MPI_LOGICAL, MPI_LOR, &
                               MPI_COMM_WORLD, mpi_ierr)

            ! Caclulate new cross sections if any file is missing or if requested by the user; else, just read the file
            if (calculate_cross_sections .or. call_calculate_cross_sections) then
                call calculate_absorption_cross_sections()
            else
                do l = 1, n_mol
                    ! Read the files
                    write(mpi_msg, '("Reading ",A, " absorption cross sections in file ''", A, "''")') &
                        trim(molecules_name(l)), trim(absorption_cross_sections_files(l))
                    call mpi_print(mpi_msg, 0)

                    open(20 + l, file=absorption_cross_sections_files(l), status='old', form='formatted')

                    do k = 1, n_level
                        read(20 + l, '(A20, F10.4, F10.4, I7, F7.2, ES16.9, ES10.3, A5, F5.3, F4.2, A3, I3)') &
                            molecule, wvn_min_file, wvn_max_file, nwvn_file, temperature_file, pressure_file, &
                            wvn_step_file, generation_method, vmr_broadener_file, vmr_file, broadener, ref

                        call check_cross_section_file()

                        if (wrong_file) then
                            exit
                        end if

                        read(20 + l, '((10ES10.3))') (absorption_cross_sections(j, k, l), j=1, n_wvn__)
                    end do

                    close(20 + l)
                end do
            end if

            ! Update call_calculate_cross_sections status to handle the case where not all the ACS files are wrong
            call MPI_ALLREDUCE(wrong_file, call_calculate_cross_sections, 1, MPI_LOGICAL, MPI_LOR, &
                               MPI_COMM_WORLD, mpi_ierr)

            if (call_calculate_cross_sections) then
                call calculate_absorption_cross_sections
                write_cross_sections = .True.
            end if

            ! Output
            if (write_cross_sections) then
                ! TODO make a more generic method to identify broadener
                do l = 1, n_mol
                    if (trim(molecules_name(l)) == 'H2') then
                        do ll = 1, n_mol
                            if (trim(molecules_name(ll)) == 'N2' .and. vmr_profiles(1, ll) > vmr_profiles(1, l)) then
                                broadener = 'Air'

                                do k = 1, n_level
                                    vmr_broadener(k) = vmr_profiles(k, ll)
                                end do

                                exit
                            end if

                            broadener = 'H2'

                            do k = 1, n_level
                                vmr_broadener(k) = vmr_profiles(k, l)
                            end do
                        end do

                        exit
                    end if
                end do

                do l = 1, n_mol
                    write(*, '(1X,"Writing absorption cross sections in file ''", A, "''")') &
                        trim(absorption_cross_sections_files(l))

                    if (molecules_name(l) == 'H2' .or. molecules_name(l) == 'He') then
                        generation_method = 'ainit'  ! ab-initio
                    else
                        generation_method = 'lines'  ! from lines
                    end if

                    open(20 + l, file=absorption_cross_sections_files(l),status='unknown',form='formatted')

                    do k = 1, n_level
                        write(20 + l, '(A20, F10.4, F10.4, I7, F7.2, ES16.9, ES10.3, A5, F5.3, F4.2, A3, I3)') &
                            trim(molecules_name(l)), wavenumber_min, wavenumber_max, n_wvn__, &
                            temperatures(k), pressures(k), wavenumber_step, generation_method, &
                            vmr_broadener(k), vmr_profiles(k, l), broadener, -99

                        write(20 + l, '((10ES10.3))') (absorption_cross_sections(j, k, l), j=1, n_wvn__)
                    end do

                    close(20 + l)
                end do
            end if

            ! Clean terminal output
            call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)

            contains
                subroutine check_cross_section_file()
                    ! """
                    !
                    ! """
                    implicit none

                    if (wvn_min_file < wavenumber_min * (1D0 - 1D-4) .or. &
                        wvn_min_file > wavenumber_min * (1D0 + 1D-4) .or. &
                        wvn_max_file < wavenumber_max * (1D0 - 1D-4) .or. &
                        wvn_max_file > wavenumber_max * (1D0 + 1D-4)) then

                        write(mpi_msg, '("in file ''", A, "'', wavenumber range (", F10.4,"-", F10.4," cm-1) &
                                         &does not corresponds to current wavenumber range &
                                         &(", F10.4,"-", F10.4," cm-1)")') &
                            trim(absorption_cross_sections_files(l)), &
                            wvn_min_file, wvn_max_file, wavenumber_min, wavenumber_max
                        call mpi_print_warning(mpi_msg, mpi_rank)

                        wrong_file = .True.
                    elseif (nwvn_file /= n_wvn__) then
                        write(mpi_msg, '("in file ''", A, "'', number of wavenumbers (", I7,") &
                                         &does not corresponds to current number of wavenumbers (", I7, ")")') &
                            trim(absorption_cross_sections_files(l)), wvn_step_file, wavenumber_step
                        call mpi_print_warning(mpi_msg, mpi_rank)

                        wrong_file = .True.
                    elseif (temperature_file < temperatures(k) * (1D0 - 1D-2) .or. &
                            temperature_file > temperatures(k) * (1D0 + 1D-2)) then
                        write(mpi_msg, '("in file ''", A, "'', temperature (", F7.2," K) &
                                         &does not corresponds to current temperature at level ", I7, &
                                         &" (", F7.2, " K)")') &
                            trim(absorption_cross_sections_files(l)), wvn_step_file, wavenumber_step
                        call mpi_print_warning(mpi_msg, mpi_rank)

                        wrong_file = .True.
                    elseif (pressure_file < pressures(k) * (1D0 - 1D-9) .or. &
                            pressure_file > pressures(k) * (1D0 + 1D-9)) then
                        write(mpi_msg, '("in file ''", A, "'', pressure (", ES16.9," bar) &
                                         &does not corresponds to current pressure at level ", I7, &
                                         &" (", ES16.9, " bar)")') &
                            trim(absorption_cross_sections_files(l)), wvn_step_file, wavenumber_step
                        call mpi_print_warning(mpi_msg, mpi_rank)

                        wrong_file = .True.
                    elseif (wvn_step_file < wavenumber_step * (1D0 - 1D-3) .or. &
                            wvn_step_file > wavenumber_step * (1D0 + 1D-3)) then
                        write(mpi_msg, '("in file ''", A, "'', wavenumber step (", ES10.3," cm-1) &
                                         &does not corresponds to current wavenumber step (", ES10.3, " cm-1)")') &
                            trim(absorption_cross_sections_files(l)), wvn_step_file, wavenumber_step
                        call mpi_print_warning(mpi_msg, mpi_rank)

                        wrong_file = .True.
                    elseif (generation_method == 'ainit') then
                        do ll = 1, n_mol
                            if (trim(molecules_name(ll)) == trim(broadener)) then
                                if (vmr_profiles(k, ll) < vmr_broadener_file * (1D0 - 1D-3) .or. &
                                    vmr_profiles(k, ll) > vmr_broadener_file * (1D0 + 1D-3)) then
                                    write(mpi_msg, '("in file ''", A, "'' (generated ab-initio) broadener ", A, &
                                                     &" VMR (", F5.3, ") &
                                                     &is not the same as current VMR at level ", I7, &
                                                     &" (", F5.3, ")")') &
                                        trim(absorption_cross_sections_files(l)), trim(broadener), &
                                        vmr_broadener_file, k, vmr_profiles(k, ll)
                                    call mpi_print_warning(mpi_msg, mpi_rank)

                                    wrong_file = .True.

                                    exit
                                end if

                                exit
                            elseif (ll == n_mol) then
                                write(mpi_msg, '("in file ''", A, "'' (generated ab-initio) , &
                                                 &broadener ''", A, "'' is used &
                                                 &but is not in current atmosphere")') &
                                    trim(absorption_cross_sections_files(l)), broadener
                                call mpi_print_warning(mpi_msg, mpi_rank)

                                wrong_file = .True.

                                exit
                            end if
                        end do
                    end if
                end subroutine check_cross_section_file
        end subroutine get_molecular_cross_sections


        subroutine get_retrieved_spectrum()
            ! """
            ! Load the retrieved spectrum and its noise.
            ! If no noise is given a SNR of 10 is assumed
            ! """
            use nrt_parameters, only:&
                noise_file, &
                retrieved_spectrum
            use retrieval, only:&
                noise_radiances, &
                n_wvn_ret, &
                radiances_ret, &
                wavenumbers_ret

            implicit none

            character(len=256) :: &
                data_params        ! dump for data parameters when reading data files

            double precision, dimension(:), allocatable :: &
                tmp_array1, &
                tmp_array2, &
                tmp_array3

            ! Load the retrieved spectrum
            write(mpi_msg, '("Reading retrieved spectrum in file ''", A ,"''")') trim(retrieved_spectrum)
            call mpi_print(mpi_msg, 0)

            call read_data_file(retrieved_spectrum, wavenumbers_ret, radiances_ret, data_params)

            ! Get the number of samples in the retrieved spectrum
            n_wvn_ret = size(wavenumbers_ret)

            call check_retrieved_spectrum()

            ! Load the retrieved spectrum noise; if there is no noise file, a SNR of 10 is assumed
            if (trim(noise_file) /= 'None') then
                write(mpi_msg, '("Reading noise of retrieved spectrum in file ''",A, "''")') trim(noise_file)
                call mpi_print(mpi_msg, 0)

                call read_data_file(noise_file, tmp_array1, noise_radiances, data_params)

                call check_noise()
            else
                call mpi_print_info('no observed noise file provided, assuming SNR of 10', 0)

                allocate(noise_radiances(n_wvn_ret))
                noise_radiances(:) = 0.1 * maxval(radiances_ret)
            end if

            ! Remove NaN in observed radiance and noise
            tmp_array1 = wavenumbers_ret(:)  ! doesn't need to be reallocated since they are of the right size
            tmp_array2 = radiances_ret(:)
            tmp_array3 = noise_radiances(:)

            call remove_invalid_in_all(tmp_array1(:), tmp_array2(:), tmp_array3(:), &
                                       wavenumbers_ret, radiances_ret, noise_radiances)

            if (size(wavenumbers_ret) /= n_wvn_ret) then
                call mpi_print_info('NaN removed from retrieved radiances and/or noise', 0)
                n_wvn_ret = size(wavenumbers_ret)
            end if

            contains
                subroutine check_retrieved_spectrum()
                    ! """
                    ! Check the retrieved spectrum parameters.
                    ! """
                    implicit none

                    integer :: &
                        j  ! index

                    ! Check number of retrieved wavenumbers
                    if (n_wvn_ret < 1) then
                        write(mpi_msg, '("number of retrieved wavenumbers (", I10, ") must be strictly positive")') &
                            n_wvn_ret
                        call mpi_print_error(mpi_msg, 0)
                    end if

                    ! Check wavenumbers
                    do j = 2, n_wvn_ret
                        if(wavenumbers_ret(j) < wavenumbers_ret(j - 1)) then
                            call mpi_print_error('retrieved wavenumbers must be in strict increasing order', 0)
                        end if
                    end do
                end subroutine check_retrieved_spectrum

                subroutine check_noise()
                    ! """
                    ! Check the noise.
                    ! """

                    ! Check noise size
                    if (size(tmp_array1) /= n_wvn_ret) then
                        write(mpi_msg, '("noise and retrieved radiance must have the same length, &
                                         &but have lenghts (", I10, ") and (", I10, ")")') &
                            size(tmp_array1), n_wvn_ret
                        call mpi_print_error(mpi_msg, 0)
                    end if

                    ! Check noise wavenumbers
                    if (tmp_array1(1) < wavenumbers_ret(1) - tiny(0.) .or. &
                        tmp_array1(1) > wavenumbers_ret(1) + tiny(0.) .or. &
                        tmp_array1(n_wvn_ret) < wavenumbers_ret(n_wvn_ret) - tiny(0.) .or. &
                        tmp_array1(n_wvn_ret) > wavenumbers_ret(n_wvn_ret) + tiny(0.)) then
                        call mpi_print_error('noise and retrieved radiance must have the same wavenumbers', 0)
                    end if
                end subroutine check_noise
        end subroutine get_retrieved_spectrum


        subroutine make_instrument_lsf()
            ! """
            ! Make the instrument Line Shape Function.
            ! """
            use math, only:&
                gaussian, &
                interp, &
                sinc_fwhm
            use spectrum, only:&
                convolution_step
            use instrument, only:&
                instrument_lsf, &
                lsf_fwhm, &
                lsf_fwhm_ref, &
                lsf_shape, &
                lsf_wvn, &
                n_lsf, &
                n_wvn_lsf
            use retrieval, only: &
                n_wvn_conv, &
                wavenumbers_conv

            implicit none

            integer :: &
                i, &  ! index
                i_max, &
                i_min, &
                j     ! index

            double precision :: &
                x_lsf  ! wavenumber at which to calculate the LSF

            double precision, dimension(:), allocatable :: &
                lsf_fwhm_interp  ! interpolated Line Spread Function Full Width Half Maximum

            if(n_lsf > 1) then
                allocate(lsf_fwhm_interp(n_wvn_conv))

                ! Find where wavenumbers overlap LSF wavenumbers
                i = 1

                do while (wavenumbers_conv(i) < lsf_wvn(1))
                    i = i + 1
                end do

                i_min = i

                i = n_wvn_conv

                do while (wavenumbers_conv(i) > lsf_wvn(n_lsf))
                    i = i - 1
                end do

                i_max = i

                lsf_fwhm_interp(i_min:i_max) = interp(wavenumbers_conv(i_min:i_max), lsf_wvn(:), lsf_fwhm(:))
                lsf_fwhm_interp(1:i_min) = lsf_fwhm_interp(i_min)
                lsf_fwhm_interp(i_max:n_wvn_conv) = lsf_fwhm_interp(i_max)

                allocate(instrument_lsf(n_wvn_lsf, n_wvn_conv))

                ! Fill instrument LSF
                do i = 1, n_wvn_lsf
                    x_lsf = (i - 1 - floor(n_wvn_lsf / 2D0)) * convolution_step

                    do j = 1, n_wvn_conv
                        if (lsf_shape == 'gaussian') then
                            instrument_lsf(i, j) = gaussian(x_lsf, lsf_fwhm_interp(j))
                        else if (lsf_shape == 'sinc') then
                            instrument_lsf(i, j) = sinc_fwhm(x_lsf, lsf_fwhm_interp(j))
                        else
                            write(mpi_msg, '("instrument function shape expected: ''gaussian''|''sinc'', &
                                             &got ",A, " instead")') lsf_shape
                            call mpi_print_error(mpi_msg, 0)
                        end if
                    end do
                end do

                write(mpi_msg, '("Convolution with sliding insrument function ''",A, "'' &
                                 &of FWHM = ",ES8.2," - ", ES8.2, " cm-1")') &
                      trim(lsf_shape), minval(lsf_fwhm), maxval(lsf_fwhm)
                call mpi_print(mpi_msg, 0)
            else
                allocate(instrument_lsf(n_wvn_lsf, 1))

                ! Fill instrument LSF
                do i = 1, n_wvn_lsf
                    x_lsf = (i - 1 - floor(n_wvn_lsf / 2D0)) * convolution_step

                    if (lsf_shape == 'gaussian') then
                        instrument_lsf(i, 1) = gaussian(x_lsf, lsf_fwhm_ref)
                    else if (lsf_shape == 'sinc') then
                        instrument_lsf(i, 1) = sinc_fwhm(x_lsf, lsf_fwhm_ref)
                    else
                        write(mpi_msg, '("instrument function shape expected: ''gaussian''|''sinc'', &
                                         &got ",A, " instead")') lsf_shape
                        call mpi_print_error(mpi_msg, 0)
                    end if
                end do

                write(mpi_msg, '("Convolution with insrument function ''",A, "'' of FWHM = ",ES8.2," cm-1")') &
                      trim(lsf_shape), lsf_fwhm_ref
                call mpi_print(mpi_msg, 0)
            end if

            ! Set first and last values of filter to 0
            instrument_lsf(1, :) = 0D0
            instrument_lsf(n_wvn_lsf, :) = 0D0

            ! Normalisation
            do j = 1, size(instrument_lsf, 2)
                instrument_lsf(:, j) = instrument_lsf(:, j) / sum(instrument_lsf(:, j))
            end do
        end subroutine make_instrument_lsf


        subroutine make_synthetic_wavenumbers()
            ! """
            ! Make the synthetic wavenumber array and share it among the processes.
            ! """
            use nrt_parameters, only:&
                convolve, &
                retrieve
            use instrument, only:&
                lsf_fwhm_ref, &
                n_wvn_lsf
            use math, only:&
                arange_include
            use spectrum, only:&
                convolution_step, &
                displs_j, &
                n_wvn, &
                n_wvn__, &
                n_wvn_x, &
                wavenumbers, &
                wavenumbers__, &
                wavenumber_max, &
                wavenumber_min, &
                wavenumber_step
            use retrieval, only:&
                wavenumbers_ret

            implicit none

            integer :: &
                j0  ! displacement index

            if (retrieve) then
                ! Change the wavenumber interval of calculation based on that of the retrieved spectra
                call mpi_print_info('Overriding set wavenumber boundaries to match those of observations...', 0)
                wavenumber_min = wavenumbers_ret(1)
                wavenumber_max = wavenumbers_ret(size(wavenumbers_ret))
            end if

            if (convolve) then
                if (retrieve) then
                    ! Convolution can be performed only close to the observed wavenumbers without losing much precision
                    ! TODO make this as a parameter
                    convolution_step = floor(lsf_fwhm_ref / 4D0 / wavenumber_step) * wavenumber_step
                else
                    convolution_step = wavenumber_step
                end if

                ! Initialise instrument LSF
                n_wvn_lsf = ceiling(10D0 * lsf_fwhm_ref / convolution_step) + 2 ! add 2 for first and last 0

                ! To get a symetric LSF, n_wvn_lsf must be an odd number
                if (mod(n_wvn_lsf, 2) == 0) then
                    n_wvn_lsf = n_wvn_lsf + 1
                end if

                ! Add padding to wavenumber array for convolution
                wavenumber_min = wavenumber_min - n_wvn_lsf * convolution_step
                wavenumber_max = wavenumber_min + &
                    (ceiling((wavenumber_max - wavenumber_min) / convolution_step) + n_wvn_lsf) * convolution_step
            end if

            wavenumbers = arange_include(wavenumber_min, wavenumber_max, wavenumber_step)

            n_wvn = size(wavenumbers)

            if (n_wvn < mpi_nproc) then
                call mpi_print_warning('wavenumber step may be too high or the wavenumber interval may be too small', 0)
            end if

            ! Share wavenumbers equitably among all processes
            call mpi_fair_sharev(n_wvn, n_wvn__, n_wvn_x, j0, displs_j)

            wavenumber_min = wavenumber_min + j0 * wavenumber_step
            wavenumber_max = wavenumber_min + (n_wvn__ - 1) * wavenumber_step

            allocate(wavenumbers__(n_wvn__))

            call MPI_SCATTERV(wavenumbers, n_wvn_x, displs_j, MPI_DOUBLE_PRECISION, &
                              wavenumbers__, n_wvn__, MPI_DOUBLE_PRECISION, &
                              0, MPI_COMM_WORLD, mpi_ierr)
        end subroutine make_synthetic_wavenumbers


        subroutine make_convolution_wavenumbers()
            ! """
            ! Make the convolved wavenumbers array and share it among the processes.
            ! Convolution is an heavy operation. It is possible to gain significant computation time by convolving
            ! values on smaller arrays, at the expense of a bit of precision.
            ! """
            use math, only:&
                arange, &
                arange_include
            use spectrum, only:&
                convolution_step, &
                n_wvn, &
                wavenumbers, &
                wavenumber_max, &
                wavenumber_min, &
                wavenumber_step
            use retrieval, only:&
                displs_j_low_res, &
                n_wvn_conv, &
                n_wvn_conv__, &
                n_wvn_conv_x, &
                wavenumbers_conv, &
                wavenumbers_conv__

            implicit none

            integer :: &
                j, &  ! index
                j0    ! index displacement

            double precision :: &
                wvn_low_res_max, &
                wvn_low_res_min

            double precision, dimension(:), allocatable :: &
                wvn_low_res_tmp

!            wavenumbers_conv = arange_include(wavenumbers(1), wavenumbers(n_wvn), convolution_step)
            if (convolution_step > wavenumber_step * (1D0 - 1D-9) .and. &
                convolution_step < wavenumber_step * (1D0 + 1D-9)) then
                allocate(wavenumbers_conv(n_wvn))
                wavenumbers_conv(:) = wavenumbers(:)
            else
                wavenumbers_conv = arange(wavenumbers(1), wavenumbers(n_wvn), convolution_step)
            end if

            n_wvn_conv = size(wavenumbers_conv)

            if (n_wvn < n_wvn_conv) then
                call mpi_print_warning('wavenumber step is greater than instrument LSF HWHM', 0)
            end if

            call mpi_fair_sharev(n_wvn_conv, n_wvn_conv__, n_wvn_conv_x, j0, displs_j_low_res)

            allocate(wvn_low_res_tmp(n_wvn_conv__))

            call MPI_SCATTERV(wavenumbers_conv, n_wvn_conv_x, displs_j_low_res, MPI_DOUBLE_PRECISION, &
                              wvn_low_res_tmp, n_wvn_conv__, MPI_DOUBLE_PRECISION, &
                              0, MPI_COMM_WORLD, mpi_ierr)

            wvn_low_res_min = wvn_low_res_tmp(1)
            wvn_low_res_max = wvn_low_res_tmp(n_wvn_conv__)

            if (mpi_rank < mpi_nproc - 1) then
                do while (wvn_low_res_max > wavenumber_max)
                    wvn_low_res_min = wvn_low_res_min - convolution_step
                    wvn_low_res_max = wvn_low_res_max - convolution_step
                    j0 = j0 - 1
                end do
            end if

            if (mpi_rank > 0) then
                do while (wvn_low_res_min > wavenumber_min + convolution_step)
                    wvn_low_res_min = wvn_low_res_min - convolution_step
                    n_wvn_conv__ = n_wvn_conv__ + 1
                    j0 = j0 - 1
                end do

                do while (wvn_low_res_min < wavenumber_min)
                    wvn_low_res_min = wvn_low_res_min + convolution_step
                    n_wvn_conv__ = n_wvn_conv__ - 1
                    j0 = j0 + 1
                end do
            end if

            call MPI_ALLGATHER(j0, 1, MPI_INTEGER, displs_j_low_res, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)
            call MPI_ALLGATHER(n_wvn_conv__, 1, MPI_INTEGER, n_wvn_conv_x, 1, MPI_INTEGER, &
                               MPI_COMM_WORLD, mpi_ierr)

            allocate(wavenumbers_conv__(n_wvn_conv__))

            do j = 1, n_wvn_conv__
                wavenumbers_conv__(j) = wvn_low_res_min + (j - 1) * convolution_step
            end do
        end subroutine make_convolution_wavenumbers


        subroutine read_collision_induced_cross_sections(file)
            ! """
            !
            ! """
            use atmosphere, only: temperatures
            use nrt_parameters, only: file_name_size
            use molecules, only: cutoffs
            use spectrum, only: wavenumber_min

            implicit none

            character(len=file_name_size), intent(in) :: file

            character(len=20) :: &
                chemical_symbol

            character(len=27) :: &
                cia_comments

            integer :: &
                cia_ref, &
                i, &
                io, &  ! iostat storage
                number_points, &
                number_sets

            double precision :: &
                cia_res, &
                cia_sigma_max, &
                cia_temperature, &
                cia_wavenumber_max, &
                cia_wavenumber_min, &
                cutoff_max, &
                temperature_max, &
                temperature_min

            double precision, dimension(:, :), allocatable :: &
                collision_induced_cross_sections

            cutoff_max = maxval(cutoffs(:))
            temperature_max = maxval(temperatures(:))
            temperature_min = minval(temperatures(:))

            open(10, file=file, status='old')

            read(10, '(I7, I3)') number_points, number_sets

            allocate(collision_induced_cross_sections(number_points, number_sets))

            do
                read(10, '(A20, F10.3, F10.3, I7, F7.1, ES10.3, F6.3, I3)', iostat=io) &
                    chemical_symbol, cia_wavenumber_min, cia_wavenumber_max, number_points, cia_temperature, &
                    cia_sigma_max, cia_res, cia_comments, cia_ref

                if (cia_temperature > temperature_max .or. io < 0) then
                    exit
                else if(cia_wavenumber_min < wavenumber_min - cutoff_max) then  ! TODO finish this (replace cia_wvn_min)
                    do i = 1, number_points
                        read(10, *, iostat=io)
                    end do
                else
                    do i = 1, number_points
                        read(10, *, iostat=io)
                    end do
                end if
            end do


        end subroutine read_collision_induced_cross_sections


        subroutine read_data_file(file, col1, col2, parameters)
            ! """
            ! Read the data files used in the program.
            ! Data files can be any file with two columns -representing a function f(x)- and a header.
            !
            ! input:
            !   file: the file to read
            !
            ! outputs:
            !   col1: the first column of the file
            !   col2: the second column of the file
            !   parameters: the parameters in the header of the file
            ! """
            use nrt_parameters, only: file_name_size

            implicit none

            character(len=file_name_size), intent(in) :: file
            character(len=file_name_size), intent(out) :: parameters
            double precision, dimension(:), allocatable, intent(out) :: col1, col2

            character(len=25) :: &
                title           ! the first 25 character of the fist line of the file

            integer :: &
                i,           &  ! index
                nb_elements     ! number of elements in the columns

            open(10, file=file, status='old')

            read(10, '(A25, I10, A)') title, nb_elements, parameters
            allocate(col1(nb_elements), col2(nb_elements))

            do i = 1, nb_elements
                read(10, *) col1(i), col2(i)
            end do

            close(10)

        end subroutine read_data_file


        subroutine read_instrument_file()
            ! """
            ! Load the the instrument data file, which contains the instrument LSF.
            ! """
            use instrument, only: lsf_fwhm, lsf_fwhm_ref, lsf_shape, lsf_wvn, n_lsf
            use nrt_parameters, only: instrument_file

            implicit none

            character(len=256) :: &
                data_params        ! dump for data parameters when reading data files

            integer :: &
                i, &  ! index
                j     ! index

            write(mpi_msg, '("Reading instrument LSF in file ''", A ,"''")') trim(instrument_file)
            call mpi_print(mpi_msg, 0)

            call read_data_file(instrument_file, lsf_wvn, lsf_fwhm, data_params)

            ! Define reference FWHM
            lsf_fwhm_ref =  minval(lsf_fwhm)

            n_lsf = size(lsf_wvn)

            ! Find shape
            i = index(data_params, '''lsf_shape'':')

            if (i > 0) then
                j = scan(data_params(i:), ',}')
                lsf_shape = trim(adjustl(data_params(i + len('''lsf_shape'':'):j + 1)))
            else
                call mpi_print_warning('no instrument LSF shape found in instrument file, &
                                       &assuming gaussian shape', 0)
                lsf_shape = 'gaussian'
            end if
        end subroutine read_instrument_file


        subroutine read_molecule_parameters(file, name, molar_mass, rotational_partition_exponent, cutoff, &
                                            n_vibrational_band, vibrational_bands_wavenumber, &
                                            vibrational_bands_degeneracy)
            ! """
            ! Read a molecule parameter file.
            ! This file is a name list that contains the molecule needed physical properties.
            !
            ! input:
            !   file: file to read
            !
            ! outputs:
            !   name: name of the molecule
            !   molar_mass: (g.mol-1) molar mass of the molecule
            !   rotational_partition_exp: rotational partition exponent of the molecule
            !   cutoff: (cm-1) spectral distance from the center of the lines within the voigt profile is calculated
            !   n_vibrational_band: number of vibrational bands
            !   vibrational_bands_wavenumber: (cm-1) vibrational bands wavenumbers
            !   vibrational_bands_degeneracy: vibrational bands degenaracies
            ! """
            use molecules, only: molecule_name_size
            use nrt_parameters, only: file_name_size

            implicit none

            character(len=file_name_size), intent(in) :: file
            character(len=molecule_name_size), intent(out) :: name
            integer, intent(out) :: n_vibrational_band
            integer, dimension(8), intent(out) :: vibrational_bands_degeneracy
            double precision, intent(out) :: cutoff, molar_mass, rotational_partition_exponent
            double precision, dimension(8), intent(out) :: vibrational_bands_wavenumber

            namelist/molecule_parameters/name, molar_mass, rotational_partition_exponent, cutoff, &
                                         n_vibrational_band, vibrational_bands_wavenumber, vibrational_bands_degeneracy

            vibrational_bands_wavenumber(:) = 0D0
            vibrational_bands_degeneracy(:) = 0

            open(10, file=file)
            read(10, NML=molecule_parameters)
            close(10)

            if (n_vibrational_band > size(vibrational_bands_wavenumber) .or. &
                n_vibrational_band > size(vibrational_bands_degeneracy)) then
                write(mpi_msg, '("number of vibrational band (", I2,") > ", I2, " in file ''", A, "''")') &
                    n_vibrational_band, size(vibrational_bands_wavenumber), file
                call mpi_print_error(mpi_msg,0 )
            end if
        end subroutine read_molecule_parameters


        subroutine read_input_parameters()
            ! """
            ! Read the input file.
            ! """
            use clouds, only: &
                allocate_cloud_parameters, &
                n_cloud, &
                p_cloud_base, &
                p_cloud_top, &
                reflectances, &
                transmittances
            use molecules, only: &
                allocate_molecular_parameters, &
                is_retrieved, &
                molecules_name, &
                molecules_vmr, &
                n_mol, &
                vmr_factors
            use math, only: &
                sec
            use nrt_parameters, only: &
                calculate_cross_sections, &
                convolve, &
                input_file, &
                instrument_file, &
                noise_file, &
                output_derivatives, &
                path_absorption_cross_sections, &
                path_cross_sections, &
                path_instrument, &
                path_lines, &
                path_molecular_parameters, &
                path_noise, &
                path_retrieved_spectrum, &
                path_sky, &
                path_temperature_profile, &
                path_vmr, &
                path_spectrum_out, &
                path_vmr_out, &
                retrieve, &
                output_spectrum_name, &
                retrieved_spectrum, &
                sky_file, &
                temperature_profile_file, &
                write_cross_sections
            use retrieval, only: &
                chi2diff_threshold, &
                doppler_shift, &
                iteration_max, &
                v_smooth, &
                weight_apriori
            use spectrum, only: &
                wavenumber_min, &
                wavenumber_max, &
                wavenumber_step
            use target, only: &
                emission_angle, &
                incidence_angle, &
                latitude, &
                light_source_effective_temperature, &
                light_source_radius, &
                light_source_range, &
                target_mass, &
                target_equatorial_radius, &
                target_polar_radius

            implicit none

            namelist/output_files/&
                output_spectrum_name
            namelist/observations/&
                retrieved_spectrum, &
                noise_file, &
                sky_file, &
                doppler_shift
            namelist/target_parameters/&
                emission_angle, &
                incidence_angle, &
                latitude, &
                light_source_effective_temperature, &
                light_source_radius, &
                light_source_range, &
                target_mass, &
                target_equatorial_radius, &
                target_polar_radius
            namelist/atmosphere_parameters/&
                temperature_profile_file, &
                n_mol, &
                n_cloud
            namelist/molecules_parameters/&
                molecules_name, &
                molecules_vmr, &
                vmr_factors, &
                is_retrieved
            namelist/clouds_parameters/&
                p_cloud_top, &
                p_cloud_base, &
                transmittances, &
                reflectances
            namelist/spectrum_parameters/&
                wavenumber_min, &
                wavenumber_max, &
                wavenumber_step
            namelist/convolution/&
                convolve, &
                instrument_file
            namelist/retrieval_parameters/&
                retrieve, &
                iteration_max, &
                chi2diff_threshold, &
                v_smooth, &
                weight_apriori
            namelist/options/&
                calculate_cross_sections, &
                write_cross_sections, &
                output_derivatives
            namelist/paths/&
                path_vmr, &
                path_lines, &
                path_molecular_parameters, &
                path_absorption_cross_sections, &
                path_cross_sections, &
                path_retrieved_spectrum, &
                path_sky, &
                path_noise, &
                path_instrument, &
                path_temperature_profile, &
                path_vmr_out, &
                path_spectrum_out

            ! Read input parameters file
            write(mpi_msg, '("Reading parameters in file ''",A, "''")') trim(input_file)
            call mpi_print(mpi_msg, 0)

            open(1,file=input_file)

            read(1,NML=output_files)
            read(1,NML=observations)
            read(1,NML=target_parameters)
            read(1,NML=atmosphere_parameters)

            call allocate_molecular_parameters()
            call allocate_cloud_parameters()

            read(1,NML=molecules_parameters)
            read(1,NML=clouds_parameters)
            read(1,NML=spectrum_parameters)
            read(1,NML=convolution)
            read(1,NML=retrieval_parameters)
            read(1,NML=options)
            read(1,NML=paths)

            close(1)

            call check_inputs()

            contains
                subroutine check_inputs()
                    ! """
                    ! Check the inputs validity.
                    ! The physical validity of the inputs are tested in the appropriate subroutine.
                    ! """
                    implicit none
                        ! Check numbers
                        if (n_mol < 0) then
                            write(mpi_msg, '("number of molecules (", I10, ") must be positive")') &
                                n_mol
                            call mpi_print_error(mpi_msg, 0)
                        end if

                        if (n_cloud < 0) then
                            write(mpi_msg, '("number of clouds (", I10, ") must be positive")') &
                                n_cloud
                            call mpi_print_error(mpi_msg, 0)
                        end if
                end subroutine check_inputs
        end subroutine read_input_parameters


        subroutine remove_invalid_in_all(array1, array2, array3, new_array1, new_array2, new_array3)
            ! """
            ! Remove NaN of three arrays of the same size. If a NaN is detected in one array at a given index, then the
            ! values of the other arrays at the same index are also removed, regardless of their validity.
            ! Example (arrays not in Fortran syntax for clarity):
            ! >>> array1 = [101, 102, NaN, 104, 105]
            ! >>> array2 = [NaN, 107, NaN, 109, 110]
            ! >>> array3 = [111, 112, 113, 114, NaN]
            ! >>> remove_invalid_in_all(array1, array2, array3, new_array1, new_array2, new_array3)
            ! >>> print *, new_array1, new_array2, new_array3
            !     [102, 104] [107, 109] [112, 114]
            !
            ! inputs:
            !   array1: first array
            !   array2: second array
            !   array3: third array
            !
            ! outputs:
            !   new_array1: first array without the NaN of array1, array2 and array3
            !   new_array2: second array without the NaN of array1, array2 and array3
            !   new_array3: third array without the NaN of array1, array2 and array3
            ! """
            implicit none

            double precision, dimension(:), intent(in) :: array1, array2, array3
            double precision, dimension(:), allocatable, intent(out) :: new_array1, new_array2, new_array3

            integer :: &
                i,           &  ! index
                j,           &  ! index
                nb_nan,      &  ! number of NaN
                size_arrays, &  ! size of the two arrays
                new_size        ! size of the new arrays

            ! Initialise size
            size_arrays = size(array1)

            ! Check array sizes
            if(size(array2) /= size_arrays .or. size(array3) /= size_arrays) then
                write(mpi_msg, '("array1, array2 and array3 must have the same length, &
                                 &but have lenghts (", I10, "), (", I10, ") and (", I10, ")")') &
                    size_arrays, size(array2), size(array3)
                call mpi_print_error(mpi_msg, 0)
            end if

            ! Count the number of NaN in all arrays
            nb_nan = 0

            do i = 1, size_arrays
                if (isnan(array1(i)) .or. isnan(array2(i)) .or. isnan(array3(i))) then
                    nb_nan = nb_nan + 1
                end if
            end do

            ! Initalise new arrays
            new_size = size_arrays - nb_nan

            allocate(new_array1(new_size), new_array2(new_size), new_array3(new_size))

            ! Fill the new arrays with the valid values of the old arrays
            j = 1

            do i = 1, size_arrays
                if (.not. (isnan(array1(i)) .or. isnan(array2(i)) .or. isnan(array3(i)))) then
                    new_array1(j) = array1(i)
                    new_array2(j) = array2(i)
                    new_array3(j) = array3(i)

                    j = j + 1
                end if
            end do

        end subroutine remove_invalid_in_all


        subroutine nrt_init()
            ! """
            ! Read the input file and initialise most of the parameters.
            ! """
            use allocations, only: allocate_all
            use atmosphere, only: n_level, pressures, temperatures
            use clouds, only: n_cloud, &
                              allocate_cloud_parameters
            use math, only: arange_include
            use molecules, only: molecule_name_size, is_retrieved, molecules_name, n_mol, &
                                 n_vibrational_band, vib_band_degeneracies, vmr_factors, cutoffs, molar_masses, &
                                 qr_temperature_exponent, vib_band_wavenumbers, vmr_profiles, &
                                 allocate_molecular_parameters
            use nrt_parameters, only: input_file, &
                                      convolve, retrieve, &
                                      log_file, output_spectrum_name, &
                                      temperature_profile_file, &
                                      molecule_parameters_files, vmr_files
            use spectrum, only: &
                                radiances_interp__, vmr_derivative_interp__, cloud_t_derivative_interp__
            use retrieval, only: n_wvn_conv__

            implicit none

            character(len=molecule_name_size) :: &
                mol_name_file      ! molecule name in molecule parameter file

            character(len=256) :: &
                data_params        ! dump for data parameters when reading data files

            integer :: &
                k,              &  ! index
                l

            double precision, dimension(:), allocatable :: &
                tmp_array1,     &  ! temporary array
                tmp_array2

            ! Read input file
            call read_input_parameters()

            ! Get the name of all files used by the program
            call get_io_files_name()

            ! Save input parameters file into the log file
            if (mpi_rank == 0) then
                call copy_file(input_file, log_file)
            end if

            write(mpi_msg, '("Outputted spectrum file: ''",A ,"''")') trim(output_spectrum_name)
            call mpi_print(mpi_msg, 0)

            ! Load the retrieved spectrum and its noise; if no noise is given a SNR of 10 is assumed
            if (retrieve) then
                call get_retrieved_spectrum()
            else
                call mpi_print('No retrieval', 0)
            end if

            ! Load instrument LSF
            if (convolve) then
                call read_instrument_file()
            end if

            ! Make and share synthetic wavenumbers
            call make_synthetic_wavenumbers()

            ! Make and share convolution wavenumbers
            if (convolve) then
                call make_convolution_wavenumbers()
            end if

            ! Make instrument LSF
            if (convolve) then
                call make_instrument_lsf()
            else
                call mpi_print('No convolution', 0)
            end if

            ! Read the sky file
            call get_sky_transmittance()

            ! Load temperature profile and determine the number of atmospheric levels
            write(mpi_msg, '("Reading temperature profile in file ''",A, "''")') trim(temperature_profile_file)
            call mpi_print(mpi_msg, 0)

            call read_data_file(temperature_profile_file, pressures, temperatures, data_params)

            n_level = size(pressures)

            ! Load molecules and molecule parameters
            call get_molecule_files_name()

            ! Alocate some parameters according to the previously determined number of wavenumbers, levels and molecules
            call allocate_all()

            if (retrieve) then
                allocate(&
                    radiances_interp__(n_wvn_conv__), &
                    vmr_derivative_interp__(n_wvn_conv__, n_level, n_mol), &
                    cloud_t_derivative_interp__(n_wvn_conv__, n_level, n_cloud)&
                )
            end if

            ! Read molecule parameters files
            do l = 1, size(molecules_name(:))
                write(mpi_msg, '("Reading parameters of molecule ", A, " in file ''", A, "''")') &
                    trim(molecules_name(l)), trim(molecule_parameters_files(l))
                call mpi_print(mpi_msg, 0)

                mol_name_file = ''
                call read_molecule_parameters(molecule_parameters_files(l), mol_name_file, molar_masses(l), &
                    qr_temperature_exponent(l), cutoffs(l), n_vibrational_band(l), vib_band_wavenumbers(l, :), &
                    vib_band_degeneracies(l, :))

                ! Check file
                if (trim(molecules_name(l)) /= trim(mol_name_file)) then
                    write(mpi_msg, '("molecule name ''", A, "'' does not match with molecule name in file &
                                     &''", A, "''")') trim(molecules_name(l)), trim(molecule_parameters_files(l))
                    call mpi_print_error(mpi_msg, 0)
                end if
            end do

            ! Read VMR profile files
            do l = 1, n_mol
                if (is_retrieved(l)) then
                    write(mpi_msg, '("Reading ", A, " a priori VMR profile in file ''",A, "''")') &
                        trim(molecules_name(l)), trim(vmr_files(l))
                else
                    write(mpi_msg, '("Reading ", A, " VMR profile in file ''",A, "''")') &
                        trim(molecules_name(l)), trim(vmr_files(l))
                end if

                call mpi_print(mpi_msg, 0)

                call read_data_file(vmr_files(l), tmp_array1, tmp_array2, data_params)

                ! Check file
                if (size(tmp_array2(:)) == size(vmr_profiles(:,l))) then
                    ! Check factors
                    if (vmr_factors(l) < 0) then
                        write(mpi_msg, '("VMR factor (", F5.2, ") of molecule ", A, " must be positive")') &
                            vmr_factors(l), trim(molecules_name(l))
                        call mpi_print_error(mpi_msg, 0)
                    end if

                    vmr_profiles(:, l) = tmp_array2(:) * vmr_factors(l)

                    do k = 1, n_level
                        if (abs(tmp_array1(k) - pressures(k)) / pressures(k) > 1D-2) Then
                            write(mpi_msg, '("pressure at level ", I10, " of file ''", A , "'' must be ", ES9.2, &
                                             &" but is ", ES9.2)') k, trim(vmr_files(l)), pressures(k), tmp_array1(k)
                            call mpi_print_error(mpi_msg, 0)
                        end if
                    end do
                else
                    write(mpi_msg, '("file ''", A,"'' and file ''", A, "''must have the same number of &
                                     &pressure levels, but have (", I10, ") and (", I10, ") pressure levels")') &
                        trim(vmr_files(l)), trim(temperature_profile_file), size(tmp_array2(:)), size(vmr_profiles(:,l))
                    call mpi_print_error(mpi_msg, 0)
                end if
            end do

            call display_parameters()

            contains
                subroutine display_parameters()
                    ! """
                    ! Display spectrum parameters and atmosphere composition.
                    ! """
                    use molecules, only: is_retrieved, molecules_name, n_mol, vmr_factors, molecules_vmr
                    use nrt_parameters, only: convolve
                    use spectrum, only: wavenumbers, n_wvn, convolution_step
                    use retrieval, only: n_wvn_conv
                    use instrument, only: n_wvn_lsf

                    implicit none

                    integer :: &
                        l  ! index

                    if (mpi_rank == 0) then
                        print '("Spectrum parameters:")'
                        print '(1X, "Min wavenumber (cm-1): ", F8.2/1X, "Max wavenumber (cm-1): ", F8.2)', &
                                wavenumbers(1), wavenumbers(n_wvn)
                        if (convolve) then
                            print '(1X,"Instrument function size (cm-1): ", F8.2)', n_wvn_lsf * convolution_step
                        else
                            print '(1X,"Instrument function size (cm-1): ", A)', "None"
                        end if

                        print '(1X, "Number of wavenumbers: ", I10)', n_wvn
                        print '(1X, "Number of convolved wavenumbers: ", I10)', n_wvn_conv

                        print '("Atmosphere composition (mean VMR x VMR factor): ")'

                        do l = 1, n_mol
                            if (is_retrieved(l)) then
                                print '(1X,I2,": ",A," (",ES8.2, " x",F5.2,") RETRIEVED")', &
                                    l, molecules_name(l), molecules_vmr(l), vmr_factors(l)
                            else
                                print '(1X,I2,": ",A," (",ES8.2, " x",F5.2,")")', &
                                        l, molecules_name(l), molecules_vmr(l), vmr_factors(l)
                            end if
                        end do
                    end if
                end subroutine display_parameters

                subroutine get_io_files_name()
                    ! """
                    ! Get the names of the files used in the program.
                    ! Join the paths and the names defined in the input parameters file.
                    ! """
                    use nrt_parameters, only: &
                        cloud_t_derivative_file, &
                        instrument_file, &
                        log_file, &
                        noise_file, &
                        output_spectrum_name, &
                        path_instrument, &
                        path_noise, &
                        path_retrieved_spectrum, &
                        path_sky, &
                        path_spectrum_out, &
                        path_vmr_out, &
                        path_temperature_profile, &
                        temperature_profile_file, &
                        retrieved_spectrum, &
                        sky_file, &
                        vmr_derivative_file, &
                        vmr_file_out

                    implicit none

                    integer :: &
                        i  ! index

                    temperature_profile_file = trim(trim(path_temperature_profile) // trim(temperature_profile_file))
                    instrument_file = trim(trim(path_instrument) // trim(instrument_file))

                    retrieved_spectrum = trim(trim(path_retrieved_spectrum) // trim(retrieved_spectrum))

                    if (trim(sky_file) /= 'None') then
                        sky_file = trim(trim(path_sky) // trim(sky_file))
                    end if

                    if (trim(noise_file) /= 'None') then
                        noise_file = trim(trim(path_noise) // trim(noise_file))
                    end if

                    vmr_file_out = trim(path_vmr_out) // trim(output_spectrum_name)

                    output_spectrum_name = trim(path_spectrum_out) // trim(output_spectrum_name)
                    i = INDEX(output_spectrum_name,' ')

                    output_spectrum_name(i:) = '_in.log'
                    log_file = output_spectrum_name

                    output_spectrum_name(i:) = 'da.dat'
                    vmr_derivative_file = output_spectrum_name

                    output_spectrum_name(i:) = '      '
                    output_spectrum_name(i:) = 'dt.dat'
                    cloud_t_derivative_file = output_spectrum_name

                    output_spectrum_name(i:) = '      '
                    output_spectrum_name = trim(output_spectrum_name)
                    output_spectrum_name(i:) = '.dat'
                endsubroutine get_io_files_name

                subroutine get_molecule_files_name()
                    ! """
                    ! Get the molecules lines, parameters and absorption cross sections files names.
                    ! """
                    use molecules, only: molecules_name, n_mol
                    use nrt_parameters, only: write_cross_sections, path_absorption_cross_sections, &
                                              path_lines, path_molecular_parameters, path_vmr, &
                                              absorption_cross_sections_files, lines_files, molecule_parameters_files, &
                                              vmr_files
                    use spectrum, only: wavenumber_max, wavenumber_min

                    implicit none

                    logical :: &
                        present     ! test for presence of a file in the indicated directory

                    integer :: &
                        l           ! index (molecules)

                    allocate(absorption_cross_sections_files(n_mol), vmr_files(n_mol), lines_files(n_mol), &
                             molecule_parameters_files(n_mol))

                    ! Include H2-He continuum and ensure H2 and He are removed from line-by-line loops
                    !call check_H2He()

                    do l = 1, size(molecules_name(:))
                        molecules_name(l) = trim(adjustl(molecules_name(l)))
                        molecule_parameters_files(l) = &
                            trim(path_molecular_parameters) // trim(molecules_name(l)) // '.nml'

                        lines_files(l) = trim(path_lines) // trim(molecules_name(l)) // '.dat'

                        inquire (file=lines_files(l), exist=present)

                        if (.not. present) then
                            if (trim(molecules_name(l)) == 'H2') then
                                lines_files(l) = 'None'
                            elseif (trim(molecules_name(l)) == 'He') then
                                lines_files(l) = 'None'
                            else
                                ! TODO try to read CIA files ?
                                write(mpi_msg, '("lines file ''", A, "'' does not exists, &
                                                 &and there is no ab-initio absorption cross-sections calculations &
                                                 &implemented for species ''", A, "''")') &
                                    trim(lines_files(l)), trim(molecules_name(l))
                                call mpi_print_error(mpi_msg, 0)
                            end if
                        end if

                        write(absorption_cross_sections_files(l), &
                            '(A, "_", ES9.3,  "_", ES9.3, "_", ES12.6, "-", ES12.6, "_n", I3.3, "-p", I3.3, A4)') &
                            trim(path_absorption_cross_sections) // trim(molecules_name(l)), &
                            temperatures(1), pressures(1), wavenumber_min, wavenumber_max, &
                            mpi_nproc, mpi_rank, '.xsc'
                        vmr_files(l) = trim(path_vmr) // trim(molecules_name(l)) // '.dat'
                    end do

                    ! Checking for file presence
                    do l = 1, size(molecules_name(:))
                        if (.not. write_cross_sections) then
                            inquire(file=absorption_cross_sections_files(l),exist=present)

                            if(.not. present) then
                                write(mpi_msg, '("absorption cross section file ''", A, "'' does not exist; &
                                                 &generating new absorption cross section files")') &
                                    trim(absorption_cross_sections_files(l))
                                call mpi_print_info(mpi_msg, 0)

                                exit
                            end if
                        end if
                    end do

                    if (.not. write_cross_sections) then
                        ! Update write status of abs files to handle the case where an absorption file is missing
                        call MPI_ALLREDUCE(.not. present, write_cross_sections, 1, MPI_LOGICAL, MPI_LOR, &
                                           MPI_COMM_WORLD, mpi_ierr)
                    end if
                end subroutine get_molecule_files_name

                subroutine get_sky_transmittance()
                    ! """
                    ! Read the sky file, which is used to simulate atmospheric absorption is the synthetic spectrum.
                    ! If the sky is already convolved, then it will be applied after the synthetic spectrum convolution.
                    ! Note that it is more accurate to use non convoluted skies.
                    !
                    ! inputs:
                    !   sky_file: the path of the file containing the sky transmittance
                    !   wavenumber_step: synthetic spectrum wavenumber stepsize
                    !   doppler_shift: doppler shift of the observed spectrum
                    !   n_wvn__: number of wavenumbers in the synthetic spectrum
                    !   wavenumber_min: minimum wavenumber of the synthetic spectrum
                    !   wavenumber_max: maximum wavenumber of the synthetic spectrum
                    !
                    ! outputs:
                    !   t_sky: the transmittance of the sky
                    !   sky_convolved: if True, the sky is already convoluted
                    ! """
                    use math, only:&
                        interp
                    use spectrum, only:&
                        n_wvn, &
                        n_wvn__, &
                        n_wvn_x, &
                        displs_j, &
                        t_sky, &
                        t_sky__, &
                        wavenumbers
                    use retrieval, only:&
                        doppler_shift
                    use nrt_parameters, only:&
                        sky_file

                    implicit none

                    character(len=256) :: data_params
                    integer :: j, j_min, j_max
                    double precision, dimension(:), allocatable :: wvn_sky, t_sky0

                    if (trim(sky_file) /= 'None') then
                        if (mpi_rank == 0) print '("Reading sky in file ''", A ,"''")', trim(sky_file)

                        call read_data_file(sky_file, wvn_sky, t_sky0, data_params)

                        ! Apply Doppler shift
                        if (doppler_shift < -tiny(0.) .or. doppler_shift > tiny(0.)) then
                            if (mpi_rank == 0) print '(1X, "Shifting sky wavenumber by ", SP, F5.2, " cm-1...")', &
                                doppler_shift
                            wvn_sky(:) = wvn_sky(:) + doppler_shift
                        end if

                        ! Get the number of wavenumbers in the synthetic spectrum
                        allocate(t_sky(n_wvn))

                        ! Find where the synthetic wavenumbers recover the sky wavenumbers
                        j_min = 1
                        j_max = n_wvn

                        do j = 1, n_wvn
                            if (wavenumbers(j) < wvn_sky(1)) then
                                j_min = j_min + 1
                            end if

                            if (wavenumbers(j) > wvn_sky(size(wvn_sky)) .and. j < j_max) then
                                j_max = j - 1
                                exit
                            end if
                        end do

                        ! Interpolate the sky where the synthetic wavenumbers__ recover the sky wavenumbers__
                        t_sky(j_min:j_max) = interp(wavenumbers(j_min:j_max), wvn_sky(:), t_sky0(:))
                        t_sky(:j_min) = t_sky(j_min)
                        t_sky(j_max:) = t_sky(j_max)
                    else
                        if (mpi_rank == 0) print '("No sky correction for the synthetic spectrum")'
                        allocate(t_sky(n_wvn))
                        t_sky(:) = 1D0
                    end if

                    ! Share the sky transmittance
                    allocate(t_sky__(n_wvn__))
                    call MPI_SCATTERV(t_sky, n_wvn_x, displs_j, MPI_DOUBLE_PRECISION, &
                                      t_sky__, n_wvn__, MPI_DOUBLE_PRECISION, &
                                      0, MPI_COMM_WORLD, mpi_ierr)
                end subroutine get_sky_transmittance
        end subroutine nrt_init


        subroutine output()
            ! """
            ! Output the results.
            ! """
            use spectrum
            use atmosphere, only: n_level, pressures
            use instrument, only: n_wvn_lsf
            use molecules, only: is_retrieved, molecules_name, n_mol, vmr_profiles, vmr_profiles_err
            use clouds, only: n_cloud, transmittances, reflectances
            use nrt_parameters, only: output_derivatives, retrieve, vmr_file_out, vmr_derivative_file, &
                                      cloud_t_derivative_file, output_spectrum_name, convolve

            implicit none

            character(len=256) :: vmr_file_out_root

            integer :: &
                j, &  ! index (wavenumber)
                j_min, &  ! min index to output
                j_max, &  ! max index to output
                k, &  ! index (atmospheric levels)
                l, &  ! index (molecules)
                n     ! index

            if (.not. retrieve) then
                if (mpi_rank == 0) print '("[MPI] Assembling final spectrum...")'
                call MPI_GATHERV(radiances__, n_wvn__, MPI_DOUBLE_PRECISION,  &
                                 radiances, n_wvn_x, displs_j, MPI_DOUBLE_PRECISION, 0, &
                                 MPI_COMM_WORLD, mpi_ierr)
            end if

            if (output_derivatives) then
                call mpi_allgatherv3D(vmr_derivative__(:,:,:), vmr_derivative(:,:,:))
                call mpi_allgatherv3D(cloud_t_derivative__(:,:,:), cloud_t_derivative(:,:,:))
            end if

            ! Output array
            if (convolve .and. .not. retrieve) then
                j_min = n_wvn_lsf + 1
                j_max = n_wvn - n_wvn_lsf
                n_wvn = n_wvn - 2 * n_wvn_lsf
            else
                j_min = 1
                j_max = n_wvn
            end if

            ! Write spectrum file
            if (mpi_rank == 0) then
                print '("Writing output files...")'
                open(99,file=output_spectrum_name,status='unknown',form='formatted')
                write(99,'(A25, I10)', advance='no') '(cm-1) (W.m-2.sr-1/cm-1)', n_wvn
                do n=1,n_cloud
                    write(99, '(" {''atmosphere.clouds[",I1,"].transmittance'': ",F5.3,", ",&
                                 &"''atmosphere.clouds[",I1,"].reflectance'': ",F5.3,"}")', advance='no') n - 1, &
                          transmittances(n), n - 1, reflectances(n)
                end do
                write(99, *)
                do j = j_min, j_max
                    write(99,'(F12.6, ES15.8)') wavenumbers(j), radiances(j)
                end do
                close(99)

                ! Write derivative files
                if(output_derivatives) then
                    open(99,file=vmr_derivative_file, status='unknown', form='formatted')
                    write(99,'(F10.3,I8,ES15.8,I8,I8)') &
                        wavenumbers(j_min), j_max - j_min + 1, wavenumber_step, n_mol, n_level
                    do l = 1, n_mol
                        write(99,'(A15)') molecules_name(l)
                        do k = 1, n_level
                            write(99,'(ES15.4)') pressures(k)
                            do j = j_min, j_max
                                write(99,'(ES20.8)') vmr_derivative(j,k,l)
                            end do
                        end do
                    end do
                    close(99)

                    open(99,file=cloud_t_derivative_file,status='unknown',form='formatted')
                    write(99,'(F10.3,I8,ES15.8,I8,I8)') wavenumber_min, j_max-j_min+1, wavenumber_step, n_cloud, n_level
                    do l = 1, n_cloud
                        write(99,'(I3)') l
                        do k = 1, n_level
                            write(99,'(ES15.4)') pressures(k)
                            do j = j_min, j_max
                                write(99,'(ES20.8)') cloud_t_derivative(j,k,l)
                            end do
                        end do
                    end do
                    close(99)
                end if

                ! Write retrieved VMR profile file
                if(retrieve) then
                    vmr_file_out_root = vmr_file_out
                    do l = 1, n_mol
                        if (is_retrieved(l)) then
                            vmr_file_out = trim(vmr_file_out_root) // '_' // trim(molecules_name(l)) // '_abd.dat'
                            open(99,file=trim(vmr_file_out),status='unknown',form='formatted')
                            write(99,'(1X,"Jup p(bar) [",A,"] sig", T26, I10)') trim(molecules_name(l)), n_level

                            do k = 1, n_level
                                if (vmr_profiles(k,l) >= 1D100) then
                                    vmr_profiles(k,l) = 9.99D99
                                else if (vmr_profiles(k,l) <= 1D-99) then
                                    vmr_profiles(k,l) = 1D-99
                                end if

                                if (vmr_profiles_err(k,l) >= 1D100) then
                                    vmr_profiles_err(k,l) = 9.99D99
                                else if (vmr_profiles_err(k,l) <= 1D-99) then
                                    vmr_profiles_err(k,l) = 1D-99
                                end if

                                write(99,'(3ES9.2)') pressures(k), vmr_profiles(k,l), vmr_profiles_err(k,l)
                            end do
                            close(99)
                        end if
                    end do
                end if
            end if
        end subroutine output
end module nrt_IO
