module nrt_retrieval
    ! """
    ! Contains the optimal non-linear inversion method of NRT.
    ! """
    use mpi_utils

    implicit none

    save

    contains
        subroutine retrieval_wrap
            ! """
            ! Manage the retrieving of the parameters.
            ! """
            use atmosphere, only: n_level, column_number_density, pressures, temperatures
            use clouds, only: n_cloud, transmittances, reflectances, cloud_base_levels, transmittances_err
            use math, only: chi2_reduced, interp
            use molecules, only: is_retrieved, n_mol, vmr_profiles, vmr_profiles_err, molecules_name
            use mpi_utils, only: mpi_fftconvolve, mpi_allgatherv1D, mpi_matinv
            use nrt_physics, only: calculate_interpolated_radiance, column_number_density2vmr, saturation_pressure, &
                vmr2column_number_density
            use retrieval, only: n_wvn_ret, iteration_max, chi2diff_threshold, v_smooth, weight_apriori, &
                                 radiances_ret, wavenumbers_ret, noise_radiances
            use spectrum, only: wavenumbers, radiances, n_wvn, &
                                radiances_interp, vmr_derivative_interp, cloud_t_derivative_interp

            implicit none

            integer :: &
                i,                          &  ! index
                j,                          &  ! index
                k,                          &  ! index
                l,                          &  ! index
                n,                          &  ! index
                n_ret_mol,                  &  ! number of molecule to retrieve
                n_param                        ! total number of parameters retrieved

            double precision :: &
                chi2_0,                     &  ! reduced chi2 of retrieval iteration n - 1
                chi2_1,                     &  ! reduced chi2 of retrieval iteration n
                chi2_diff                      ! difference in chi2 between two iterations

            double precision, dimension(n_wvn_ret) :: &
                errors                         ! measurment error covariance matrix, e.g. the noise of the observed
                                               ! spectrum

            double precision, dimension(:, :), allocatable :: &
                apriori_parameters,         &  ! a priori parameters
                pre_retrieved_parameters,   &  !
                saturation_profiles,        &  ! saturation profiles
                S,                          &  ! square root of the diagonal of the retrieval uncertainties
                retrieved_parameters           ! retrieved parameters

            double precision, dimension(:, :, :), allocatable :: &
                covariances,                &  ! covariance of the parameters
                derivatives,                &  ! all the parameters derivatives packed together
                retrieval_uncertainties        ! uncertainties of the retrieved parameters

            if (mpi_rank == 0) print '("Starting retrieval...")'

            if (mpi_rank == 0) then
                print '(1X,"Max number of iterations:      ",I3)', iteration_max
                print '(1X,"Max difference of chi2:       ",F5.3)', chi2diff_threshold
                print '(1X,"Vertical cor. (height scale): ",F5.3)', v_smooth
                print '(1X,"Weight of a priori:           ",F5.3)', weight_apriori
            end if

            ! Initialization
            i = 0

            n_ret_mol = 0 ! number of molecule retrieved
            do l = 1, n_mol
                if (is_retrieved(l)) then
                    n_ret_mol = n_ret_mol + 1
                end if
            end do

            n_param = n_ret_mol + n_cloud ! total number of parameters retrieved

            allocate(apriori_parameters(n_level, n_param))
            allocate(cloud_t_derivative_interp(n_wvn_ret, n_level, n_cloud))
            allocate(covariances(n_level,n_level, n_param))
            allocate(derivatives(n_wvn_ret, n_level, n_param))
            allocate(radiances_interp(n_wvn_ret))
            allocate(retrieval_uncertainties(n_level, n_level, n_param))
            allocate(retrieved_parameters(n_level, n_param))
            allocate(pre_retrieved_parameters(n_level, n_param))
            allocate(S(n_level, n_ret_mol))
            allocate(saturation_profiles(n_level, n_ret_mol))
            allocate(vmr_derivative_interp(n_wvn_ret, n_level, n_mol))

            apriori_parameters(:, :) = 0D0
            retrieved_parameters(:, :) = 0D0
            pre_retrieved_parameters(:, :) = 0D0
            derivatives(:, :, :) = 0D0
            retrieval_uncertainties(:, :, :) = 0D0
            saturation_profiles(:, :) = 1D0

            ! Initialise saturation profiles
            j = 0
            do l = 1, n_mol
                if (is_retrieved(l)) then
                    j = j + 1

                    do k = 1, n_level
                        ! Get saturation profile
                        saturation_profiles(k, j) = &
                            saturation_pressure(molecules_name(l), temperatures(k)) / pressures(k)

                        if (saturation_profiles(k, j) >= huge(0.) / pressures(k)) then
                            saturation_profiles(k, j) = min(saturation_profiles(k, j), 1D0)
                            exit
                        else
                            saturation_profiles(k, j) = min(saturation_profiles(k, j), 1D0)
                        end if
                    end do

                    saturation_profiles(:, j) = log(vmr2column_number_density(saturation_profiles(:, j)))
                end if
            end do

            ! Initialise covariances
            ! TODO covariances as input parameters ? Migth be useful to set specific retrieval rules for some molecules
            l = 0
            n = 0
            do k = 1, n_param
                if (k <= n_ret_mol) then
l = l + 1
do while (.not. is_retrieved(l) .and. l <= n_mol) !*TEST
    l = l + 1
end do
                    do j = 1, n_level
                        do i = 1, n_level
                            covariances(i, j, k) = exp(-log(pressures(i) / pressures(j))**2 / (2 * v_smooth**2))
if ((pressures(i) >= 0.8 .or. pressures(j) >= 0.8) .and. molecules_name(l) == 'NH3') covariances(i, j, k) = 1D0 !*TEST
if ((pressures(i) >= 1. .or. pressures(j) >= 1.) .and. molecules_name(l) == 'PH3') covariances(i, j, k) = 1D0 !*TEST
if ((pressures(i) >= 1. .or. pressures(j) >= 1.) .and. molecules_name(l) == 'AsH3') covariances(i, j, k) = 1D0 !*TEST
if ((pressures(i) >= 1. .or. pressures(j) >= 1.) .and. molecules_name(l) == 'GeH4') covariances(i, j, k) = 1D0 !*TEST
if (molecules_name(l) == 'H2O') covariances(i, j, k) = 1D0 !*TEST
if (molecules_name(l) == 'CH3D') covariances(i, j, k) = 1D0 !*TEST
                        end do
                    end do
                else
                    n = n + 1
                    do j = 1, n_level
                        do i = 1, n_level
                            covariances(i, j, k) = exp(-log(pressures(i) / pressures(j))**2 / (2 * v_smooth**2))
                        end do
                    end do
                end if
            end do

            do j = 1, n_wvn_ret
                errors(j) = noise_radiances(j)**2
            end do

            ! Initialise a priori parameters
            l = 0
            n = 0
            do k = 1, n_param
                if (k <= n_ret_mol) then
                    l = l + 1
                    do while (.not. is_retrieved(l) .and. l <= n_mol)
                        l = l + 1
                    end do
                    do j = 1, n_level
                        ! A priori parameters are also updated even if its slows the convergence (c.f. Rodgers 2000)
                        ! In all the tested case the convergence was estimated fast enough
                        apriori_parameters(j, k) = log(column_number_density(j, l))
                    end do
                else
                    n = n + 1
                    apriori_parameters(cloud_base_levels(n), k) = transmittances(n)
                end if
                pre_retrieved_parameters(:, k) = apriori_parameters(:, k)
            end do

            ! Initialize chi2
            chi2_0 = huge(0.)
            chi2_1 = huge(0.)
            chi2_diff = huge(0.)

            ! Main loop
            i = 0
            do while (chi2_diff > chi2diff_threshold .and. i < iteration_max)
                ! Calculate radiance and derivatives with a priori parameters
                call calculate_interpolated_radiance()

                ! Estimate goodness of fit
                ! ====
                ! TODO check why not updating a priori parameters (according to Rodgers2000) seems to give worse fits
                ! Use this to follow Rodgers2000 criterion on convergence (no apriori updates)
!                chi2_1 = 0D0  ! Rodgers2000
!
!                do k = 1, n_ret_mol
!                    do j = 1, n_level
!                        S(j, k) = max(sqrt(retrieval_uncertainties(j, j, k)), dble(tiny(0.)))
!                    end do
!
!                   chi2_1 = chi2_1 + chi2_reduced(pre_retrieved_parameters(:, k), retrieved_parameters(:, k), S(:, k))
!                end do
!                chi2_1 = chi2_1 / n_level
!                chi2_diff = chi2_1

                ! OR use this if you chose to update the a priori
                chi2_1 = chi2_reduced(radiances_ret(:), radiances_interp(:), noise_radiances(:)) / (n_wvn_ret - 1)
                chi2_diff = abs(chi2_1-chi2_0) / chi2_0
                ! ====
                if(mpi_rank == 0) print '(1X,"chi2 = ",ES8.2," (",F4.2,") (ite.",I3,")")', chi2_1, chi2_diff, i

                ! Reinitilize chi2 for the next iteration
                chi2_0 = chi2_1

                ! Update retrieving parameters (derivatives and calculated radiance)
                if(mpi_rank==0) print '(1X, "Updating retrieval parameters...")'

                l = 0
                n = 0
                do k = 1, n_param
                    if (k <= n_ret_mol) then
                        l = l + 1
                        do while (.not. is_retrieved(l) .and. l <= n_mol)
                            l = l + 1
                        end do
                        do j = 1, n_level
                            ! A priori parameters are also updated even if its slows the convergence (c.f. Rodgers 2000)
                            ! In all the tested case the convergence was estimated fast enough
                            apriori_parameters(j, k) = log(column_number_density(j, l))!***
                            pre_retrieved_parameters(j, k) = log(column_number_density(j, l))
                            derivatives(:, j, k) = vmr_derivative_interp(:, j, l)
                        end do
                    else
                        n = n + 1
                        apriori_parameters(cloud_base_levels(n), k) = transmittances(n)!***
                        pre_retrieved_parameters(cloud_base_levels(n), k) = transmittances(n)
                        do j = 1, n_level
                            derivatives(:, j, k) = cloud_t_derivative_interp(:, j, n)
                        end do
                    end if
                end do

                ! Retrieval
                if(mpi_rank==0) print '(1X, "Retrieving...")'
                call retrieval_core(apriori_parameters(:, :), pre_retrieved_parameters(:, :), &
                                    radiances_ret(:), radiances_interp(:), &
                                    derivatives(:, :, :), covariances(:, :, :), errors(:), &
                                    retrieved_parameters(:, :), retrieval_uncertainties(:, :, :))

                ! Update retrieved parameters (cloud and abundance profiles)
                if(mpi_rank==0) print '(1X, "Updating retrieved parameters...")'
                l = 0
                n = 0
                do k = 1, n_param
                    if (k <= n_ret_mol) then  ! update retreived VMR profiles
                        l = l + 1

                        do while (.not. is_retrieved(l) .and. l <= n_mol)
                            l = l + 1
                        end do

                        do j = 1, n_level
                            ! Warn of a future overflow
                            ! If this happen it probably means that an ugly error occured during the retrieval
                            if (retrieved_parameters(j, k) > log(huge(0D0))) then
                                write(mpi_msg, '("overflow in retrieved abundance of molecule ",I2, " &
                                                 &at level ", I3, "(", ES15.6, ")")') &
                                    l, j, retrieved_parameters(j, k)
                                call mpi_print_warning(mpi_msg, 0)
                            end if

                            if (retrieved_parameters(j, k) < log(tiny(0D0))) then
                                write(mpi_msg, '("underflow in retrieved abundance of molecule ",I2, " &
                                                 &at level ", I3, "(", ES15.6, ")")') &
                                    l, j, retrieved_parameters(j, k)
                                call mpi_print_warning(mpi_msg, 0)
                                retrieved_parameters(j, k) = log(tiny(0D0))
                            end if

                            ! Ensure that the molecular abundance stay under 1 or the saturation abundance
                            retrieved_parameters(j, k) = min(retrieved_parameters(j, k), saturation_profiles(j, k))

                            column_number_density(j, l) = exp(retrieved_parameters(j, k))

                            ! Update VMR profiles error
                            ! Since ln(vmr) is retrieved instead of vmr, the output is the covariance of ln(vmr)
                            ! In this case the error on vmr is given by: vmr * cov(ln(vmr)), hence the value below is
                            ! a *factor* of uncertainty, not directly the uncertainty
                            vmr_profiles_err(j, l) = sqrt(retrieval_uncertainties(j, j, k))
                        end do
                    else  ! update clouds transmittance
                        n = n + 1

                        ! Ensure that the retrieved transmittance is always comprised between 0 and 1
                        if (retrieved_parameters(cloud_base_levels(n), k) < 0.1 * transmittances(n)) then
                            retrieved_parameters(cloud_base_levels(n), k) = 0.1 * transmittances(n)
                        else if (retrieved_parameters(cloud_base_levels(n),k) > 1.0) then
                            retrieved_parameters(cloud_base_levels(n),k) = (transmittances(n) + 1) / 2.0
                        end if

                        ! Update cloud transmittance
                        transmittances(n) = retrieved_parameters(cloud_base_levels(n),k)
                        transmittances_err(cloud_base_levels(n),n) = &
                            sqrt(retrieval_uncertainties(cloud_base_levels(n), cloud_base_levels(n),k))

                        if (mpi_rank==0) print '(1X,"cloud ",I2," T = ", F5.3, " +/- ", F5.3, "; R = ", F5.3)',&
                            n, transmittances(n), transmittances_err(cloud_base_levels(n),n), &
                            min(reflectances(n), 1 - transmittances(n))
                    end if
                end do

                i = i + 1
                call MPI_BARRIER(MPI_COMM_WORLD,mpi_ierr)  ! ensure that all the processes are updated
            end do

            ! Assemble spectrum and derivatives
            call MPI_BARRIER(MPI_COMM_WORLD,mpi_ierr)  ! just make a clean terminal output
            if (mpi_rank == 0) print '("Assembling spectrum...")'

            call calculate_interpolated_radiance

            ! Compute last chi2
            if (mpi_rank==0) then
                chi2_1 = chi2_reduced(radiances_ret(:), radiances_interp(:), noise_radiances(:)) / (n_wvn_ret - 1)
                print '(1X,"chi2 = ",ES12.6)',chi2_1
            end if

            ! Prepare vmr_profiles for output
            deallocate(wavenumbers, radiances)
            allocate(wavenumbers(n_wvn_ret), radiances(n_wvn_ret))

            wavenumbers(:) = wavenumbers_ret(:)
            radiances(:) = radiances_interp(:)
            n_wvn = n_wvn_ret

            do l = 1, n_mol
                vmr_profiles(:, l) = column_number_density2vmr(column_number_density(:, l))
            end do

            if(mpi_rank==0) print '(1X, "ite. to result = ", I2)', i
        end subroutine retrieval_wrap


        subroutine retrieval_core(x_a, x_i, y, y_a, K, S_a, S_e, x, S)
            ! """
            !Core of the retrieval method. From Rodgers 2000.
            !
            ! inputs:
            !   x_a: a priori estimates of the parameters, e.g. the a prioris of the abundance profiles
            !   y: measurment vector, e.g. the observed spectrum
            !   y_a: (F(x_a) in Rodgers formalism) forward a priori model, e.g. the synthetic spectrum
            !   K: weighting function matrix, K_ij = dF_i(x_k)/dx_j, the 3rd dim contains the K of each params
            !   S_a: covariance matrix of the paramters, i.e. the smoothing function/uncertainty on the parameters
            !   S_e: measurment error covariance matrix, e.g. the noise of the observed spectrum
            !
            ! outputs:
            !   x: the best estimators of the paramters
            !   S: convariance matrix of x, their diagonal are the std deviation of x
            ! """
            use retrieval, only: weight_apriori

            implicit none

            double precision, dimension(:), intent(in) :: y, y_a, S_e
            double precision, dimension(:, :), intent(in) :: x_a, x_i
            double precision, dimension(:, :, :), intent(in) :: K, S_a

            double precision, dimension(size(x_a, 1), size(x_a, 2)), intent(out) :: x
            double precision, dimension(size(S_a, 1), size(S_a, 2), size(S_a, 3)), intent(out) :: S

            integer :: &
                i,              &  ! index
                j                  ! index

            integer :: &
                n_i,            &  ! number of state
                n_k,            &  ! number of level
                n_p                ! number of parameter

            double precision :: &
                trace_error,    &  ! trace of the measurment error covariance matrix
                trace_KSKt         ! trace of the KSK^t matrix

            double precision, dimension(size(y_a), size(y_a)) :: &
                A,              &  ! inversed matrix ("uncertainty matrix")
                A_inv,          &  ! inverse of matrix A
                KSKt               ! KSK^t matrix

            double precision, dimension(size(K, 3)) :: &
                alpha              ! weight correction factor

            double precision, dimension(size(K, 2), size(y_a), size(K, 3)) :: &
                G                  ! gain matrix

            ! Initialization
            n_i = size(y_a)
            n_p = size(K, 3)
            n_k = size(S_a, 2)

            A(:, :) = 0D0
            G(:, :, :) = 0D0

            trace_error = sum(S_e(:)) / sqrt(dble(size(S_e(:))))

            ! Calculate the KSKt of each independant parameters, then the "uncertainty matrix"
            do j = 1, n_p
                KSKt(:, :) = matmul(matmul(K(:, :, j), S_a(:, :, j)), transpose(K(:, :, j)))

                trace_KSKt = sum((/(KSKt(i, i), i = 1, n_i)/))

                if (trace_KSKt > tiny(0D0)) then  ! ignore the cases where KSKt = 0 <=> the parameter has no influence
                    alpha(j) = weight_apriori * (trace_error / trace_KSKt)
                else
                    alpha(j) = 0D0
                end if

                A(:, :) = A(:, :) + alpha(j) * KSKt(:, :)
            end do

            ! Add the noise to the diagonal of the "uncertainty matrix"
            do i = 1, n_i
                A(i, i) = A(i, i) + S_e(i)
            end do

            ! Inverse the "uncertainty matrix"
            call mpi_matinv(A, A_inv, n_i)

            ! Calculate the retrieved parameters and their uncertainties
            do j = 1, n_p
                G(:, :, j) = matmul(matmul(alpha(j) * S_a(:, :, j), transpose(K(:, :, j))), A_inv(:, :))

                x(:, j) = x_a(:, j) + matmul(G(:, :, j), y(:) - y_a(:) + matmul(K(:, :, j), x_i(:, j) - x_a(:, j)))

                S(:, :, j) = alpha(j) * (S_a(: ,:, j) - matmul(matmul(G(:, :, j), K(:, :, j)), S_a(:, :, j)))
            end do

        end subroutine retrieval_core

end module nrt_retrieval
