module nrt_physics
    ! """
    ! Contains the physics of NRT, notably the radiative transfer.
    ! """
    use mpi_utils

    implicit none

    save

    contains
        double precision function saturation_pressure(molecule, temperature)
            ! """
            ! Return the saturation pressure of a given molecule at a given temperature.
            !
            ! inputs:
            !   molecule: name of the molecule
            !   temperature: (K) temperature
            !
            ! output:
            !   saturation_pressure: (bar) saturation pressure of "molecule" at "temperature"
            ! """
            implicit none

            character(len=*), intent(in) :: molecule
            double precision, intent(in) :: temperature

            if (trim(molecule) == 'H2O') then
                saturation_pressure = h2o_saturation_pressure()
            elseif (trim(molecule) == 'NH3') then
                saturation_pressure = nh3_saturation_pressure()
            else
                write(mpi_msg, '("no saturation pressure formulae for species ", A)') trim(molecule)
                call mpi_print_info(mpi_msg, 0)
                saturation_pressure = huge(0.)
            end if

            contains
                double precision function h2o_saturation_pressure()
                    ! """
                    ! Calculate the H2O pressure of saturation.
                    ! Sources: Fray & Schmitt 2009, Flatau et al. 1992.
                    ! """
                    implicit none

                    double precision, parameter :: &
                        temperature_triple_point = 273.16D0, &  ! (K) temperature of H2O triple point (F&S 2009)
                        pressure_triple_point = 6.11657D-3, & ! (bar) pressure of H2O triple point (Fray & Schmitt 2009)
                        temperature_critical_point = 647.096D0, &  ! (K) temperature of H2O critical point (IAPWS 2011)
                        pressure_critical_point = 220.64D0  ! (bar) pressure of H2O critical point (IAPWS 2011)

                    double precision, parameter, dimension(6) :: &
                        a = [-7.85951783D0, 1.84408259D0, -11.7866497D0, 22.6807411D0, -15.9618719D0, 1.80122502D0], &
                        tau_pow = [1D0, 1.5D0, 3D0, 3.5D0, 4D0, 7.5D0]
                        ! Coefficients from Wagner and Pruss 1993 (IAPWS 1992)
                    double precision, parameter, dimension(7) :: &
                        e = [20.996967D0, 3.724375D0, -13.920548D0, 29.698877D0, -40.197239D0, 29.788048D0, -9.130510D0]
                        ! Coefficients from Feistel and Wagner 2007
                    integer :: &
                        i  ! index

                    double precision :: &
                        eta, &  ! intermediate factor
                        tau     ! intermediate factor

                    if (temperature <= temperature_triple_point) then
                        eta = 0D0

                        do i = 1, size(e)
                            eta = eta + e(i) * (temperature / temperature_triple_point) ** (i - 1)
                        end do

                        h2o_saturation_pressure = pressure_triple_point * &
                            exp(3D0 / 2D0 * log(temperature / temperature_triple_point) + &
                                (1D0 - temperature_triple_point / temperature) * eta)
                    elseif (temperature <= temperature_critical_point) then
                        ! This formula misses the triple point by less than 0.08 percent
                        tau = 1D0 - temperature / temperature_critical_point
                        eta = 0D0

                        do i = 1, size(a)
                            eta = eta + a(i) * tau ** tau_pow(i)
                        end do

                        h2o_saturation_pressure = pressure_critical_point * &
                            exp(temperature_critical_point / temperature * eta)
                    else
                        h2o_saturation_pressure = pressure_critical_point
                    end if
                end function h2o_saturation_pressure

                double precision function nh3_saturation_pressure()
                    ! """
                    ! Calculate the H2O pressure of saturation.
                    ! Sources: Fray & Schmitt 2009, Flatau et al. 1992.
                    ! """
                    use math, only: interp

                    implicit none

                    double precision, parameter :: &
                        temperature_triple_point = 195.41D0, &  ! (K) temperature of H2O triple point (F&S 2009)
                        pressure_triple_point = 6.09D-2, & ! (bar) pressure of H2O triple point (Fray & Schmitt 2009)
                        temperature_critical_point = 405.5D0, &  ! (K) temperature of H2O critical point (IAPWS 2011)
                        pressure_critical_point = 113.5D0  ! (bar) pressure of H2O critical point (IAPWS 2011)

                    double precision, parameter, dimension(5) :: &
                        a = [1.596E1, -3.537E3, -3.310E4, 1.742E6, -2.995E7]
                        ! Coefficients from Wagner and Pruss 1993 (IAPWS 1992)
                    double precision, parameter, dimension(23) :: &
                        saturation_pressures_ref = [&
                            pressure_triple_point, &
                            8.7D0 * 1D-2, &
                            12.6D0 * 1D-2, &
                            17.9D0 * 1D-2, &
                            24.9D0 * 1D-2, &
                            34.1D0 * 1D-2, &
                            45.9D0 * 1D-2, &
                            60.8D0 * 1D-2, &
                            79.6D0 * 1D-2, &
                            103D0 * 1D-2, &
                            131D0 * 1D-2, &
                            165D0 * 1D-2, &
                            207D0 * 1D-2, &
                            256D0 * 1D-2, &
                            313D0 * 1D-2, &
                            381D0 * 1D-2, &
                            460D0 * 1D-2, &
                            552D0 * 1D-2, &
                            655D0 * 1D-2, &
                            774D0 * 1D-2, &
                            909D0 * 1D-2, &
                            1062D0 * 1D-2, &
                            pressure_critical_point &
                            ], &
                        temperatures_ref = [&
                            temperature_triple_point, &
                            200D0, &
                            205D0, &
                            210D0, &
                            215D0, &
                            220D0, &
                            225D0, &
                            230D0, &
                            235D0, &
                            240D0, &
                            245D0, &
                            250D0, &
                            255D0, &
                            260D0, &
                            265D0, &
                            270D0, &
                            275D0, &
                            280D0, &
                            285D0, &
                            290D0, &
                            295D0, &
                            300D0, &
                            temperature_critical_point &
                            ]
                        ! Data from The CRC Handbook of Chemistry and Physics (Lide 2006)
                    integer :: &
                        i  ! index

                    double precision, dimension(1) :: &
                        pressure_interp

                    if (temperature <= temperature_triple_point) then
                        ! Coefficients from Fray and Schmitt 2009
                        ! Not valid below 15 K, but extrapolated anyway
                        nh3_saturation_pressure = 0D0

                        do i = 1, size(a)
                            nh3_saturation_pressure = nh3_saturation_pressure + a(i) / temperature ** (i - 1)
                        end do

                        nh3_saturation_pressure = exp(nh3_saturation_pressure)
                    elseif (temperature <= temperature_critical_point) then
                        pressure_interp = interp(log([temperature]), &
                                                 log(temperatures_ref), log(saturation_pressures_ref))
                        nh3_saturation_pressure = exp(pressure_interp(1))
                    else
                        nh3_saturation_pressure = pressure_critical_point
                    end if
                end function nh3_saturation_pressure
        end function saturation_pressure


        double precision function surface_gravity(mass, equatorial_radius, polar_radius, latitude)
            ! """
            ! Return the surface gravity of an ellipsoid based on its mass and radii, at a given latitude.
            ! """
            use math, only: ellipse_polar_form
            use physical_constants, only: cst_G

            implicit none

            double precision, intent(in) :: latitude, equatorial_radius, mass, polar_radius

            double precision :: &
                r

            r = ellipse_polar_form(equatorial_radius, polar_radius, latitude)

            surface_gravity = mass * cst_G / r**2

            return
        end function surface_gravity


        function vmr2column_number_density(vmr) result(column_number_density)
            ! """
            ! Return the column number density for the given Volume Mixing Ratio in the current atmosphere
            ! """
            use atmosphere, only: dz, n0_standard

            implicit none

            double precision, dimension(:), intent(in) :: vmr
            double precision, dimension(size(vmr)) :: column_number_density

            column_number_density(:) = vmr(:) * n0_standard(:) * dz(:)
        end function vmr2column_number_density


        function column_number_density2vmr(column_number_density) result(vmr)
            ! """
            ! Return the Volume Mixing Ratio for the given column number density in the current atmosphere
            ! """
            use atmosphere, only: dz, n0_standard

            implicit none

            double precision, dimension(:), intent(in) :: column_number_density
            double precision, dimension(size(column_number_density)) :: vmr

            vmr(:) = column_number_density(:) / (n0_standard(:) * dz(:))
        end function column_number_density2vmr


        function cloud_emission(wavenumbers, pressures)
            ! """
            ! Calculate the emission from the cloud layers.
            !
            ! notes:
            !   The cloud emission is needed in the radiative transfer to satisfy Kirchhoff's law.
            !   It represents the reflection of the thermal emission of a level inside a cloud.
            !   Based on: roos-serote et al. 1998
            ! """
            use atmosphere, only: tau, sec_e, temperatures
            use clouds, only: total_transmittance

            implicit none

            double precision, dimension(:), intent(in) :: wavenumbers, pressures
            double precision, dimension(size(wavenumbers), size(pressures)) :: cloud_emission
            integer :: j, k  ! indexes

            cloud_emission(:,size(pressures)) = 0D0

            do j=1,size(wavenumbers)
                do k=1,size(pressures)-1
                    cloud_emission(j,k) = (total_transmittance(k + 1) - total_transmittance(k)) * &
                                          planck_function(wavenumbers(j), temperatures(k)) * exp(-tau(j, k) / sec_e)
                end do
            end do

            return
        end function cloud_emission


        function tau_gas(wavenumbers, pressures)
            ! """
            ! Calculate the molecule-induced optical depth in the atmosphere for given pressures and wavenumbers.
            !
            ! inputs:
            !   wavenumbers: array containing the wavenumbers
            !   pressures: array containing the pressures
            !
            ! output:
            !   tau_gas: array containing the gas-induced optical depth of the atmosphere
            ! """
            use atmosphere, only : absorption_coefficient, column_number_density, absorption_cross_sections
            use molecules, only: n_mol

            implicit none

            double precision, dimension(:), intent(in) :: wavenumbers, pressures
            double precision, dimension(size(wavenumbers), size(pressures)) :: tau_gas  ! output
            double precision, dimension(size(wavenumbers), size(pressures)) :: tau_H2He, tau_mol, tau_cloud
            double precision, dimension(size(wavenumbers), size(pressures), n_mol) :: tau_mols
            integer :: j, k, l  ! indexes

            tau_gas(:, :) = 0D0
            tau_H2He(:, :) = 0D0
            tau_mol(:, :) = 0D0
            tau_cloud(:, :) = 0D0

            absorption_coefficient(:, :, :) = 0D0
            tau_mols(:, :, :) = 0D0

            do j = 1, size(wavenumbers)
                do k = size(pressures), 1, -1
                    do l = 1, n_mol
                        absorption_coefficient(j, k, l) = &
                            absorption_cross_sections(j, k, l) * column_number_density(k, l)

                        tau_mols(j, k, l) = sum(absorption_coefficient(j, k:size(pressures) ,l))
                    end do

                    tau_gas(j, k) = sum(tau_mols(j, k, :))

                    ! Moderate tau_gas value to avoid potential IEEEs
                    if(tau_gas(j, k) > -log(tiny(0.))) then
                        tau_gas(j, k) = -log(tiny(0.))
                    end if
                end do ! k
            end do ! j

            return
        end function tau_gas


        function total_cloud_transmittance(pressures) result(total_transmittance)
            ! """
            ! Calculate the integral of the cloud transmittance for given pressures.
            !
            ! input:
            !   pressures: array containing the pressures
            !
            ! output:
            !   total_transmittance: array containing the total cloud transmittance at the pressures pressures
            ! """
            use atmosphere, only: n_level
            use clouds, only: n_cloud, tau_cloud, p_cloud_base, p_cloud_top, transmittances, cloud_base_levels

            implicit none

            double precision, dimension(:), intent(in) :: pressures
            double precision, dimension(size(pressures)) :: total_transmittance, cloud_transmittance
            integer :: k, n
            double precision :: hscale

            total_transmittance(:) = 1D0

            do n=1,n_cloud
                cloud_transmittance(:) = 1D0

                if(p_cloud_top(n) < p_cloud_base(n)) then
                    hscale = log(p_cloud_top(n) / p_cloud_base(n)) / log(-1D-2 / log(transmittances(n)))
                else
                    hscale = 0D0
                end if

                do k = n_level - 1, 1,-1
                    ! optical depth cloud profile
                    if (k == cloud_base_levels(n)) then
                        tau_cloud(k,n) = - log(transmittances(n))
                    else if (pressures(k) > p_cloud_base(n)) then
                        tau_cloud(k,n) = 0D0
                    else if (pressures(k) > p_cloud_top(n)) then
                        tau_cloud(k,n) = - log(transmittances(n)) * (pressures(k) / p_cloud_base(n))**(1D0/hscale)
                    else
                        tau_cloud(k,n) = 0D0
                    end if

                    cloud_transmittance(k) = cloud_transmittance(k) * cloud_transmittance(k+1) * exp(-tau_cloud(k,n))
                end do

                total_transmittance(:) = total_transmittance(:) * cloud_transmittance(:)
            end do

            return
        end function total_cloud_transmittance


        function planck_function(wavenumber, temperature) result(B)
            ! """
            ! Calculate the Planck function.
            ! The facor 1D2 is for the conversion from m.s-1 to cm.s-1. The factor 1D4 is for the conversion from
            ! W.cm-2.sr-1/cm-1 to W.m-2.sr-1/cm-1.
            ! """
            use physical_constants, only: cst_c, cst_h, cst_k

            implicit none

            double precision, intent(in) :: wavenumber  ! (cm-1) wavenumber
            double precision, intent(in) :: temperature  ! (K) temperature
            double precision             :: B  ! (W.m-2.sr-1/cm-1) radiance in wavenumber of a black body

            B = 2D0 * cst_h * (cst_c * 1D2)**2 * wavenumber**3 / &
                (exp(cst_h * (cst_c * 1D2) * wavenumber / (cst_k * temperature)) - 1D0) * 1D4

            return
        end function planck_function


        subroutine calculate_absorption_cross_sections()
            ! """
            ! Calculate the absorption coeffient at every level in the atmosphere, for every molecule and at every
            ! wavenumber, from parameters taken from the GEISA database.
            ! The parameters from the database need to be adapted for the desired conditions. For line intensities, this
            ! is done by re-calculating probability of states p_i = exp(-E_i / kT) / Q(T) at the right temperatures.
            ! Refers to for example the book of Hanel et al. (2003) for more details.
            ! Air broadening HWHMs are also adapted to the right pressure and temperatures.
            ! The intensities are then broadened following a Voigt function. This gives the cross sections.
            !
            ! important note :
            !   For results with high accuracy, air broadenings HWHM and temperature dependent air broadening HWHM
            !   coefficients need to be adapted to the right air composition before any calculations.
            !
            ! notes:
            !   In absorption spectra E_i = E_low.
            !   Vibrational modes play only a small role in infrared spectra: they do not contribute much more than
            !   10% of the intensity at worst.
            !   Some variables exists only to speed up calculations. The time gain is tiny in most cases, but is
            !   probably significative for very large line files and/or extensives temperature profiles.
            ! """
            use nrt_parameters, only: lines_files
            use math, only: voigt
            use physical_constants, only: cst_c, cst_h, cst_k, cst_n0, cst_R, p_0, T_ref, T_0
            use atmosphere, only: absorption_cross_sections, n_level, pressures, temperatures
            use molecules, only: molecules_name, n_mol, cutoffs, molar_masses, qr_temperature_exponent, vmr_profiles
            use spectrum, only: n_wvn, n_wvn__, wavenumbers__, wavenumber_max, wavenumber_min, wavenumber_step

            implicit none

            double precision, parameter :: &
                HCK = -cst_h * (cst_c * 1D2) / cst_k, &  ! (K.cm) used to speed up calculations
                HCKT_ref = HCK / T_ref                   ! (cm) used to speed up calculations

            integer :: &
                io,             &  ! iostat storage
                j,              &  ! index (wavenumber)
                j_line,         &  ! number of wavenumbers where a line has an influence, depends on cutoff
                j_line_max,     &  ! index (max wavenumber for voigt profile)
                j_line_min,     &  ! index (min wavenumber for voigt profile)
                k,              &  ! index (atmospheric level)
                l                  ! index (molecule)

            double precision :: &
                broad_T_coeff,  &  ! temperature dependence coefficient of the air broadening HWHM
                broadening,     &  ! (cm-1.atm-1) air broadening pressure HWHM at 296K (1 atm = 1.01325 bar)
                collision_width,&  ! (cm-1) collision-induced HWHM of line shape
                doppler_width,  &  ! (cm-1) Doppler-induced half-width at 1/e of line shape
                E_low,          &  ! (cm-1) energy of the lower transition level
                I_ref,          &  ! (cm-1/(molecule.cm-2) intensity of the line at 296K
                intensity,      &  ! (cm-1) intensity of the line
                min_broadening__,& ! (cm-1) parallelized minimal HWHM line broadening
                min_broadening, &  ! (cm-1) minimal HWHM line broadening
                ratio_Qr,       &  ! Qr(T0) / Qr(T); rotational partition function at reference temperature over
                                   ! rotational partition function at atmospheric level temperature
                ratio_Qs,       &  ! Qs(T0) / Qs(T); stimulated emission partition function at reference temperature
                                   ! over stimulated emission partition function at atmospheric level temperature
                ratio_Qv,       &  ! Q_v(T0) / Qv(T); vibrational partition function at reference temperature over
                                   ! vibrational partition function at atmospheric level temperature (WIP)
                wvn_line           ! line wavenumber

            double precision, dimension(2), parameter :: &
                WH2 = [1D0, 3D0]   ! used in H2-H2 CIA calculation, unknown meaning

            double precision, dimension(n_wvn__) :: &
                collision_induced_absorption

            double precision, dimension(n_level) :: &  ! variables used to speed up calculations
                h2_vmr,         &  ! volume mixing ratio of H2
                HCKTT_ref,      &  ! (cm) = -(hc/kT - hc/kT_ref)
                HCKT,           &  ! (cm) = -hc/kT
                p_atm,          &  ! (atm) pressure converted from bar to atm
                speed_rms_c,    &  ! root mean square line-of-sight (1-D) speed of the molecules in the gas over c
                T_ref_T            ! = T_ref / T

            double precision, dimension(n_wvn) :: &
                wvn_voigt           ! = abs(wavenumbers(j)-wvn_line(n))

            ! Initialisation messages
            call MPI_BARRIER(MPI_COMM_WORLD,mpi_ierr)
            call mpi_print('Calculating absorption cross sections...', 0)

            do l = 1, n_mol
                if (trim(lines_files(l)) /= 'None') then
                    write(mpi_msg, '(1X, "Reading lines of species ", A, " in file ''", A, "''")') &
                        trim(molecules_name(l)), trim(lines_files(l))
                    call mpi_print(mpi_msg, 0)
                elseif (trim(molecules_name(l)) == 'H2' .or. trim(molecules_name(l)) == 'He') then
                    write(mpi_msg, '(1X, "Generating cross-sections of species ", A, " from ab-initio calculations")') &
                        trim(molecules_name(l))
                    call mpi_print(mpi_msg, 0)
                else
                    write(mpi_msg, '(1X, "no method to calculate cross-sections of species ", A)') &
                        trim(molecules_name(l)), trim(lines_files(l))
                    call mpi_print_error(mpi_msg, 0)
                end if
            end do

            call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr) ! make a clean terminal output

            ! Pre-calculate useful quantities to speed up calculations
            do k = 1, n_level
                HCKTT_ref(k) = HCK * (1D0 / temperatures(k) - 1D0 / T_ref)
                HCKT(k) = HCK / temperatures(k)
                p_atm(k) = pressures(k) / p_0  ! line parameters from GEISA database are given in atm
                T_ref_T(k) = T_ref / temperatures(k)  ! T_ref = 296 K is the reference temperature for the GEISA databse
            end do

            do l = 1, n_mol
                if (molecules_name(l) == 'H2') then
                    do k = 1, n_level
                        h2_vmr(k) = vmr_profiles(k, l)
                    end do

                    exit
                end if
            end do

            min_broadening__ = huge(0D0)

            ! Calculate cross sections
            do l = 1, n_mol
                print '(1X, "P", I3.3, " calculating absorption cross sections for species ", &
                       &A, " (",I2,"/",I2,")...")', mpi_rank, trim(molecules_name(l)), l, n_mol

                if (molecules_name(l) == 'H2') then
                    do k = 1, n_level
                        call get_h2h2_cia(collision_induced_absorption(:), &
                            wavenumber_min, wavenumber_max, wavenumber_step, temperatures(k))

                        absorption_cross_sections(:, k, l) = &
                            vmr_profiles(k, l) * T_0 / temperatures(k) * p_atm(k) / (cst_n0 * 1D-6) * &
                            collision_induced_absorption(:)
                    end do
                elseif (molecules_name(l) == 'He') then
                    do k = 1, n_level
                        call get_h2he_cia(collision_induced_absorption(:), &
                            wavenumber_min, wavenumber_max, wavenumber_step, temperatures(k))
                        absorption_cross_sections(:, k, l) = &
                            h2_vmr(k) * T_0 / temperatures(k) * p_atm(k) / (cst_n0 * 1D-6) * &
                            collision_induced_absorption(:)
                    end do
                else
                    call read_lines_file()
                end if
            end do

            print '(1X, "P", I3.3, " done")', mpi_rank

            call MPI_ALLREDUCE(min_broadening__ / 2D0, min_broadening, 1, MPI_DOUBLE_PRECISION, MPI_MIN, &
                               MPI_COMM_WORLD, mpi_ierr)

            if (min_broadening < wavenumber_step) then
                write(mpi_msg, '("lowest line HWHM is ", ES15.8, " cm-1, but wavenumber step is ", ES15.8, &
                                 &"; except loss in accuracy")') &
                    min_broadening, wavenumber_step
                call mpi_print_warning(mpi_msg, 0)
            end if

            ! Clean terminal output
            call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)
            call mpi_print('done', 0)

            contains
                subroutine read_lines_file()
                    ! """
                    !
                    ! """
                    implicit none

                    ! Pre-calculate speed_rms_c to speed up calculations
                    do k = 1, n_level
                        ! Leading factor is 2 in 1-D (along the line-of-sight), while it is 3 in 3-D (not relevant here)
                        speed_rms_c(k) = sqrt(2D0 * cst_R * temperatures(k) / (molar_masses(l) * 1D-3)) / cst_c
                    end do

                    open(10, file=lines_files(l), status='old', form='formatted')

                    min_broadening__ = huge(0D0)

                    do
                        ! Read the molecule line file
                        read(10, '(F12.6, 1PD11.4, 0PF6.4, F10.4, T120, F4.2)', iostat=io) &
                            wvn_line, I_ref, broadening, E_low, broad_T_coeff

                        if (wvn_line > wavenumber_max + cutoffs(l) .or. io < 0) then
                            exit
                        else if(wvn_line < wavenumber_min - cutoffs(l)) then
                            cycle
                        end if

                        do k = 1, n_level
                            ! Calculate partition functions
                            ratio_Qr = T_ref_T(k)**qr_temperature_exponent(l)
                            ratio_Qs = (1D0 - exp(wvn_line * HCKT(k))) / (1D0 - exp(wvn_line * HCKT_ref))

                            ratio_Qv = 1D0
                            ! TODO add vibrational bands of molecules in parameters
                            !do i = 1, n_vibrational_band(l)
                            !    ratio_Qv = ratio_Qv * ((1D0 - exp(vib_band_wavenumbers(l, i) * HCKT(k))) / &
                            !               (1D0 - exp(vib_band_wavenumbers(l, i) * HCKT_ref)))**vib_band_degeneracies(l, i)
                            !end do

                            collision_width = broadening * T_ref_T(k)**broad_T_coeff * p_atm(k)
                            doppler_width = wvn_line * speed_rms_c(k)  ! TODO HWHM or 1/e half-width ?

                            if (min_broadening__ > max(collision_width, doppler_width)) then
                                min_broadening__ = max(collision_width, doppler_width)
                            end if

                            ! Calculate intensity for the actual probability of state
                            intensity = I_ref * exp(E_low * HCKTT_ref(k)) * ratio_Qv * ratio_Qr * ratio_Qs

                            ! Determine indexes of wavenumbers where curent line has an influence
                            j_line_max = minloc(abs(wavenumbers__(:) - wvn_line - cutoffs(l)), dim=1)
                            j_line_min = minloc(abs(wavenumbers__(:) - wvn_line + cutoffs(l)), dim=1)
                            j_line = j_line_max - j_line_min + 1

                            ! Determine the wavenumbers passed to the Voigt function (parameter x)
                            do j = 1, j_line
                                wvn_voigt(j) = abs(wavenumbers__(j_line_min + (j - 1)) - wvn_line)
                            end do

                            ! Broad the line
                            absorption_cross_sections(j_line_min:j_line_max, k, l) = &
                                absorption_cross_sections(j_line_min:j_line_max, k, l) + &
                                intensity / doppler_width * &
                                voigt(wvn_voigt(:j_line) / doppler_width, collision_width / doppler_width)
                        end do
                    end do

                    close(10)
                end subroutine read_lines_file

                ! Here be dragons...
                ! Subroutines and functions below this comment originate from version 1996-08-21 (F77) of ab-initio
                ! H2/He CIA calculation program "PROF_BOR.F" by Jacek Borysow and Lothar Frommhold.
                subroutine get_h2h2_cia(abs_H2, wvn0, wvnmax, dwvn, T)
                    !     THIS PROGRAM GENERATES THE H2-H2 TRANSLATIONAL/ROTATIONL
                    !     CIA SPECTRA. IT IS BASED ON QUANTUM LINE SHAPE COMPUTATIONS AND
                    !     THE AB INITIO DIPOLE DATA BE W. MEYER. DIMER FINE STRUCTURES ARE
                    !     SUPPRESSED. THE PROGRAM WAS WRITTEN BY ALEKSANDRA BORYSOW AND
                    !     LOTHAR FROMMHOLD. THIS IS THE NOVEMBER 1985 VERSION

                    !     H2-H2 COMPUTATIONS REFERENCE: MEYER, FROMMHOLD AND BIRNBAUM,
                    !     TO BE PUBLISHED IN PHYS.REV.A IN 1985;
                    !     THE H2-H2 MODELING USED HERE IS BEING PUBLISHED: J.BORYSOW,
                    !     L.TRAFTON, L.FROMMHOLD, G.BIRNBAUM, AP.J. (1985)

                    !     TAPE3 IS OUTPUT: HEADER PLUS ABSORPTION COEFF. ALPHA(NU)
                    Implicit none !double precision (a-h,o-z) ! modified 12/10/2015

                    integer :: IAUX,I,NF
                    double precision, intent(in) :: wvn0,wvnmax,dwvn,T
                    double precision :: NORMAL,x,AUXIL,dnu,E,fnumax,fnumin,S,T1,T2,T3,T4,temp
                    double precision, dimension(:), intent(inout) :: abs_H2
                    double precision, dimension(:), allocatable :: freq, ABSCOEF

                    Common /RESULT1/ NF
                    Common /H2PART1/ AUXIL(5)
                    Common /h2part2/ NORMAL,IAUX

                    normal=0
                    temp=T

                    fnumin=wvn0
                    fnumax=wvnmax
                    dnu=dwvn

                    NF=INT((FNUMAX-FNUMIN)/DNU+0.5)+1

                    allocate(freq(NF), abscoef(NF))

                    FNUMAX=FNUMIN+DFLOAT(NF-1)*DNU
                    call PARTSUM(TEMP)

                    ! THE H2-H2 SPECTRA
                    X=DLOG(TEMP)
                    freq(:) = 0D0
                    abs_H2 = 0D0
                    abscoef(:) = 0D0
                    do I = 1,NF
                        FREQ(I)=FNUMIN+DFLOAT(I-1)*DNU
                        abs_H2(i)=0.d0
                        ABSCOEF(I)=0.
                    end do

                    !  THE LAMBDA1,LAMBDA2,LAMBDA,L = 2023 AND 0223 COMPONENTS:
                    !  (QUADRUPOLE PLUS OVERLAP INDUCED COMPONENT)
                    !	sum of 2023+0223:
                    S=expfoo(X,2.881d-61,-1.1005d0,0.1310d0)
                    E=expfoo(X,7.3485d0,-1.3874d0,0.1660d0)
                    T1=expfoo(X,7.883d-13,-.3652d0,-.0271d0)
                    T2=expfoo(X,3.803d-13,-.4048d0,-.0091d0)
                    T3=expfoo(X,1.0922d-12,-.4810d0,-.0127d0)
                    T4=expfoo(X,5.174d-12,-.9841d0,0.0483d0)

                    call  ADDSPEC1 (S,E,T1,T2,T3,T4,TEMP,1,0,2,2,0,0,FREQ,ABSCOEF)

                    do i = 1,nf
                        abs_H2(i) = abs_H2(i) + abscoef(i)
                    end do
                    !print *,sum(abs_H2)

                    !     PARAMETERS FOR 4045 AND 0445 (PURE HEXADECAPOLE) COMPONENTS
                    S=expfoo(X,2.404d-64,-1.4161d0,0.1847d0)
                    E=expfoo(X,-.8033d0,-.4474d0,-.0235d0)
                    T1=expfoo(X,3.873d-13,-.4226d0,-.0183d0)
                    T2=expfoo(X,2.743d-13,-.3566d0,-.0140d0)
                    T3=expfoo(X,4.171d-13,-.5223d0,0.0097d0)
                    T4=expfoo(X,2.2725d-12,-1.1056d0,0.0139d0)

                    call ADDSPEC1(S,E,T1,T2,T3,T4,TEMP,1,4,0,4,0,0,FREQ,ABSCOEF)

                    do i = 1,nf
                        abs_H2(i) = abs_H2(i) + abscoef(i)
                    end do

                    !     PARAMETERS FOR 0221 AND 2021 (PURE OVERLAP) COMPONENTS
                    S=expfoo(X,6.393d-63,-1.5964d0,0.2359d0)
                    E=expfoo(X,21.414d0,-1.2511d0,0.1178d0)
                    T1=expfoo(X,1.876d-13,-.4615d0,-.0012d0)
                    T2=expfoo(X,4.839d-13,-.5158d0,0.0075d0)
                    T3=expfoo(X,4.550d-13,-.5507d0,0.0095d0)
                    T4=expfoo(X,2.045d-12,-.5266d0,-.0240d0)

                    call  ADDSPEC1(S,E,T1,T2,T3,T4,TEMP,1,0,2,2,0,0,FREQ,ABSCOEF)

                    do i = 1,nf
                        abs_H2(i) = abs_H2(i) + abscoef(i)
                    end do

                    !     PARAMETERS FOR 2233 QUADRUPOLE INDUCED doUBLE TRANSITIONS
                    S=expfoo(X,5.965d-63,-1.0394d0,0.1184d0)
                    E=expfoo(X,6.674d0    ,-.9459d0,0.1124d0)
                    T1=expfoo(X,4.764d-13,-.1725d0,-.0450d0)
                    T2=expfoo(X,4.016d-13,-.3802d0,-.0134d0)
                    T3=expfoo(X,1.0752d-12,-.4617d0,-.0085d0)
                    T4=expfoo(X,1.1405d-11,-1.2991d0,0.0729d0)

                    call ADDSPEC1(S,E,T1,T2,T3,T4,TEMP,1,2,2,3,0,0,FREQ,ABSCOEF)
                    do i = 1,nf
                        abs_H2(i) = abs_H2(i) + abscoef(i)
                    end do

                    Return
                end subroutine get_h2h2_cia

                subroutine get_h2he_cia(abs_He, wvn0, wvnmax, dwvn, T)

                    !     THIS PROGRAM GENERATES THE H2-He ROTATIONAL/TRANSLATIONAL
                    !     CIA SPECTRA. IT IS BASED ON QUANTUM LINE SHAPE COMPUTATIONS AND
                    !     THE AB INITIO DIPOLE DATA BY W. MEYER.

                    !     File out.h2he IS OUTPUT: HEADER PLUS ABSORPTION COEFF. ALPHA(NU)

                    implicit none

                    integer :: I, ifun, nf
                    double precision, intent(in) :: wvn0,wvnmax,dwvn,T
                    double precision :: x, xm0, xm1, xm2, t1, t2
                    double precision, dimension(:), intent(inout) :: abs_He
                    double precision, dimension(:), allocatable :: freq, abscoef
                    COMMON /RESULT1/ NF

                    NF=INT((wvnmax-wvn0)/dwvn+0.5)+1

                    allocate(freq(NF), abscoef(NF))

                    !wvnmax=wvn0+DFLOAT(NF-1)*dwvn
                    if(T <= 40.d0 .OR. T > 3000.d0) stop 4444

                    call PARTSUM(T)

                    !     THE H2-He SPECTRA: 40K -- 3000K
                    X=DLOG(T)
                    do I=1,NF
                        FREQ(I)=wvn0+DFLOAT(I-1)*dwvn
                        abs_He(i)=0.d0
                        ABSCOEF(I)=0.d0
                    end do

                    !     THE LAMBDA,L = 23 COMPONENT:
                    !     (QUADRUPOLE PLUS OVERLAP INDUCED COMPONENT)
                    XM0=exppoly3(X,.262d-62, -0.71411d0, 0.14587d0, -0.00352d0 )
                    XM1=exppoly3(X, 0.269d-49, -0.98315d0, 0.21988d0, -0.00729d0)
                    XM2=exppoly3(X, 0.406d-35, -2.25664d0, 0.50098d0, -0.01925d0)

                    call BCparam(T, XM0,XM1,XM2, t1, t2)

                    ifun = 1

                    call  ADDSPEC2 (XM0,T1,T2,T,0,-1,-1,2,0,0,ifun,FREQ,ABSCOEF)

                    do i=1, nf
                        abs_He(i) = abs_He(i) + abscoef(i)
                    end do

                    !     PARAMETERS FOR 21 (PURE OVERLAP) COMPONENT
                    XM0=exppoly3(X, 0.424d-64, -0.37075d0, 0.17473d0, -0.00489d0)
                    XM1=exppoly3(X, 0.174d-49, -1.89232d0, 0.44399d0, -0.02029d0)
                    XM2=exppoly3(X, 0.874d-35, -3.27717d0, 0.69166d0, -0.02865d0)

                    call K0param(T, XM0, XM1, XM2, t1, t2)

                    ifun = 0

                    call  ADDSPEC2(XM0,T1,T2,T,0,-1,-1,2,0,0,ifun,FREQ,ABSCOEF)

                    do i=1, nf
                        abs_He(i) = abs_He(i) + abscoef(i)
                    end do

                    !     PARAMETERS FOR 01 (PURE OVERLAP) COMPONENT
                    XM0=exppoly3(X, 0.223d-61, -1.89198d0, 0.45505d0, -0.02238d0)
                    XM1=exppoly3(X, 0.625d-48, -1.96486d0, 0.47059d0, -0.02402d0)
                    XM2=exppoly3(X, 0.316d-33, -3.40400d0, 0.72793d0, -0.03277d0)
                    !S=XM0

                    call K0param(T, XM0, XM1, XM2, t1, t2)

                    ifun = 0

                    call  ADDSPEC2(XM0, T1,T2,T,0,-1,-1,0,0,0, ifun,FREQ,ABSCOEF)

                    do i=1, nf
                        abs_He(i) = abs_He(i) + abscoef(i)
                    end do

                    return
                end subroutine get_h2he_cia

                function EH2(N,I)
                    implicit none

                    integer, intent(in) :: N,I
                    double precision :: EH2

                    EH2 = 4395.34*(DFLOAT(N)+.5)-117.905*(DFLOAT(N)+.5)**2 &
                    +(60.809-2.993*(DFLOAT(N)+.5)+.025*(DFLOAT(N)+.5)**2)* &
                    DFLOAT(I) - (.04648-.00134*(DFLOAT(N)+.5))*DFLOAT(I*I)
                    return
                end function EH2

                function PH2(J,T)
                    implicit none

                    integer, intent(in) :: J
                    double precision, intent(in) :: T
                    double precision :: PH2

                    PH2 = DFLOAT(2*J+1)*WH2(1+MOD(J,2)) * DEXP(-1.4387859/T*EH2(0,J*(J+1)))
                    return
                end function PH2

                function exppoly3(x,a,b,c,d)
                    implicit none

                    double precision, intent(in) :: x,a,b,c,d
                    double precision :: exppoly3

                    exppoly3 = A*EXP(B*X + C*X**2 + D*X**3)
                    return
                end function exppoly3

                function expfoo(x,a,b,c)
                    implicit none

                    double precision, intent(in) :: x,a,b,c
                    double precision :: expfoo

                    expfoo = A*DEXP((C*X+B)*X)
                    return
                end function expfoo

                function BGAMA2 (FNU,T1,T2,TEMP,ifun)
                    implicit none

                    integer, intent(in) :: ifun
                    double precision, intent(in) :: fnu,T1,T2,TEMP
                    double precision :: bgama2

                    bgama2 = 0D0
                    if(ifun == 0) bgama2=bgama0(fnu,t1,t2,temp)
                    if(ifun == 1) bgama2=bgama1(fnu,t1,t2,temp)
                    RETURN
                end function BGAMA2

                function BGAMA0(FNU,TAU5,TAU6,TEMP)
                !     K0 LINE SHAPE MODEL
                !     NORMALIZATION SUCH THAT 0TH MOMENT EQUALS UNITY
                !     FNU IS THE FREQUENCY IN CM-1; TEMP IN KELVIN.
                    implicit none

                    double precision, intent(in) :: fnu,tau5,tau6,temp
                    double precision :: bgama0
                    double precision, parameter :: &
                        TWOPIC = 1.883651568D11, &
                        BKW = 0.6950304256D0, &
                        HBOK = 7.638280918D-12, &
                        PI = 3.141592654D0

                    double precision :: tau4, omega,xnom,x,tau56

                    TAU4=dSQRT(TAU5*TAU5+(HBOK/(TEMP*2.d0))**2)
                    OMEGA=TWOPIC*FNU
                    XNOM=1.d0/(TAU6*TAU6)+OMEGA*OMEGA
                    X=TAU4*DSQRT(XNOM)
                    TAU56=TAU5/TAU6
                    TAU56=DMIN1(TAU56,430.d0)
                    BGAMA0=(TAU5/PI)*DEXP(TAU56+FNU/(2.d0*BKW*TEMP))* &
                    XK0(X)
                    RETURN
                end function BGAMA0

                function BGAMA1(FNU,TAU1,TAU2,TEMP)
                !     BIRNBAUM S CIA LINE SHAPE MODEL (K1)
                !     NORMALIZATION SUCH THAT 0TH MOMENT EQUALS UNITY
                !     FNU IS THE FREQUENCY IN CM-1; TEMP IN KELVIN.
                    implicit none

                    double precision, intent(in) :: fnu,tau1,tau2,temp
                    double precision :: bgama1
                    double precision, parameter :: &
                        TWOPIC = 1.883651568D11, &
                        BKW = 0.6950304256D0, &
                        HBOK = 7.638280918D-12, &
                        PI = 3.141592654D0
                    double precision :: tau3, omega,denom,x,AAA

                    TAU3=dSQRT(TAU2*TAU2+(HBOK/(TEMP*2.))**2)
                    OMEGA=TWOPIC*FNU
                    DENOM=1.d0+(OMEGA*TAU1)**2
                    X=(TAU3/TAU1)*dSQRT(DENOM)
                    AAA=TAU2/TAU1
                    AAA=dMIN1(AAA,430.d0)
                    BGAMA1=(TAU1/PI)*dEXP(AAA+FNU/(2.d0*BKW*TEMP))* &
                    XK1(X)/DENOM
                    RETURN
                end function BGAMA1

                function XK0(X)
                !     MODIFIED BESSEL function K0(X)
                !     ABRAMOWITZ AND STEGUN P.379
                    implicit none

                    double precision, intent(inout) :: X
                    double precision:: xk0
                    double precision :: T,FI0,P
                    IF(X-2.d0 <= 0) then
                        T=(X/3.75d0)**2
                        FI0=(((((.0045813*T+.0360768)*T+.2659732)*T &
                        +1.2067492)*T+3.0899424)*T+3.5156229)*T+1.
                        T=(X/2.)**2
                        P=(((((.00000740*T+.00010750)*T+.00262698)*T &
                        +.03488590)*T+.23069756)*T+.42278420)*T+ &
                        (-.57721566)
                        X=DABS(X)
                        XK0=-DLOG(X/2.)*FI0+P
                        RETURN
                    else
                        T=(2./X)
                        P=(((((.00053208*T-.00251540)*T+.00587872)*T &
                        -.01062446)*T+.02189568)*T-.07832358)*T+ &
                        &  1.25331414
                        X=DMIN1(X,330.d0)
                        XK0=DEXP(-X)*P/DSQRT(X)
                        RETURN
                    end if
                end function XK0

                function XK1(X)
                !     MODIFIED BESSEL function K1(X) TIMES X
                !     PRECISION IS BETTER THAN 2.2e-7 EVERYWHERE.
                !     ABRAMOWITZ AND S,TEGUN, P.379; TABLES P.417.
                    implicit none

                    double precision, intent(inout) :: X
                    double precision :: xk1
                    double precision :: T,FI1,P

                        IF(X-2. <= 0) then
                        T=(X/3.75)**2
                        FI1=X*((((((.00032411*T+.00301532)*T+.02658733)*T+.15084934) &
                        *T+.51498869)*T+.87890594)*T+.5)
                        T=(X/2.)**2
                        P=(((((-.00004686*T-.00110404)*T-.01919402)*T-.18156897)*T- &
                        .67278579)*T+.15443144)*T+1.
                        XK1=X*dLOG(X/2)*FI1+P
                        RETURN
                    else
                        T=2./X
                        P=(((((-.00068245*T+.00325614)*T-.00780353)*T+.01504268)*T- &
                        .03655620)*T+.23498619)*T+1.25331414
                        X=dMIN1(X,330.d0)
                        XK1=dSQRT(X)*dEXP(-X)*P
                        RETURN
                    end if
                end function XK1

                subroutine ADDSPEC1(G0,EP,TAU1,TAU2,TAU5,TAU6,TEMP,LIKE,lambda1,LAMBDA2,LAMBDA,NVIB1,NVIB2,FREQ,ABSCOEF)
                    !     THIS PROGRAM GENERATES A LISTING OF THE CIA TR ALFA(OMEGA)
                    !     IF BOTH LAMBDA1 AND LAMBDA2 ARE NEGATIVE: SINGLE TRANSITIONS;
                    !     doUBLE TRANSITIONS ARE ASSUMED OTHERWISE.
                    !     MP=1 GIVES LISTINGS OF INTERMEDIATE RESULTS.
                    !     LIKE=1 FOR LIKE SYSTEMS (AS H2-H2), SET LIKE=0 ELSE.
                    Implicit none!double precision (a-h,o-z) ! modified 08/10/2015

                    integer :: i,j,nf ! added 08/10/2015
                    integer :: LIKE,lambda1,LAMBDA2,LAMBDA,LIST,NVIB1,NVIB2,JP,J1,JP1,J2,JP2,&
                                             JPLUSL,JRANGE1,I1,I2,IP,IP1,IP2,IQ ! added 08/10/2015
                    doUBLE PRECISION :: G0,EP,TAU1,TAU2,TAU5,TAU6,BETA,B0,&
                                        BOLTZWN,CALIB,CG1S,CG2S,CLIGHT,CLOSCHM,D0,FAC,&
                                        FRQ,HBAR,CGS,&
                                        NORMAL,OMEGA1,&
                                        OMEGA2,P,P1,P2,PI,Q,TWOPIC,WH2,WKF,&
                                        WKI,XBG,TEMP ! added 08/10/2015
                    double precision, dimension(:) :: FREQ,ABSCOEF

                    Common /H2PART1/ Q,WH2(2),B0,D0
                    Common /h2part2/ NORMAL,JRANGE1
                    Common /RESULT1/ NF

                    DATA CLOSCHM,BOLTZWN/2.68675484E19,.6950304/
                    DATA HBAR,PI,CLIGHT/1.054588757d-27,3.1415926535898,2.9979250E10/

                    do i = 1,nf
                        abscoef(i) = 0.d0
                    end do

                    TWOPIC=2.*PI*CLIGHT

                    CALIB = TWOPIC*((4.*PI**2)/(3.*HBAR*CLIGHT))*CLOSCHM**2
                    CALIB = CALIB/DFLOAT(1+LIKE)
                    BETA = 1./(BOLTZWN*TEMP)
                    LIST = NF

                    !     ROTATIONAL SPECTRUM FOR THE DETAILED LISTING   *******************
                    IF ((LAMBDA1 < 0) .AND. (LAMBDA2 < 0)) GO TO 60
                    JPLUSL=JRANGE1+MAX0(LAMBDA1,LAMBDA2)
                    do I1=1,JRANGE1
                        J1=I1-1
                        do IP1=1,JPLUSL
                            JP1=IP1-1
                            CG1S=CLEBSQR(J1,LAMBDA1,JP1)

                            IF (CG1S <= 0) then
                                cycle
                            else
                                P1=PH2(J1,TEMP)/Q
                            end if
                            IF (P1 < 0.001) cycle
                            OMEGA1=EH2(NVIB1,JP1*IP1)-EH2(0,J1*I1)
                            do I2=1,JRANGE1
                                J2=I2-1
                                do IP2=1,JPLUSL
                                    JP2=IP2-1
                                    CG2S=CLEBSQR(J2,LAMBDA2,JP2)
                                    IF (CG2S <= 0) then
                                        cycle
                                    else
                                        P2=PH2(J2,TEMP)/Q
                                    end if
                                    IF (P2 < 0.001) cycle
                                    OMEGA2=EH2(NVIB2,JP2*IP2)-EH2(0,J2*I2)
                                    FAC=CALIB*P1*P2*CG1S*CG2S
                                    do I=1,LIST
                                        FRQ=FREQ(I)-OMEGA1-OMEGA2
                                        WKI=FREQ(I)*(1.-DEXP(-BETA*FREQ(I)))
                                        WKF=WKI*FAC
                                        XBG=G0*BGAMA(FRQ,TAU1,TAU2,EP,TAU5,TAU6,TEMP)
                                        ABSCOEF(I)=ABSCOEF(I)+XBG*WKF
                                    end do
                                end do
                            end do
                        end do
                    end do
                    GO TO 100
                    60 JPLUSL=JRANGE1+LAMBDA
                    do I=1,JRANGE1
                        J=I-1
                        do IP=1,JPLUSL
                            JP=IP-1
                            CGS=CLEBSQR(J,LAMBDA,JP)
                            IF (CGS <= 0) then
                                cycle
                            else
                                P=PH2(J,TEMP)/Q
                            end if
                            IF (P < 0.001) cycle
                            OMEGA1=EH2(NVIB1,JP*IP)-EH2(0,J*I)
                            FAC=CALIB*P*CGS
                            do IQ=1,LIST
                                FRQ=FREQ(IQ)-OMEGA1
                                WKI=FREQ(IQ)*(1.-DEXP(-BETA*FREQ(IQ)))
                                WKF=WKI*FAC
                                XBG=G0*BGAMA(FRQ,TAU1,TAU2,EP,TAU5,TAU6,TEMP)
                                ABSCOEF(IQ)=ABSCOEF(IQ)+XBG*WKF
                            end do
                        end do
                    end do
                    100 CONTINUE
                    RETURN

                end subroutine ADDSPEC1

                function CLEBSQR (L,LAMBDA,LP)

                !     SQUARE OF CLEBSCH-GORDAN COEFFICIENT (L,LAMBDA,0,0;LP,0)
                !     FOR integer ARGUMENTS ONLY
                !     NOTE THAT LAMBDA SHOULD BE SMALL, MAYBE @10 OR SO.

                    implicit none !double precision (a-h,o-z) ! modified 08/10/2015

                    integer, intent(in) :: L,LAMBDA,LP
                    integer :: I,I0,I1
                    doUBLE PRECISION :: CLEBSQR,FC,F,P
                    FC=DFLOAT(2*LP+1)

                    CLEBSQR=0.
                    IF (((L+LAMBDA) < LP) .OR. ((LAMBDA+LP) < L) .OR. ((L+LP) < LAMBDA)) RETURN
                    IF (MOD(L+LP+LAMBDA,2) /= 0) RETURN
                    IF ((L < 0) .OR. (LP < 0) .OR. (LAMBDA < 0)) RETURN
                    F=1./DFLOAT(L+LP+1-LAMBDA)
                    IF (LAMBDA /= 0) then
                        I1=(L+LP+LAMBDA)/2
                        I0=(L+LP-LAMBDA)/2+1
                        do I=I0,I1
                            F=F*DFLOAT(I)/DFLOAT(2*(2*I+1))
                        end do
                    end if
                    P=FC*F*FCTL(LAMBDA+L-LP)*FCTL(LAMBDA+LP-L)
                    CLEBSQR=P/(FCTL((LAMBDA+L-LP)/2)*FCTL((LAMBDA+LP-L)/2))**2
                    RETURN

                end function CLEBSQR

                function FCTL (N)
                    implicit none !double precision (a-h,o-z) ! modified 08/10/2015
                    integer :: N,I,J
                    doUBLE PRECISION :: FCTL,P,Z

                    FCTL=1.
                    IF (N <= 1) RETURN
                    if (N <= 15) then
                        J=1
                        do I=2,N
                            J=J*I
                        end do
                        FCTL=DFLOAT(J)
                        RETURN
                    else
                        Z=DFLOAT(N+1)

                        P=((((-2.294720936d-4)/Z-(2.681327160d-3))/Z+(3.472222222d-3))/ &
                        Z+(8.333333333d-2))/Z+1.
                        FCTL=(DEXP(-Z)*(Z**(Z-0.5))*P*2.5066282746310)
                        RETURN
                    end if
                    end function FCTL

                function BGAMA(FNU,T1,T2,EPS,T3,T4,TEMP)

                !     EQUATION 13, SO-callED EBC MODEL, OF BORYSOW,TRAFTON,FROMMHOLD,
                !     AND BIRNBAUM, ASTROPHYS.J., TO BE PUBLISHED (1985)
                !     NOTE THAT BGAMA REDUCES TO THE BC PROFILE FOR EPS=0.

                    implicit none !double precision (a-h,o-z) ! modified 08/10/2015
                    double precision :: K0
                    doUBLE PRECISION :: BGAMA,FNU,T1,T2,EPS,T3,T4,TEMP,OMEGA,T0,Z,XK1,ZP,&
                                        BOLTZ,HBAR,PI,CLIGHT,&
                                        P1,P2,P3,P4,P5,P6,X

                    DATA PI,CLIGHT/3.1415926535898,2.99792458E10/
                    DATA HBAR,BOLTZ/1.0545887d-27,1.380662d-16/

                    OMEGA=2.*PI*CLIGHT*FNU
                    T0=HBAR/(2.*BOLTZ*TEMP)
                    Z=SQRT((1.+(OMEGA*T1)**2)*(T2*T2+T0*T0))/T1

                    if (Z-2D0 <= 0) then
                        X = (Z/3.75D0)**2
                        P3 = (((((( 0.00032411D0*X+0.00301532D0)*X+0.02658733D0)*X+&
                             0.15084934D0)*X+0.51498869D0)*X+0.87890594D0)*X+0.5D0)
                        X = (Z/2D0)**2
                        P4 = ((((((-0.00004686D0*X-0.00110404D0)*X-0.01919402D0)*X-&
                             0.18156897D0)*X-0.67278579D0)*X+0.15443144D0)*X+1D0)
                        XK1=Z*Z*DLOG(Z/2D0)*P3+P4
                    else
                        X = 2D0/Z
                        P6 = ((((((-0.00068245D0*X+0.00325614D0)*X-0.00780353D0)*X+&
                             0.01504268D0)*X-0.03655620D0)*X+0.23498619D0)*X+&
                        1.25331414D0)
                        XK1=SQRT(Z)*DEXP(-Z)*P6
                    end if

                    ZP=SQRT((1.+(OMEGA*T4)**2)*(T3*T3+T0*T0))/T4
                    if (ZP-2D0 <= 0) then
                        X = (ZP/3.75)**2
                        P1 = ((((((0.00458130D0*X+0.03607680D0)*X+0.26597320D0)*X+&
                             1.20674920D0)*X+3.08994240D0)*X+3.51562290D0)*X+1D0)
                        X = (ZP/2.)**2
                        P2 = ((((((0.00000740D0*X+0.00010750D0)*X+0.00262698D0)*X+&
                             0.03488590D0)*X+0.23069756D0)*X+0.42278420D0)*X-0.57721566D0)
                        K0= -LOG(ZP/2D0)*P1+P2
                    else
                        X = 2D0/ZP
                        P5 = ((((((0.00053208D0*X-0.00251540D0)*X+0.00587872D0)*X-&
                             0.01062446D0)*X+0.02189568D0)*X-0.07832358D0)*X+1.25331414D0)
                        K0= EXP(-ZP)*P5/SQRT(ZP)
                    end if

                    BGAMA=((T1/PI)*DEXP(T2/T1+T0*OMEGA)* &
                    XK1/(1.+(T1*OMEGA)**2)+EPS*(T3/ &
                    PI)*DEXP(T3/T4+T0*OMEGA)*K0)/(1.+EPS)
                    RETURN

                    end function BGAMA

                subroutine PARTSUM (TEMP)

                !     H2 ROTATIONAL PARTITION SUM Q = Q(T).

                    implicit none !double precision (a-h,o-z) ! modified 12/10/2015
                    integer :: JRANGE1,J
                    double precision :: NORMAL,TEMP,B0,D0,DS,DQ,Q,S,SEV,SODD,WH2
                    COMMON /H2PART1/ Q,WH2(2),B0,D0
                    common /h2part2/ NORMAL,JRANGE1

                !     NORMAL=0 INITIATES EQUILIBRIUM HYDROGEN;
                !     NORMAL=1 ADJUSTS FOR ORTHO/PARA HYDROGEN RATIO OF 3:1
                    B0 = 59.3392
                    D0 = 0.04599
                    WH2 = (/1.,3./)
                    !DATA B0,D0,WH2(1),WH2(2)/59.3392,0.04599,1.,3./
                    !EH2(N,I)=4395.34*(DFLOAT(N)+.5)-117.905*(DFLOAT(N)+.5)**2 &
                    !+(60.809-2.993*(DFLOAT(N)+.5)+.025*(DFLOAT(N)+.5)**2)* &
                    !DFLOAT(I) - (.04648-.00134*(DFLOAT(N)+.5))*DFLOAT(I*I)
                    !PH2(J,T)=DFLOAT(2*J+1)*WH2(1+MOD(J,2))*DEXP(-1.4387859*EH2(0,J*(J+1))/T)

                !     Q,B0,D0,WH2 - PARTITION FCT., ROT.CONSTANTS, WEIGHTS FOR H2

                    Q=0.
                    J=0
                    10 DQ=PH2(J,TEMP)
                    Q=Q+DQ
                    J=J+1
                    IF (DQ > Q/900.) GO TO 10
                    JRANGE1=J
                    IF (NORMAL <= 0) then
                        RETURN
                    else
                        S=0.
                        SEV=0.
                        do j=0,jrange1,2
                            DS=PH2(J,TEMP)
                            S=S+DS
                            SEV=SEV+DS
                            S=S+PH2(J+1,TEMP)
                        end do
                        SODD=S-SEV
                        WH2(2)=WH2(2)*3.*SEV/SODD
                        Q=4.*SEV
                        RETURN
                    end if

                end subroutine PARTSUM

                subroutine ADDSPEC2(G0,TAU1,TAU2,TEMP,LIKE,LAMBDA1,LAMBDA2,LAMBDA,NVIB1,NVIB2,ifun,FREQ,ABSCOEF)

                !     THIS PROGRAM GENERATES A LISTING OF THE CIA TR ALFA(OMEGA)
                !     IF BOTH LAMBDA1 AND LAMBDA2 ARE NEGATIVE: SINGLE TRANSITIONS;
                !     doUBLE TRANSITIONS ARE ASSUMED OTHERWISE.
                !     MP=1 GIVES LISTINGS OF INTERMEDIATE RESULTS.
                !     LIKE=1 FOR LIKE SYSTEMS (AS H2-H2), SET LIKE=0 ELSE.

                    implicit none !double precision (a-h,o-z) ! modified 12/10/2015

                    integer :: JRANGE1,I1,I2,IP,IP1,IP2,IQ,J1,J2,JP,JP1,JP2,JPLUSL,LIST,NF,I,J,&
                                LIKE,LAMBDA1,LAMBDA2,LAMBDA,NVIB1,NVIB2,ifun
                    double precision :: NORMAL,G0,TAU1,TAU2,TEMP,&
                                        B0,BETA,BOLTZWN,CALIB,CG1S,CG2S,CGS,CLIGHT,CLOSCHM,D0,FAC,FRQ,&
                                        HBAR,OMEGA1,OMEGA2,P,P1,P2,PI,Q,WH2,WKF,WKI,XBG,TWOPIC
                    double precision, dimension(:) :: FREQ,ABSCOEF
                    COMMON /H2PART1/ Q,WH2(2),B0,D0
                    COMMON /H2PART2/ NORMAL,JRANGE1
                    COMMON /RESULT1/ NF
                    DATA CLOSCHM,BOLTZWN/2.68675484E19,.6950304/
                    DATA HBAR,PI,CLIGHT/1.054588757d-27,3.1415926535898,2.9979250E10/

                    do i=1, nf
                        abscoef(i) = 0.d0
                    end do

                    TWOPIC=2.*PI*CLIGHT
                !      IF (MP.NE.1) MP=0
                !      IF (LIKE.NE.1) LIKE=0
                    CALIB=TWOPIC*((4.*PI**2)/(3.*HBAR*CLIGHT))*CLOSCHM**2
                    CALIB=CALIB/DFLOAT(1+LIKE)
                    BETA=1./(BOLTZWN*TEMP)
                    LIST=NF

                !     ROTATIONAL SPECTRUM FOR THE DETAILED LISTING   *******************

                    IF ((LAMBDA1 < 0) .AND. (LAMBDA2 < 0)) GO TO 60
                    JPLUSL=JRANGE1+MAX0(LAMBDA1,LAMBDA2)
                    do I1=1,JRANGE1
                        J1=I1-1
                        do IP1=1,JPLUSL
                            JP1=IP1-1
                            CG1S=CLEBSQR(J1,LAMBDA1,JP1)
                            IF (CG1S <= 0) then
                                cycle
                            else
                                P1=PH2(J1,TEMP)/Q
                                IF (P1 < 0.001) cycle
                                OMEGA1=EH2(NVIB1,JP1*IP1)-EH2(0,J1*I1)
                                do I2=1,JRANGE1
                                    J2=I2-1
                                    do IP2=1,JPLUSL
                                        JP2=IP2-1
                                        CG2S=CLEBSQR(J2,LAMBDA2,JP2)
                                        IF (CG2S <= 0) then
                                            cycle
                                        else
                                            P2=PH2(J2,TEMP)/Q
                                            IF (P2 < 0.001) cycle
                                            OMEGA2=EH2(NVIB2,JP2*IP2)-EH2(0,J2*I2)
                                            FAC=CALIB*P1*P2*CG1S*CG2S
                                            do I=1,LIST
                                                FRQ=FREQ(I)-OMEGA1-OMEGA2
                                                WKI=FREQ(I)*(1.-DEXP(-BETA*FREQ(I)))
                                                WKF=WKI*FAC
                                                XBG=G0*BGAMA2(FRQ,TAU1,TAU2,TEMP,ifun)
                                                ABSCOEF(I)=ABSCOEF(I)+XBG*WKF
                                            end do
                                        end if
                                    end do
                                end do
                            end if
                        end do
                    end do
                    GO TO 100
                !	Single transitions here:
                    60 JPLUSL=JRANGE1+LAMBDA
                    do I=1,JRANGE1
                        J=I-1
                        do IP=1,JPLUSL
                            JP=IP-1
                            CGS=CLEBSQR(J,LAMBDA,JP)
                            IF (CGS <= 0) then
                                cycle
                            else
                                P=PH2(J,TEMP)/Q
                                IF (P < 0.001) cycle
                                OMEGA1=EH2(NVIB1,JP*IP)-EH2(0,J*I)
                                FAC=CALIB*P*CGS
                                do IQ=1,LIST
                                    FRQ=FREQ(IQ)-OMEGA1
                                    WKI=FREQ(IQ)*(1.-DEXP(-BETA*FREQ(IQ)))
                                    WKF=WKI*FAC
                                    XBG=G0*BGAMA2(FRQ,TAU1,TAU2,TEMP,ifun)
                                    ABSCOEF(IQ)=ABSCOEF(IQ)+XBG*WKF
                                end do
                            end if
                        end do
                    end do
                    100 CONTINUE
                    RETURN

                end subroutine ADDSPEC2

                subroutine BCparam(T, G0, G1, G2, tau1, tau2)
                    implicit none

                    double precision, intent(in) :: T,G0,G1,G2
                    double precision, intent(out) :: tau1,tau2
                    double precision :: tau0,tta
                    double precision, parameter :: hbar=1.05458875D-27, BOLTZK=1.38054D-16
                !	G0,G1,G2 are tree lowest translational moments

                    TAU0=HBAR/(2.*BOLTZK*T)
                    TTA=G0*TAU0/G1
                    TAU1=DSQRT((G2*TTA-G0*(1.+TAU0**2/TTA))/(G0*(TAU0/TTA)**2))
                    TAU2=TTA/TAU1
                    return
                end subroutine BCparam

                subroutine K0param(T, G0,G1,G2, tau1, tau2)
                    implicit none

                    double precision, intent(in) :: T,G0,G1,G2
                    double precision, intent(out) :: tau1,tau2
                    double precision :: tau0,tau10,delt
                    double precision, parameter :: hbar=1.05458875D-27, BOLTZK=1.38054D-16
                !	G0,G1,G2 are tree lowest translational moments
                    TAU0=HBAR/(2.*BOLTZK*T)
                    DELT=(TAU0*G1)**2 -4.*(G1*G1/G0+G1/TAU0-G2)*TAU0*TAU0*G0
                    if(delt <= 0.d0) go to 88
                    TAU1=(-DSQRT(DELT)-TAU0*G1)/(2.*(G1*G1/G0+G1/TAU0-G2))
                    if(tau1 <= 0.d0) go to 88
                    TAU1=DSQRT(TAU1)
                    TAU2=TAU0*TAU1*G0/(G1*TAU1*TAU1-TAU0*G0)
                    return
                    88 write (6,177) delt, tau10
                    177 format(' Problem: one of the following is negative for K0, ', &
                    /,' delt, tau1:', 2e13.4)
                    stop 9955
                end subroutine K0param
        end subroutine calculate_absorption_cross_sections


        subroutine init_atmosphere()
            ! """
            ! Simulate the atmosphere for the given parameters.
            ! Calculate the molecular column number density, needed to calculate the optical depths.
            ! Initlialise the clouds.
            ! """
            use math, only: interp, sec
            use physical_constants, only: cst_n0, cst_R, p_0, T_0
            use atmosphere, only: column_number_density, dz, &
                n_level, n0_standard, sec_e, sec_sol, pressures, temperatures
            use molecules, only: molecules_name, n_mol, molar_masses, vmr_profiles
            use target, only: emission_angle, incidence_angle, latitude, target_equatorial_radius, target_mass, &
                target_polar_radius

            implicit none

            integer :: &
                k,          &  ! counter (atmospheric level)
                l              ! counter (molecule)

            double precision :: &
                gravity,      &  ! (m.s-2) surface gravity of the target
                H_scale,      &  ! scale heights in hydrostatic equilibrium,
                H_scale_up,   &  ! used for the calculation of dz
                H_scale_down, &  ! used for the calculation of dz
                Rgm,          &  ! 100 * cst_R/( gravity * M_atm ), used to calculate gas scale height in hydrostatic eq.
                M_atm            ! mean molar mass of the atmosphere (kg)

            call mpi_print('Initializing atmosphere...', 0)

            ! Check atmospheric angles
            if (emission_angle >= 90) then
                write(mpi_msg, '("emission angle (", F5.2, ") must be lower than 90 degrees")') &
                    emission_angle
                call mpi_print_error(mpi_msg, 0)
            end if

            if (incidence_angle >= 90) then
                write(mpi_msg, '("incidence angle (", F5.2, ") must be lower than 90 degrees")') &
                    incidence_angle
                call mpi_print_error(mpi_msg, 0)
            end if

            ! Calculate the secant of atmospheric angles
            sec_e = sec(emission_angle)
            sec_sol = sec(incidence_angle)

            ! Calculate gravity
            gravity = surface_gravity(target_mass, target_equatorial_radius, target_polar_radius, latitude)

            do  k = 1, n_level
                ! Calculate mean molar mass of the atmosphere (kg.mol-1)
                M_atm = 0D0

                do l = 1, size(molecules_name(:))  ! take into account *all* molecules, even continuum molecules
                    M_atm = M_atm + molar_masses(l) * 1D-3 * vmr_profiles(k, l)
                end do

                ! Calculate altitude differences between levels (hydrostatic equilibrium)
                Rgm = 100D0 * cst_R / (gravity * M_atm) ! (cm) Atreya et al. 1999

                if (k == 1) then
                    H_scale = Rgm * (temperatures(k) + temperatures(k + 1)) / 2D0
                    dz(k) = H_scale * log(pressures(k) / pressures(k + 1))
                else if (k == n_level) then
                    H_scale = Rgm * (temperatures(k) + temperatures(k - 1)) / 2D0
                    dz(k) = H_scale * log(pressures(k - 1) / pressures(k))
                else
                    H_scale_up = Rgm * (temperatures(k) + temperatures(k + 1)) / 2D0
                    H_scale_down = Rgm * (temperatures(k) + temperatures(k - 1)) / 2D0

                    dz(k) = (H_scale_up * log(pressures(k) / pressures(k + 1)) + &
                            H_scale_down * log(pressures(k - 1) / pressures(k))) / 2D0
                end if

                ! Calculate the density of particles at the pressure and temperature of the atmospheric level
                ! The Loschmidt constant is in particles.m-3, the factor 1D-6 is for the conversion to particles.cm-3
                ! N.B. the Loschmidt constant is given at 0 degrees Celsius and at 1 atm (hence the factors T_0 and p_0)
                n0_standard(k) = cst_n0 * 1D-6 * T_0 / temperatures(k) * pressures(k) / p_0
            end do

            ! Calculate the column number density
            do l = 1, n_mol  ! take into account only the non-continuum molecules
                do k = 1, n_level
                    column_number_density(k, l) = vmr_profiles(k, l) * n0_standard(k) * dz(k)
                end do
            end do

            call mpi_print('Adding clouds...', 0)
            call add_clouds

            contains
                subroutine add_clouds()
                    ! """
                    ! Calculate cloud pressure and temperatures.
                    ! """
                    use clouds

                    implicit none

                    integer :: n

                    ! Check clouds
                    do n = 1, n_cloud
                        ! Check pressures
                        if (p_cloud_base(n) > pressures(1) .or. p_cloud_base(n) < pressures(n_level)) then
                            write(mpi_msg, '("pressure level of cloud ", I2, " (",F6.2,") is out of pressure range (", &
                                            & F6.2, "--", F6.2, ")")') &
                                n, p_cloud_base(n), pressures(1), pressures(n_level)
                            call mpi_print_error(mpi_msg, 0)
                        end if

                        if(p_cloud_base(n) < p_cloud_top(n)) then
                            write(mpi_msg, '("pressure at base of cloud ", I2, " (", F6.2," bar) &
                                             &is above pressure at its top (", F6.2," bar)")') &
                                n, p_cloud_base(n), p_cloud_top(n)
                            call mpi_print_error(mpi_msg, 0)
                        end if

                        ! Check transmittances
                        if (transmittances(n) < 0 .or. transmittances(n) > 1) then
                            write(mpi_msg, '("cloud", I2, "transmittance must be between 0 and 1  (", F4.2,")")') &
                                n, transmittances(n)
                            call mpi_print_error(mpi_msg, 0)
                        end if

                        ! Check reflectances
                        if (reflectances(n) < 0 .or. reflectances(n) > 1) then
                            write(mpi_msg, '("cloud", I2, "maximum reflectance must be between 0 and 1 (", F4.2,")")') &
                                n, transmittances(n)
                            call mpi_print_error(mpi_msg, 0)
                        end if
                    end do

                    ! Find cloud level
                    cloud_base_levels(:) = 1
                    cloud_top_levels(:) = n_level

                    do n = 1, n_cloud
                        do k = 1, n_level
                            if (pressures(k) <= p_cloud_base(n)) then
                                if (abs(pressures(k) - p_cloud_base(n)) <= abs(pressures(k-1) - p_cloud_base(n))) then
                                    cloud_base_levels(n) = k
                                else
                                    cloud_base_levels(n) = k - 1
                                end if

                                exit
                            end if
                        end do

                        do k = 1, n_level-1
                            if (pressures(k) <= p_cloud_top(n)) then
                                if (abs(pressures(k) - p_cloud_top(n)) <= abs(pressures(k - 1) - p_cloud_top(n))) then
                                    cloud_top_levels(n) = k
                                else
                                    cloud_top_levels(n) = k - 1
                                end if

                                exit
                            end if
                        end do
                    end do

                    do n = 1, n_cloud
                        T_cloud(n) = temperatures(cloud_base_levels(n))
                        p_cloud_base(n) = pressures(cloud_base_levels(n))

                        if (p_cloud_base(n) < pressures(cloud_base_levels(n)) * (1D0 - 1D-6) .or. &
                            p_cloud_base(n) > pressures(cloud_base_levels(n)) * (1D0 + 1D-6)) then

                            write(mpi_msg, '("Cloud ", I2, " base anchored from ", F9.5, " bar to ", F9.5, " bar")') &
                                n, p_cloud_base(n), pressures(cloud_base_levels(n))
                            call mpi_print_info(mpi_msg, 0)
                        end if
                    end do

                    ! Display atmosphere parameters
                    if (mpi_rank == 0) then
                        print '("Atmosphere parameters:")'
                        print '(2X, "Surface gravity (m.s-2): ", F0.2)', gravity
                        print '(T11, "p(bar)", T21, "T(K)", T31, "Trans", T41, "Refl", T51, "level")'
                        print '(2X, "Top", T11, ES9.3, T21, F9.3, T31, "-", T41, "-", T51, I5)', &
                            pressures(n_level), temperatures(n_level), n_level
                        do n = 1, n_cloud
                            if (transmittances(n) < 1 .or. reflectances(n) > 0) then
                                print '(2X, "Cloud", I2, T11, ES9.3, T21, F9.3, T31, F5.3, T41, F5.3, T51, I5)', &
                                        n, p_cloud_base(n), T_cloud(n), transmittances(n), reflectances(n), &
                                        cloud_base_levels(n)
                            end if
                        end do
                        print '(2X, "Bottom", T11, ES9.3, T21, F9.3, T31, "-", T41, "-", T51, I5)', &
                            pressures(1), temperatures(1), 1
                    end if

                end subroutine add_clouds
        end subroutine init_atmosphere


        subroutine calculate_radiance()
            ! """
            ! Calculate the radiance in wavenumber of the spectra in the wavenumber range.
            ! It is a wrap for the radiative transfer function.
            ! May be useful in the future to calculate different radiative transfer depending on the situation (i.e.
            ! nadir vs limb, etc.)
            ! """
            use spectrum, only: wavenumbers__, radiances__, vmr_derivative__, cloud_t_derivative__
            use nrt_parameters, only: convolve

            implicit none

            call radiative_transfer(wavenumbers__(:), radiances__(:), vmr_derivative__(:, :, :), &
                                    cloud_t_derivative__(:, :, :))

            if (convolve) then
                call instrument_convolution(radiances__(:), vmr_derivative__(:, :, :), cloud_t_derivative__(:, :, :))
            end if

        end subroutine calculate_radiance


        subroutine calculate_interpolated_radiance()
            ! """
            ! Calculate the radiance in wavenumber of the spectra in the wavenumber range.
            ! It is a wrap for the radiative transfer function.
            ! May be useful in the future to calculate different radiative transfer depending on the situation (i.e.
            ! nadir vs limb, etc.)
            ! """
            use atmosphere, only: n_level
            use molecules, only: n_mol
            use clouds, only: n_cloud
            use spectrum, only: wavenumbers__, radiances__, vmr_derivative__, cloud_t_derivative__, &
                                wavenumbers, radiances, &
                                vmr_derivative__, vmr_derivative, &
                                cloud_t_derivative__, cloud_t_derivative, &
                                radiances_interp, vmr_derivative_interp, cloud_t_derivative_interp
            use retrieval, only: wavenumbers_ret, wavenumbers_conv, n_wvn_conv__, &
                                 n_wvn_conv
            use math, only: interp, mean_restep

            implicit none

            integer :: &
                k, &
                l, &
                n

            double precision, dimension(n_wvn_conv__) :: &
                radiances_conv__  ! Parallelised radiance to convolve

            double precision, dimension(n_wvn_conv) :: &
                radiances_conv  ! Radiance to convolve

            double precision, dimension(n_wvn_conv__, n_level, n_cloud) :: &
                cloud_t_derivative_conv__   ! interpolation of radiance cloud transmittance derivative

            double precision, dimension(n_wvn_conv, n_level, n_cloud) :: &
                cloud_t_derivative_conv   ! interpolation of radiance cloud transmittance derivative

            double precision, dimension(n_wvn_conv__, n_level, n_mol) :: &
                vmr_derivative_conv__          ! interpolation of radiance VMR derivative

            double precision, dimension(n_wvn_conv, n_level, n_mol) :: &
                vmr_derivative_conv          ! interpolation of radiance VMR derivative

            call radiative_transfer(wavenumbers__(:), radiances__(:), vmr_derivative__(:, :, :), &
                                    cloud_t_derivative__(:, :, :))

            call mpi_allgatherv1D(radiances__(:), radiances(:))
            call mpi_allgatherv3D(vmr_derivative__(:, :, :), vmr_derivative(:, :, :))
            call mpi_allgatherv3D(cloud_t_derivative__(:, :, :), cloud_t_derivative(:, :, :))

            radiances_conv(:) = mean_restep(wavenumbers_conv(:), wavenumbers(:), radiances(:))

            do k = 1, n_level
                do l = 1, n_mol
                    vmr_derivative_conv(:, k, l) = mean_restep(&
                        wavenumbers_conv(:), wavenumbers(:), vmr_derivative(:, k, l)&
                    )
                end do
                do n = 1, n_cloud
                    cloud_t_derivative_conv(:, k, n) = mean_restep(&
                        wavenumbers_conv(:), wavenumbers(:), cloud_t_derivative(:, k, n)&
                    )
                end do
            end do

            call mpi_scatterv1D(radiances_conv(:), radiances_conv__(:), 0)
            call mpi_scatterv3D(vmr_derivative_conv(:,:,:), vmr_derivative_conv__(:,:,:), 0)
            call mpi_scatterv3D(cloud_t_derivative_conv(:,:,:), cloud_t_derivative_conv__(:,:,:), 0)

            call instrument_convolution(radiances_conv__(:), vmr_derivative_conv__(:, :, :), &
                                        cloud_t_derivative_conv__(:, :, :))

            call mpi_allgatherv1D(radiances_conv__(:), radiances_conv(:))
            call mpi_allgatherv3D(vmr_derivative_conv__(:, :, :), vmr_derivative_conv(:, :, :))
            call mpi_allgatherv3D(cloud_t_derivative_conv__(:, :, :), cloud_t_derivative_conv(:, :, :))

            radiances_interp(:) = interp(wavenumbers_ret(:), wavenumbers_conv(:), radiances_conv(:))

            do k = 1, n_level
                do l = 1, n_mol
                    vmr_derivative_interp(:,k,l) = interp(&
                        wavenumbers_ret(:), wavenumbers_conv(:), vmr_derivative_conv(:, k, l)&
                    )
                end do

                do n = 1, n_cloud
                    cloud_t_derivative_interp(:,k,n) = interp(&
                        wavenumbers_ret(:), wavenumbers_conv(:), cloud_t_derivative_conv(:, k, n)&
                    )
                end do
            end do
        end subroutine calculate_interpolated_radiance


        subroutine instrument_convolution(radiances, vmr_derivative, cloud_t_derivative)
            ! """
            ! Convolve radiances, VMR derivative and cloud transmittance derivative over wavenumbers.
            ! Wavenumbers size can be different than radiances, etc, size. In that case the outputs are interpolated
            ! over their new wavenumbers.
            !
            ! input:
            !   wavenumbers: wavenumbers at which the convolution will be performed
            !   radiances: spectral radiance in wavenumber of the spectrum
            !   vmr_derivative: derivative of the radiance with respect to the logarithm of the VMR of each molecule
            !   cloud_t_derivative: derivative of the radiance with respect to the cloud tranmittance
            !   convolved_wavenumbers: wavenumbers at which to convolve the inputs
            !
            ! outputs:
            !   convolved_radiances: radiances convolved over wavenumbers
            !   convolved_vmr_derivative: VMR derivative convolved over wavenumbers
            !   convolved_cloud_t_derivative: cloud transmittance derivative convolved over wavenumbers
            ! """
            use atmosphere, only: n_level
            use clouds, only: n_cloud
            use instrument, only: instrument_lsf, n_lsf
            use math, only: interp, slide_convolve
            use molecules, only: is_retrieved, n_mol
            use mpi_utils, only: mpi_fftconvolve
            use nrt_parameters, only: output_derivatives, retrieve
            use retrieval, only: n_wvn_conv

            implicit none

            double precision, dimension(:), intent(inout) :: radiances
            double precision, dimension(size(radiances), n_level, n_mol), intent(inout) :: vmr_derivative
            double precision, dimension(size(radiances), n_level, n_cloud), intent(inout) :: cloud_t_derivative

            integer :: &
                k, &
                l, &
                n

            double precision, dimension(:), allocatable :: &
                rads

            double precision, dimension(:, :, :), allocatable :: &
                dvmr, &
                dcld

            call mpi_print('Radiance convolution...', 0)

            if (n_lsf == 1) then
                radiances(:) = mpi_fftconvolve(radiances(:), instrument_lsf(:, 1))

                do k = 1, n_level
                    do l = 1, n_mol
                        if (is_retrieved(l) .or. output_derivatives) then
                            vmr_derivative(:, k, l) = mpi_fftconvolve(vmr_derivative(:, k, l), &
                                                                      instrument_lsf(:, 1))
                        end if
                    end do

                    do n = 1, n_cloud
                        cloud_t_derivative(:, k ,n) = mpi_fftconvolve(cloud_t_derivative(:, k, n), &
                                                                      instrument_lsf(:, 1))
                    end do
                end do
            else
                allocate(rads(n_wvn_conv), dvmr(n_wvn_conv, n_level, n_mol), dcld(n_wvn_conv, n_level, n_cloud))

                call mpi_allgatherv1D(radiances(:), rads(:))
                call mpi_allgatherv3D(vmr_derivative(:, :, :), dvmr(:, :, :))
                call mpi_allgatherv3D(cloud_t_derivative(:, : , :), dcld(:, :, :))

                rads(:) = slide_convolve(rads(:), instrument_lsf(:, :))

                if(retrieve .or. output_derivatives) then
                    do k = 1, n_level
                        do l = 1, n_mol
                            if (is_retrieved(l) .or. output_derivatives) then
                                dvmr(:, k, l) = slide_convolve(dvmr(:, k, l), instrument_lsf(:, :))
                            end if
                        end do

                        do n = 1, n_cloud
                            dcld(:, k ,n) = slide_convolve(dcld(:, k, n), instrument_lsf(:, :))
                        end do
                    end do
                end if

                call mpi_scatterv1D(rads(:), radiances(:), 0)
                call mpi_scatterv3D(dvmr(:, :, :), vmr_derivative(:, :, :), 0)
                call mpi_scatterv3D(dcld(:, :, :), cloud_t_derivative(:, : , :), 0)
            end if

        end subroutine instrument_convolution


        subroutine radiative_transfer(wavenumbers, radiances, vmr_derivative, cloud_t_derivative)
            ! """
            ! Calculate the radiative transfer integral over the whole atmosphere, assuming Local Thermodynamic
            ! Equilibrium.
            ! The contribution of the molecules, of the clouds, and of the reflected sunlight are taken into account
            !
            ! input:
            !   wavenumbers: wavenumbers at which the spectrum radiances are calculated
            !
            ! outputs:
            !   radiances: spectral radiance in wavenumber of the spectrum
            !   vmr_derivative: derivative of the radiance with respect to the logarithm of the VMR of each molecule
            !   cloud_t_derivative: derivative of the radiance with respect to the cloud tranmittance
            !
            ! notes:
            !   This radiative transfer is validated for nardir observations. Limb observations may obey different laws.
            !   Satisfactory results were obtained with one monlayer grey cloud. More complex clouds are yet to be
            !   tested
            !   The atmosphere is assumed to be isothermal below the lowest level and above the highest.
            !   Motsly based on Roos-Serote et al. 1998
            ! """
            use mpi_utils, only: mpi_fftconvolve
            use atmosphere, only: n_level, pressures, temperatures, tau, sec_e, sec_sol, absorption_coefficient
            use molecules, only: n_mol
            use clouds, only: n_cloud, total_transmittance, transmittances, reflectances, cloud_base_levels, tau_cloud
            use spectrum, only: n_wvn__, t_sky__
            use target, only: light_source_effective_temperature, light_source_radius, light_source_range

            implicit none

            double precision, dimension(:), intent(in) :: wavenumbers
            double precision, dimension(size(wavenumbers)), intent(inout) :: radiances
            double precision, dimension(size(wavenumbers), n_level, n_mol), intent(inout) :: vmr_derivative
            double precision, dimension(size(wavenumbers), n_level, n_cloud), intent(inout) :: cloud_t_derivative

            integer :: &
                j, &
                k, &
                l, &
                n  ! indexes

            double precision :: &
                L_sol, &  ! (W.m-2.sr-1/cm-1) radiance illuminating the object
                L_reflected  ! (W.m-2.sr-1/cm-1) radiance reflected by the object

            double precision, dimension(n_level) :: &
                d_thermal_vmr_derivative  ! thermal radiance derivative with respect to the VMR logarithm divided by
                                          ! variations of molecule-induced optical depth in function of atmospheric
                                          ! levels (absorption_coefficient)

            double precision, dimension(n_level,n_cloud) :: &
                cloud_cloud_t_derivative,   &  ! cloud emission derivative with respect to the cloud transmittance
                thermal_cloud_t_derivative     ! thermal radiance derivative with respect to the cloud transmittance

            double precision, dimension(n_wvn__,n_level) :: &
                L_thermal, &  ! (W.m-2.sr-1/cm-1) thermal radiance
                L_cloud       ! (W.m-2.sr-1/cm-1) cloud radiance

            call mpi_print('Calculating radiance...', 0)

            ! Initialization
            total_transmittance(:) = total_cloud_transmittance(pressures(:))
            tau(:,:) = tau_gas(wavenumbers(:), pressures(:))

            L_cloud(:,:) = cloud_emission(wavenumbers(:), pressures(:))
            d_thermal_vmr_derivative(:) = 0D0
            thermal_cloud_t_derivative(:,:) = 0D0
            cloud_cloud_t_derivative(:,:) = 0D0
            vmr_derivative(:,:,:) = 0D0
            cloud_t_derivative(:,:,:) = 0D0

            ! Main loop (parallelised wavenumbers)
            do j = 1, n_wvn__
                ! Reflection
                L_sol = planck_function(wavenumbers(j), light_source_effective_temperature) * &
                    (light_source_radius / light_source_range)**2  ! # TODO stellar spectrum as input
                L_reflected = min(reflectances(1), 1 - transmittances(1)) * L_sol * &
                              exp(-tau(j, cloud_base_levels(1)) * sec_sol) * &
                              exp(-tau(j, cloud_base_levels(1)) * sec_e)

                ! Thermal contribution
                do k = 1, n_level - 1
                    L_thermal(j, k) = total_transmittance(k) * &
                                      planck_function(wavenumbers(j), temperatures(k)) * &
                                      (exp(-tau(j,k + 1) * sec_e) - exp(-tau(j,k) * sec_e))
                end do

                L_thermal(j, n_level) = total_transmittance(n_level) * &
                                        planck_function(wavenumbers(j), temperatures(n_level)) * &
                                        (1D0 - exp(-tau(j, n_level) * sec_e))

                ! Total radiance
                radiances(j) = sum(L_thermal(j , :)) + sum(L_cloud(j, :)) + L_reflected

                ! Derivatives
                d_thermal_vmr_derivative(1) = total_transmittance(1) * sec_e * &
                                              (planck_function(wavenumbers(j), temperatures(1)) * &
                                              exp(-tau(j, 1) * sec_e))
                do k = 2, n_level - 1  ! derivatives are = 0 at lowest and highest level
                    ! VMR derivative
                    d_thermal_vmr_derivative(k) = total_transmittance(k) * sec_e * &
                                                ((planck_function(wavenumbers(j), temperatures(k)) - &
                                                  planck_function(wavenumbers(j), temperatures(k - 1))) * &
                                                 exp(-tau(j, k) * sec_e))

                    do l = 1, n_mol
                        if (k < cloud_base_levels(1)) then
                            vmr_derivative(j, k, l) = absorption_coefficient(j, k, l) * &
                                (sum(d_thermal_vmr_derivative(:k)) - sec_e * sum(L_cloud(j, :k)))  ! cloud VMR deriv
                        else  ! reflection takes place only above the highest cloud
                            vmr_derivative(j, k, l) = absorption_coefficient(j, k, l) * &
                                                      (sum(d_thermal_vmr_derivative(:k)) - &
                                                       sec_e * sum(L_cloud(j, :k)) - &  ! cloud VMR derivative
                                                       (sec_e + sec_sol) * L_reflected)  ! reflection VMR deriv.
                        end if
                    end do

                    ! Cloud transmittance derivative
                    do n = 1, n_cloud
                        thermal_cloud_t_derivative(k, n) = sum(L_thermal(j, :k)) / exp(-tau_cloud(k, n))
                        cloud_cloud_t_derivative(k, n) = 1D0 / exp(-tau_cloud(k, n)) * (sum(L_cloud(j, :k-1)) - &
                                                         total_transmittance(k) * &
                                                         planck_function(wavenumbers(j), temperatures(k)) * &
                                                         exp(-tau(j, k) * sec_e))

                        if (exp(-tau_cloud(k, n)) < 1D0) then
                            cloud_t_derivative(j, k, n) = thermal_cloud_t_derivative(k, n) + &
                                                          cloud_cloud_t_derivative(k, n)
                            if(n == 1 .and. k >= cloud_base_levels(1)) then  ! reflection effect
                                cloud_t_derivative(j, k, n) = cloud_t_derivative(j, k, n) - &
                                                              transmittances(1) / exp(-tau_cloud(k, n)) * &
                                                              L_sol * exp(-tau(j,cloud_base_levels(1)) * sec_sol) * &
                                                              exp(-tau(j,cloud_base_levels(1)) * sec_e)
                            end if
                        end if
                    end do
                end do

                ! Sky effect (sky should be deconvolved)
                radiances(j) = t_sky__(j) * radiances(j)
                vmr_derivative(j, :, :) = t_sky__(j) * vmr_derivative(j, :, :)
                cloud_t_derivative(j, :, :) = t_sky__(j) * cloud_t_derivative(j, :, :)
            end do
        end subroutine radiative_transfer
end module nrt_physics
