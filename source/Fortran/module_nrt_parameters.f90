! """
! Contains the parameters common to at least two subroutines of NRT.
! """
module atmosphere
    ! """
    ! Contains the atmopsheric parameters.
    ! """
    implicit none

    integer :: &
        n_level            ! number of atmospheric levels

    double precision :: &
        sec_e,                  &  ! secant (1/cos) of the emission angle
        sec_sol                    ! secant (1/cos) of the light incident form the sun

    double precision, dimension (:), allocatable :: &
        pressures,              &  ! pressure (bar)
        temperatures               ! temperature (K)

    double precision, dimension (:), allocatable :: &
        dz,                     &  ! (cm) distance between atmospheric levels in hydrostatic case
        n0_standard                ! (particles.cm-3) number density of particles of a standard gas (see Loschmidt cst.)

    double precision, dimension(:, :), allocatable :: &
        column_number_density,  &  ! column number density of each atmospheric levels and each molecules
        tau                        ! optical depth for every level in the atmosphere and every wavenumbers

    double precision, dimension(:, :, :), allocatable :: &
        absorption_cross_sections, &  ! (cm2.particles-1) molecular attenuation cross section
        absorption_coefficient        ! spectral molecular column attenuation coefficient for each atmospheric level

    save
end module atmosphere


module clouds
    ! """
    ! Contains the cloud-related parameters.
    ! """
    implicit none

    integer :: &
        n_cloud                    ! number of cloud

    integer, dimension(:), allocatable :: &
        cloud_base_levels,      &  ! level of the base of cloud n
        cloud_top_levels           ! level of the top of cloud n

    double precision, dimension(:), allocatable :: &
        p_cloud_base,           &  ! pressure of the base of cloud n
        p_cloud_top,            &  ! pressure of the top of cloud n
        reflectances,           &  ! reflectance at the base of cloud n
        T_cloud,                &  ! temperature at the base of cloud n
        transmittances             ! transmittance at the base of cloud n

    double precision, dimension(:), allocatable :: &
        total_transmittance        ! integral of the transmittance above one level

    double precision, dimension(:,:), allocatable :: &
        tau_cloud,              &  ! cloud optical depth
        transmittances_err         ! Error on the transmittance of each cloud at each level

    save

    contains
        subroutine allocate_cloud_parameters
            !"""
            ! Allocate and initialise most of the cloud paramters.
            !"""
            implicit none

            allocate(cloud_base_levels(n_cloud), cloud_top_levels(n_cloud), p_cloud_base(n_cloud), &
                     p_cloud_top(n_cloud), &
                     transmittances(n_cloud), reflectances(n_cloud), T_cloud(n_cloud))

            cloud_base_levels(:) = 1
            cloud_top_levels(:) = 1
            p_cloud_base(:) = 0D0
            p_cloud_top(:) = 0D0
            transmittances(:) = 1D0
            reflectances(:) = 0D0
            T_cloud(:) = 0D0
        end subroutine allocate_cloud_parameters
end module clouds


module instrument
    ! """
    ! Contains instrument-related parameters
    ! """
    implicit none

    character(len=120) :: &
        lsf_shape  ! shape of the instrument Line Shape Function

    integer :: &
        n_lsf, &   ! number of Line Spread Functions of the instrument
        n_wvn_lsf  ! number of wavenumbers in the instrument Line Shape Function

    double precision :: &
        lsf_fwhm_ref ! reference Full Widht Half Maximums of the instrument Line Shape Function

    double precision, dimension(:), allocatable :: &
        lsf_fwhm, &  ! Full Widht Half Maximums of the instrument Line Shape Function
        lsf_wvn      ! wavenumbers of the instrument Line Shape Function

    double precision, dimension(:, :), allocatable :: &
        instrument_lsf  ! instrument Light Spread Functions

    save

    contains
        subroutine allocate_instrument_parameters()
            implicit none

            allocate(lsf_fwhm(n_lsf))

            lsf_fwhm(:) = 0D0
        end subroutine allocate_instrument_parameters
end module instrument


module molecules
    ! """
    ! Contains the molecular parameters.
    ! """
    implicit none

    integer, parameter :: &
        n_vibrational_band_max = 8, &  ! maximum number of Qv bands for a molecule
        molecule_name_size = 32        ! maximum number of character in molecule name
                                       ! (longest known chemical formula, aka "Tintin", is 31 characters long)

    logical, dimension(:), allocatable :: &
        is_retrieved                   ! if True, the molecule vmr is retrieved

    character(len=molecule_name_size), dimension(:), allocatable :: &
        molecules_name                 ! name of the molecules

    integer :: &
        n_mol                          ! number of molecules

    integer, dimension(:), allocatable :: &
        n_vibrational_band,         &  ! number of vibrational bands of the molecule
        vmr_sorted_index               ! index of any molecule parameter sorted by descending mean vmr

    integer, dimension(:, :), allocatable :: &
        vib_band_degeneracies          ! degeneracies of a vibrational band

    double precision, dimension(:), allocatable :: &
        cutoffs,                    &  ! (cm-1) sublorentzian cutoff of the molecule
        molar_masses,               &  ! (g.mol-1) molar mass of the molecule
        molecules_vmr,              &  ! mean volume mixing ratio of the molecule
        qr_temperature_exponent,    &  ! rotational partition temperature exponent of the molecule
        vmr_factors                    ! molecule abundance factor modifier

    double precision, dimension(:, :), allocatable :: &
        vib_band_wavenumbers           ! (cm-1) wavenumber of a Qv band

    double precision, dimension(:, :), allocatable :: &
        vmr_profiles,               &  ! vertical molecular volume mixing ratio profiles
        vmr_profiles_err               ! error on vertical molecular volume mixing ratio profiles

    save

    contains
        subroutine allocate_molecular_parameters
            implicit none

            allocate(is_retrieved(n_mol), molecules_name(n_mol), n_vibrational_band(n_mol), &
                     vmr_sorted_index(n_mol), vmr_factors(n_mol), cutoffs(n_mol), &
                     qr_temperature_exponent(n_mol), molecules_vmr(n_mol), molar_masses(n_mol))
            allocate(&
                vib_band_degeneracies(n_mol, n_vibrational_band_max), &
                vib_band_wavenumbers(n_mol, n_vibrational_band_max) &
            )

            is_retrieved(:) = .False.
            molecules_name(:) = ''
            n_vibrational_band(:) = 0
            vmr_sorted_index(:) = 1
            vmr_factors(:) = 0D0
            cutoffs(:) = 0D0
            qr_temperature_exponent(:) = 0D0
            molecules_vmr(:) = 0D0
            molar_masses(:) = 0D0
            vib_band_degeneracies(: , :) = 0
            vib_band_wavenumbers(:, :) = 0D0
        end subroutine allocate_molecular_parameters

end module molecules


module nrt_parameters
    ! """
    ! Contains the flags, paths and names needed in nrt.
    ! Most of these parameters are read from the input parameter file.
    ! """
    implicit none

    integer, parameter :: &
        file_name_size = 256  ! maximum number of character in file name

    character(len=file_name_size), parameter :: &
        input_file = '../inputs/input_parameters.nml' ! contains all the params for nrt

    logical :: &
        calculate_cross_sections,       &  ! if True, enforce the calculation of the molecular absorption cross sections
        convolve,                       &  ! if True, convolve the spectrum
        output_derivatives,             &  ! if True, output the derivatives into files
        retrieve,                       &  ! if True, launch the retrieving of the observed spectrum
        write_cross_sections               ! if True, enforce the writing of the molecular absorption X-sections files

    character(len=file_name_size) :: &
        path_absorption_cross_sections, &  ! path of the continuum column absorption coefficients
        path_cross_sections,            &  ! path of the molecular absorption cross sections
        path_instrument,                &  ! path of the instrument data file
        path_lines,                     &  ! path of the line files
        path_molecular_parameters,      &  ! path of the molecular paramters files
        path_noise,                     &  ! path of the observed radiance noise file
        path_retrieved_spectrum,        &  ! path of the observed spectrum file
        path_sky,                       &  ! path of the sky file
        path_temperature_profile,       &  ! path to the temperature profile
        path_vmr,                       &  ! path of the molecular abundance profile files

        path_spectrum_out,              &  ! path of the spectrum file
        path_vmr_out,                   &  ! path of the outputted  abundance profiles

        cloud_t_derivative_file,        &  ! file containing the radiance derivative with respect to the cloud trans
        log_file,                       &  ! file containing informations on spec_name (out)
        instrument_file,                &  ! file containing the instrument Line Spread Functions Full Width Half Max
        noise_file,                     &  ! file containing the sky transmittance
        output_spectrum_name,           &  ! file containing the radiances
        retrieved_spectrum,             &  ! file containing the observed radiances
        sky_file,                       &  ! file containing the sky transmittance
        temperature_profile_file,       &  ! file containing the pressure-temperature atmospheric profile
        vmr_derivative_file,            &  ! file containing drad/dln(abd)
        vmr_file_out                       ! file containing the outputted abundance profiles

    character(len=file_name_size), dimension(:), allocatable :: &
        absorption_cross_sections_files, &  ! files containing the absorption coefficients of every molecule
        lines_files,                     &  ! files containing the lines parameters of every molecule
        molecule_parameters_files,       &  ! files containing the physical parameters of every molecule
        vmr_files                           ! files containing the abundances of every molecule

    save
end module nrt_parameters


module physical_constants
    ! """
    ! Contains all the physical constants needed in nrt.
    ! """
    implicit none

    double precision, parameter :: &
        cst_c = 2.99792458D+8 , &  ! (m.s-1) light velocity [exact]
        cst_G = 6.67408D-11,    &  ! (m3.kg-1.s-2) newtonian constant of gravitation [2014 CODATA]
        cst_h = 6.62607004D-34, &  ! (J.s) planck constant [2014 CODATA]
        cst_k = 1.38064852D-23, &  ! (J.K-1) boltzmann constant [2014 CODATA]
        cst_n0 = 2.6867811D+25, &  ! (particles.m-3) Loschmidt constant at 273.15 K and 1.01325E5 Pa [2014 CODATA]
        cst_R = 8.3144598D0,    &  ! (J.mol-1.K-1) molar gas constant [2014 CODATA]

        p_0 = 1.01325D0,        &  ! (bar) Loschmidt reference pressure: 1 atm (also reference pressure for GEISA 2015)
        T_0 = 273.15D0,         &  ! (K) Loschmidt reference temperature: 0 degrees Celsius
        T_ref = 296D0              ! (K) GEISA 2015 database reference temperature

    save
end module physical_constants


module retrieval
    ! """
    ! Contains the retrieving parameters.
    ! """
    implicit none

    integer :: &
        n_wvn_ret,          &  ! number of wavenumbers in the observed spectrum
        n_wvn_ret__,        &  ! number of retrieved wavenumbers in 1 process
        n_wvn_conv,      &
        n_wvn_conv__,    &
        iteration_max          ! maximum number of retreival iterations

    integer,dimension(:),allocatable :: &
        displs_j_ret, &        ! store all the displacements of in retrieved wavenumbers
        displs_j_low_res, &        ! store all the displacements of in retrieved wavenumbers
        n_wvn_ret_x,  &        ! number of retrieved wavenumbers in each processes
        n_wvn_conv_x

    double precision :: &
        chi2diff_threshold, &  ! if the difference of chi2 between two retrievings if below this, stop to iterate
        doppler_shift,      &  ! the doppler shift of the retrieved spectrum
        v_smooth,           &  ! (height scale) vertical smoothness of the retrieved parameters
        weight_apriori         ! weight of the a priori (low weight means small variations)

    double precision, dimension(:), allocatable :: &
        noise_radiances,    &  ! noise equivalent radiance of the retrieved spectrum
        radiances_ret,      &  ! radiance array of the observed spectrum
        wavenumbers_ret,    &  ! wavenumber array of the observed spectrum
        wavenumbers_ret__,  &
        wavenumbers_conv,&
        wavenumbers_conv__

    save
end module retrieval


module spectrum
    ! """
    ! Contains the spectrum parameters.
    ! """
    implicit none

    integer :: &
        n_wvn,                     &  ! total number of wavenumbers in the spectrum
        n_wvn__,                   &  ! number of wavenumbers in the spectra for one process
        n_wvn_H2He                    ! number of wavenumbers at which H2-He opacity is calculated

    integer, dimension(:), allocatable :: &
        displs_j,                  &  ! displacement relative to radiances at which to place radiances__ from process i
        n_wvn_x                       ! number of wavenumbers (n_wvn__) of each process

    double precision :: &
        convolution_step,          &  ! (cm-1) convolved wavenumber stepsize
        wavenumber_max,            &  ! (cm-1) last wavenumber of the spectra
        wavenumber_min,            &  ! (cm-1) first wavenumber of the spectra
        wavenumber_step               ! (cm-1) wavenumber stepsize

    double precision, dimension(:), allocatable :: &
        radiances,                 &  ! (W.m-2.sr-1/cm-1) radiances in wavenumber of the spectrum
        radiances__,               &  ! parallelised radiance in wavenumber
        radiances_interp,          &  ! interpolated parallelised radiance in wavenumber
        radiances_interp__,        &  ! interpolated parallelised radiance in wavenumber
        t_sky,                     &  ! sky transmittance
        t_sky__,                   &  ! parallelised sky transmittance
        wavenumbers,               &  ! wavenumber array
        wavenumbers__,             &  ! parallelised wavenumber array
        wvn_H2He                      ! wavenumber array for H2 and He

    double precision, dimension(:, :, :), allocatable :: &
        vmr_derivative,            &  ! radiance derivative in ln of molecular abundance for all the processes
        vmr_derivative__,          &  ! radiance derivative in ln of molecular abundance for 1 process
        vmr_derivative_interp,   &  ! interpolated radiance derivative in ln of molecular abundance for 1 process
        vmr_derivative_interp__,   &  ! interpolated radiance derivative in ln of molecular abundance for 1 process
        cloud_t_derivative,        &  ! derivative in total cloud transmittance of radiance for all the processes
        cloud_t_derivative__,      &  ! derivative in total cloud transmittance of radiance for 1 process
        cloud_t_derivative_interp,      &  ! derivative in total cloud transmittance of radiance for 1 process
        cloud_t_derivative_interp__   ! interpolated derivative in total cloud transmittance of radiance for 1 process

    save
end module spectrum


module target
    ! """
    ! Contains the target parameters
    ! """
    implicit none

    double precision :: &
        emission_angle,                     &  ! (degree) emission angle
        incidence_angle,                    &  ! (degree) incidence angle
        latitude,                           &  ! (deg) latitude of observation on the target
        light_source_effective_temperature, &  ! (K) light source effective temperature
        light_source_radius,                &  ! (m) radius of the light source
        light_source_range,                 &  ! (m) distance between the target and the light source
        target_equatorial_radius,           &  ! (m) equatorial radius of the target
        target_mass,                        &  ! (kg) mass of the target
        target_polar_radius                    ! (m) polar radius of the target

    save
end module target


module allocations
    ! """
    ! Wrap most of the parameters allocations.
    ! """
    implicit none

    save

    contains
        subroutine allocate_all
            ! """
            ! Allocate and initialise most of the parameters.
            ! """
            use spectrum, only: cloud_t_derivative, cloud_t_derivative__, n_wvn, n_wvn__, radiances, radiances__, &
                vmr_derivative, vmr_derivative__
            use atmosphere, only: n_level, dz, n0_standard, column_number_density, tau, absorption_coefficient, &
                absorption_cross_sections
            use molecules, only: n_mol, vmr_profiles, vmr_profiles_err
            use clouds, only: tau_cloud, n_cloud, total_transmittance, transmittances_err

            implicit none

            allocate(&
                radiances(n_wvn), &
                radiances__(n_wvn__), &

                dz(n_level), &
                n0_standard(n_level), &
                total_transmittance(n_level), &

                tau(n_wvn__, n_level), &

                column_number_density(n_level, n_mol), &
                vmr_profiles(n_level, n_mol), &
                vmr_profiles_err(n_level, n_mol), &

                tau_cloud(n_level, n_cloud), &
                transmittances_err(n_level, n_cloud), &

                absorption_coefficient(n_wvn__, n_level, n_mol), &
                absorption_cross_sections(n_wvn__, n_level, n_mol), &
                vmr_derivative(n_wvn, n_level, n_mol), &
                vmr_derivative__(n_wvn__, n_level, n_mol), &

                cloud_t_derivative(n_wvn, n_level, n_cloud), &
                cloud_t_derivative__(n_wvn__, n_level, n_cloud)&
            )

            radiances(:) = 0D0
            radiances__(:) = 0D0

            dz(:) = 0D0
            n0_standard(:) = 0D0
            total_transmittance(:) = 0D0

            column_number_density(:, :) = 0D0
            vmr_profiles(:, :) = 0D0
            vmr_profiles_err(:, :) = 0D0

            tau(:, :) = 0D0

            tau_cloud(:, :) = 0D0
            transmittances_err(:, :) = 0D0

            absorption_coefficient(:, :, :) = 0D0
            absorption_cross_sections(:, :, :) = 0D0
            vmr_derivative(:, :, :) = 0D0
            vmr_derivative__(:, :, :) = 0D0

            cloud_t_derivative(:, :, :) = 0D0
            cloud_t_derivative__(:, :, :) = 0D0
        end subroutine allocate_all

end module allocations
