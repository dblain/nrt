"""
Useful physical and mathematical functions.
"""
import numpy as np
import scipy.constants as cst


def chi2(observed_data, calculated_data):
    """
    Calculate the chi-squared deviation between observed and calculated data.
    :param observed_data: independent measurements of a quantity
    :param calculated_data: modelling of the observation
    :return: chi2 between the observations and the model
    """
    return sum((observed_data - calculated_data) ** 2 / calculated_data)


def chi2_reduced(observed_data, calculated_data, input_deviation):
    """
    Calculate the reduced chi-squared deviation between observed and calculated data.
    :param observed_data: independent measurements of a quantity
    :param calculated_data: modelling of the observation
    :param input_deviation: error on the measurements
    :return: reduced chi2 between the observations and the model
    """
    return sum((observed_data - calculated_data) ** 2 / input_deviation ** 2)


def ellipse_polar_form(semi_major_axis, semi_minor_axis, angle):
    """
    Return the ellipse polar form of an ellipse relative to its center.
    :param angle: (deg) angle between the semi-major axis, the center, and the point of the ellipse
    :param semi_major_axis: semi-major axis of the ellipse
    :param semi_minor_axis: semi-minor axis of the ellipse
    """
    theta = np.deg2rad(angle)

    r = semi_major_axis * semi_minor_axis * \
        np.sqrt((semi_minor_axis * np.cos(theta))**2 + (semi_major_axis * np.sin(theta))**2)

    return r


def extrapolate(x_new, x, y):
    """
    One-dimensional linear extrapolation.
    :param x_new: the x-coordinates of the extrapolated values
    :param x: the x-coordinates of the data points
    :param y: the y-coordinates of the data points, same length as x
    :return: the interpolated and extrapolated values, same shape as x_new
    """
    if np.ndim(x_new) is not 1:
        raise ValueError('x_new is not a 1-D numpy array')
    if np.ndim(x) is not 1:
        raise ValueError('x is not a 1-D numpy array')
    if np.ndim(y) is not 1:
        raise ValueError('y is not a 1-D numpy array')

    np.zeros(np.size(x_new))
    y_lt = np.array([])
    y_gt = np.array([])

    if x_new[0] < x[0]:
        x_new = x_new[::-1]
        x = x[::-1]
        y = y[::-1]
        wh_diff_x = np.where(x_new < x[-1])
        size_diff_x = np.size(wh_diff_x)
        dx = x[-2] - x[-1]

        dy = (y[-2] - y[-1]) / dx
        y_lt = np.zeros(size_diff_x)
        y_lt[0] = y[-1] + dy * (x_new[np.min(wh_diff_x)] - x[-1])

        for i in range(size_diff_x - 1):
            dx_i = (x_new[np.min(wh_diff_x) + i + 1]) - (x_new[np.min(wh_diff_x) + i])
            y_lt[i + 1] = y_lt[i] + dy * dx_i
        x_new = x_new[::-1]
        x = x[::-1]
        y = y[::-1]
        y_lt = y_lt[::-1]

    if x_new[-1] > x[-1]:
        wh_diff_x = np.where(x_new > x[-1])
        size_diff_x = np.size(wh_diff_x)
        dx = x[-1] - x[-2]

        dy = (y[-1] - y[-2]) / dx
        y_gt = np.zeros(size_diff_x)
        y_gt[0] = y[-1] + dy * (x_new[np.min(wh_diff_x)] - x[-1])

        for i in range(size_diff_x - 1):
            dx_i = (x_new[np.min(wh_diff_x) + i + 1]) - (x_new[np.min(wh_diff_x) + i])
            y_gt[i + 1] = y_gt[i] + dy * dx_i

    y_new = np.concatenate((y_lt, y, y_gt))
    return y_new


def interpx(x_new, x, y):
    """
    One-dimensional linear interpolation and extrapolation.
    Performs a linear interpolation using numpy.interp() followed by a linear extrapolation.
    :param x_new: the x-coordinates of the interpolated and extrapolated values
    :param x: the x-coordinates of the data points
    :param y: the y-coordinates of the data points, same length as x
    :return y_new: the interpolated and extrapolated values, same shape as x_new
    """
    # numpy array is necessary for numpy.where()
    x_new = np.asarray(x_new)
    y_new = np.zeros(np.size(x_new))
    x = np.asarray(x)
    y = np.asarray(y)

    if not np.all(np.diff(x) > 0):
        raise ValueError('x is not strictly increasing')
    if not np.all(np.diff(x_new) > 0):
        raise ValueError('x_new is not strictly increasing')

    if np.min(x_new) < np.min(x):
        interp_bound_inf = np.max(np.where(x_new < np.min(x)))
    else:
        interp_bound_inf = -1

    if np.max(x_new) > np.max(x):
        interp_bound_sup = np.min(np.where(x_new > np.max(x)))
    else:
        interp_bound_sup = -1

    interp_bounds = [interp_bound_inf, interp_bound_sup]
    y_new[interp_bounds[0] + 1:interp_bounds[1]] = np.interp(x_new[interp_bounds[0] + 1:interp_bounds[1]], x, y)
    y_new[:interp_bounds[0] + 1] = extrapolate(x_new, x, y)[:interp_bounds[0] + 1]
    y_new[interp_bounds[1]:] = extrapolate(x_new, x, y)[::-1][:np.size(y_new[interp_bounds[1]:])][::-1]
    return y_new


def velocity2doppler(source_velocity, observed_wavenumber):
    """
    Calculate the Doppler shift and the source wavenumber at a given measured wavenumber for a given velocity.
    If we are in the referential of the source, we have:
        observed_wavenumber = (cst.c + observer_velocity) / cst.c * wavenumber_source
    with observer_velocity the velocity of the observer relative to the source, which is >0 if the observer is moving
    towards the source; the observed wavenumber is higher than the emitted wavenumber if the observer is moving towards
    the source.
    The velocity of the source is >0 if the source is moving away from the observer. The observed wavenumber is
    higher than the emitted wavenumber if the source is moving towards the observer, so if source_velocity <0.
    So in the referential of the source, we have :
        observer_velocity = -source_velocity.
    Finally, from all the above:
        wavenumber_source = cst.c * observed_wavenumber / (cst.c - source_velocity)
    :param source_velocity: (m.s-1) velocity of the source relative to the observer; >0 if the source is moving away
    :param observed_wavenumber: (cm-1) wavenumber measured by the observer
    :return: the mean Doppler shift at the given wavenumber and the wavenumber corrected from the Doppler shift
    """
    wavenumber_source = cst.c * observed_wavenumber / (cst.c - source_velocity)
    doppler_shifts = wavenumber_source - observed_wavenumber

    return doppler_shifts, wavenumber_source
