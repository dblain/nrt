"""
Manage Python/Fortran NRT interface.
"""
import collections
import os
import re
from subprocess import call

import numpy as np

from .files import NameListFile, File, ObjList

BASH = ''


def _set_bash():
    """Get the path to bash. Useful for windows distributions."""
    global BASH
    if not BASH:
        bash_paths = []
        for root, dirs, files in os.walk(os.path.join(os.path.expanduser('~'), '..', '..')):
            if 'cygwin' in BASH:
                break
            for file in files:
                if re.match(r'^bash.exe$', file):
                    if 'cygwin' in root:
                        BASH = os.path.join(root, 'bash')
                        break
                    bash_paths.append(os.path.join(root, 'bash'))

        if len(bash_paths) == 0 and not BASH:
            raise FileNotFoundError("No bash interpreter found, please use cygwin or another bash interpreter")
        elif not BASH:
            BASH = bash_paths[0]
        BASH += ' -l -c '


def mpicompile_nrt(compiler_flags='-O3', linker_flags='-s'):
    """
    Compile NRT using mpif90. OpenMPI is recommended.
    Compatible with linux or windows given it has a bash interpreter (cygwin is recommended).
    :param compiler_flags: flags to give to the compiler (i.e. '-march=corei7-avx -O3' or '-fcheck=all -pg -g')
    :param linker_flags: flags to give to compilers when they are supposed to invoke the linker (i.e. '-s')
    """
    print('Compiling...')
    global BASH

    # NRT absolute paths
    nrt_abs_path = os.path.abspath(File.PATH)
    nrt_bin_abs_path = os.path.join(os.path.abspath(File.PATH), 'bin', '')
    os.chdir(nrt_bin_abs_path)

    # makefile
    compiler = 'mpifort'

    source_directory = os.path.join(nrt_abs_path, 'source', 'Fortran', '')
    binary = ''

    objects = ['module_math',
               'module_mpi_utils',
               'module_nrt_parameters',
               'module_nrt_physics',
               'module_nrt_IO',
               'module_nrt_retrieval',
               'nrt']

    for i in objects:
        binary = binary + ' ' + i + '.o'

    # TODO (minor) support for MacOS
    if os.name is 'posix':  # linux distributions
        BASH = ''
        quote = ''
    elif os.name is 'nt':  # openmpi is *very* difficult to get working on windows ! D:
        # search for bash (tested with cygwin installed)
        quote = '\"'
        _set_bash()
        source_directory = source_directory.replace(os.path.sep, '/')
    else:
        raise OSError("os '%s' is not supported, please run on linux or windows or compile NRT manually" % os.name)

    for i in objects:
        call(BASH + quote + compiler + ' ' + compiler_flags + ' -c ' + source_directory + i + '.f90' + quote,
             shell=True)

    call(BASH + quote + compiler + ' ' + linker_flags + ' ' + binary + ' -o ' + 'nrt.mpi' + quote, shell=True)
    call(BASH + quote + 'rm -f *.o *.mod' + quote, shell=True)


def mpiexec_nrt(logfile, n=4):
    """
    Execute NRT.
    :param logfile: File where the log will be saved
    :param n: number of processes
    """
    os.chdir(File.PATH_BIN)
    if os.name is 'posix':  # linux
        call('mpiexec -n ' + str(n) + ' ' + File.PATH_BIN + 'nrt.mpi | tee ' + logfile, shell=True)
    elif os.name is 'nt':  # windows
        _set_bash()
        path_bin_nt = File.PATH_BIN.replace(os.path.sep, '/')
        call(BASH + '\"' + 'mpiexec -n ' + str(n) + ' ' + path_bin_nt + 'nrt.mpi | tee ' + logfile + '\"', shell=True)
    else:
        raise OSError("'%s' os is not supported, please run on linux or windows" % os.name)


def run_nrt(compile_nrt=False, number_processes=4,
            retrieved_spectrum=None, noise_file='None', output_spectrum=None, doppler_shift=None, atmosphere=None,
            wavenumber_min=0.0, wavenumber_max=1.0, wavenumber_step=1.0, convolve=False, instrument=None, target=None,
            retrieve=False, iteration_max=16, chi2diff_threshold=0.01, vertical_smoothness=0.75, weight_apriori=0.1,
            calculate_cross_sections=False, write_cross_sections_files=False,
            output_derivatives=False, path_vmr='', path_lines='', path_molecule_parameters='',
            path_absorption_cross_sections='', path_sky='', path_retrieved_spectrum='',
            path_noise='', path_instrument='', path_temperature_profile='', path_output_spectrum='',
            path_output_vmr=''):
    """
    Run NRT with the given parameters.
    Write the input parameters file, compile the Fortran code and execute it.
    :param compile_nrt: if True, compile NRT fortran code
    :param number_processes: number of processes to use for the run
    :param retrieved_spectrum: the retrieved spectrum
    :param output_spectrum: the synthetic spectrum
    :param doppler_shift: observed Doppler shift
    :param atmosphere: atmosphere used for the synthetic spectrum
    :param noise_file: noise file
    :param wavenumber_min: minimum wavenumber of synthetic spectrum
    :param wavenumber_max: maximum wavenumber of synthetic spectrum
    :param wavenumber_step: step between wavenumbers
    :param convolve: if True the synthetic spectrum is convolved
    :param instrument: instrument used to convolve the synthetic spectrum
    :param target: target of the observation
    :param retrieve: if True the parameters of the observed spectrum will be retrieved
    :param iteration_max: max retrieving iterations
    :param chi2diff_threshold: chi2 difference threshold for retrieving
    :param vertical_smoothness: retrieving vertical smoothing parameter in height scale
    :param weight_apriori: retrieving weight of a priori
    :param calculate_cross_sections: if True, force calculation of the molecular attenuation cross sections
    :param write_cross_sections_files: if True, force writing of absorption coefficient files
    :param output_derivatives: if True, output the derivative files
    :param path_vmr: path to the a priori vmr profile files
    :param path_lines: path to the molecular lines files
    :param path_molecule_parameters: path to the molecular parameters files
    :param path_absorption_cross_sections: path to the molecular absorption cross sections files
    :param path_sky: path to the sky files
    :param path_retrieved_spectrum: path to the observed spectrum files
    :param path_noise: path to the noise files
    :param path_instrument: path to the instrument file
    :param path_temperature_profile: path to the temperature profile files
    :param path_output_spectrum: path where to output the calculated spectrum
    :param path_output_vmr: path where to output the retrieved vmr profiles
    """
    write_input_parameters_file(
        output_spectrum_name=output_spectrum.name,
        retrieved_spectrum=retrieved_spectrum,
        noise_file=noise_file,
        wavenumber_min=wavenumber_min,
        wavenumber_max=wavenumber_max,
        wavenumber_step=wavenumber_step,
        doppler_shift=doppler_shift,
        target=target,
        atmosphere=atmosphere,
        instrument=instrument,
        iteration_max=iteration_max,
        chi2diff_threshold=chi2diff_threshold,
        vertical_smoothness=vertical_smoothness,
        weight_apriori=weight_apriori,
        write_cross_section_files=write_cross_sections_files,
        convolve=convolve,
        retrieve=retrieve,
        output_derivatives=output_derivatives,
        calculate_cross_sections=calculate_cross_sections,
        path_vmr=path_vmr,
        path_lines=path_lines,
        path_molecule_parameters=path_molecule_parameters,
        path_absorption_cross_sections=path_absorption_cross_sections,
        path_sky=path_sky,
        path_retrieved_spectrum=path_retrieved_spectrum,
        path_noise=path_noise,
        path_instrument=path_instrument,
        path_temperature_profile=path_temperature_profile,
        path_output_spectrum=path_output_spectrum,
        path_output_vmr=path_output_vmr
    )

    if compile_nrt:
        mpicompile_nrt()

    mpiexec_nrt(logfile=output_spectrum.file.split('.')[0] + '.log', n=number_processes)


def write_input_parameters_file(retrieved_spectrum=None, output_spectrum_name='new_spectrum',
                                doppler_shift=0.0, target=None, atmosphere=None,
                                noise_file='None', wavenumber_min=0.0, wavenumber_max=0.0, wavenumber_step=0.0,
                                convolve=False, instrument=None, retrieve=False, iteration_max=0,
                                chi2diff_threshold=0.0, vertical_smoothness=0.0,
                                weight_apriori=0.0, calculate_cross_sections=False,
                                write_cross_section_files=False,
                                output_derivatives=False,
                                path_vmr='', path_lines='', path_molecule_parameters='',
                                path_absorption_cross_sections='', path_sky='',
                                path_retrieved_spectrum='', path_noise='', path_instrument='',
                                path_temperature_profile='', path_output_spectrum='', path_output_vmr=''):
    """
    Make the input file of the Fortran program.
    :param retrieved_spectrum: the retrieved spectrum
    :param output_spectrum_name: synthetic spectrum
    :param doppler_shift: observed Doppler shift
    :param target: target of the observation
    :param atmosphere: atmosphere used for the synthetic spectrum
    :param noise_file: noise file
    :param wavenumber_min: minimum wavenumber of synthetic spectrum
    :param wavenumber_max: maximum wavenumber of synthetic spectrum
    :param wavenumber_step: step between wavenumbers
    :param convolve: if True the synthetic spectrum is convolved
    :param instrument: instrument used to convolve the synthetic spectrum
    :param retrieve: if True the parameters of the observed spectrum will be retrieved
    :param iteration_max: max retrieving iterations
    :param chi2diff_threshold: chi2 difference threshold for retrieving
    :param vertical_smoothness: retrieving vertical smoothing parameter in height scale
    :param weight_apriori: retrieving weight of a priori
    :param calculate_cross_sections: if True, force calculation of the molecular attenuation cross sections
    :param write_cross_section_files: if True, force writing of absorption coefficient files
    :param output_derivatives: if True, output the derivative files
    :param path_vmr: path to the a priori vmr profile files
    :param path_lines: path to the molecular lines files
    :param path_molecule_parameters: path to the molecular parameters files
    :param path_absorption_cross_sections: path to the molecular absorption cross sections files
    :param path_sky: path to the sky files
    :param path_retrieved_spectrum: path to the observed spectrum files
    :param path_noise: path to the noise files
    :param path_instrument: path to the instrument
    :param path_temperature_profile: path to the temperature profile files
    :param path_output_spectrum: path where to output the calculated spectrum
    :param path_output_vmr: path where to output the retrieved vmr profiles
    """
    if retrieved_spectrum is None:
        retrieved_spectrum_name = 'None'
    else:
        retrieved_spectrum_name = retrieved_spectrum.file.rsplit(os.path.sep)[-1]

    if target is None:
        latitude = 0.0
        light_source_radius = 0.0
        light_source_range = 1.0
        light_source_effective_temperature = 0.0
        target_mass = 0.0
        target_equatorial_radius = 0.0
        target_polar_radius = 0.0
        incidence_angle = 0.0
        emission_angle = 0.0
    else:
        if np.isnan(target.latitude).any():
            latitude = 0.0
        elif np.size(target.latitude) > 1:
            latitude = target.latitude[0]
        else:
            latitude = target.latitude

        if np.isnan(target.incidence_angle):
            incidence_angle = 0.0
        else:
            incidence_angle = target.incidence_angle

        if np.isnan(target.emission_angle):
            emission_angle = 0.0
        else:
            emission_angle = target.emission_angle

        light_source_range = target.light_source_range

        if target.light_source is None:
            light_source_radius = 0.0
            light_source_effective_temperature = 0.0
        else:
            light_source_radius = target.light_source.radius
            light_source_effective_temperature = target.light_source.effective_temperature

            if light_source_range == 0.0 or np.isnan(light_source_range):
                light_source_range = target.light_source.radius

        target_mass = target.mass
        target_equatorial_radius = target.equatorial_radius
        target_polar_radius = target.polar_radius

    if light_source_range == 0.0:
        light_source_range = 1.0

    if wavenumber_min > wavenumber_max:
        raise ValueError(
            f'minimal wavenumber ({wavenumber_min} cm-1) is greater than maximal wavenumber ({wavenumber_max} cm-1)')

    if noise_file is None:
        noise_file = 'None'

    if not convolve:
        class NoInstrument:
            pass

        instrument = NoInstrument()
        instrument.lsf_shape = 'None'
        instrument.lsf_fwhm = 0
        instrument.data_file = File('empty')
        instrument.data_file.file = ''
    elif instrument is None and convolve:
        raise AttributeError('an instrument is required for convolution')
    else:
        instrument.load()

    sky_file = 'None'

    sep = os.path.sep

    if atmosphere.sky is not None:
        sky_file = atmosphere.sky.file.rsplit(sep)[-1]

    molecules = ObjList(list(collections.OrderedDict(sorted(atmosphere.molecules.items())).values()))

    # Get mean Volume Mixing Ratios
    loshmidt_factors = 273.15 / atmosphere.temperature_profile.temperatures * atmosphere.temperature_profile.pressures
    mol_vmr = list(np.sum(np.asarray(molecules.getsubattr_list('vmr_profile.vmr')) * loshmidt_factors, 1) /
                   np.sum(loshmidt_factors))

    # Change directory for relative paths
    os.chdir(File.PATH_BIN)

    input_parameters_file = NameListFile(path=File.PATH_IN, name='input_parameters')

    name_lists = {
        'output_files': {
            'output_spectrum_name': NameListFile.NameListLine(
                value=output_spectrum_name,
                comment='name of the outputted spectrum file'
            ),
        },
        'observations': {
            'retrieved_spectrum': NameListFile.NameListLine(
                value=retrieved_spectrum_name,
                comment='name of the retrieved spectrum file'
            ),
            'noise_file': NameListFile.NameListLine(
                value=noise_file,
                comment='file of the retrieved spectrum noise-equivalent-radiance'
            ),
            'sky_file': NameListFile.NameListLine(value=sky_file, comment='file of the retrieved spectrum sky'),
            'doppler_shift': NameListFile.NameListLine(value=doppler_shift, comment='the observed doppler shift'),
        },
        'target_parameters': {
            'emission_angle': NameListFile.NameListLine(
                value=emission_angle,
                comment='(deg) emission angle'
            ),
            'incidence_angle': NameListFile.NameListLine(
                value=incidence_angle,
                comment='(deg) solar incident light relative to the normal of the atmosphere angle'
            ),
            'latitude': NameListFile.NameListLine(
                value=latitude,
                comment='(deg) latitude of observation on the target'
            ),
            'light_source_effective_temperature': NameListFile.NameListLine(
                value=light_source_effective_temperature,
                comment='(K) light source effective temperature'
            ),
            'light_source_radius': NameListFile.NameListLine(
                value=light_source_radius,
                comment='(m) radius of the light source'
            ),
            'light_source_range': NameListFile.NameListLine(
                value=light_source_range,
                comment='(m) distance between the target and the light source'
            ),
            'target_mass': NameListFile.NameListLine(
                value=target_mass,
                comment='(kg) mass of the target'
            ),
            'target_equatorial_radius': NameListFile.NameListLine(
                value=target_equatorial_radius,
                comment='(m) equatorial radius of the target'
            ),
            'target_polar_radius': NameListFile.NameListLine(
                value=target_polar_radius,
                comment='(m) polar radius of the target'
            )
        },
        'atmosphere_parameters': {
            'temperature_profile_file': NameListFile.NameListLine(
                value=atmosphere.temperature_profile.file.rsplit(os.path.sep)[-1],
                comment='temperature profile file'
            ),
            'n_cloud': NameListFile.NameListLine(
                value=np.size(atmosphere.clouds),
                comment='number of clouds in atmosphere'
            ),
            'n_mol': NameListFile.NameListLine(
                value=np.size(molecules),
                comment='number of molecules in atmosphere'
            ),
        },
        'molecules_parameters': {
            'molecules_name': NameListFile.NameListLine(
                value=molecules.get('name'),
                comment='molecules in atmosphere'
            ),
            'molecules_vmr': NameListFile.NameListLine(value=mol_vmr, comment='volume mixing ratio of the molecule'),
            'vmr_factors': NameListFile.NameListLine(
                value=molecules.get('vmr_factor'),
                comment='vmr factor modifier'
            ),
            'is_retrieved': NameListFile.NameListLine(
                value=molecules.get('retrieve'),
                comment='if True, and if \'retrieve\' is True, the vmr profile of the molecule is retrieved'
            ),
        },
        'clouds_parameters': {
            'p_cloud_top': NameListFile.NameListLine(
                value=ObjList(atmosphere.clouds).get('pressure_top'),
                comment='(bar) pressure of the top of the clouds'
            ),
            'p_cloud_base': NameListFile.NameListLine(
                value=ObjList(atmosphere.clouds).get('pressure_base'),
                comment='(bar) pressure of the base of the clouds'
            ),
            'transmittances': NameListFile.NameListLine(
                value=ObjList(atmosphere.clouds).get('transmittance'),
                comment='transmittance at the base of the clouds'
            ),
            'reflectances': NameListFile.NameListLine(
                value=ObjList(atmosphere.clouds).get('reflectance'),
                comment='reflectance at the base of the clouds'
            ),
        },
        'spectrum_parameters': {
            'wavenumber_min': NameListFile.NameListLine(value=wavenumber_min, comment='(cm-1) first wavenumber'),
            'wavenumber_max': NameListFile.NameListLine(value=wavenumber_max, comment='(cm-1) last wavenumber'),
            'wavenumber_step': NameListFile.NameListLine(value=wavenumber_step, comment='(cm-1) wavenumber step size'),
        },
        'convolution': {
            'convolve': NameListFile.NameListLine(
                value=convolve,
                comment='convolve the spectrum using the settings below'
            ),
            'instrument_file': NameListFile.NameListLine(
                value=instrument.data_file.file.rsplit(os.path.sep)[-1],
                comment='instrument file'
            ),
        },
        'retrieval_parameters': {
            'retrieve': NameListFile.NameListLine(
                value=retrieve,
                comment='if True, retrieve the spectrum using the observed spectrum and the settings below'
            ),
            'iteration_max': NameListFile.NameListLine(value=iteration_max, comment='max number of iterations'),
            'chi2diff_threshold': NameListFile.NameListLine(
                value=chi2diff_threshold,
                comment='max difference of chi2 between 2 iterations to end the retrieving'
            ),
            'v_smooth': NameListFile.NameListLine(
                value=vertical_smoothness,
                comment='(height scale) vertical profile smoothing'
            ),
            'weight_apriori': NameListFile.NameListLine(
                value=weight_apriori,
                comment='weight of the a priori, low weight means small variations)'
            ),
        },
        'options': {
            'calculate_cross_sections': NameListFile.NameListLine(
                value=calculate_cross_sections,
                comment='if True, force the calculation of the absorption coefficients'),
            'write_cross_sections': NameListFile.NameListLine(
                value=write_cross_section_files,
                comment='if True, save the absorption coefficients'
            ),
            'output_derivatives': NameListFile.NameListLine(
                value=output_derivatives,
                comment='if True, output the functional derivatives'
            ),
        },
        'paths': {
            'path_vmr': NameListFile.NameListLine(
                value=os.path.relpath(path_vmr) + sep,
                comment='path to the molecular vmr profile files'
            ),
            'path_lines': NameListFile.NameListLine(
                value=os.path.relpath(path_lines) + sep,
                comment='path to the molecular lines files'
            ),
            'path_molecular_parameters': NameListFile.NameListLine(
                value=os.path.relpath(path_molecule_parameters) + sep,
                comment='path to molecular parameters files'
            ),
            'path_absorption_cross_sections': NameListFile.NameListLine(
                value=os.path.relpath(path_absorption_cross_sections) + sep,
                comment='path to the molecular absorption cross sections files'),
            'path_sky': NameListFile.NameListLine(
                value=os.path.relpath(path_sky) + sep,
                comment='path to the sky files'
            ),
            'path_retrieved_spectrum': NameListFile.NameListLine(
                value=os.path.relpath(path_retrieved_spectrum) + sep,
                comment='path to the retrieved spectrum file'
            ),
            'path_noise': NameListFile.NameListLine(
                value=os.path.relpath(path_noise) + sep,
                comment='path to the noise files'
            ),
            'path_instrument': NameListFile.NameListLine(
                value=os.path.relpath(path_instrument) + sep,
                comment='path to the instrument data file'
            ),
            'path_temperature_profile': NameListFile.NameListLine(
                value=os.path.relpath(path_temperature_profile) + sep,
                comment='path to the temperature profile file'
            ),
            'path_spectrum_out': NameListFile.NameListLine(
                value=os.path.relpath(path_output_spectrum) + sep,
                comment='path where to output the calculated spectrum'
            ),
            'path_vmr_out': input_parameters_file.NameListLine(
                value=os.path.relpath(path_output_vmr) + sep,
                comment='path where to output the retrieved vmr profile files'
            ),
        },
    }

    # Back to main directory
    os.chdir(File.PATH)

    input_parameters_file.write(name_lists)
