"""
Ephemeris module.
"""
import os
import re
import struct

from jdcal import gcal2jd, jd2gcal

from .files import File


class Ephemeris(File):
    PATH_DATA = os.path.join(File.PATH_DATA, 'ephemeris')
    EXT = 'dat'
    JULIAN_DATE_ORIGIN = '-4713-11-24T12:00:00.000'  # (ISO 8601) assuming Gregorian calendar before 1582
    J2000_DATE_ORIGIN = '2000-01-01T12:00:00.000'  # (ISO8601)

    def __init__(self, name='new_ephemeris', path=PATH_DATA, ext=EXT, exists=False, find=False):
        """
        Ephemeris of an astronomical object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param exists: c.f. File
        :param find: c.f. File
        """
        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        # time
        self.date = ''
        self.julian_date = 0

        # solar presence
        self.daylight = True

        # moon presence
        self.moon_above_horizon = None

        # object
        self.right_ascension = None
        self.declination = None
        self.azimuth = None
        self.elevation = None
        self.apparent_magnitude = None
        self.surface_brightness = None
        self.angular_diameter = None
        self.obs_sublon = None
        self.obs_sublat = None
        self.distance_to_obs = None
        self.relative_velocity = None
        self.sol_obs_target_angle = None
        self.sol_target_obs_angle = None

    def read_from_jplhorizon(self, file=None):
        """
        Read an Ephemeris file in JPL Horizon format (https://ssd.jpl.nasa.gov/horizons.cgi).
        :param file: if None, the file read is the file of the current Ephemeris.
        """
        if file is None:
            file = self.file

        self.julian_date = []
        self.date = []
        self.daylight = []
        self.moon_above_horizon = []

        self.right_ascension = []
        self.declination = []
        self.azimuth = []
        self.elevation = []
        self.apparent_magnitude = []
        self.surface_brightness = []
        self.angular_diameter = []
        self.obs_sublon = []
        self.obs_sublat = []
        self.distance_to_obs = []
        self.relative_velocity = []
        self.sol_obs_target_angle = []
        self.sol_target_obs_angle = []

        with open(file, 'rb') as f:
            for line in f:
                try:
                    (date, sun, moon, ra, dec, azi, elev, ap_mag, s_brt, ang_diam, ob_lon, ob_lat, delta, deldot, sot,
                     r, sto) = struct.unpack('26s2s1s11s10s9s9s7s7s9s7s7s17s12s9s3s9s', line)
                except struct.error:
                    continue

                if not re.match(r'.*[0-9].*-', str(date, 'utf-8')):
                    continue

                date = list(str(date, 'utf-8').strip())
                date[5:8] = list(self.month2str_number(''.join(date[5:8])))
                date = ''.join(date)
                self.date.append(date)
                self.julian_date.append(self.date2julian(date))

                if '*' or 'A' or 'N' or 'C' in str(sun):
                    self.daylight.append(True)

                if 'm' or 'r' or 't' or 's' in str(moon):
                    self.moon_above_horizon.append(True)

                self.right_ascension.append(float(ra))
                self.declination.append(float(dec))
                self.azimuth.append(float(azi))
                self.elevation.append(float(elev))
                self.apparent_magnitude.append(float(ap_mag))
                self.surface_brightness.append(float(s_brt))
                self.angular_diameter.append(float(ang_diam))
                self.obs_sublon.append(float(ob_lon))
                self.obs_sublat.append(float(ob_lat))
                self.distance_to_obs.append(float(delta))
                self.relative_velocity.append(float(deldot))
                self.sol_obs_target_angle.append(float(sot))
                # r is /L or /T for leading or trailing
                self.sol_target_obs_angle.append(float(sto))

    @staticmethod
    def date2julian(date):
        """
        Convert a date (format yyyy-mm-ddThh:mm:ss.sss, i.e. ISO 8601) into a julian date.
        :param date: (ISO 8601) date
        :return: (days) julian date
        """
        if '.' in date:
            ymdhms, seconds_decimals = date.split('.')
        else:
            ymdhms = date
            seconds_decimals = 0

        ymdhms = ymdhms[0] + ymdhms[1:].replace('-', '').replace('T', '').replace(':', '')
        smhdmy = ymdhms[::-1]
        seconds = float(smhdmy[0:2][::-1] + '.' + seconds_decimals)
        minutes = float(smhdmy[2:4][::-1])
        hours = float(smhdmy[4:6][::-1])
        day = int(smhdmy[6:8][::-1])
        month = int(smhdmy[8:10][::-1])
        year = int(smhdmy[10:][::-1])

        day_fraction = (hours + (minutes + seconds / 60.0) / 60.0) / 24.0

        return sum(gcal2jd(year, month, day)) + day_fraction

    @staticmethod
    def date2j2000(date):
        return Ephemeris.julian2j2000(Ephemeris.date2julian(date))

    @staticmethod
    def days2seconds(days):
        """
        Convert days into seconds.
        :param days: (days)
        :return: (sec)
        """
        return days * 24.0 * 60.0 * 60.0

    @staticmethod
    def j20002date(j2000_date):
        return Ephemeris.julian_date2date(Ephemeris.j20002julian(j2000_date))

    @staticmethod
    def j20002julian(j2000_date):
        return Ephemeris.seconds2days(j2000_date) + Ephemeris.date2julian(Ephemeris.J2000_DATE_ORIGIN)

    @staticmethod
    def julian_date2date(julian_date):
        """
        Convert a julian date (in days since the julian date origin) into a date in format ISO 8601.
        :param julian_date: (days) julian date to convert
        :return: (ISO 8601) converted date
        """
        year, month, day, day_fraction = jd2gcal(0, julian_date)

        hours = int(day_fraction * 24)
        minutes = int(day_fraction * 24 * 60) - hours * 60
        seconds = float(day_fraction * 24 * 60 * 60) - hours * 3600 - minutes * 60

        return f'{year}-{month:02}-{day:02}T{hours:02}:{minutes:02}:{seconds:06.3f}'

    @staticmethod
    def julian2j2000(julian_date):
        return Ephemeris.days2seconds(julian_date - Ephemeris.date2julian(Ephemeris.J2000_DATE_ORIGIN))

    @staticmethod
    def month2str_number(month):
        """
        Convert a literal month (i.e. "April") into a numbered month (i.e. 04).
        :param month: month string to convert
        :return: string number representing the month
        """
        if re.match(r'[jJ]an.*', month):
            return '01'
        elif re.match(r'[fF]eb.*', month):
            return '02'
        elif re.match(r'[mM]ar.*', month):
            return '03'
        elif re.match(r'[aA]pr.*', month):
            return '04'
        elif re.match(r'[mM]ay', month):
            return '05'
        elif re.match(r'[jJ]un.*', month):
            return '06'
        elif re.match(r'[jJ]ul.*', month):
            return '07'
        elif re.match(r'[aA]ug.*', month):
            return '08'
        elif re.match(r'[sS]ep.*', month):
            return '09'
        elif re.match(r'[oO]ct.*', month):
            return '10'
        elif re.match(r'[nN]ov.*', month):
            return '11'
        elif re.match(r'[dD]ec.*', month):
            return '12'
        else:
            raise ValueError(f'could not convert month to number: {month}')

    @staticmethod
    def seconds2days(seconds):
        """
        Convert seconds into days
        :param seconds: (sec)
        :return: (days)
        """
        return seconds / 60.0 / 60.0 / 24.0
