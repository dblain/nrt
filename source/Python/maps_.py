"""
Map plotting methods module.
IMPORTANT NOTE (cartopy 0.16.0):
    In order for hexbin to actually work, site-packages/cartopy/mpl/geoaxes.py must be modified by adding
    https://github.com/SciTools/cartopy/issues/884#issuecomment-303474972 to class GeoAxes
"""
import os
import site
from warnings import warn

import cartopy.crs as ccrs
import cartopy.mpl.geoaxes as cgeoaxes
import matplotlib.cm
import matplotlib.pyplot as plt
import numpy as np

from .files import Figure
from .utils import ObjList, stack_arrays

# The data are defined in lat/lon coordinate system, so PlateCarree() is the appropriate choice:
data_crs = ccrs.PlateCarree()


def _add_grid_labels(geoax, meridians=None, parallels=None, size=12,
                     parallels_label_longitude=None, meridians_label_latitude=None,
                     spherical=False, central_longitude=0, western_longitudes=False):
    """
    Add grid line labels manually for projections that aren't supported (cartopy 0.16.0).
    From: https://github.com/SciTools/cartopy/issues/881#issuecomment-414777952
    :param geoax: cartopy GeoaxesSubplot
    :param meridians: meridians to label
    :param parallels: parallels to label
    :param size: (pt) size of the labels
    :param parallels_label_longitude: longitude where to plot the parallels labels
    :param meridians_label_latitude: latitude where to plot the meridians labels
    :param spherical: pad the labels better if a side of ax is spherical
    :param central_longitude: central longitude of the geoax
    :param western_longitudes: if True, longitudes are considered as Western longitudes (0-360)
    """
    if parallels is not None:
        if parallels_label_longitude is None:
            if western_longitudes:
                parallels_label_longitude = np.rint(np.min(meridians)) + central_longitude - 359
            else:
                parallels_label_longitude = np.rint(np.min(meridians)) + central_longitude

        for lat in parallels:
            if spherical:
                if lat > 0:
                    va = 'bottom'
                    lat_label = f'{int(np.rint(lat))}$^\circ$N  '
                elif lat < 0:
                    va = 'top'
                    lat_label = f'{int(np.rint(lat))}$^\circ$S  '
                else:
                    va = 'center'
                    lat_label = f'{-int(np.rint(lat))}$^\circ$  '
            else:
                va = 'center'
                lat_label = f'{int(np.rint(lat))}$^\circ$N  '

            geoax.text(parallels_label_longitude, lat, lat_label, va=va, ha='right', fontsize=size,
                       transform=data_crs)

    if meridians is not None:
        if meridians_label_latitude is None:
            meridians_label_latitude = np.rint(np.min(np.abs(parallels)))

        for lon in meridians:
            if western_longitudes:
                lon_label = f'{180 - int(np.rint(lon))}$^\circ$W'
            else:
                lon_label = f'{int(np.rint(lon))}$^\circ$E'

            # Do not display labels at the left edge of the map and avoid label superposition at -180/180 or 0/360
            if lon == central_longitude - 180 or lon == - 180:
                continue

            geoax.text(lon, meridians_label_latitude, lon_label, va='bottom', ha='center',
                       fontsize=size, color='gray',
                       transform=data_crs)


def _init_plotted_vars(spectral_cubes, attribute,
                       attribute_type=float, masked_value=np.nan,
                       projection=None, western_longitudes=True, central_longitude=0):
    """

    :param spectral_cubes:
    :param attribute:
    :param attribute_type:
    :param masked_value:
    :param projection:
    :param western_longitudes:
    :param central_longitude:
    :return:
    """
    # Get latitudes and longitudes of all the spectral cubes
    if not hasattr(spectral_cubes, '__iter__'):
        spectral_cubes = [spectral_cubes]

    spectral_cubes = ObjList(spectral_cubes)

    try:
        latitudes = stack_arrays(spectral_cubes.call('get_from_spectra', 'target.latitude[0]'))
        longitudes = stack_arrays(spectral_cubes.call('get_from_spectra', 'target.longitude[0]'))
    except ValueError as error_message:
        if 'setting an array element with a sequence' in str(error_message):
            warn('latitudes and longitudes may have more than 2 dimensions: using mean values', Warning)
            latitudes = stack_arrays(spectral_cubes.call('get_mean_from_spectra', 'target.latitude'))
            longitudes = stack_arrays(spectral_cubes.call('get_mean_from_spectra', 'target.longitude'))
        else:
            raise error_message

    lat = latitudes.flatten()

    if western_longitudes:
        lon = 180 - longitudes.flatten()
        central_longitude = 180 - central_longitude
    else:
        lon = longitudes.flatten()

    if longitudes.shape != latitudes.shape:
        raise IndexError(f'stacked latitudes and longitudes does not have the same shape: '
                         f'{latitudes.shape} {longitudes.shape}')

    # Projection of the data
    if projection is None:
        ax = plt.axes(projection=ccrs.Mollweide(central_longitude=central_longitude))
    else:
        ax = plt.axes(projection=projection)

    # Get attributes
    if isinstance(attribute, str):
        attributes = []

        for i, spec_cube in enumerate(spectral_cubes):
            attributes.append(
                spec_cube.get_from_spectra(
                    attribute=attribute,
                    attribute_type=attribute_type,
                    masked_value=masked_value
                )
            )

        attributes = stack_arrays(attributes)
    elif isinstance(attribute, list):
        attributes = stack_arrays(attribute)
    else:
        attributes = attribute

    if attributes.shape != latitudes.shape:
        raise IndexError(f'stacked attributes does not have the same shape than stacked latitudes and longitudes:'
                         f'{attributes.shape} {latitudes.shape}')

    return ax, lon, lat, attributes


def hexbin_spectral_cubes_attribute_map(spectral_cubes, attribute,
                                        attribute_type=float, masked_value=np.nan,
                                        projection=None, western_longitudes=True, set_global=True, central_longitude=0,
                                        lat_step=5, lon_step=30, cmap='plasma',
                                        resolution=1080, dpi=300, aspect=16 / 9, size=6, cbar_label='', save=False,
                                        *args, **kwargs):
    """
    Matplotlib hexbin plot of SpectralCubes attribute.
    :param spectral_cubes: SpectralCubes to consider
    :param attribute: attribute to consider
    :param attribute_type: type of the attribute
    :param masked_value: value to mask
    :param projection: map projection
    :param western_longitudes: if True, longitudes are considered as Western longitudes (0-360)
    :param set_global: if True, the whole globe is represented
    :param central_longitude: central longitude of the map
    :param lat_step: steps of the parallels grid
    :param lon_step: steps of the meridians grid
    :param cmap: colormap to represents the data
    :param resolution: (px) resolution of the figure
    :param dpi: dpi of the figure
    :param aspect: aspect ratio of the figure
    :param size: size of the figure labels
    :param cbar_label: label of the colorbar
    :param save: if True, the figure is automatically saved
    :return: cartopy GeoaxesSubplot
    """
    # TODO modify background color

    # Create a matplotlib figure
    plt.close('all')
    plt.figure(figsize=Figure.get_figsize_from_resolution(resolution, dpi, aspect))

    ax, lon, lat, attributes = _init_plotted_vars(
        spectral_cubes, attribute, attribute_type, masked_value, projection, western_longitudes, central_longitude
    )

    # Hexbin plot
    if 'hexbin' in vars(cgeoaxes.GeoAxes):
        hexbin = ax.hexbin(lon, lat, attributes.flatten(), transform=data_crs, cmap=cmap, *args, **kwargs)
    else:  # TODO this is to ensure hexbin to work correctly, it should be removed as soon as cartopy correct this issue
        raise AttributeError(f'hexbin in not supported yet by cartopy v.0.16.0\n\n'
                             f'In order to make hexbin work with cartopy, you must modify the file '
                             f'\'{site.getsitepackages()[0] + os.path.join("", "cartopy", "mpl", "geoaxes.py") }\''
                             f'by adding https://github.com/SciTools/cartopy/issues/884#issuecomment-303474972 '
                             f'to class GeoAxes.\n'
                             f'Alternatively, you can:\n'
                             f' 1. download the file \'Hotfixes.tar.xz\' available at '
                             f'https://gitlab.obspm.fr/dblain/nrt/tree/master/dist;\n'
                             f' 2. decompress the file;\n'
                             f' 3. put the decompressed file \'goeaxes.py\' into the directory '
                             f'\'{site.getsitepackages()[0] + os.path.join("", "cartopy", "mpl") }\'.')

    Figure.plot_nice_colorbar(hexbin, size=size, cbar_label=cbar_label)
    Figure.resize_labels(ax, size=size)
    plt.tight_layout(rect=(0.02, 0, 1, 1))

    # Add grid lines and labels
    # TODO waiting a cartopy update to simplify this !
    if set_global and isinstance(ax, cgeoaxes.GeoAxesSubplot):
        ax.set_global()  # display the whole globe, makes grid lines and labels easier to add
        meridians = np.arange(-180, 181, lon_step)
        parallels = np.arange(-90, 91, lat_step)

        ax.gridlines(crs=data_crs, linestyle='-', xlocs=meridians, ylocs=parallels)
        _add_grid_labels(ax, meridians=meridians, parallels=parallels, size=size,
                         spherical=True, central_longitude=central_longitude, western_longitudes=western_longitudes)

    # Save the figure
    if save:
        Figure.save_fig(prefix='map', main=attribute)
    else:
        plt.show()

    return plt.gca()


def block_plot(latitudes, longitudes, attribute_map):
    """
    [WIP]
    Make a map of the attribute based on latitudes and longitudes of dimension 3 (x, y, 4).
    Each of the 4 points in the last dimension represents the points of a polygon defining the FOV of a pixel.
    :param latitudes:
    :param longitudes:
    :param attribute_map:
    :return:
    """
    for i, attribute_slice in enumerate(attribute_map):
        for j, attribute in enumerate(attribute_slice):
            points = []
            for k in range(latitudes[i, j].size):
                points.append([longitudes[i, j, k], latitudes[i, j, k]])

            cmap = matplotlib.cm.get_cmap('inferno')
            plt.Polygon(points, color=cmap(attribute / 256))
