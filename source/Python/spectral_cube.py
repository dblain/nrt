"""
Spectral cube module.
"""
import copy
import os
from warnings import warn

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import scipy.constants as cst
from scipy.io import readsav
from tqdm import tqdm

from .atmosphere import Atmosphere
from .ephemeris import Ephemeris
from .files import File, PickleFile, Figure
from .math_ import velocity2doppler
from .spectrum import Spectrum, Instrument, Noise, Target
from .utils import info, ObjList
from .version import compact_version, major_version


class SpectralSlice(PickleFile):
    """
    Spectral slice object.
    Contains a 1-D array of spectra as well as useful metadata.
    """
    PATH = File.PATH_DATA + os.path.join('obs', 'exctracted_slices', '')
    EXT = PickleFile.EXT

    def __init__(self, name, path, ext=EXT, exists=False, find=False,
                 start_time=None, stop_time=None, instrument=None, target=None,
                 sub_observer_latitude=None, sub_observer_longitude=None, sub_observer_azimuth=None,
                 sub_solar_latitude=None, sub_solar_longitude=None, sub_solar_azimuth=None,
                 atmosphere=None, wavenumbers=None, spectra=None, noise=None, metadata=None):
        """
        Spectral slice object.
        :param start_time: (ISO 8601, UTC) time at the beginning of observation
        :param stop_time: (ISO 8601, UTC) time at the end of observation
        :param instrument: instrument used for the observations
        :param target: object observed (if any)
        :param sub_observer_latitude: (deg) sub-observer latitude
        :param sub_observer_longitude: (deg) sub-observer longitude
        :param sub_observer_azimuth: (deg) sub-observer azimuth
        :param sub_solar_latitude: (deg) sub-solar latitude
        :param sub_solar_longitude: (deg) sub-solar longitude
        :param sub_solar_azimuth: (deg) sub-solar azimuth
        :param atmosphere: default atmosphere of the spectral slice
        :param wavenumbers: wavenumbers of the spectral slice
        :param spectra: spectra in the spectral slice
        :param noise: noise of the spectral slice
        :param metadata: other metadata
        """
        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        self.start_time = start_time
        self.stop_time = stop_time
        self.instrument = instrument
        self.target = target
        self.sub_observer_latitude = sub_observer_latitude
        self.sub_observer_longitude = sub_observer_longitude
        self.sub_observer_azimuth = sub_observer_azimuth
        self.sub_solar_latitude = sub_solar_latitude
        self.sub_solar_longitude = sub_solar_longitude
        self.sub_solar_azimuth = sub_solar_azimuth
        self.atmosphere = atmosphere
        self.wavenumbers = wavenumbers
        self.spectra = spectra
        self.noise = noise
        self.metadata = metadata

    @classmethod
    def from_radiance_file(cls, radiance_slice_file, metadata_file=None, radiance_per_wavenumber=False,
                           wavenumbers=None, noise=None, order='C', atmosphere=None, **kwargs):
        """
        Load a Spectral slice from a slice file.
        :param radiance_slice_file: (FortranUnformattedFile) file containing the radiances of the spectra in the slice
        :param radiance_per_wavenumber: if True, the radiance is assumed to be a spectral radiance per wavenumber
        :param metadata_file: (tested with PDS3LabelFile) file containing metadata about the slice
        :param wavenumbers: (cm-1) wavenumbers of the spectra of the slice
        :param noise: noise File for the spectra of the slice
        :param order: place the radiance into the spectral slice array using this index order; see numpy.reshape
        :param atmosphere: atmosphere for the spectra in the slice
        :param kwargs: radiance file read arguments
        :return: the SpectralSlice
        """
        radiance_slice = radiance_slice_file.read(**kwargs)

        if metadata_file is not None:
            metadata = metadata_file.read()
        else:
            metadata = {}

        if wavenumbers is not None:
            spectrum_number = int(np.size(radiance_slice) / np.size(wavenumbers))

            if np.size(radiance_slice) % np.size(wavenumbers) != 0:
                raise ValueError(f'cannot reshape spectral slice array of size {np.size(radiance_slice)} '
                                 f'into {int(spectrum_number)} spectra of {np.size(wavenumbers)} wavenumbers')

            shape = (spectrum_number, np.size(wavenumbers))
            radiance_slice = radiance_slice.reshape(shape, order=order)

            if wavenumbers[0] > wavenumbers[-1]:
                wavenumbers = wavenumbers[::-1]  # wavenumbers in increasing order

            # If radiance units is not per wavenumber, it is assumed to be per wavelength
            if not radiance_per_wavenumber:
                radiance_slice = radiance_slice[:, ::-1]  # flip radiances to follow wavenumbers in increasing order

        elif 'rows' in metadata and 'columns' in metadata:
            shape = (metadata.pop('rows'), metadata.pop('columns'))
            radiance_slice = radiance_slice.reshape(shape, order=order)

        if not hasattr(noise, '__iter__'):
            if noise is not None:
                info(f'1 noise will be shared with the {radiance_slice.shape[0]} pixels of the slit')

            noise = np.expand_dims(noise, 0).repeat(radiance_slice.shape[0])  # 1 noise per pixel of the slit
        elif np.size(noise) != radiance_slice.shape[0]:
            raise ValueError(f'number of noises provided ({np.size(noise)}) '
                             f'is not the same than the number of pixel ({radiance_slice.shape[0]})')

        if 'start_time' in metadata:
            start_time = metadata.pop('start_time')
        else:
            start_time = None

        if 'stop_time' in metadata:
            stop_time = metadata.pop('stop_time')
        else:
            stop_time = None

        if 'instrument_name' in metadata:
            instrument_name = metadata.pop('instrument_name').lower().replace(' ', '_').strip()
        else:
            instrument_name = 'unknown'

        if 'instrument_host_name' in metadata:
            instrument_host_name = metadata.pop('instrument_host_name').lower().replace(' ', '_').strip()
        else:
            instrument_host_name = 'unknown'

        if 'target_name' in metadata:
            target = metadata.pop('target_name')

            if target is not None:
                target = target.strip().lower()

                try:
                    target = Target.from_pkl(target)
                except FileNotFoundError as error_message:
                    warn(str(error_message), Warning)
                    target = Target(target)
            else:
                target = Target('None')
        else:
            target = Target('unknown')

        if 'sub_spacecraft_latitude' in metadata:
            sub_observer_latitude = metadata.pop('sub_spacecraft_latitude')
        else:
            sub_observer_latitude = None

        if 'sub_spacecraft_longitude' in metadata:
            sub_observer_longitude = metadata.pop('sub_spacecraft_longitude')
        else:
            sub_observer_longitude = None

        if 'sub_spacecraft_azimuth' in metadata:
            sub_observer_azimuth = metadata.pop('sub_spacecraft_azimuth')
        else:
            sub_observer_azimuth = None

        if 'sub_solar_latitude' in metadata:
            sub_solar_latitude = metadata.pop('sub_solar_latitude')
        else:
            sub_solar_latitude = None

        if 'sub_solar_longitude' in metadata:
            sub_solar_longitude = metadata.pop('sub_solar_longitude')
        else:
            sub_solar_longitude = None

        if 'sub_solar_azimuth' in metadata:
            sub_solar_azimuth = metadata.pop('sub_solar_azimuth')
        else:
            sub_solar_azimuth = None

        if 'rows' in metadata:  # spectrometer case
            instrument_array_shape = (1, metadata.pop('rows'))
        elif 'lines' in metadata and 'line_samples' in metadata:  # imager case
            instrument_array_shape = (metadata.pop('lines'), metadata.pop('line_samples'))
        else:
            instrument_array_shape = None

        instrument = Instrument(name=instrument_name, host_name=instrument_host_name,
                                array_shape=instrument_array_shape)

        if os.path.isfile(instrument.file):
            instrument.load()
        else:
            print(f'Creating new instrument {instrument.name}...')
            instrument.save()

        spectra = []
        for i, radiances in enumerate(radiance_slice):
            spectrum = Spectrum(name=radiance_slice_file.name + f'_{i}', path=Spectrum.PATH_IN,
                                wavenumbers=wavenumbers, radiances=radiances, noise=noise[i],
                                atmosphere=atmosphere, instrument=instrument, target=target,
                                goodness_of_fit=None, location=i, start_time=start_time,
                                stop_time=stop_time, reference='', label='', color='b', linestyle='', marker='+'
                                )
            spectra.append(spectrum)
        spectra = np.asarray(spectra)

        name = f'slice_{target.name}_{instrument.name}_{start_time}-{stop_time}'
        path = SpectralSlice.PATH
        ext = SpectralSlice.EXT

        return cls(name=name, path=path, ext=ext,
                   start_time=start_time, stop_time=stop_time, instrument=instrument, target=target,
                   sub_observer_latitude=sub_observer_latitude, sub_observer_longitude=sub_observer_longitude,
                   sub_observer_azimuth=sub_observer_azimuth,
                   sub_solar_latitude=sub_solar_latitude, sub_solar_longitude=sub_solar_longitude,
                   sub_solar_azimuth=sub_solar_azimuth,
                   atmosphere=atmosphere, wavenumbers=wavenumbers, spectra=spectra, noise=noise, metadata=metadata)


class SpectralCube(PickleFile):
    """
    Spectral cube object.
    Contains a 2-D array of spectra as well as useful metadata. Contains also specific data treatment functions.
    """
    base_name = 'spectral_cube'
    ext_fig = '.png'
    ext_spec = '.dat'
    path_fig = os.path.join(Figure.PATH, 'fig_TEXES', '')
    EXT = PickleFile.EXT
    PATH_DATA = os.path.join(File.PATH_DATA, 'obs', 'spectral_cubes')
    PATH_OUT = os.path.join(File.PATH_OUT, 'spectral_cubes', '')

    __version__ = major_version()

    def __init__(self, name, path, ext=EXT, exists=False, find=False, reference='',
                 start_time=None, stop_time=None, instrument=None, target=None, metakernel=None,
                 sub_observer_latitude=None, sub_observer_longitude=None, sub_observer_azimuth=None,
                 sub_solar_latitude=None, sub_solar_longitude=None, sub_solar_azimuth=None,
                 atmosphere=None, wavenumbers=None, spectra=None, noise=None, _metadata=None):
        """
        Spectral cube object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param exists: c.f. File
        :param find: c.f. File
        :param reference: identification of the spectral cube (if any)
        :param start_time: (yyyy-mm-ddThh:mm:ss.sss, UTC) time at the beginning of observation
        :param stop_time: (yyyy-mm-ddThh:mm:ss.sss, UTC) time at the end of observation
        :param instrument: instrument used for the observation (if any)
        :param target: object observed (if any)
        :param metakernel: SPICE metakernel file corresponding to the SpectralCube
        :param sub_observer_latitude: (deg) sub-observer latitude
        :param sub_observer_longitude: (deg) sub-observer longitude
        :param sub_observer_azimuth: (deg) sub-observer azimuth
        :param sub_solar_latitude: (deg) sub-solar latitude
        :param sub_solar_longitude: (deg) sub-solar longitude
        :param sub_solar_azimuth: (deg) sub-solar azimuth
        :param atmosphere: default atmosphere of the spectral cube
        :param wavenumbers: wavenumbers of the spectral cube
        :param spectra: spectra in the spectral cube
        :param noise: noise of the spectral cube
        :param _metadata: other metadata
        """
        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        self.reference = reference
        self.start_time = start_time
        self.stop_time = stop_time
        self.instrument = instrument
        self.target = target
        self.metakernel = metakernel
        self.sub_observer_latitude = sub_observer_latitude
        self.sub_observer_longitude = sub_observer_longitude
        self.sub_observer_azimuth = sub_observer_azimuth
        self.sub_solar_latitude = sub_solar_latitude
        self.sub_solar_longitude = sub_solar_longitude
        self.sub_solar_azimuth = sub_solar_azimuth
        self.atmosphere = atmosphere

        if wavenumbers is None:
            self.wavenumbers = np.asarray([np.nan])
        else:
            self.wavenumbers = wavenumbers

        if spectra is None:
            self.spectra = np.asarray([[None]])
        else:
            self.spectra = spectra

        self.noise = noise

        if _metadata is None:
            self._metadata = {}
        else:
            self._metadata = _metadata

    def _airmass_fx(self):
        """Recalculate the airmass with in a more precise way."""
        import warnings
        warnings.simplefilter('ignore', RuntimeWarning)  # warnings shows because airm is 0 in some places, causing NaN

        z = np.arccos(1 / self._airmass)
        z[np.where(z > 80 * np.pi / 180)] = 0.

        warnings.simplefilter('default', RuntimeWarning)  # reset warning action

        z[np.where(np.isnan(z))] = 0.
        g = 23.42
        temp0 = 165
        mu_h2 = 2.0158
        mu_he = 4.0026
        mu_ch4 = 16.043
        q_h2 = 0.8630
        q_he = 0.1350
        q_ch4 = 2.0E-03
        m_atm = (mu_h2 * q_h2 + mu_he * q_he + mu_ch4 * q_ch4) * 1E-3 / cst.N_A
        y_atm = cst.k * temp0 / (m_atm * g)

        radius_jup = 69911E3

        s = np.sqrt((radius_jup + y_atm) ** 2 - radius_jup ** 2 * np.sin(z) ** 2) - radius_jup * np.cos(z)
        self._airmass = s / y_atm

    def _initialize_feature_map(self, attribute_shape=None, attribute_type=float, masked_value=np.nan):
        """
        Initialize the array that will contain the feature map.
        :param attribute_shape: shape of the attribute
        :param attribute_type: type of the attribute
        :param masked_value: value to mask in feature map
        :type attribute_type: type
        :return: array
        """
        if attribute_shape is not None:
            shape_ = list(np.shape(self.spectra))
            shape_.extend(attribute_shape)
            feature_map = np.full(tuple(shape_), np.nan, dtype=attribute_type)

            if not np.isfinite(masked_value) and attribute_type != object:
                feature_map = np.ma.masked_invalid(feature_map)
            else:
                feature_map = np.ma.masked_values(feature_map, masked_value)
        else:
            if attribute_type == int or attribute_type == float or attribute_type == complex:
                feature_map = np.full(np.shape(self.spectra), np.nan, dtype=attribute_type)
            else:
                feature_map = np.empty(np.shape(self.spectra), dtype=attribute_type)

        return feature_map

    def auto_rename(self, nickname=''):
        """
        Rename the SpectralCube based on its metadata.
        :param nickname: string added at the end of the name, a '_' will be added at the beginning of the string
        """
        self.name = SpectralCube.auto_name(self.instrument.name, self.target, self.start_time, self.wavenumbers,
                                           nickname)
        self.update_file()

    def get_figure_directory(self, figure_type='', nickname=''):
        """
        Get a figure directory based on the instrument of the SpectralCube and the given figure type.
        :param figure_type: the type of the figure
        :param nickname: string added at the end of the directory, a '_' will be added at the beginning of the string
        :return:
        """
        if nickname != '' and nickname[0] != '_':
            nickname = '_' + nickname

        fig_dir = Figure.PATH
        if self.instrument is not None:
            fig_dir = os.path.join(fig_dir, self.instrument.name)

            if figure_type != '':
                fig_dir = os.path.join(fig_dir, figure_type)

            if self.instrument.name in self.name:
                fig_dir = os.path.join(
                    fig_dir, self.name.split(self.instrument.name + '_')[1])
            else:
                fig_dir = os.path.join(fig_dir, self.name)

            fig_dir += nickname
        else:
            if nickname == '':
                fig_dir = os.path.join(fig_dir, 'divers')
            else:
                fig_dir = os.path.join(fig_dir, nickname)

            if figure_type != '':
                fig_dir = os.path.join(fig_dir, figure_type)

        return fig_dir

    def get_from_spectra(self, attribute, attribute_shape=None, attribute_type=float, masked_value=np.nan):
        """
        Extract a feature map from a spectral map.
        :param attribute: name of the feature to extract
        :param attribute_shape: expected shape of the attribute to extract; if None, no shape is assumed
        :param attribute_type: expected type of the attribute to extract
        :param masked_value: value to mask
        :type attribute_type: type
        :type attribute_shape: tuple
        :return: the map of the specified feature
        """
        feature_map = self._initialize_feature_map(attribute_shape, attribute_type, masked_value)

        flat_spectrum_map = self.spectra.flatten()
        for spectrum in flat_spectrum_map:
            if spectrum is not None:
                x = spectrum.location[0]
                y = spectrum.location[1]

                if isinstance(ObjList.getsubattr(spectrum, attribute), dict):
                    attribute_values = list(ObjList.getsubattr(spectrum, attribute).values())
                    if len(attribute_values) >= 1:
                        feature_map[x, y] = attribute_values[0]
                else:
                    try:
                        feature_map[x, y] = ObjList.getsubattr(spectrum, attribute)
                    except ValueError as error_message:
                        if str(error_message) == 'setting an array element with a sequence.':
                            raise ValueError(f'setting an array element with a sequence: '
                                             f'attribute \'{attribute}\' may have more than 2 dimensions, '
                                             f'using parameter attribute_shape may resolve the issue')
                        else:
                            raise error_message

        return feature_map

    def get_mean_from_spectra(self, attribute, attribute_type=float, masked_value=np.nan):
        """
        Extract the mean of a feature map from a spectral map.
        :param attribute: name of the feature to extract
        :param attribute_type: expected type of the attribute to extract
        :param masked_value: value to mask
        :type attribute_type: type
        :return: the map of the specified feature
        """
        feature_map = self._initialize_feature_map(
            attribute_shape=None,
            attribute_type=attribute_type,
            masked_value=masked_value
        )

        flat_spectrum_map = self.spectra.flatten()
        for spectrum in flat_spectrum_map:
            if spectrum is not None:
                x = spectrum.location[0]
                y = spectrum.location[1]

                if isinstance(ObjList.getsubattr(spectrum, attribute), dict):
                    attribute_values = list(ObjList.getsubattr(spectrum, attribute).values())
                    if len(attribute_values) >= 1:
                        feature_map[x, y] = attribute_values[0]
                else:
                    feature_map[x, y] = np.mean(ObjList.getsubattr(spectrum, attribute))

        return feature_map

    def plot_feature_map(self, feature_map, attribute='', cmap='plasma', aspect='auto', norm=None,
                         vmin=None, vmax=None, cbar_label='', title=None,
                         lat_lon_overlay=False, size=1,
                         save=False, fig_dir='', fig_name='', fmt='png', figsize=(6.5, 5), dpi=80):
        """
        :param feature_map:
        :param attribute:
        :param cmap: the colormap of the figure
        :param aspect: aspect ratio of the figures
        :param norm: the normalisation of the values on the figures
        :param vmin: minimum value of the colormap
        :param vmax: maximum value of the colormap
        :param cbar_label: label of the colorbar
        :param title: title of the figure
        :param lat_lon_overlay: if True, add longitude and latitude overlay on the figure
        :param size: size of the elements on the figure
        :param save: if True, the figure will be saved
        :param fig_dir: directory on which to save the figure
        :param fig_name: name of the figure
        :param fmt: format of the figure
        :param figsize: figure size
        :param dpi: resolution of the figure
        :return:
        """
        if fig_name == '':
            fig_name = 'map_' + attribute

        # Handle arrays of latitudes and longitudes (for example a pixel may have one latitude/longitude per corner)
        if lat_lon_overlay:
            try:
                latitudes = self.get_from_spectra('latitude')
                longitudes = self.get_from_spectra('longitude')
            except ValueError as error_message:
                if str(error_message) == 'setting an array element with a sequence.':
                    warn('latitudes and longitudes may have more than 2 dimensions: using mean values', Warning)
                    latitudes = self.get_mean_from_spectra('latitude')
                    longitudes = self.get_mean_from_spectra('longitude')
                else:
                    raise error_message
        else:
            latitudes = None
            longitudes = None

        if np.ndim(feature_map) > 2:
            attribute_map = np.mean(feature_map, 2)
        else:
            attribute_map = feature_map

        SpectralCube.plot_map(fig_name=fig_name, attribute_map=attribute_map, cmap=cmap, vmin=vmin, vmax=vmax,
                              title=title, cbar_label=cbar_label, size=size, aspect=aspect, norm=norm, save=save,
                              fig_dir=fig_dir, latitudes=latitudes, longitudes=longitudes,
                              fmt=fmt, figsize=figsize, dpi=dpi)

    def plot_all_maps(self, spec_map=None, cmap='plasma', aspect='auto', norm=None,
                      molecules=None, atm_level=11, lat_lon_overlay=False, fig_dir='', nickname='', size=1):
        """
        Plot the radiance map, the chi2 map, the cloud map and the molecule abundance maps at the specified atmospheric
        level of all the molecules specified in molecules of the specified spectral map.
        :param spec_map: the spectral map whose the maps will be extracted
        :param cmap: the colormap used for the figures
        :param aspect: aspect ratio of the figures
        :param norm: the normalisation of the values on the figures
        :param molecules: the molecules whose the maps will be plotted
        :param atm_level: the atmospheric level from which to take the molecular abundances values
        :param lat_lon_overlay: if True, add longitude and latitude overlay on the figures
        :param fig_dir: the figures directory
        :param nickname: the nickname of the figures
        :param size: the size of the elements of the figures
        """
        if spec_map is None:
            spec_map = self.spectra

        if molecules is None and self.atmosphere.molecules is not None:
            molecules = []
            for mol in self.atmosphere.molecules:
                if self.atmosphere.molecules[mol].retrieve:
                    molecules.append(mol)

        if fig_dir == '':
            fig_dir = self.get_figure_directory(figure_type='maps')

        if nickname != '' and nickname[0] != '_':
            nickname = '_' + nickname

        rad_map = self.get_from_spectra(attribute='radiances', attribute_shape=np.shape(self.wavenumbers))
        self.plot_feature_map(
            rad_map,
            fig_name=f'radiance_{compact_version()}{nickname}',
            cmap=cmap,
            size=size,
            title=None,
            cbar_label=rf'Mean radiance ({Spectrum.RADIANCE_UNITS})',
            aspect=aspect,
            save=True,
            fig_dir=fig_dir,
            lat_lon_overlay=lat_lon_overlay
        )

        chi2_map = self.get_from_spectra(attribute='chi2')
        self.plot_feature_map(
            chi2_map,
            fig_name=f'chi2_{compact_version()}{nickname}',
            cmap=cmap,
            vmin=0.1,
            vmax=20,
            size=size,
            title=None,
            cbar_label=r'$\chi^2$/n',
            aspect=aspect,
            save=True,
            fig_dir=fig_dir,
            lat_lon_overlay=lat_lon_overlay
        )

        cld_map = self.get_from_spectra(attribute='atmosphere.clouds[0].transmittance')
        self.plot_feature_map(
            cld_map,
            fig_name=f'cloud_{compact_version()}{nickname}',
            cmap=cmap,
            vmin=0.001,
            vmax=0.3,
            size=size,
            title=None,
            cbar_label=r'Cloud transmittance',
            aspect=aspect,
            save=True,
            fig_dir=fig_dir,
            lat_lon_overlay=lat_lon_overlay
        )
        mol_maps = []
        if molecules is not None:
            for mol in molecules:
                cbar_label = f'[{mol}] at {self.atmosphere.molecules[mol].vmr_profile.pressure[atm_level]} bar'

                array = np.asarray(
                    ObjList([x for x in np.asarray(spec_map).flatten() if x is not None]).getsubattr_list(
                        f'atmosphere.molecules[{mol}].vmr_profile.vmr[{atm_level}]'))

                for i, array_is_nan in enumerate(np.isnan(array)):
                    if array_is_nan:
                        array[i] = 0.0
                        warn(f'nan in abundance profile of molecule {mol} at level {atm_level} of spectrum {i}',
                             Warning)

                vmax = np.min((
                    np.max(array[np.where(array < 10 * np.median(array))]),
                    np.max(array[np.where(array < np.percentile(array, 97))])
                ))
                vmin = np.min(array[np.where(array > 0.01 * np.median(array))])

                if 10 * vmin < vmax:
                    norm_mol = norm
                else:
                    norm_mol = None

                mol_map = self.get_from_spectra(attribute=f'atmosphere.molecules[{mol}].vmr_profile.vmr[{atm_level}]')
                self.plot_feature_map(
                    mol_map,
                    fig_name=f'{mol}_{compact_version()}_lvl{atm_level}{nickname}',
                    cmap=cmap,
                    vmin=vmin,
                    vmax=vmax,
                    size=size,
                    title=None,
                    cbar_label=cbar_label,
                    aspect=aspect,
                    norm=norm_mol,
                    save=True,
                    fig_dir=fig_dir,
                    lat_lon_overlay=lat_lon_overlay
                )
                mol_maps.append(mol_map)

        return rad_map, chi2_map, cld_map, mol_maps

    def quick_stats(self):
        """Display some basic stats on the spectral map of this SpectralCube."""
        for spectrum in np.asarray(self.spectra).flatten():
            if spectrum is not None:
                print(f'wavenumber range: '
                      f'{np.floor(np.min(spectrum.wavenumbers))}-{np.ceil(np.max(spectrum.wavenumbers))} cm-1')
                break
        print(f'size (px): {np.shape(self.spectra)}')
        lat = np.ma.masked_invalid(self.get_from_spectra(attribute='latitude'))
        lon = np.ma.masked_invalid(self.get_from_spectra(attribute='longitude'))
        print(f'latitude range: {np.floor(np.min(lat[np.where(lat != 0)]))}-'
              f'{np.ceil(np.max(lat[np.where(lat != 0)]))} deg')
        print(f'longitude range: {np.floor(np.min(lon[np.where(lon != 0)]))}-'
              f'{np.ceil(np.max(lon[np.where(lon != 0)]))} deg')
        cld = self.get_from_spectra(attribute='atmosphere.clouds[0].transmittance')
        print(f'retrieved spectra: {np.size(np.where(np.asarray(cld).flatten() > 0.0))}')

    def retrieve(self, cloud_base_pressure=1.0, cloud_transmittance_apriori=0.150, cloud_reflectance=0.15,
                 molecules_to_retrieve=None, emission_angle_max=90.0, nickname='', sc=None, overwrite=False,
                 compile_nrt=False, write_cross_sections_files=False, plot=False, **kwargs):
        """
        Retrieve all the spectra in spectral_map using the given parameters.
        :param cloud_base_pressure: a priori base pressure of the cloud used in the retrieval
        :param cloud_transmittance_apriori: a priori transmittance for the cloud used in the retrieval
        :param cloud_reflectance: reflectance of the cloud used in the retrieval
        :param molecules_to_retrieve: molecule to retrieve
        :param emission_angle_max: spectra with a emission angle higher than this will be ignored
        :param nickname: string added at the end of the name, a '_' will be added at the beginning of the string
        :param sc: reference spectral cube, used to determine a spectra-by-spectra cloud a priori
        :param overwrite: if True, overwrite the saved retrievals, hence rerunning the retrieval from the beginning
        :param compile_nrt: c.f. spectrum.retrieve()
        :param write_cross_sections_files: c.f. spectrum.retrieve()
        :param plot: if True, a figure of each retrieved spectra is saved
        :param kwargs: retrieval arguments, c.f. run
        :return: the retrieved SpectralCube
        """
        if nickname != '' and nickname[0] != '_':
            nickname = '_' + nickname

        if molecules_to_retrieve is None:
            molecules_to_retrieve = []
            nickname = '_cloud-only' + nickname

        sc_ret = SpectralCube(self.name + nickname, SpectralCube.PATH_OUT, instrument=copy.deepcopy(self.instrument),
                              reference=self.reference, start_time=self.start_time, stop_time=self.stop_time,
                              target=self.target, metakernel=copy.deepcopy(self.metakernel),
                              sub_observer_latitude=self.sub_observer_latitude,
                              sub_observer_longitude=self.sub_observer_longitude,
                              sub_observer_azimuth=self.sub_observer_azimuth,
                              sub_solar_latitude=self.sub_solar_latitude, sub_solar_longitude=self.sub_solar_longitude,
                              sub_solar_azimuth=self.sub_solar_azimuth,
                              atmosphere=None, wavenumbers=None, spectra=None, noise=None,
                              _metadata=copy.deepcopy(self._metadata))
        # sc_ret._metadata['retrieval_noise'] = copy.deepcopy(self.noise.radiances)
        specs_bf = ObjList()

        if sc is not None:
            sc_ret.atmosphere = copy.deepcopy(sc.atmosphere)
            molecules = copy.deepcopy(sc.atmosphere.molecules)
            for molecule in molecules_to_retrieve:
                if molecule in molecules:
                    molecules[molecule].retrieve = True
                else:
                    molecules[molecule].retrieve = False
        else:
            molecules = copy.deepcopy(self.atmosphere.molecules)
            sc_ret.atmosphere = copy.deepcopy(self.atmosphere)
            sc_ret.atmosphere.clouds[0].transmitance = cloud_transmittance_apriori
            sc_ret.atmosphere.clouds[0].reflectance = cloud_reflectance
            sc_ret.atmosphere.clouds[0].pressure_base = cloud_base_pressure
            sc_ret.atmosphere.clouds[0].pressure_top = cloud_base_pressure

        spectrum_map = self.spectra

        for retrieved_spectrum in tqdm(spectrum_map.flatten()):
            if retrieved_spectrum is not None:

                if spectrum_map[retrieved_spectrum.location[0]][retrieved_spectrum.location[1]] is not None:
                    if retrieved_spectrum.target.emission_angle > emission_angle_max:
                        continue
                    elif np.isnan(retrieved_spectrum.target.emission_angle):
                        continue

                    if sc is not None:
                        if retrieved_spectrum.location[0] <= list(np.shape(sc.spectra))[0] - 1 and \
                                retrieved_spectrum.location[1] <= list(np.shape(sc.spectra))[1] - 1:
                            if sc.spectra[retrieved_spectrum.location[0]][retrieved_spectrum.location[1]] is None:
                                continue

                            if sc.spectra[
                                retrieved_spectrum.location[0]][retrieved_spectrum.location[1]
                                                                ].atmosphere.clouds[0].transmittance <= 0:
                                continue

                            retrieved_spectrum.atmosphere.clouds = copy.deepcopy(
                                sc.spectra[retrieved_spectrum.location[0]][
                                    retrieved_spectrum.location[1]].atmosphere.clouds)
                        else:
                            continue
                    else:
                        retrieved_spectrum.atmosphere.clouds[0].transmittance = cloud_transmittance_apriori
                        retrieved_spectrum.atmosphere.clouds[0].reflectance = cloud_reflectance
                        retrieved_spectrum.atmosphere.clouds[0].pressure_base = cloud_base_pressure
                        retrieved_spectrum.atmosphere.clouds[0].pressure_top = cloud_base_pressure

                    retrieved_spectrum.atmosphere.molecules = molecules
                    retrieved_spectrum.color = 'b'
                    retrieved_spectrum.linestyle = ''
                    retrieved_spectrum.marker = '+'

                    specs_bf.append(retrieved_spectrum.retrieve(
                        overwrite=overwrite, compile_nrt=compile_nrt,
                        write_cross_sections_files=write_cross_sections_files,
                        nickname=nickname, **kwargs))
                    specs_bf[-1].atmosphere.sky = sc_ret.atmosphere.sky

                    compile_nrt = False
                    write_cross_sections_files = False

                    fig_dir = specs_bf[-1].get_fig_dir(retrieved=True)
                    if plot and \
                            (not os.path.isfile(os.path.join(fig_dir, 'inv_' + specs_bf[-1].name) + '.png') or
                             overwrite):
                        Spectrum.errorbar(spectra=[retrieved_spectrum, specs_bf[-1]],
                                          plot_vmr_profiles=True,
                                          plot_vmr_apriori=True,
                                          plot_residuals=True, interp_for_plot=True,
                                          save=True, fig_name='inv_' + specs_bf[-1].name,
                                          fig_dir=fig_dir, size=18, line_widths=2)

        sc_ret.spectra = SpectralCube.make_spec_map(specs_bf)
        sc_ret.wavenumbers = sc_ret.get_from_spectra('wavenumbers', attribute_type=object)
        for wavenumbers in sc_ret.wavenumbers.flatten():
            if np.size(wavenumbers) > 1:
                if None not in list(wavenumbers):
                    sc_ret.wavenumbers = wavenumbers
                    break

        sc_ret.save()
        # fig_dir = self.get_figure_directory(figure_type='maps')
        # nickname = SpectralCube.auto_name(
        #     sc_ret.instrument.name,
        #     sc_ret.target,
        #     sc_ret.start_time,
        #     sc_ret.wavenumbers
        # )
        # sc_ret.plot_all_maps(fig_dir=fig_dir, nickname=nickname, norm=matplotlib.colors.LogNorm(),
        #                      molecules=molecules_to_retrieve, lat_lon_overlay=True)

        return sc_ret

    def retrieve_accurate(self, cloud_base_pressure=1.0, cloud_transmittance_apriori=0.150, cloud_reflectance=0.15,
                          molecules_to_retrieve=None, emission_angle_max=90.0, nickname='', overwrite=False,
                          compile_nrt=False, write_cross_sections_files=False, verbose=True,
                          weight_apriori_cloud_only=5E-1, weight_apriori_final=1E-1, plot_cloud_only=False,
                          plot_final=True, **kwargs):
        """
        Retrieve the SpectralCube two times:
        1. Only the cloud is retrieved, hence giving a good a priori for each spectra.
        2. The cloud and the molecule abundances are retrieved, using the cloud parameter previously retrieved.
        This methodology limit the risk of being trapped in a local minimum during the retrieval, but assumes that the
        clouds are the main contributor to radiances.
        :param cloud_base_pressure: c.f. retrieve
        :param cloud_transmittance_apriori: c.f. retrieve
        :param cloud_reflectance: c.f. retrieve
        :param molecules_to_retrieve: c.f. retrieve
        :param emission_angle_max: c.f. retrieve
        :param nickname: c.f. retrieve
        :param overwrite: c.f. retrieve
        :param compile_nrt: c.f. retrieve
        :param write_cross_sections_files: c.f. retrieve
        :param verbose: if True, a message will be raised if a spectrum has already been retrieved
        :param weight_apriori_cloud_only: weight of the a priori for the cloud-only retrieval
        :param weight_apriori_final: weight of the a priori for the final retrieval
        :param plot_cloud_only: if True, plot the spectra obtained in the cloud-only retrieval
        :param plot_final: if True, plot the spectra obtained in the final retrieval
        :param kwargs: retrieval parameters, c.f. Spectrum.retrieve()
        :return: the retrieved SpectralCube
        """
        if nickname != '' and nickname[0] != '_':
            nickname = '_' + nickname

        sc_cloud = self.retrieve(cloud_base_pressure=cloud_base_pressure,
                                 cloud_transmittance_apriori=cloud_transmittance_apriori,
                                 cloud_reflectance=cloud_reflectance,
                                 emission_angle_max=emission_angle_max, nickname=nickname,
                                 overwrite=overwrite, weight_apriori=weight_apriori_cloud_only,
                                 compile_nrt=compile_nrt,
                                 write_cross_sections_files=write_cross_sections_files,
                                 verbose=verbose, plot=plot_cloud_only, **kwargs)

        if molecules_to_retrieve is not None:
            retrieved_spectral_cube = self.retrieve(sc=sc_cloud, molecules_to_retrieve=molecules_to_retrieve,
                                                    emission_angle_max=emission_angle_max, nickname=nickname,
                                                    overwrite=overwrite, weight_apriori=weight_apriori_final,
                                                    verbose=verbose, plot=plot_final, **kwargs)
        else:
            retrieved_spectral_cube = sc_cloud

        return retrieved_spectral_cube

    def save_in_dict(self):
        """
        Save the spectral cube into a dictionary.
        Useful for data sharing.
        """
        # TODO finish this
        key_dict = {
            'start_time': '(yyyy-mm-ddThh:mm:ss.sss, UTC) time at the beginning of observation',
            'stop_time': '(yyyy-mm-ddThh:mm:ss.sss, UTC) time at the end of observation',
            'instrument': 'instrument used to acquire the spectral cube',
            'instrument_host': 'name of the instrument host (telescope, spacecraft, ...)',
            'instrument_slit_resolution': '(arcsec) resolution of the instrument slit',
            'instrument_platescale': '(arcsec) inter-pixel distance along the instrument slit',
            'instrument_slit_step': '(arcsec) step between two slits positions, '
                                    'i.e. inter-pixel distance across the slits',
            'instrument_lsf_fwhm': '(cm-1) full width half maximum of the instrument function (estimation)',
            'instrument_lsf_shape': 'shape of the instrument function (estimation)',
            'target': 'celestial object observed',
            'latitude_map': '(deg) planetocentric latitude of the spectra',
            'longitude_map': '(deg) western longitude in System III of the spectra',
            'emission_angle_map': '(deg) emission angle of the spectra, calculated using airmass',
            'airmass_map': 'path length for light relative to that at the zenith at 1 bar, for each spectrum',
            'incidence_angle_map': '(deg) local solar incidence angle of the spectra, '
                                   'calculated using sub solar latitude and longitude',
            'sub_solar_latitude': '(deg) latitude of the sub solar point',
            'sub_solar_longitude': '(deg) longitude of the sub solar point',
            'radiances_map': '(W.cm-2.sr-1/cm-1) radiances corrected of the Doppler shift, '
                             'not divided by the sky transmittances measured by the instrument',
            'radiances_map_raw': '(W.cm-2.sr-1/cm-1) radiances without Doppler correction and '
                                 'divided by the sky transmittances measured by the instrument',
            'wavenumbers': '(cm-1) wavenumbers of the spectra in the reference frame of the target'
                           '(corrected of Doppler shift); keys noise and radiances_map use these wavenumbers',
            'wavenumbers_raw': '(cm-1) wavenumbers without correction; all *_raw keys use these wavenumbers',
            'sky_transmittances': 'Earth atmosphere synthetic transmittances at the time of observation (estimation)',
            'sky_wavenumbers': '(cm-1) wavenumbers of the Earth atmosphere synthetic transmittances, in the'
                               'reference frame of the Earth (no Doppler shift)',
            'sky_transmittances_raw': 'sky transmittances as measured by the instrument (usage not recommended)',
            'noise': '(W.cm-2.sr-1/cm-1) noise-equivalent spectral radiance of the spectra (estimation)',
            'doppler_shift_map': '(cm-1) Doppler shifts of each spectrum, calculated using the velocity map',
            'velocity_map': '(m.s-1) velocities between the target and the instrument, for each spectrum',
            'docstring': 'this docstring'
        }

        spectral_cube_dict = dict.fromkeys(list(key_dict.keys()))

        spectral_cube_dict['start_time'] = self.start_time
        spectral_cube_dict['stop_time'] = self.stop_time
        spectral_cube_dict['instrument'] = self.instrument.name
        spectral_cube_dict['instrument_host'] = self.instrument.host_name
        spectral_cube_dict['instrument_pixel_resolution'] = self.instrument.pixel_resolution
        spectral_cube_dict['instrument_lsf_fwhm'] = self.instrument.lsf_fwhm
        spectral_cube_dict['instrument_lsf_shape'] = self.instrument.lsf_shape
        spectral_cube_dict['target'] = self.target
        spectral_cube_dict['latitude_map'] = self.get_from_spectra('latitude')
        spectral_cube_dict['longitude_map'] = self.get_from_spectra('longitude')
        spectral_cube_dict['emission_angle_map'] = self.get_from_spectra('emission_angle')
        spectral_cube_dict['airmass_map'] = self._metadata['airmass_map']
        spectral_cube_dict['incidence_angle_map'] = self.get_from_spectra('incidence_angle')
        spectral_cube_dict['sub_solar_latitude'] = self.sub_solar_latitude
        spectral_cube_dict['sub_solar_longitude'] = self.sub_solar_longitude
        spectral_cube_dict['radiances_map'] = self.get_from_spectra('radiances')
        spectral_cube_dict['radiances_map_raw'] = self._metadata['radiances_map_raw']
        spectral_cube_dict['wavenumbers'] = self.wavenumbers
        spectral_cube_dict['wavenumbers_raw'] = self._metadata['wavenumbers_raw']
        spectral_cube_dict['sky_transmittances'] = self.atmosphere.sky.transmittances
        spectral_cube_dict['sky_wavenumbers'] = self.atmosphere.sky.wavenumbers
        spectral_cube_dict['sky_transmittances_raw'] = self._metadata['sky_transmittances_raw']
        spectral_cube_dict['noise'] = self.noise.radiances
        spectral_cube_dict['doppler_shift_map'] = self.get_from_spectra('doppler_shift')
        spectral_cube_dict['velocity_map'] = self._metadata['velocity_map']
        spectral_cube_dict['docstring'] = ''

    def signal_noise_ratio(self):
        """Calculate the signal to noise ratio of all the spectra in spec_map."""
        length_rad = np.size(np.asarray(self.spectra)[np.where(np.asarray(self.spectra) != [None])][0].radw)
        radiances = np.zeros((list(np.shape(self.spectra))[0], list(np.shape(self.spectra))[1], length_rad))
        stderr_arr = np.zeros((list(np.shape(self.spectra))[0], list(np.shape(self.spectra))[1], length_rad))

        for i, row in enumerate(self.spectra):
            for j, spec in enumerate(row):
                if spec is not None and isinstance(spec, Spectrum):
                    radiances[i, j] = spec.radiances
                    stderr_arr[i, j] = spec.noise.radw
                else:
                    radiances[i, j] = np.zeros(length_rad)
                    stderr_arr[i, j] = np.ones(length_rad)

        return radiances / stderr_arr

    def ppmv2vmr(self):
        """
        Convert the abundances of all the spectra in spectral_map from Part Per Million Volume to Volume Mixing Ratio.
        """
        for spec in self.spectra.flatten():
            if spec is not None:
                self.spectra[
                    spec.location[0]][
                    spec.location[1]].atmosphere.molecules['NH3'].vmr_profile.ppmv2vmr()

    def latitude_mean(self, feature, lat_min=-90, lat_max=90, lat_bin=1, lon_min=0, lon_max=360,
                      zero_to_mask=True):
        """
        Give a pressure-latitude matrix of a spectrum_map feature.
        All the feature with the same latitude and longitude bins are averaged.
        :param feature: the feature to represent
        :param lat_min: (deg) minimum latitude to consider
        :param lat_max: (deg) maximum latitude to consider
        :param lat_bin: (deg) latitude step of the latitude interval
        :param lon_min: (deg) minimum longitude to consider
        :param lon_max: (deg) maximum longitude to consider
        :param zero_to_mask: if True, mask all the zeros in the feature slice
        :return:
        """
        lat_range = np.arange(lat_min, lat_max, lat_bin)
        feature_lat = np.zeros(len(lat_range))

        latitude_map = self.get_from_spectra('latitude')
        longitude_map = self.get_from_spectra('longitude')

        specs = np.asarray(self.spectra).flatten()
        wh_specs_is_not_none = np.where(np.not_equal(specs, [None]))
        longitudes = np.asarray(longitude_map).flatten()[wh_specs_is_not_none]
        wh_longitudes_in_range = np.where(np.logical_and(longitudes > lon_min, longitudes < lon_max))
        lats = np.asarray(latitude_map).flatten()[wh_specs_is_not_none][wh_longitudes_in_range]
        specs = specs[wh_specs_is_not_none][wh_longitudes_in_range]

        for j, lat in enumerate(lat_range):
            results = np.asarray(ObjList(
                specs[np.where(np.logical_and(lats >= lat, lats < lat + lat_bin))]).getsubattr_list(feature))
            if np.size(results) > 0:
                feature_lat[j] = np.mean(results)
            else:
                feature_lat[j] = 0

        if zero_to_mask:
            feature_lat = np.ma.masked_values(feature_lat, 0.0)

        return feature_lat

    def pressure_latitude_slice(self, feature2d, lat_min=-90, lat_max=90, lat_bin=1, lon_min=0, lon_max=360,
                                plot=False, cmap='RdBu_r', xlim=None, ylim=None, cbar_label='',
                                save=False, fig_dir='', fig_name='', nickname='', fmt='png', figsize=(6.5, 5), dpi=80,
                                font_size=12, **kwargs):
        """
        Give a pressure-latitude matrix of a spectrum_map feature.
        All the feature with the same latitude and longitude bins are averaged.
        :param feature2d: the feature to represent
        :param lat_min: (deg) minimum latitude to consider
        :param lat_max: (deg) maximum latitude to consider
        :param lat_bin: (deg) latitude step of the latitude interval
        :param lon_min: (deg) minimum longitude to consider
        :param lon_max: (deg) maximum longitude to consider
        :param plot: if True, plot the slice with the contourf method
        :param cmap: c.f. contourf
        :param xlim: c.f. contourf
        :param ylim: c.f. contourf
        :param cbar_label: c.f. contourf
        :param save: If True, save the obtained figure
        :param fig_dir: directory in which to save the figure
        :param fig_name: name of the figure
        :param nickname: string added at the end of the name, a '_' will be added at the beginning of the string
        :param fmt: format of the figure
        :param figsize: size of the figure
        :param dpi: dpi of the figure
        :param font_size: font size of all the figure elements
        :param kwargs: contourf arguments
        :return:
        """
        lat_range = np.arange(lat_min, lat_max, lat_bin)
        feature_slice = np.zeros((len(self.atmosphere.temperature_profile.pressure), len(lat_range)))

        latitude_map = self.get_from_spectra('latitude')
        longitude_map = self.get_from_spectra('longitude')

        specs = np.asarray(self.spectra).flatten()
        wh_specs_is_not_none = np.where(np.not_equal(specs, [None]))
        longitudes = np.asarray(longitude_map).flatten()[wh_specs_is_not_none]
        wh_longitudes_in_range = np.where(np.logical_and(longitudes > lon_min, longitudes < lon_max))
        lats = np.asarray(latitude_map).flatten()[wh_specs_is_not_none][wh_longitudes_in_range]

        specs = specs[wh_specs_is_not_none][wh_longitudes_in_range]

        for i in range(len(self.atmosphere.temperature_profile.pressure)):
            for j, lat in enumerate(lat_range):
                results = np.asarray(ObjList(
                    specs[np.where(np.logical_and(lats >= lat, lats < lat + lat_bin))]).getsubattr_list(feature2d))
                if np.size(results) > 0:
                    feature_slice[i, j] = np.mean(results[:, i])
                else:
                    feature_slice[i, j] = np.nan

        feature_slice = np.ma.masked_invalid(feature_slice)

        if plot:
            plt.close('all')
            plt.figure(figsize=figsize)
            ax = plt.gca()

            im = plt.contourf(lat_range, self.atmosphere.temperature_profile.pressure, feature_slice, cmap=cmap,
                              interpolation='none', **kwargs)

            ax.invert_yaxis()
            plt.semilogy()

            if xlim is None:
                xlim = [None, None]
            if ylim is None:
                ylim = [None, None]

            plt.xlim(xlim)
            plt.ylim(ylim)

            plt.xlabel(r'Planetocentric latitude (degree)')
            plt.ylabel(r'Pressure (bar)')

            cbar_format = ticker.ScalarFormatter(useMathText=True, useOffset=True)
            cbar_format.set_powerlimits((-3, 3))

            cbar = plt.colorbar(im, format=cbar_format)

            cbar.ax.tick_params(labelsize=font_size)

            cbar.set_label(cbar_label, size=font_size)

            for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(font_size)

            plt.tight_layout()

            if save:
                fig_name = fig_name + nickname
                if fig_dir == '':
                    fig_dir = os.path.join(Figure.PATH, 'divers', 'maps')

                if not os.path.isdir(fig_dir):
                    os.makedirs(fig_dir)

                plt.savefig(os.path.join(fig_dir, fig_name + '.' + fmt),
                            dpi=dpi, format=fmt)
                print(f'Figure saved in \'{os.path.join(fig_dir, fig_name)}\'...')
            else:
                plt.show()

        return feature_slice

    def vmr2ppmv(self):
        """
        Convert the abundances of all the spectra in spectral_map from Volume Mixing Ratio to Part Per Million Volume.
        """
        for spec in self.spectra.flatten():
            if spec is not None:
                self.spectra[
                    spec.location[0]][
                    spec.location[1]].atmosphere.molecules['NH3'].vmr_profile.vmr2ppmv()

    @classmethod
    def from_sav(cls, name, path=PATH_DATA, noise_corner_boxes_size=5, nickname='', **kwargs):
        """
        Load a SpectralCube from a IDL sav file.
        :param name: name of the sav file
        :param path: path of the sav file
        :param noise_corner_boxes_size: (specific) if > 0, calculate the noise using the standard deviation of the
        radiances of the spectra in the four size x size px square at the corners of the cube
        :param nickname: nickname of the SpectralCube
        :param kwargs: (specific) SpectralCube._sav2pkl arguments
        :return: the SpectralCube
        """
        file = File(name=name, path=path, ext='sav', find=True).file
        dictionary = readsav(file)

        instrument = None
        atmosphere = Atmosphere()
        date = None
        time = None
        observation_time = None
        start_time = None
        stop_time = None
        radiance_unit_factor = 1.0
        reference = ''
        target = Target('unknown')

        if 'hdr' in dictionary:
            if hasattr(dictionary['hdr'], '__iter__'):
                if isinstance(dictionary['hdr'][0], type(bytes())):
                    for bytes_str in dictionary['hdr']:
                        string = bytes_str.decode('UTF-8')

                        if 'filename' in string and '=' in string:
                            reference = string.strip().split('=')[1].strip().split('.')[1]
                        elif 'tottime' in string and '=' in string:
                            observation_time = float(string.strip().split('=')[1].strip())
                        elif 'date' in string and '=' in string:
                            date = string.strip().split('=')[1].strip()
                            if len(date) == 8 and '-' in date:
                                date = '20' + date
                        elif 'time' in string and ':' in string and '=' in string:
                            time = string.strip().split('=')[1].strip()
                        elif 'instrum' in string and '=' in string:
                            instrument = string.strip().split('=')[1].strip()
                            instrument = Instrument(instrument)
                            if os.path.isfile(instrument.file):
                                instrument.load()
                        elif 'object' in string and '=' in string:
                            obj = string.strip().split('=')[1].strip().lower()
                            atmosphere = Atmosphere(obj + '_atmosphere')
                            target.name = obj
                        elif 'units' in string and '=' in string:
                            if 'erg' in string:
                                radiance_unit_factor *= 1E-7  # erg -> J
                            if r'cm2' in string:
                                radiance_unit_factor *= 1E4  # cm-2 -> m-2

                        if isinstance(instrument, Instrument):
                            if instrument.pixel_resolution is None:
                                instrument.pixel_resolution = [np.nan, np.nan]

                            if instrument.host_name == instrument.DEFAULT_HOST and \
                                    'telescop' in string and '=' in string:
                                instrument.host_name = string.strip().split('=')[1].strip().lower()
                            elif 'pltscl' in string and '=' in string:
                                instrument.pixel_resolution[0] = float(string.strip().split('=')[1].strip())
                            elif 'slitwid' in string and '=' in string:
                                instrument.pixel_resolution[1] = float(string.strip().split('=')[1].strip())

                if date is not None and time is not None:
                    start_time = date + 'T' + time
                    stop_time = start_time

                if start_time is not None and observation_time is not None:
                    start = Ephemeris.date2julian(start_time)
                    stop_time = Ephemeris.julian_date2date(start + Ephemeris.seconds2days(observation_time))

        if 'sublat' in dictionary:
            sub_solar_latitude = dictionary.pop('sublat')
        else:
            sub_solar_latitude = None

        if 'sublon' in dictionary:
            sub_solar_longitude = dictionary.pop('sublon')
        else:
            sub_solar_longitude = None

        if 'wavenumber' in dictionary:
            wavenumbers_raw = dictionary.pop('wavenumber')
            dictionary['wavenumbers_raw'] = wavenumbers_raw
        else:
            wavenumbers_raw = None

        if 'sky' in dictionary:
            sky_transmittances_raw = dictionary.pop('sky')
            dictionary['sky_transmittances_raw'] = sky_transmittances_raw
        else:
            sky_transmittances_raw = None

        if 'longitude' in dictionary:
            longitude_map = np.fliplr(np.transpose(dictionary['longitude']))
            where_disk = np.where(np.max(longitude_map, axis=0) != 0)
            longitude_map = SpectralCube._sav_2d_transformation(dictionary.pop('longitude'), where_disk)
            where_dark = np.where(np.isnan(longitude_map))
            dictionary['longitude_map'] = longitude_map
        else:
            where_disk = None
            where_dark = None
            longitude_map = None

        if 'latitude' in dictionary:
            latitude_map = SpectralCube._sav_2d_transformation(dictionary.pop('latitude'), where_disk)
            dictionary['latitude_map'] = latitude_map
        else:
            latitude_map = None

        if 'airm' in dictionary:
            airmass_map = SpectralCube._sav_2d_transformation(dictionary.pop('airm'), where_disk)
            dictionary['airmass_map'] = airmass_map
        else:
            airmass_map = None

        if 'vel_map' in dictionary:
            velocity_map = SpectralCube._sav_2d_transformation(dictionary.pop('vel_map'), where_disk) * 1000
            dictionary['velocity_map'] = velocity_map
        else:
            velocity_map = None

        if 'spec_cube' in dictionary:
            radiances_map_raw = SpectralCube._sav_3d_transformation(dictionary.pop('spec_cube'), where_disk,
                                                                    axes=[1, 0, 2])
            radiances_map_raw *= radiance_unit_factor
            dictionary['radiances_map_raw'] = radiances_map_raw
        elif 'hdr' in dictionary and 'longitude' in dictionary:
            raise KeyError(f'no key \'spec_cube\' in sav file \'{file}\'')
        else:
            radiances_map_raw = None

        if 'radec_offset' in dictionary:
            dictionary['ra_dec_offset'] = dictionary.pop('radec_offset')

        if airmass_map is not None:
            emission_angle_map = Atmosphere.airmass2angle(airmass_map)
        else:
            emission_angle_map = None

        if sub_solar_latitude is not None and sub_solar_longitude is not None and \
                latitude_map is not None and longitude_map is not None:
            incidence_angle_map = Atmosphere.sub_coordinates2angle(sub_solar_latitude, sub_solar_longitude,
                                                                   latitude_map, longitude_map)
        else:
            incidence_angle_map = None

        wavenumbers, radiances_map, doppler_shift_map = SpectralCube._sav2pkl(wavenumbers_raw, radiances_map_raw,
                                                                              sky_transmittances_raw, velocity_map,
                                                                              **kwargs)

        name = SpectralCube.auto_name(instrument.name, target.name, start_time, wavenumbers, nickname)

        noise_radiances = SpectralCube._noise_from_corners(where_dark, wavenumbers, radiances_map,
                                                           noise_corner_boxes_size)
        noise = Noise('noise_' + name, wavenumbers=wavenumbers, radiances=noise_radiances)
        noise.save()

        spectra = np.asarray(np.zeros(np.shape(longitude_map)), dtype=object)
        spectra[np.where(spectra == 0)] = None

        for i in tqdm(range(np.size(radiances_map, 0)), 'Building spectral cube'):
            for j in range(np.size(radiances_map, 1)):
                location = (i, j)
                trg = copy.deepcopy(target)
                atm = copy.deepcopy(atmosphere)
                trg.emission_angle = emission_angle_map[i, j]
                trg.incidence_angle = incidence_angle_map[i, j]
                trg.latitude = latitude_map[i, j]
                trg.longitude = longitude_map[i, j]
                spectra[i, j] = Spectrum(name=name.replace(SpectralCube.base_name + '_', '') + '_' +
                                         Spectrum.location2str(location), path=Spectrum.PATH_IN,
                                         wavenumbers=wavenumbers, radiances=radiances_map[i, j], noise=noise,
                                         target=trg, atmosphere=atm, instrument=instrument,
                                         doppler_shift=doppler_shift_map[i, j],
                                         location=location, start_time=start_time, stop_time=stop_time,
                                         reference=reference,
                                         label=name.replace(SpectralCube.base_name + '_', '') + '_' +
                                         Spectrum.location2str(location).replace('_', ' '),
                                         color='b', linestyle='', marker='+')

        return cls(name=name, path=path, reference=reference,
                   start_time=start_time, stop_time=stop_time, instrument=instrument, target=target,
                   sub_observer_latitude=None, sub_observer_longitude=None,
                   sub_observer_azimuth=None,
                   sub_solar_latitude=sub_solar_latitude, sub_solar_longitude=sub_solar_longitude,
                   sub_solar_azimuth=None,
                   atmosphere=atmosphere, wavenumbers=wavenumbers, spectra=spectra, noise=noise,
                   _metadata=dict(dictionary))

    @classmethod
    def from_spectral_slices(cls, spectral_slices, metakernel=None, target=None, nickname=''):
        """
        Load a SpectralCube from SpectralSlices.
        The slices will be appended together to constitute the SpectralCube
        :param spectral_slices: SpectralSlices from which to form the SpectralCube
        :param metakernel: SPICE metakernel file corresponding to the SpectralCube
        :param target: in case the SpectralSlices aims to multiple targets
        :param nickname: nickname of the SpectralCube
        :return: the SpectralCube
        """
        if metakernel is not None:
            metakernel.load()

        sorted_indexes = sorted(range(np.size(ObjList(spectral_slices).getsubattr_list('start_time'))),
                                key=lambda k: ObjList(spectral_slices).getsubattr_list('start_time')[k])

        ordered_slices = np.empty(np.shape(spectral_slices), dtype=object)
        for i in range(np.size(spectral_slices)):
            ordered_slices[i] = spectral_slices[sorted_indexes[i]]

        spectral_slices = ordered_slices

        start_time = 'T'
        stop_time = 'T'

        if metakernel is not None:
            start_times = []
            stop_times = []

            for s_t in ObjList(spectral_slices).getsubattr_list('start_time'):
                start_times.append(metakernel.date2spice(s_t))

            for s_t in ObjList(spectral_slices).getsubattr_list('stop_time'):
                stop_times.append(metakernel.date2spice(s_t))

            start_time = metakernel.spice2date(np.min(start_times))
            stop_time = metakernel.spice2date(np.max(stop_times))

        instruments = np.asarray(ObjList(spectral_slices).getsubattr_list('instrument'))
        instruments_name = np.asarray(ObjList(spectral_slices).getsubattr_list('instrument.name'))
        targets = np.asarray(ObjList(spectral_slices).getsubattr_list('target.name'))
        sub_observer_latitudes = np.asarray(ObjList(spectral_slices).getsubattr_list('sub_observer_latitude'))
        sub_observer_longitudes = np.asarray(ObjList(spectral_slices).getsubattr_list('sub_observer_longitude'))
        sub_observer_azimuths = np.asarray(ObjList(spectral_slices).getsubattr_list('sub_observer_azimuth'))
        sub_solar_latitudes = np.asarray(ObjList(spectral_slices).getsubattr_list('sub_solar_latitude'))
        sub_solar_longitudes = np.asarray(ObjList(spectral_slices).getsubattr_list('sub_solar_longitude'))
        sub_solar_azimuths = np.asarray(ObjList(spectral_slices).getsubattr_list('sub_solar_azimuth'))
        atmospheres = np.asarray(ObjList(spectral_slices).getsubattr_list('atmosphere'))
        atmospheres_name = np.asarray(ObjList(spectral_slices).getsubattr_list('atmosphere.name'))
        wavenumbers_ = np.asarray(ObjList(spectral_slices).getsubattr_list('wavenumbers'))
        spectra_ = np.asarray(ObjList(spectral_slices).getsubattr_list('spectra'))
        noises = np.asarray(ObjList(spectral_slices).getsubattr_list('noise'))
        metadata_ = np.asarray(ObjList(spectral_slices).getsubattr_list('metadata'))

        if not np.all(instruments_name == instruments_name[0]):
            warn(f'instrument names in spectral slices are not all the same; '
                 f'instruments name list: {set(instruments_name)}', Warning)

        instrument = instruments[0]

        if not np.all(wavenumbers_ == wavenumbers_[0]):
            warn('wavenumbers in spectral slices are not all the same', Warning)

        if not np.all(atmospheres_name == atmospheres_name[0]):
            info(f'atmosphere names in spectral slices are not all the same; '
                 f'atmospheres name list: {set(atmospheres_name)}')

        if not np.all(noises == noises[0]):
            info('noises in spectral slices are not all the same')

        if target is not None:
            if not np.any(targets == target.name):
                raise ValueError(f'target \'{target.name}\' not in spectral slices; targets list: {set(targets)}')
            elif not np.all(targets == target):
                targets_removed = list(set(targets))
                targets_removed.pop(int(np.where(np.asarray(targets_removed) == target.name)[0]))
                info(f'Selected target \'{target.name}\'; slices with targets {targets_removed} will be removed')

        name = SpectralCube.auto_name(instrument_name=instrument.name, target=target.name, start_time=start_time,
                                      nickname=nickname)

        spectral_cube = cls(name=name, path=SpectralCube.PATH_DATA, ext=SpectralCube.EXT,
                            start_time=start_time, stop_time=stop_time, instrument=instrument, target=target,
                            sub_observer_latitude=sub_observer_latitudes,
                            sub_observer_longitude=sub_observer_longitudes,
                            sub_observer_azimuth=sub_observer_azimuths,
                            sub_solar_latitude=sub_solar_latitudes, sub_solar_longitude=sub_solar_longitudes,
                            sub_solar_azimuth=sub_solar_azimuths,
                            atmosphere=atmospheres[0], wavenumbers=wavenumbers_[0], spectra=spectra_,
                            noise=noises[0], _metadata=metadata_)

        for i, slice_ in enumerate(tqdm(spectral_cube.spectra, desc='Calculating geometry')):
            if target is None:
                target_ = targets[i]
            else:
                if targets[i] == target.name:
                    target_ = target.name
                else:
                    spectral_cube.spectra[i, :] = None
                    info(f'rejected slice {i}: slice target \'{targets[i].name}\' '
                         f'is different of desired target \'{target.name}\'')
                    continue

            for j in range(np.size(slice_)):
                if metakernel is not None and target_ is not None:
                    latitudes, longitudes, intercept_time, vectors, intercept_point = metakernel.get_pixel_geometry(
                        target=target_,
                        epoch=spectral_cube.spectra[i, j].start_time,
                        observer=spectral_cube.instrument.host_name,
                        instrument_frame=spectral_cube.instrument.name,
                        instrument_array_shape=spectral_cube.instrument.array_shape,
                        viewing_px=(0, j),  # slices are 1-px wide on their x-axis: array shape is (1, len(slice))
                    )

                    if not np.isnan(intercept_time):
                        light_source_range = metakernel.get_observer_point_distance(
                            target='sun',
                            epoch=intercept_time,
                            observer=target_,
                            surface_point=intercept_point
                        ) * 1E3  # km -> m

                        # doppler_shift = velocity2doppler(
                        #     source_velocity=metakernel.get_observer_surface_point_velocity(
                        #         target=target_,
                        #         epoch=intercept_time,
                        #         observer=instrument.host_name,
                        #         intercept_point=intercept_point,
                        #         intercept_vector=vectors[0],
                        #         target_equatorial_radius=target.equatorial_radius,
                        #         target_polar_radius=target.polar_radius,
                        #         target_sideral_rotation_period=target.sideral_rotation_period
                        #     ),
                        #     observed_wavenumber=spectral_cube.wavenumbers
                        # )
                        doppler_shift = velocity2doppler(
                            source_velocity=metakernel.get_observer_surface_point_velocity(
                                target=target_,
                                epoch=spectral_cube.spectra[i, j].start_time,
                                observer=instrument.host_name,
                                intercept_point=intercept_point,
                                intercept_vector=vectors[0]
                            ),
                            observed_wavenumber=spectral_cube.wavenumbers
                        )

                        incidence_angle, emission_angle = \
                            metakernel.get_illumination_angles(target=target_, epoch=intercept_time,
                                                               observer=instrument.host_name,
                                                               surface_coordinates=(latitudes[0], longitudes[0]))[1:]

                        # TODO actually take into account doppler shift by interpolating wavenumbers (or not ?)
                    else:
                        doppler_shift = 0.0
                        light_source_range = np.nan
                        incidence_angle = np.nan
                        emission_angle = np.nan
                else:
                    latitudes = np.full(5, np.nan)
                    longitudes = np.full(5, np.nan)
                    doppler_shift = 0.0
                    light_source_range = np.nan
                    incidence_angle = np.nan
                    emission_angle = np.nan

                spectral_cube.spectra[i, j].doppler_shift = doppler_shift
                spectral_cube.spectra[i, j].target = copy.deepcopy(target)
                spectral_cube.spectra[i, j].target.latitude = latitudes
                spectral_cube.spectra[i, j].target.longitude = longitudes
                spectral_cube.spectra[i, j].target.light_source_range = light_source_range
                spectral_cube.spectra[i, j].target.incidence_angle = incidence_angle
                spectral_cube.spectra[i, j].target.emission_angle = emission_angle
                print(j, spectral_cube.spectra[i, j].target.latitude, spectral_cube.spectra[i, j-1].target.latitude)
        # spectral_cube.spectra = np.delete(spectral_cube.spectra,  # syntax below -> numpy does not recognize "is None"
        #                                   np.where(np.all(spectral_cube.spectra == [None][0], axis=1)), axis=0)

        for i, slice_ in enumerate(spectral_cube.spectra):
            for j, spec in enumerate(slice_):
                if spectral_cube.spectra[i, j] is not None:
                    spectral_cube.spectra[i, j].location = (i, j)

        return spectral_cube

    @classmethod
    def from_pkl(cls, name, path=File.PATH):
        return cls.loader(File(name=name, path=path, ext='pkl', find=True).file)

    @staticmethod
    def _noise_from_corners(where_dark, wavenumbers, radiances_map, noise_corner_boxes_size=5):
        """
        (Specific) if > 0, calculate the noise using the standard deviation of the radiances of the spectra in the four
        size x size pixels square at the corners of the cube.
        :param where_dark: 2-D array containing the coordinates of the off-target pixels.
        :param wavenumbers: wavenumbers of the spectra
        :param radiances_map: 2-D array containing the radiances of the spectra
        :param noise_corner_boxes_size: (px) length of the edges of the squares at the corner of the cubes from which to
        calculate the noise
        :return: the radiances of the noise
        """
        noise_radiances = []

        if noise_corner_boxes_size > 0:
            wh = np.where(
                np.logical_or(np.logical_or(np.logical_and(where_dark[0] < noise_corner_boxes_size,
                                                           where_dark[1] < noise_corner_boxes_size),
                                            np.logical_and(where_dark[0] < noise_corner_boxes_size,
                                                           where_dark[1] > np.max(
                                                               where_dark[1]) - noise_corner_boxes_size)),
                              np.logical_or(
                                  np.logical_and(where_dark[0] > np.max(where_dark[0]) - noise_corner_boxes_size,
                                                 where_dark[1] < noise_corner_boxes_size),
                                  np.logical_and(where_dark[0] > np.max(where_dark[0]) - noise_corner_boxes_size,
                                                 where_dark[1] > np.max(where_dark[1]) - noise_corner_boxes_size))
                              ))
            mask_std = [where_dark[0][wh], where_dark[1][wh]]
        else:
            mask_std = where_dark

        for i in range(np.size(wavenumbers)):
            noise_radiances.append(np.std(radiances_map[mask_std], 0)[i])

        return np.asarray(noise_radiances)

    @staticmethod
    def _sav_2d_transformation(array2d, filter_):
        """
        Specific transformation of a 2-D array.
        :param array2d: 2-D array to transform
        :param filter_: filter some of the elements of the array
        :return: the transformed 2-D array
        """
        transformed_array2d = np.transpose(np.transpose(np.fliplr(np.transpose(array2d)))[filter_])
        transformed_array2d[np.where(transformed_array2d == 0)] = np.nan
        return transformed_array2d

    @staticmethod
    def _sav_3d_transformation(array3d, filter_, axes=None):
        """
        Specific transformation of a 3-D array.
        :param array3d: 3-D array to transform
        :param filter_: filter some of the elements of the array
        :param axes: see numpy.fliplr
        :return: the transformed array
        """
        transformed_array3d = np.fliplr(np.transpose(array3d[filter_], axes=axes))
        for i in range(np.size(transformed_array3d, 0)):
            for j in range(np.size(transformed_array3d, 1)):
                if not transformed_array3d[i, j, :].any():
                    transformed_array3d[i, j, :] = np.nan
        return transformed_array3d

    @staticmethod
    def _sav2pkl(wavenumbers_raw=None, radiances_map_raw=None, sky_transmittances_raw=None, velocity_map=None,
                 wavenumbers_trim=5, nullify_sky_correction=True, nullify_doppler_shift=True):
        """
        Specific corrections of the wavenumbers, radiances map and Doppler shift effect of an IDL sav file.
        :param wavenumbers_raw: wavenumbers extracted from the sav file
        :param radiances_map_raw: 2-D array containing the radiances, extracted from the sav file
        :param sky_transmittances_raw: sky transmittances extracted from the sav file
        :param velocity_map: velocity map extracted from the sav file
        :param wavenumbers_trim: trim the wavenumber array of the given length at both endings of the array
        :param nullify_sky_correction: if True, multiply the radiances by the sky transmittances
        :param nullify_doppler_shift: if True, recalculate the wavenumbers of the spectra to remove the Doppler shift
        :return: corrected wavenumbers, radiances map and Doppler shift map
        """
        if nullify_sky_correction:
            print('Nullifying sky correction...')
            if radiances_map_raw is not None and sky_transmittances_raw is not None:
                radiances_map_ = radiances_map_raw * sky_transmittances_raw
                radiances_map_[np.where(radiances_map_ < 0)] = 0
            else:
                raise ValueError('arguments \'radiances_map_raw\' and \'sky_transmittances_raw\' '
                                 'must not be None')
        else:
            info('No changes in radiances map')
            radiances_map_ = radiances_map_raw

        if nullify_doppler_shift:
            print('Correcting from Doppler shift...')
            if wavenumbers_raw is not None and radiances_map_raw is not None and \
                    sky_transmittances_raw is not None and velocity_map is not None:
                wavenumbers = velocity2doppler(np.mean(velocity_map), wavenumbers_raw)[1]
                doppler_shift_map = np.zeros(np.shape(velocity_map)) * np.nan
                radiances_map = np.zeros(np.append(np.shape(velocity_map), np.size(wavenumbers)))

                for i in range(0, np.size(velocity_map, 0)):
                    for j in range(0, np.size(velocity_map, 1)):
                        doppler_shifts, wavenumber_doppler = velocity2doppler(velocity_map[i, j], wavenumbers_raw)
                        doppler_shift_map[i, j] = np.mean(doppler_shifts)
                        wh = np.where(np.logical_and(wavenumbers >= np.min(wavenumber_doppler),
                                                     wavenumbers <= np.max(wavenumber_doppler)))
                        radiances_map[i, j, wh] = np.interp(wavenumbers[wh],
                                                            wavenumber_doppler, radiances_map_[i, j, :])
            else:
                raise ValueError('arguments \'wavenumbers_raw\', \'radiances_map_raw\', \'sky_transmittances_raw\' '
                                 'and \'velocity_map\' '
                                 'must not be None')
        else:
            info('No Doppler shift correction')
            radiances_map = radiances_map_
            doppler_shift_map = None
            wavenumbers = wavenumbers_raw

        if wavenumbers_trim >= 0:
            print(f'Trimming wavenumbers of length {wavenumbers_trim}...')
            if wavenumbers_raw is not None and radiances_map_raw is not None and sky_transmittances_raw is not None:
                i_min = 0
                i_max = np.size(wavenumbers)
                where_sky_gt0 = np.where(sky_transmittances_raw > 0)[0]
                for row in radiances_map_raw:
                    for radiance in row:
                        where_spectrum_gt0 = np.where(radiance[where_sky_gt0] > 0)[0]
                        if np.size(where_spectrum_gt0) > 1:
                            if np.min(where_spectrum_gt0) > i_min:
                                i_min = np.min(where_spectrum_gt0)

                            if np.max(where_spectrum_gt0) < i_max:
                                i_max = np.max(where_spectrum_gt0)

                where_spectra_gt0 = np.arange(i_min, i_max + 1)
                wavenumbers = wavenumbers[where_sky_gt0][where_spectra_gt0]
                wavenumbers = wavenumbers[wavenumbers_trim:-wavenumbers_trim]
                radiances_map = radiances_map[:, :, where_sky_gt0]
                radiances_map = radiances_map[:, :, where_spectra_gt0]
                radiances_map = radiances_map[:, :, wavenumbers_trim:-wavenumbers_trim]
            else:
                raise ValueError('arguments \'wavenumbers_raw\', \'radiances_map_raw\' and \'sky_transmittances_raw\' '
                                 'must not be None')
        else:
            info('no changes in wavenumbers')

        return wavenumbers, radiances_map, doppler_shift_map

    @staticmethod
    def auto_name(instrument_name, target, start_time, wavenumbers=None, nickname=''):
        """
        Build a name from the given parameters.
        Format:
        <SpectralCube.base_name>_<instrument_name>_<target>_<date>-<time>[_<wavenumber_min>-<wavenumber_max>]_NRT<ver-
        -sion>[_nickname]
        """
        if nickname is None:
            nickname = ''
        elif nickname != '':
            nickname = '_' + nickname

        if wavenumbers is None:
            wavenumbers = ''
        else:
            wavenumbers = '_' + str(int(np.min(wavenumbers))) + '-' + str(int(np.max(wavenumbers)))

        date, time = start_time.split('T', 1)
        name = SpectralCube.base_name + '_' + instrument_name + '_' + str(target) + '_' + date.replace('-', '') + \
            'T' + time.replace(':', '').split('.')[0] + wavenumbers + nickname + '_NRT' + compact_version()

        return name

    @staticmethod
    def graphic2centric(lat_planetographic, equatorial_polar_radius_ratio):
        """
        Convert planetographic latitude into planetocentric latitude.
        :param lat_planetographic: (rad) planetographic latitude
        :param equatorial_polar_radius_ratio: ratio of the the equatorial radius over the polar radius of the object
        :return: (rad) planetocentric latitude
        """
        return np.arctan(np.tan(lat_planetographic) / equatorial_polar_radius_ratio ** 2)

    @staticmethod
    def make_spec_map(spectra):
        """Make a 2D spectral map from a list of spectra, according to the spectra location."""
        x_spec = [loc[0] for loc in spectra.get('location')]
        y_spec = [loc[1] for loc in spectra.get('location')]
        spec_map = ObjList()
        for x in range(np.max(x_spec)):
            spec_map.append([])
            for y in range(np.max(y_spec)):
                spec_map[x].append(None)
                for spectrum in spectra:
                    if spectrum.location[0] == x and spectrum.location[1] == y:
                        spec_map[x][y] = spectrum
                        break

        return np.asarray(spec_map)

    @staticmethod
    def plot_map(attribute_map, cmap='plasma', aspect='auto', norm=None, vmin=None, vmax=None,
                 cbar_label='', latitudes=None, longitudes=None, lat_step=5, lon_step=30, title='', size=1,
                 save=False, fig_dir='', fig_name='', nickname='', fmt='png', figsize=(6.5, 5), dpi=80):
        """
        Plot a map.
        :param attribute_map: a 2 dimensional array containing the values to plot
        :param cmap: the colormap of the figure
        :param aspect: the aspect ratio of the figure
        :param norm: the normalisation of the values on the figures
        :param vmin: minimum value of the colormap
        :param vmax: maximum value of the colormap
        :param cbar_label: the label of the colorbar
        :param latitudes: if not None, must be a latitude map and add latitude levels according to it
        :param longitudes: if not None, must be a latitude map and add longitude levels according to it
        :param lat_step: step between the latitude levels
        :param lon_step: step between the longitude levels
        :param title: title of the figure
        :param size: size of the elements on the figure
        :param save: if True, save the figure
        :param fig_dir: directory on which to save the figure
        :param fig_name: name of the figure
        :param nickname: nickname of the figure
        :param fmt: format of the figure
        :param figsize: figure size
        :param dpi: figure resolution
        """
        plt.close('all')
        plt.figure(figsize=figsize)
        ax = plt.gca()

        if vmin is None:
            vmin = np.min(attribute_map)

        if vmax is None:
            vmax = np.max(attribute_map)

        im = ax.imshow(attribute_map, cmap=cmap, aspect=aspect, norm=norm,
                       interpolation='none', vmin=vmin, vmax=vmax)

        if latitudes is not None:
            latitudes = np.ma.masked_values(latitudes, 0.0)
            lat = ax.contour(latitudes, levels=np.arange(-90, 100, lat_step),
                             colors='w', linestyles='-', linewidths=0.5)
            ax.clabel(lat, fontsize=8 * size, fmt='%1.0f°')

        if longitudes is not None:
            longitudes = np.ma.masked_values(longitudes, 0.0)
            lon_not_nan = longitudes[np.where(np.logical_not(np.isnan(longitudes)))]

            # Avoid weird contours around longitude 0
            if np.max(lon_not_nan) > 360 - lon_step and np.min(lon_not_nan) < 0 + lon_step:
                longitudes[np.where(longitudes < 180)] += 360

            lon = ax.contour(longitudes,
                             levels=np.arange(0, np.floor(np.max(lon_not_nan) / lon_step) * lon_step + lon_step,
                                              lon_step),
                             colors='w', linestyles='-', linewidths=0.5)
            lon.levels[np.where(lon.levels >= 360)] -= 360
            ax.clabel(lon, fontsize=8 * size, fmt='%1.0f°')

        if title is not None:
            plt.title(title)

        # Ex: Spectrum of row 30 and column 40 is accessed with spectra[30, 40] => spectra[x, y]
        # It is counter-intuitive to access this spectrum with spectra[40, 30], but x and y would match the x and y axis
        plt.xlabel('y px')  # the x-axis represents the column (y/vertical) number of the matrix
        plt.ylabel('x px')  # the y-axis represent the row (x/horizontal) number of the matrix

        cmap = plt.get_cmap(cmap)
        cmap.set_bad('k')
        cmap.set_under('k')

        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(12 * size)

        cbar_format = ticker.ScalarFormatter(useMathText=True, useOffset=True)
        cbar_format.set_powerlimits((-3, 3))

        if vmin < 0.03 * vmax and norm is not None:
            pow_min = int(np.floor(np.log10(vmin))) + 1
            pow_max = int(np.floor(np.log10(vmax)))

            sub_pow_min = np.int(np.ceil(vmin / np.exp(int(np.floor(np.log10(vmin))) * np.log(10))))
            sub_pow_max = np.int(np.floor(vmax / np.exp(int(np.floor(np.log10(vmax))) * np.log(10))))
            ticks = [sub_pow_min * np.exp((pow_min - 1) * np.log(10))] + list(
                np.exp((np.asarray(list(range(np.abs(pow_max - pow_min) + 1))) + pow_min) * np.log(10))) + [
                        sub_pow_max * np.exp(pow_max * np.log(10))]

            minorticks = []
            for pow_range in range(np.abs(pow_max - pow_min) + 2):
                if pow_range == 0:
                    sub_pow_min = np.int(np.floor(vmin / np.exp(int(np.floor(np.log10(vmin))) * np.log(10)))) + 1
                    sub_pow_range = 10 - sub_pow_min
                elif pow_range == np.abs(pow_max - pow_min) + 1:
                    sub_pow_min = 2
                    sub_pow_max = np.int(np.floor(vmax / np.exp(int(np.floor(np.log10(vmax))) * np.log(10))))
                    sub_pow_range = sub_pow_max - 1
                else:
                    sub_pow_min = 2
                    sub_pow_range = 8

                for minor in range(sub_pow_range):
                    minorticks.append((minor + sub_pow_min) * np.exp((pow_range + pow_min - 1) * np.log(10)))

            minorticks = im.norm(minorticks)
            cbar = plt.colorbar(im, format=cbar_format, ticks=ticks)
            cbar.ax.yaxis.set_ticks(minorticks, minor=True)
        else:
            cbar = plt.colorbar(im, format=cbar_format)

        cbar.ax.tick_params(labelsize=12 * size)
        cbar.set_label(cbar_label, size=12 * size)
        plt.tight_layout()

        if save:
            fig_name = fig_name + nickname

            if fig_dir == '':
                fig_dir = os.path.join(Figure.PATH, 'divers', 'maps')

            if not os.path.isdir(fig_dir):
                os.makedirs(fig_dir)

            plt.savefig(os.path.join(fig_dir, fig_name + '.' + fmt),
                        dpi=dpi, format=fmt)
            print(f'Figure saved in \'{os.path.join(fig_dir, fig_name)}\'...')
        else:
            plt.show()
