"""
Scripted runs (not generalised and not intended to be, use with caution).
"""
# from .plots_basemap import *
from .spectral_cube import *
from .spectrum import *
from .spice_kernels import *
from .maps_ import *


def _nrt_quick_setup():
    from subprocess import call
    print('setup')
    call('python3 setup.py sdist', shell=True)


def _texes_retrieval_launch_1():
    """2017-11-10 launch"""
    # sav to pkl
    sco1 = SpectralCube.from_sav('4004')
    sco2 = SpectralCube.from_sav('4019')
    sco3 = SpectralCube.from_sav('4034')

    # change skies (need to save right after due to a bug in function from_sav.
    print('Changing skies...')
    sco1.atmosphere.sky = Sky('za70', find=True)
    for spec in np.asarray(sco1.spectra).flatten():
        if spec is not None:
            sco1.spectra[spec.location[0]][spec.location[1]].atmosphere.sky = sco1.atmosphere.sky
    sco1.save()
    sco2.atmosphere.sky = Sky('za48', find=True)
    for spec in np.asarray(sco2.spectra).flatten():
        if spec is not None:
            sco2.spectra[spec.location[0]][spec.location[1]].atmosphere.sky = sco2.atmosphere.sky
    sco2.save()
    sco3.atmosphere.sky = Sky('za26', find=True)
    for spec in np.asarray(sco3.spectra).flatten():
        if spec is not None:
            sco3.spectra[spec.location[0]][spec.location[1]].atmosphere.sky = sco3.atmosphere.sky
    sco3.save()

    # retrieval
    sc_ret = []
    for sc in [sco1, sco2, sco3]:
        sc_ret.append(sc.retrieve_accurate(molecules_to_retrieve=['NH3'], compile_nrt=True))

    # clean figures
    for sc in sc_ret:
        sc.longitude_map = np.ma.masked_values(sc.longitude_map, 0.0)
        sc.latitude_map = np.ma.masked_values(sc.latitude_map, 0.0)

    for sco in [sco1, sco2, sco3]:
        sco.longitude_map = np.ma.masked_values(sco.longitude_map, 0.0)
        sco.latitude_map = np.ma.masked_values(sco.latitude_map, 0.0)

    # slices
    # sc1.longitude_map += 7
    lon_bin = 3
    f = None
    for sc in sc_ret:
        for lon in np.arange(np.ceil(np.min(sc.longitude_map[np.where(sc.longitude_map != 0)])) + lon_bin,
                             np.floor(np.max(sc.longitude_map)) - lon_bin, lon_bin * 2):
            f = sc.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr',
                                           lon_min=lon - lon_bin,
                                           lon_max=lon + lon_bin, ylim=[20, 5E-1], levels=np.linspace(0, 600, 21),
                                           lat_min=-40, lat_max=41, plot=True, save=True,
                                           fig_name=f'slice_lon{lon}pm{lon_bin}', cbar_label=r'[NH$_3$] (ppmv)',
                                           nickname=sc.name)

    # maps
    hexbin_spectral_cubes_attribute_map([sco1, sco2, sco3], 'radiances', western_longitudes=True, central_logitude=240,
                                        save=True, vmin=0, vmax=3.6E-8,
                                        cbar_label=r'Mean radiance (W.cm$^{-2}$).sr$^{-1}$/cm$^{-1}$)')
    hexbin_spectral_cubes_attribute_map(sc_ret, 'chi2', western_longitudes=True, central_logitude=240, save=True,
                                        cbar_label=r'$\chi^2/n$', vmin=0, vmax=20)
    hexbin_spectral_cubes_attribute_map(sc_ret, 'atmosphere.clouds[0].transmittance', western_longitudes=True,
                                        central_logitude=240, save=True, cbar_label=r'Cloud transmittance',
                                        viewing_angle_max=75, vmin=0, vmax=0.2)
    # VMR to ppmv
    for sc in sc_ret:
        sc.vmr2ppmv()

    hexbin_spectral_cubes_attribute_map(sc_ret, 'atmosphere.molecules[NH3].vmr_profile.vmr[16]',
                                        western_longitudes=True, central_logitude=240, save=True,
                                        cbar_label=r'[NH$_3$] at 2 bar (ppmv)', vmin=0, vmax=600, cmap='RdBu_r')

    return sc_ret, f


def _texes_article2018_all_figs():
    """
    Plot all the figures of texes 2018 article.
    Works with NRT 4.10.1.
    """
    # Load Spectral Cubes
    sco1 = SpectralCube.from_pkl('09.*4100', path=SpectralCube.PATH_DATA)
    sco2 = SpectralCube.from_pkl('1110.*4100', path=SpectralCube.PATH_DATA)
    sco3 = SpectralCube.from_pkl('124.*4100', path=SpectralCube.PATH_DATA)

    scr1 = SpectralCube.from_pkl('09.*4100$', path=SpectralCube.PATH_OUT)
    scr2 = SpectralCube.from_pkl('1110.*4100$', path=SpectralCube.PATH_OUT)
    scr3 = SpectralCube.from_pkl('124.*4100$', path=SpectralCube.PATH_OUT)

    scr1.vmr2ppmv()
    scr2.vmr2ppmv()
    scr3.vmr2ppmv()

    # Mean observed radiance 11h10
    sco2.get_from_spectra('radiances', attribute_shape=np.shape(sco2.wavenumbers), plot=True, lat_lon_overlay=True,
                          vmin=0, vmax=3.5E-4, cbar_label=f'Mean radiance ({Spectrum.RADIANCE_UNITS})')

    # Mean observed radiance all
    radiances = []
    for i, spec_cube in enumerate([sco1, sco2, sco3]):
        radiances.append(np.mean(spec_cube.get_from_spectra(attribute='radiances',
                                                            attribute_shape=[np.shape(sco1.wavenumbers),
                                                                             np.shape(sco2.wavenumbers),
                                                                             np.shape(sco3.wavenumbers)][i]), 2))

    radiances = stack_arrays(radiances)

    hexbin_spectral_cubes_attribute_map([sco1, sco2, sco3], radiances,
                                        western_longitudes=True, central_logitude=240, save=True,
                                        vmin=0, vmax=3.5E-4, cbar_label=f'Mean radiance ({Spectrum.RADIANCE_UNITS})')

    # Chi2 all
    hexbin_spectral_cubes_attribute_map([scr1, scr2, scr3], 'chi2',
                                        western_longitudes=True, central_logitude=240, save=True,
                                        vmin=0, vmax=10, cbar_label=r'$\chi^{2}/n$')

    # Spectrum fit
    sco2.spectra[36, 35].atmosphere.clouds[0].transmittance = 0.132  # a priori spectrum initialisation
    sco2.spectra[36, 35].atmosphere.molecules['NH3'].retrieve = False

    sc = sco2.spectra[36, 35].retrieve(iteration_max=32, weight_apriori=0.01)
    sc.label = r'NH$_3$ VMR of 300 ppmv at 2 bar (a priori)'
    sc.color = 'k'

    sco2.atmosphere.sky.load()  # load observed SpectralCube sky for plot
    scr2.spectra[36, 35].label = r'NH$_3$ VMR of 125 ppmv at 2 bar (best fit)'
    Spectrum.errorbar([sco2.spectra[36, 35], sc, scr2.spectra[36, 35]], plot_residuals=True, plot_sky=True, size=14)

    # Cloud transmittance all
    hexbin_spectral_cubes_attribute_map([scr1, scr2, scr3], 'atmosphere.clouds[0].transmittance',
                                        western_longitudes=True, central_logitude=240, save=True,
                                        vmin=0, vmax=0.25, cbar_label=r'Cloud transmittance')

    # NH3 VMR fit
    _vmr_fits(spectrum=sco2.spectra[36, 35], special_retrieved_spectrum=scr2.spectra[36, 35])

    # NH3 map all
    trans = stack_arrays(ObjList([scr1, scr2, scr3]).call('get_from_spectra', 'atmosphere.clouds[0].transmittance'))
    nh3 = stack_arrays(
        ObjList([scr1, scr2, scr3]).call('get_from_spectra', 'atmosphere.molecules[NH3].vmr_profile.vmr[17]'))
    nh3[np.where(trans < 0.05)] = np.nan

    hexbin_spectral_cubes_attribute_map([scr1, scr2, scr3], nh3,
                                        western_longitudes=True, central_logitude=240, save=True, cmap='RdBu_r',
                                        vmin=0, vmax=400, cbar_label=r'NH$_3$ Volume Mixing Ratio')

    # Zonal-averaged NH3
    _nh3_latitude_averaged(scr1, scr2, scr3)

    # NH3 VMR fit zones
    soez = _spectrum_average(specs=[scr1.spectra, scr2.spectra, scr3.spectra], sco1=sco1, sco2=sco2, sco3=sco3,
                             latitude_range=[-15, 5])
    soez.name = 'Jupiter_texes_20160116_ez.dat'
    soez.update_file()
    soez.save()
    soz = _spectrum_average(specs=[scr1.spectra, scr2.spectra, scr3.spectra], sco1=sco1, sco2=sco2, sco3=sco3)
    soz.save()

    # plot soz
    aspect = 6.5 / 5
    resolution = 720
    dpi = 80
    plt.figure(figsize=((resolution * aspect) / dpi, resolution / dpi))

    vmr_aps = [100, 300, 1000]
    i_max = [23, 22, 22]
    colors = ['y', 'orange', 'r']
    colors_ap = ['k', 'k', 'k']
    lss = ['-', '-', '-']
    lss_ap = [':', '--', '-']

    vmr_ap_0 = Molecule.VMRProfile('NH3__', path=Molecule.VMRProfile.PATH_IN, find=True).vmr
    vmr_file = Atmosphere().molecules['NH3'].vmr_profile
    vmr_file.vmr = copy.deepcopy(vmr_ap_0)
    vmr_file.save()

    soz.atmosphere.molecules['NH3'].retrieve = True

    srs = []
    for i, vmr_ap in enumerate(vmr_aps):
        vmr_file.vmr[:i_max[i]] = vmr_ap * 1E-6  # ppmv2vmr
        vmr_file.save()

        sc = soz.retrieve(iteration_max=32, weight_apriori=0.1)
        sc.color = colors[i]
        srs.append(sc)
        vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

        vmr_ret.vmr2ppmv()

        plt.plot(vmr_file.vmr * 1E6, vmr_ret.pressure,
                 color=colors_ap[i], ls=lss_ap[i])

        plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color=colors[i], ls=lss[i],
                     xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                     capsize=5)

        vmr_file.vmr = copy.deepcopy(vmr_ap_0)
        vmr_file.save()

    vmr_ap = copy.deepcopy(vmr_ap_0)
    vmr_ap[11:23] = (190E-6 - 600E-6) / (np.log(vmr_file.pressure[23]) - np.log(vmr_file.pressure[13])) * \
        np.log(vmr_file.pressure[11:23]) + 190E-6
    vmr_ap[:11] = 600E-6
    vmr_file.vmr = vmr_ap
    vmr_file.save()

    sc = soz.retrieve(iteration_max=32, weight_apriori=0.1)
    sc.color = 'm'
    srs.append(sc)
    vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.plot(vmr_ap * 1E6, vmr_ret.pressure, color='grey', ls=':')
    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='m', ls='-',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    vmr_file.vmr = copy.deepcopy(vmr_ap_0)
    vmr_file.save()

    vmr_ap = Molecule.VMRProfile('NH3_MWR_lat4', path=Molecule.VMRProfile.PATH_IN, find=True).vmr
    vmr_file.vmr = vmr_ap
    vmr_file.save()

    sc = soz.retrieve(iteration_max=32, weight_apriori=0.1)
    sc.color = 'b'
    srs.append(sc)
    vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.plot(vmr_ap * 1E6, vmr_ret.pressure, color='grey', ls='--')
    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='b', ls='-',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    vmr_file.vmr = vmr_ap_0
    vmr_file.vmr[:22] = 300E-6
    vmr_file.save()

    plt.ylim([20, 0.5])
    plt.xlim([0, 1300])
    plt.semilogy()
    plt.xlabel('NH$_3$ Volume Mixing Ratio (ppmv)')
    plt.ylabel('Pressure (bar)')

    ax = plt.gca()
    size = 18
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                  ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(size)
    plt.tight_layout()

    # plot soez
    vmr_aps = [100, 300, 1000]
    i_max = [23, 22, 22]
    colors = ['y', 'orange', 'r']
    lss = [':', ':', ':']

    vmr_ap_0 = Molecule.VMRProfile('NH3__', path=Molecule.VMRProfile.PATH_IN, find=True).vmr
    vmr_file = Atmosphere().molecules['NH3'].vmr_profile
    vmr_file.vmr = copy.deepcopy(vmr_ap_0)
    vmr_file.save()

    soez.atmosphere.molecules['NH3'].retrieve = True

    srs = []
    for i, vmr_ap in enumerate(vmr_aps):
        vmr_file.vmr[:i_max[i]] = vmr_ap * 1E-6  # ppmv2vmr
        vmr_file.save()

        sc = soez.retrieve(iteration_max=32, weight_apriori=0.1)
        sc.color = colors[i]
        srs.append(sc)
        vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

        vmr_ret.vmr2ppmv()

        plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color=colors[i], ls=lss[i],
                     xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                     capsize=5)

        vmr_file.vmr = copy.deepcopy(vmr_ap_0)
        vmr_file.save()

    vmr_ap = copy.deepcopy(vmr_ap_0)
    vmr_ap[11:23] = (190E-6 - 600E-6) / (np.log(vmr_file.pressure[23]) - np.log(vmr_file.pressure[13])) * \
        np.log(vmr_file.pressure[11:23]) + 190E-6
    vmr_ap[:11] = 600E-6
    vmr_file.vmr = vmr_ap
    vmr_file.save()

    sc = soez.retrieve(iteration_max=32, weight_apriori=0.1)
    sc.color = 'm'
    srs.append(sc)
    vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='m', ls=':',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    vmr_file.vmr = copy.deepcopy(vmr_ap_0)
    vmr_file.save()

    vmr_ap = Molecule.VMRProfile('NH3_MWR_lat4', path=Molecule.VMRProfile.PATH_IN, find=True).vmr
    vmr_file.vmr = vmr_ap
    vmr_file.save()

    sc = soez.retrieve(iteration_max=32, weight_apriori=0.1)
    sc.color = 'b'
    srs.append(sc)
    vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='b', ls=':',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    vmr_file.vmr = vmr_ap_0
    vmr_file.vmr[:22] = 300E-6
    vmr_file.save()

    # nh3 constant
    # mpicompile_nrt()

    sr_spe_z = soz.retrieve(iteration_max=32)
    vmr_ret = sr_spe_z.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='g', ls='-',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    sr_spe_z = soez.retrieve(iteration_max=32)
    vmr_ret = sr_spe_z.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='g', ls=':',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    # spec plot
    soz.atmosphere.sky.load()
    soez.atmosphere.sky.load()
    Spectrum.errorbar([soz, srs[3], srs[0], sr_spe_z], plot_residuals=True, plot_sky=True, size=18)
    # plt.legend(loc=3, prop={'size': 18})

    # Multi plot vmr-transmittance
    nh31 = scr1.get_from_spectra('atmosphere.molecules[NH3].vmr_profile.vmr[16]')
    nh32 = scr2.get_from_spectra('atmosphere.molecules[NH3].vmr_profile.vmr[16]')
    nh33 = scr3.get_from_spectra('atmosphere.molecules[NH3].vmr_profile.vmr[16]')
    cld1 = scr1.get_from_spectra('atmosphere.clouds[0].transmittance')
    cld2 = scr2.get_from_spectra('atmosphere.clouds[0].transmittance')
    cld3 = scr3.get_from_spectra('atmosphere.clouds[0].transmittance')

    lat1 = scr1.get_from_spectra('latitude')
    lat2 = scr2.get_from_spectra('latitude')
    lat3 = scr3.get_from_spectra('latitude')
    lon1 = scr1.get_from_spectra('longitude')
    lon2 = scr2.get_from_spectra('longitude')
    lon3 = scr3.get_from_spectra('longitude')

    nh3s = [nh31, nh32, nh33]
    clds = [cld1, cld2, cld3]
    lats = [lat1, lat2, lat3]
    lons = [lon1, lon2, lon3]

    for i, nh3 in enumerate(nh3s):
        _multi_plot_vmr_transmittance(nh3_test=nh3, cld_test=clds[i], lat_test=lats[i], lon_test=lons[i])

    # NH3-transmittance Hackermann
    _hackermann(nh3=stack_arrays([nh31, nh32, nh33]).flatten(), cld=stack_arrays([cld1, cld2, cld3]).flatten())


def _hackermann(nh3, cld):
    wh = np.where(cld > 0)

    aspect = 6.5 / 5
    resolution = 720
    dpi = 100
    plt.figure(figsize=((resolution * aspect) / dpi, resolution / dpi))
    plt.plot(nh3.flatten()[wh], cld.flatten()[wh], marker='+', ls='', color='grey', label='data')
    plt.xlim([0, 400])
    plt.ylim([0, 0.3])
    plt.xlabel(r'NH$_3$ Volume Mixing Ratio (ppmv)')
    plt.ylabel('Cloud transmittance')
    ax = plt.gca()
    size = 18
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                  ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(size)
    plt.tight_layout()

    eps_nh3_h2he = 17 / 2.2
    eps_nh4sh_h2he = 51 / 2.2
    p = 1.013E5
    q_nh3 = np.linspace(0, 600, 100)
    grav = 24
    rho_nh3 = 817
    rho_nh4sh = 1170

    # q_ext_nh3_10mic = 2.466
    w0_nh3_10mic = 0.923
    g_nh3_10mic = 0.775
    p_size = 10E-6  # (m)
    mol = r'NH$_3$'
    p_ = 0.8  # (bar)
    f_rains = [45, 80, 120, 200]
    ls = [':', '-', '--', '-.']
    color = 'c'
    for i, f_rain in enumerate(f_rains):
        tau_ackermann = 3 / 2 * eps_nh3_h2he * p_ * p * q_nh3 * 1E-6 / (grav * p_size * (1 + f_rain) * rho_nh3)
        u = ((1 - g_nh3_10mic * w0_nh3_10mic) / (1 - w0_nh3_10mic)) ** 0.5
        t = (3 * (1 - w0_nh3_10mic) * (1 - g_nh3_10mic * w0_nh3_10mic)) ** 0.5 * tau_ackermann
        tc = 4 * u / ((u + 1) ** 2 * np.exp(t) - (u - 1) ** 2 * np.exp(-t))
        plt.plot(q_nh3, tc, color=color, ls=ls[i],
                 label=f'{p_size*1E6} um ' + mol + ' @ ' + f'{p_} bar' + ' f$_{rain}$' + f' = {f_rain}')

    # q_ext_nh4sh_10mic = 2.344
    w0_nh4sh_10mic = 0.968
    g_nh4sh_10mic = 0.567
    p_size = 10E-6  # (m)
    mol = r'NH$_4$SH'
    p_ = 1.2  # (bar)
    f_rains = [150, 250, 350, 700]
    ls = [':', '-', '--', '-.']
    color = 'darkorange'
    for i, f_rain in enumerate(f_rains):
        tau_ackermann = 3 / 2 * eps_nh4sh_h2he * p_ * p * q_nh3 * 1E-6 / (grav * p_size * (1 + f_rain) * rho_nh4sh)
        u = ((1 - g_nh4sh_10mic * w0_nh4sh_10mic) / (1 - w0_nh4sh_10mic)) ** 0.5
        t = (3 * (1 - w0_nh4sh_10mic) * (1 - g_nh4sh_10mic * w0_nh4sh_10mic)) ** 0.5 * tau_ackermann
        tc = 4 * u / ((u + 1) ** 2 * np.exp(t) - (u - 1) ** 2 * np.exp(-t))
        plt.plot(q_nh3, tc, color=color, ls=ls[i],
                 label=f'{p_size*1E6} um ' + mol + ' @ ' + f'{p_} bar' + ' f$_{rain}$' + f' = {f_rain}')

    ax = plt.gca()
    size = 18
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                  ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(size)
    plt.tight_layout()


def _vmr_fits(spectrum, special_retrieved_spectrum=None, vmr_aps=(50, 200, 300, 400), i_max=(24, 22, 22, 22),
              colors=('m', 'y', 'orange', 'r'), colors_ap=('grey', 'k', 'k', 'k'), lss=(':', '-', '-', '-'),
              lss_ap=(':', ':', '--', '-'), vmr_ap_0=None, atmosphere=Atmosphere(),
              aspect=6.5 / 5, resolution=720, dpi=100, size=18, xlim=(0, 400), ylim=(20, 0.5)):
    plt.figure(figsize=((resolution * aspect) / dpi, resolution / dpi))

    if vmr_ap_0 is None:
        vmr_ap_0 = Molecule.VMRProfile('NH3__', path=Molecule.VMRProfile.PATH_IN, find=True).vmr

    vmr_file = atmosphere.molecules['NH3'].vmr_profile
    vmr_file.vmr = copy.deepcopy(vmr_ap_0)
    vmr_file.save()

    spectrum.atmosphere.clouds[0].transmittance = 0.132  # a priori spectrum initialisation
    spectrum.atmosphere.molecules['NH3'].retrieve = True

    for i, vmr_ap in enumerate(vmr_aps):
        vmr_file.vmr[:i_max[i]] = vmr_ap * 1E-6  # ppmv2vmr
        vmr_file.save()

        sc = spectrum.retrieve(iteration_max=32)
        vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

        vmr_ret.vmr2ppmv()

        plt.plot(vmr_file.vmr * 1E6, vmr_ret.pressure,
                 color=colors_ap[i], ls=lss_ap[i])

        plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color=colors[i], ls=lss[i],
                     xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                     capsize=5)

        vmr_file.vmr = copy.deepcopy(vmr_ap_0)
        vmr_file.save()

    vmr_ap = Molecule.VMRProfile('NH3_MWR_lat8', path=Molecule.VMRProfile.PATH_IN, find=True).vmr
    vmr_file.vmr = vmr_ap
    vmr_file.save()

    sc = spectrum.retrieve(iteration_max=32)
    vmr_ret = sc.atmosphere.molecules['NH3'].vmr_profile

    vmr_ret.vmr2ppmv()

    plt.plot(vmr_ap * 1E6, vmr_ret.pressure, color='grey', ls='--')
    plt.errorbar(vmr_ret.vmr, vmr_ret.pressure, color='b', ls='--',
                 xerr=[vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor, vmr_ret.vmr * vmr_ret.vmr_uncertainty_factor],
                 capsize=5)

    vmr_file.vmr = vmr_ap_0
    vmr_file.vmr[:22] = 300E-6
    vmr_file.save()

    if special_retrieved_spectrum is not None:
        plt.errorbar(special_retrieved_spectrum.atmosphere.molecules['NH3'].vmr_profile.vmr,  # vmr2ppmv
                     special_retrieved_spectrum.atmosphere.molecules['NH3'].vmr_profile.pressure,
                     color='g',
                     xerr=[special_retrieved_spectrum.atmosphere.molecules['NH3'].vmr_profile.vmr *
                           special_retrieved_spectrum.atmosphere.molecules['NH3'].vmr_profile.vmr_uncertainty_factor,
                           special_retrieved_spectrum.atmosphere.molecules['NH3'].vmr_profile.vmr *
                           special_retrieved_spectrum.atmosphere.molecules['NH3'].vmr_profile.vmr_uncertainty_factor],
                     capsize=5)

    plt.ylim(ylim)
    plt.xlim(xlim)
    plt.semilogy()
    plt.xlabel('NH$_3$ Volume Mixing Ratio (ppmv)')
    plt.ylabel('Pressure (bar)')

    ax = plt.gca()
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                  ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(size)
    plt.tight_layout()


def _nh3_latitude_averaged(scr1, scr2, scr3, size=18):
    scli = SpectralCube.from_pkl('MWR$')

    li = scli.get_from_spectra('atmosphere.molecules[NH3].vmr_profile.vmr',
                               np.shape(scli.spectra[0, 0].atmosphere.molecules['NH3'].vmr_profile.vmr))
    lll = copy.deepcopy(li[:, :, 589])
    lll.mask[:] = False

    lll = lll[np.where(lll > 0)]
    lat_li = np.linspace(-40, 40, np.size(lll))

    nh3_lat1 = scr1.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr')
    nh3_lat2 = scr2.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr')
    nh3_lat3 = scr3.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr')
    nh3_lat_m = np.mean([nh3_lat1[16, :], nh3_lat2[16, :], nh3_lat3[16, :]], axis=0)
    tc_lat1 = scr1.latitude_mean('atmosphere.clouds[0].transmittance')
    tc_lat2 = scr2.latitude_mean('atmosphere.clouds[0].transmittance')
    tc_lat3 = scr3.latitude_mean('atmosphere.clouds[0].transmittance')
    tc_lat_m = np.mean([tc_lat1, tc_lat2, tc_lat3], axis=0)

    errs1 = scr1.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr_uncertainty_factor')
    errs2 = scr2.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr_uncertainty_factor')
    errs3 = scr3.pressure_latitude_slice('atmosphere.molecules[NH3].vmr_profile.vmr_uncertainty_factor')
    err = np.mean([errs1[16, :], errs2[16, :], errs3[16, :]], axis=0)

    aspect = 16 / 9
    resolution = 720
    dpi = 100
    plt.figure(figsize=((resolution * aspect) / dpi, resolution / dpi))

    lat_range = np.arange(-90, 90, 1)
    std_ = np.zeros((3, np.size(lat_range)))

    for i, sc in enumerate([scr1, scr2, scr3]):
        specs = np.asarray(sc.spectra).flatten()
        wh_specs_is_not_none = np.where(np.not_equal(specs, [None]))
        latitude_map = sc.get_from_spectra('latitude')
        lats = np.asarray(latitude_map).flatten()[wh_specs_is_not_none]
        specs = specs[wh_specs_is_not_none]

        for j, lat in enumerate(lat_range):
            results = []
            for spec in specs[np.where(np.logical_and(lats >= lat, lats < lat + 1))]:
                results.append(spec.atmosphere.molecules['NH3'].vmr_profile.vmr[16])

            results = np.asarray(results)

            if np.size(results) > 0:
                std_[i, j] = np.std(results)
            else:
                std_[i, j] = 0

    std = np.mean(std_, axis=0)

    plt.plot(lat_li, lll * 1E6, label='MWR 2016-08-27 97$^\circ$W @ 1.86 bar', color='b')
    plt.plot(np.linspace(-90, 90, 180), nh3_lat_m,
             label='Texes 2016-01-16 @ 1.85 bar', color='g')
    plt.fill_between(np.linspace(-90, 90, 180), nh3_lat_m - nh3_lat_m * err,
                     nh3_lat_m + nh3_lat_m * err, facecolor='yellow')
    plt.fill_between(np.linspace(-90, 90, 180), nh3_lat_m - std,
                     nh3_lat_m + std, facecolor='palegreen')

    plt.ylim([0, 400])
    plt.ylabel(r'Mean NH$_3$ Volume Mixing Ratio (ppmv)')
    plt.xlabel(r'Planetocentric latitude ($^\circ$)')

    ax = plt.gca()
    font_size = size
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(font_size)

    plt.tight_layout()

    ax2 = plt.gca().twinx()
    ax2.plot(np.linspace(-90, 90, 180), np.asarray(tc_lat_m), color='grey', ls=':',
             label=r'mean cloud transmittance')
    plt.xlim([-69, 70])
    plt.ylim([0, 0.15])
    plt.ylabel(r'Mean cloud transmittance')
    plt.xlabel(r'Planetocentric latitude ($^\circ$)')

    ax = plt.gca()
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(font_size)
    plt.tight_layout()


def _spectrum_average(specs, sco1, sco2, sco3, latitude_range=(-90, 90)):
    wh_zones = []
    for spec in specs:
        wh_zones.append([[], []])
        for s in spec.flatten():
            if s is not None:
                if s.atmosphere.clouds[0].transmittance < 0.05:
                    if latitude_range[0] < s.latitude < latitude_range[1]:
                        wh_zones[-1][0].append(s.location[0])
                        wh_zones[-1][1].append(s.location[1])
        wh_zones[-1][:] = np.asarray(wh_zones[-1])

    wh_zones = np.asarray(wh_zones)

    sc_wh = np.array([])
    for i, so in enumerate([sco1.spectra, sco2.spectra, sco3.spectra]):
        sc_wh = np.append(sc_wh, so[tuple(wh_zones[i])])

    wvn_g = np.linspace(np.min(sco3.spectra[36][36].wavenumbers), np.max(sco2.spectra[36][36].wavenumbers), 221)

    wvn_whs = ObjList(sc_wh).get('wavenumbers')
    rad_whs = ObjList(sc_wh).get('radiances')

    rad_whs_ = []
    for i, rad in enumerate(rad_whs):
        rad_whs_.append(np.interp(wvn_g, wvn_whs[i], rad))

    nn_whs = ObjList(sc_wh).getsubattr_list('noise.radiances')

    nn_whs_ = []
    for i, rad in enumerate(nn_whs):
        nn_whs_.append(np.interp(wvn_g, wvn_whs[i], rad))

    ds_whs = ObjList(sc_wh).get('doppler_shift')

    soz = Spectrum.from_spectrum(sco2.spectra[38, 36])
    soz.wavenumbers = wvn_g
    soz.doppler_shift = np.mean(ds_whs)
    soz.atmosphere.sky = copy.deepcopy(sco2.atmosphere.sky)  # !!
    soz.radiances = np.mean(rad_whs_, axis=0)
    soz.atmosphere.clouds[0].transmittance = 0.02
    soz.atmosphere.clouds[0].reflectance = 0.15
    soz.atmosphere.clouds[0].pressure_top = 0.8
    soz.atmosphere.clouds[0].pressure_base = 0.8
    soz.atmosphere.molecules['NH3'].retrieve = True
    soz.file = '/home/dblain/Documents/OWNCLOUD/JUPITER/nrt/inputs/spectra/Jupiter_texes_20160116_zones.dat'
    soz.name = 'Jupiter_texes_20160116_zones'
    soz.path = Spectrum.PATH_IN

    nnz = copy.deepcopy(sco2.spectra[38, 36].noise)
    nnz.wavenumbers = wvn_g
    nnz.radiances = np.mean(nn_whs_, axis=0) / np.sqrt(np.size(sc_wh))
    nnz.file = \
        '/home/dblain/Documents/OWNCLOUD/JUPITER/nrt/inputs/noises/' \
        'noise_spec_cube_NRT441_Jupiter_texes_20160116-111015_1930.dat'
    nnz.file = '/home/dblain/Documents/OWNCLOUD/JUPITER/nrt/inputs/noises/noise_Jupiter_texes_20160116_zones.dat'
    nnz.name = 'noise_Jupiter_texes_20160116_zones'
    # remove major sky line
    nnz.wavenumbers = nnz.wavenumbers[70:]
    nnz.radiances = nnz.radiances[70:]
    soz.wavenumbers = soz.wavenumbers[70:]
    soz.radiances = soz.radiances[70:]

    soz.noise = nnz
    soz.save()
    nnz.save()

    soz.color = 'b'
    soz.marker = '+'
    soz.linestyle = ''

    return soz


def _multi_plot_vmr_transmittance(nh3_test, cld_test, lat_test, lon_test):
    tc_zones = 0.05
    x = np.linspace(0, 400)

    wh_zone = np.where(cld_test <= tc_zones)
    wh_belt = np.where(np.logical_and(np.logical_or(lat_test < 0, lat_test > 16), cld_test > tc_zones))
    wh_neb = np.where(np.logical_and(np.logical_and(lat_test > 0, lat_test < 16), cld_test > tc_zones))

    aspect = 6.5 / 5
    resolution = 720
    dpi = 100
    plt.figure(figsize=((resolution * aspect) / dpi, resolution / dpi))

    # plt.plot(nh3_test.flatten(), cld_test.flatten(), marker='+', ls='', color='grey')
    plt.plot(nh3_test[wh_belt].flatten(), cld_test[wh_belt].flatten(), marker='+', ls='', color='b', label='belts')
    plt.plot(nh3_test[wh_zone].flatten(), cld_test[wh_zone].flatten(), marker='+', ls='', color='r', label='zones')
    plt.plot(nh3_test[wh_neb].flatten(), cld_test[wh_neb].flatten(), marker='+', ls='', color='g', label='NEB')
    plt.plot(x, np.ones(np.size(x)) * 0.05, color='r', ls='--', label='zones')
    plt.xlim([0, 400])
    plt.ylim([0, 0.3])
    plt.xlabel(r'NH$_3$ Volume Mixing Ratio (ppmv)')
    plt.ylabel('Cloud transmittance')
    ax = plt.gca()

    size = 18

    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                  ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(size)
    plt.tight_layout()
    # belts
    plt.figure()
    nh3_ = copy.deepcopy(nh3_test)
    nh3_[wh_zone] = -1
    nh3_[wh_neb] = -1
    SpectralCube.plot_map(nh3_, cmap='RdBu_r', vmin=0, vmax=400, aspect='auto',
                          latitudes=lat_test, longitudes=lon_test)
    # zones
    plt.figure()
    nh3_ = copy.deepcopy(nh3_test)
    nh3_[wh_belt] = -1
    nh3_[wh_neb] = -1
    SpectralCube.plot_map(nh3_, cmap='RdBu_r', vmin=0, vmax=400, aspect='auto',
                          latitudes=lat_test, longitudes=lon_test)
    # NEB
    plt.figure()
    nh3_ = copy.deepcopy(nh3_test)
    nh3_[wh_zone] = -1
    nh3_[wh_belt] = -1
    SpectralCube.plot_map(nh3_, cmap='RdBu_r', vmin=0, vmax=400, aspect='auto',
                          latitudes=lat_test, longitudes=lon_test)


def _vmr_figs(sc_ret):
    """
    Plots 2 different latitudes vmr profiles side-by-side.
    Light Grey: all the vmrs profiles at the given latitude.
    Green: the mean of those vmr profiles, and their 16th and 84th percentiles.
    """
    plt.close('all')
    plt.figure(figsize=(14.8, 7.2))
    plt.subplot(122)
    lats = stack_arrays(ObjList(sc_ret).get('latitude_map'))
    spec_map = stack_arrays([sc_ret[0].spectrum_map, sc_ret[1].spectrum_map, sc_ret[2].spectrum_map])
    plots = []
    for i, spec in enumerate(spec_map.flatten()):
        if spec is not None and spec != 0:
            if 6 < lats.flatten()[i] < 8 and spec.atmosphere.clouds[0].t > 0.05:
                plots.append(spec.atmosphere.molecules['NH3'].vmr_profile.vmr)
                plt.plot(spec.atmosphere.molecules['NH3'].vmr_profile.vmr,
                         spec.atmosphere.molecules['NH3'].vmr_profile.p, color='lightgrey')
    plt.plot(np.mean(plots, axis=0), spec_map[35][35].atmosphere.molecules['NH3'].vmr_profile.p, color='g')
    plt.plot(np.percentile(plots, axis=0, q=16), spec_map[35][35].atmosphere.molecules['NH3'].vmr_profile.p,
             color='g', linestyle=':')
    plt.plot(np.percentile(plots, axis=0, q=84), spec_map[35][35].atmosphere.molecules['NH3'].vmr_profile.p,
             color='g', linestyle=':')
    plt.ticklabel_format(useMathText=True)
    plt.gca().get_xaxis().get_major_formatter().set_powerlimits((-3, 3))
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
    plt.gca().invert_yaxis()
    plt.ylim(2E1, 1E-1)
    plt.xlim(0, None)
    plt.semilogy()
    plt.xlabel(r'[NH$_3$] (ppmv)')
    plt.ylabel('Pressure (bar)')
    plt.text(300, 2E-1, r'latitude = 7$\pm$1$^\circ$N', horizontalalignment='right', size=12)
    for item in ([plt.gca().title, plt.gca().xaxis.label,
                  plt.gca().yaxis.label] + plt.gca().get_xticklabels() + plt.gca().get_yticklabels()):
        item.set_fontsize(12 * 1)

    plt.subplot(121)
    plots = []
    for i, spec in enumerate(spec_map.flatten()):
        if spec is not None and spec != 0:
            if -20 < lats.flatten()[i] < -18 and spec.atmosphere.clouds[0].t > 0.05 and \
                    spec.atmosphere.viewing_angle < 75:
                plots.append(spec.atmosphere.molecules['NH3'].vmr_profile.vmr)
                plt.plot(spec.atmosphere.molecules['NH3'].vmr_profile.vmr,
                         spec.atmosphere.molecules['NH3'].vmr_profile.p, color='lightgrey')
    plt.plot(np.mean(plots, axis=0), spec_map[35][35].atmosphere.molecules['NH3'].vmr_profile.p, color='g')
    plt.plot(np.percentile(plots, axis=0, q=16), spec_map[35][35].atmosphere.molecules['NH3'].vmr_profile.p,
             color='g', linestyle=':')
    plt.plot(np.percentile(plots, axis=0, q=84), spec_map[35][35].atmosphere.molecules['NH3'].vmr_profile.p,
             color='g', linestyle=':')
    plt.ticklabel_format(useMathText=True)
    plt.gca().get_xaxis().get_major_formatter().set_powerlimits((-3, 3))
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
    plt.gca().invert_yaxis()
    plt.ylim(2E1, 1E-1)
    plt.xlim(0, None)
    plt.semilogy()
    plt.xlabel(r'[NH$_3$] (ppmv)')
    plt.ylabel('Pressure (bar)')
    plt.text(400, 2E-1, r'latitude = 19$\pm$1$^\circ$S', horizontalalignment='right', size=12)
    for item in ([plt.gca().title, plt.gca().xaxis.label,
                  plt.gca().yaxis.label] + plt.gca().get_xticklabels() + plt.gca().get_yticklabels()):
        item.set_fontsize(12 * 1)
    plt.savefig('NH3_vmr.eps', dpi=300, orientation='portrait', format='eps')


def _spec_figs(sc_ret, sco):
    """
    Plots all the spectra of a given latitude. Similar to _vmr_figs().
    """
    plt.close('all')
    plt.figure(figsize=(7.2, 7.2))
    lats = stack_arrays(ObjList([sc_ret[0], sc_ret[1], sc_ret[2]]).get('latitude_map'))
    lats_o = stack_arrays(ObjList([sco[0], sco[1], sco[2]]).get('latitude_map'))
    spec_map = stack_arrays([sc_ret[0].spectrum_map, sc_ret[1].spectrum_map, sc_ret[2].spectrum_map])
    spec_map_o = stack_arrays([sco[0].spectrum_map, sco[1].spectrum_map, sco[2].spectrum_map])
    plots = []
    plots_obs = []

    masked_map = np.ma.masked_values(spec_map_o, 0).filled(0)
    wvn_obs_min = np.max(np.array(ObjList(masked_map[np.where(masked_map != 0)]).getsubattr_list('wvn[0]')))
    wvn_obs_max = np.min(np.array(ObjList(masked_map[np.where(masked_map != 0)]).getsubattr_list('wvn[-1]')))
    print(wvn_obs_min, wvn_obs_max)
    for i, spec in enumerate(spec_map_o.flatten()):
        if spec is not None and spec != 0:
            if 6 < lats_o.flatten()[i] < 8 and np.max(spec.radw) > 3E-8:
                plots_obs.append(spec.radw[np.where(np.logical_and(spec.wvn >= wvn_obs_min, spec.wvn <= wvn_obs_max))])
                plt.plot(spec.wvn, spec.radw, color='lightgrey')

    masked_map = np.ma.masked_values(spec_map_o, 0).filled(0)
    wvn_min = np.max(np.array(ObjList(masked_map[np.where(masked_map != 0)]).getsubattr_list('wvn[0]')))
    wvn_max = np.min(np.array(ObjList(masked_map[np.where(masked_map != 0)]).getsubattr_list('wvn[-1]')))
    print(wvn_min, wvn_max)
    for i, spec in enumerate(spec_map.flatten()):
        if spec is not None and spec != 0:
            if 6 < lats.flatten()[i] < 8 and np.max(spec.radw) > 3E-8:
                plots.append(spec.radw[np.where(np.logical_and(spec.wvn > wvn_min, spec.wvn < wvn_max))])

    print(1)
    plt.plot(
        spec_map_o[125][35].wvn[
            np.where(np.logical_and(spec_map_o[125][35].wvn >= wvn_obs_min, spec_map_o[125][35].wvn <= wvn_obs_max))],
        np.mean(plots_obs, axis=0), color='b')
    print(2)
    plt.plot(
        spec_map[35][35].wvn[np.where(np.logical_and(spec_map[35][35].wvn > wvn_min, spec_map[35][35].wvn < wvn_max))],
        np.mean(plots, axis=0), color='g')
    print(3)
    plt.ticklabel_format(useMathText=True)
    plt.xlabel(r'[NH$_3$]')
    plt.ylabel('Pressure (bar)')
    plt.ylim(0, None)
    plt.xlim(1932.5, 1940.5)
    plt.text(3E-4, 2E-1, r'latitude = 7$\pm$1$^\circ$E', horizontalalignment='right')
    plt.savefig('specs.eps', dpi=300, orientation='portrait', format='eps')


def _read_li_data():
    li = DataFile('1019_hanley', find=True, ext='txt')
    with open(li.file, 'r') as f:
        pressures = f.readline()
        pressures = pressures.strip()
        pressures = pressures.split()

    pressures = [float(val) for val in pressures]

    data = li.read()

    sm = SpectralCube('NH3_MWR', path=SpectralCube.PATH_OUT)

    sm.spectra = ObjList()
    for i, lat in tqdm(enumerate(data[0])):
        sm.spectra.append(Spectrum(sm.name + f'_lat{lat}'))
        sm.spectra[-1].latitude = lat
        sm.spectra[-1].instrument = 'MWR'
        sm.spectra[-1].location = i
        sm.spectra[-1].atmosphere = copy.deepcopy(Atmosphere.from_pkl('jupiter'))

        vmr = []
        for j, p in enumerate(pressures):
            vmr.append(np.asarray(data)[j + 1, i])

        sm.spectra[-1].atmosphere.molecules['NH3'].vmr_profile.vmr = np.asarray(vmr)
        sm.spectra[-1].atmosphere.molecules['NH3'].vmr_profile.pressures = np.asarray(pressures)

    sm.save()


def h2o_sublimation_pressure(temperature):
    """
    Calculate the h2o pressure of sublimation (below the triple point).
    Source: Fray & Schmitt 2009.
    :param temperature: (K) temperature
    :return: (bar) H2O sublimation pressure
    """
    pressure_triple_point = 6.11657E-3  # pressure of H2O triple point
    temperature_triple_point = 273.16  # temperature of H2O triple point
    e = [20.996967, 3.724375, -13.920548, 29.698877, -40.197239, 29.788048, -9.130510]  # Feistel and Wagner 2007
    eta = 0

    for i, ei in enumerate(e):
        eta += ei * (temperature / temperature_triple_point) ** i

    pressure_sublimation = pressure_triple_point * np.exp(3 / 2 * np.log(temperature / temperature_triple_point) +
                                                          (1 - temperature_triple_point / temperature) * eta)

    return pressure_sublimation


def rayleigh_scattering(i_0=1, r=1, alpha=0.865, lmd=np.linspace(100E-9, 10E-6, 100),
                        theta=np.linspace(0, 2 * np.pi, 100)):
    """
    Rayleigh scattering from individual molecules.
    :param i_0: initial intensity
    :param r: distance to the particle
    :param alpha: polarizability of the molecule
    :param lmd: (m) wavelengths of light
    :param theta (rad) angles of diffusion
    :return:
    """
    i = []
    for ll in lmd:
        i.append(i_0 * 8 * np.pi ** 4 * alpha ** 2 / (ll ** 4 * r ** 2) * (1 + np.cos(theta) ** 2))

    return np.asarray(i)


def extract_slices(path, wavelengths, target, max_slices=-1, noise=None, i_min=None, i_max=None,
                   max_slices_of_slices=500):
    wavenumbers = wavelength2wavenumber(wavelengths)[::-1]
    atmosphere = Atmosphere('jupiter_default')

    # Get slice files
    slice_files = FortranUnformattedFile.search('JIR_SPE_RDR', path=path, ext='DAT')

    slices_of_slices_files = []
    i = 0

    while np.size(slice_files) > i <= max_slices > 0:

        slices = []

        max_sf = np.min([i + max_slices_of_slices, np.size(slice_files)])
        i0 = i

        for slice_file in tqdm(slice_files[i:max_sf],
                               desc=f'Extracting slices, {i} / {np.size(slice_files)}'):
            if i >= max_slices > 0:
                i = np.inf
                break

            label_file = PDS3LabelFile(slice_file.name, path=path, exists=True)
            new_slice = SpectralSlice.from_radiance_file(slice_file, metadata_file=label_file,
                                                         atmosphere=atmosphere,
                                                         wavenumbers=wavenumbers, noise=noise)
            if new_slice.target is not None:
                if new_slice.target.name != target:
                    info(f'rejected slice {i}: slice target \'{new_slice.target.name}\' '
                         f'is different of desired target \'{target}\'')
                    # slices.append(None)

                    i += 1
                    continue
                else:
                    slices.append(new_slice)
            else:
                info(f'rejected slice {i}: slice target \'{None}\' '
                     f'is different of desired target \'{target}\'')
                i += 1
                continue

            # Convert from W.m-2.sr-1/um to W.m-2.sr-1/cm-1, and keep
            for j, spectrum in enumerate(slices[-1].spectra):
                slices[-1].spectra[j].radiances = radiance_um2radiance_wvn(spectrum.radiances, wavelengths[::-1])
                slices[-1].spectra[j].radiances = slices[-1].spectra[j].radiances[i_min:i_max]
                slices[-1].spectra[j].wavenumbers = slices[-1].spectra[j].wavenumbers[i_min:i_max]

            slices[-1].wavenumbers = slices[-1].wavenumbers[i_min:i_max]

            # Assign instrument
            slices[-1].instrument.name = 'juno_jiram_s'
            slices[-1].instrument.update_file()
            if not os.path.isfile(slices[-1].instrument.file):
                slices[-1].instrument.save()

            i += 1

        file = PickleFile(name=f'slices_{i0}-{i - 1}', path=SpectralSlice.PATH)
        slices_of_slices_files.append(file)

        # Save slices
        with open(file.file, 'wb') as file:
            pickle.dump(slices, file, -1)

    return slices_of_slices_files


def get_jiram_noise(path, wavelengths, nickname=''):
    noise_slices = extract_slices(path=path, wavelengths=wavelengths, target=None)

    # Clear None in noise_slices
    # cleaned_noise_slices = np.delete(noise_slices, np.where(np.logical_not(noise_slices)))

    sc_noise = SpectralCube.from_spectral_slices(
        spectral_slices=noise_slices,
        nickname=nickname,
        target=None,
    )

    radiances = sc_noise.get_from_spectra('radiances', sc_noise.wavenumbers.shape)
    radiances = np.ma.masked_invalid(radiances)
    noise = np.std(radiances, axis=0)

    # Filter for each pixel the spectels with a noise 5-sigmas higher than the average noise of this spectel on the slit
    noise_spectel_threshold = np.mean(noise, axis=0) + 5 * np.std(noise, axis=0)  # spectel threshold

    noise_filtered = np.ma.masked_greater(noise, noise_spectel_threshold).filled(np.nan)

    # Filter for all pixels the spectels with a noise 3-sigma higher than the total mean of the noise
    noise_threshold = np.mean(noise) + 3 * np.std(noise)
    threshold_condition = np.mean(noise, axis=0) > noise_threshold
    threshold_condition = np.expand_dims(threshold_condition, axis=0).repeat(noise.shape[0], 0)

    noise_filtered = np.ma.masked_where(threshold_condition, noise_filtered).filled(np.nan)

    noise_files = []
    for i, pixel_noise in enumerate(noise_filtered):
        noise_name = f'noise_jiram_{nickname}_{i}'

        noise_files.append(Noise(
            name=noise_name,
            wavenumbers=sc_noise.wavenumbers,
            radiances=noise_filtered[i]
        ))
        noise_files[-1].save()

    return noise_files


def read_jiram_directory(directory, jiram_data_path=File.PATH_DATA + 'obs/juno/jiram', use_metakernel=False,
                         pixel_number=256, max_slices=np.inf, target=None):
    """
    Read a JIRAM directory and extract its content into SpectralCubes.
    :param directory: name of the directory following the jiram_data_path (2002, 2003, ...)
    :param jiram_data_path: NRT path where the JIRAM data are stored
    :param use_metakernel: if True, read the Spice metakernel corresponding to the data being read, and use it to
    calculate geometry
    :param pixel_number: number of pixels in a JIRAM's slit
    :param max_slices: maximum number of slices to be extracted (useful for debuggong)
    :param target: target of the observations (i.e. 'jupiter')
    """
    if target is None:
        target = Target('None')

    # Get JIRAM data path
    path = jiram_data_path + '/' + directory

    # Get wavenumbers from Grassi wavelengths file
    wavelengths = DataFile('JIR_WVL', path=jiram_data_path, find=True).read()[0] * 1E-9  # nm to m

    # Get noise from files or JIRAM data
    noise_files = []

    for i in range(pixel_number):
        noise_name = f'noise_jiram_{directory}_{i}'

        noise_files.append(Noise(noise_name))

        # if not os.path.isfile(noise_files[-1].file):
        #     info(f'no .{Noise.EXT} file with name \'{noise_name}\' found in directory \'{Noise.PATH_IN}\'\n'
        #          f'Extracting noises from JIRAM data...')
        #     noise_files = get_jiram_noise(path=path, wavelengths=wavelengths, nickname=directory)

    # Extract slices
    # Keep only the 70 (i_max) first wavenumbers (1994--2275 cm-1), as most information is stored in this spectral range
    # And cut the extraction by slices of 200 slices, to keep memory usage low (roughly 7 Go/SpectralCube).
    slices_files = extract_slices(path=path, wavelengths=wavelengths, target=target.name,
                                  max_slices=max_slices, noise=noise_files,
                                  i_max=70, max_slices_of_slices=200)

    for sf in tqdm(slices_files, desc='Loading slices'):
        with open(sf.file, 'rb') as file:
            slices = pickle.load(file)

        # Get SPICE metakernel to calculate geometry
        if use_metakernel:
            metakernel = SpiceMetaKernel('jiram.*' + directory, path=SpiceMetaKernel.PATH_DATA, find=True)
        else:
            metakernel = None

        # Calculate geometry and store results in a SpectralCube
        sc = SpectralCube.from_spectral_slices(spectral_slices=slices, metakernel=metakernel,
                                               nickname=directory + sf.name, target=target)

        # noise_spectra = np.full((sc.spectra.shape[0], sc.spectra.shape[1], sc.wavenumbers.size), np.nan)
        #
        # for i, slc in enumerate(tqdm(sc.spectra, desc='Extracting noise')):
        #     for j, spec in enumerate(slc):
        #         if spec is not None:
        #             if np.isnan(spec.target.latitude).all() or np.isnan(spec.target.longitude).all():
        #                 noise_spectra[i, j] = spec.radiances
        #             elif np.isnan(spec.target.latitude).any() or np.isnan(spec.target.longitude).any():
        #                 sc.spectra[i, j] = None  # reject not fully intercepted spectra
        #             else:
        #                 sc.spectra[i, j].doppler_shift = np.mean(spec.doppler_shift[0])

        for i, s in tqdm(enumerate(sc.spectra)):
            for j, s_ in enumerate(s):
                if s_ is not None:
                    if s_.target.emission_angle > 90 or s_.target.incidence_angle > 90 or np.isnan(
                            s_.target.latitude).any() or np.isnan(s_.target.longitude).any():
                        sc.spectra[i, j] = None
                    else:
                        sc.spectra[i, j].doppler_shift = np.mean(s_.doppler_shift)

                        print(sc.spectra[i, j].target.light_source_range)  # TODO remove this line

        # Save
        sc.save()

    return


def jiram_5um_selection(spectral_cube):
    i_5um = 70

    # Noise 5 um
    noises_5um = []
    for i, noise in enumerate(spectral_cube.noise):
        noise.load()
        noises_5um.append(noise)
        noises_5um[-1].wavenumbers = noises_5um[-1].wavenumbers[:i_5um]
        noises_5um[-1].radiances = noises_5um[-1].radiances[:i_5um]

        noises_5um[-1].file = noises_5um[-1].file.rsplit('_', 1)[0] + f'_5um_{i}.{noise.EXT}'
        noises_5um[-1].update_parameters()
        noises_5um[-1].save()

    spectral_cube.noise = noises_5um

    for i, spectral_slice in enumerate(spectral_cube.spectra):
        for j, spectrum in enumerate(spectral_slice):
            if spectrum is not None:
                # Normalisation: for the quantities below, the Fortran code accept only one value as input, not an array
                # TODO modify code to make this step unnecessary
                spectral_cube.spectra[i, j].doppler_shift = np.mean(spectrum.doppler_shift)
                spectral_cube.spectra[i, j].atmosphere.emission_angle = np.mean(spectrum.atmosphere.emission_angle)
                spectral_cube.spectra[i, j].atmosphere.incidence_angle = np.mean(spectrum.atmosphere.incidence_angle)

                # 5um
                spectral_cube.spectra[i, j].wavenumbers = spectral_cube.spectra[i, j].wavenumbers[:i_5um]
                spectral_cube.spectra[i, j].radiances = spectral_cube.spectra[i, j].radiances[:i_5um]
                spectral_cube.spectra[i, j].noise = spectral_cube.noise[j]

    spectral_cube.file = spectral_cube.file.rsplit('.')[0] + f'_5um.{SpectralCube.EXT}'
    spectral_cube.update_parameters()
    spectral_cube.save()

    return spectral_cube


def jiram_retrieval(spectral_cubes, molecules_to_retrieve=('NH3', 'PH3'), coordinate_drool_tolerance=-1):
    """

    :param spectral_cubes:
    :param molecules_to_retrieve:
    :param coordinate_drool_tolerance:
    :return:
    """
    if not hasattr(spectral_cubes, '__iter__'):
        spectral_cubes = [spectral_cubes]

    if coordinate_drool_tolerance < 0:
        coordinate_drool_tolerance = 360

    # Data selection and preparation
    for s, spectral_cube in enumerate(spectral_cubes):
        for i, spectral_slice in enumerate(spectral_cube.spectra):
            for j, spectrum in enumerate(spectral_slice):
                if spectrum is not None:
                    # Get pixel boresights mean coordinates
                    lat = np.mean(spectrum.latitude, axis=1)
                    lon = np.mean(spectrum.longitude, axis=1)

                    lat_exposure_time_drool = np.abs(lat[0] - lat[1])
                    lon_exposure_time_drool = np.abs(lon[0] - lon[1])

                    if lon_exposure_time_drool > 180:
                        lon_exposure_time_drool = 360 - lon_exposure_time_drool

                    if \
                            lat_exposure_time_drool > coordinate_drool_tolerance or \
                            lon_exposure_time_drool > coordinate_drool_tolerance:
                        spectral_cubes[s].spectra[i, j] = None
                    else:
                        # Mean of emission and incidence angles over time
                        spectral_cubes[s].spectra[i, j].atmosphere.emission_angle = \
                            np.mean(spectrum.atmosphere.emission_angle)
                        spectral_cubes[s].spectra[i, j].atmosphere.incidence_angle = \
                            np.mean(spectrum.atmosphere.incidence_angle)

                        # Mean of Doppler shift over wavenumber
                        spectral_cubes[s].spectra[i, j].doppler_shift = \
                            np.mean(spectrum.doppler_shift)

    # Retrieval
    sc_ret = []
    for sc in spectral_cubes:
        sc_ret.append(sc.retrieve_accurate(
            molecules_to_retrieve=molecules_to_retrieve,
            wavenumber_step=0.1,
            iteration_max=32,
            plot_final=False
        ))
        sc_ret[-1].save()

    return sc_ret


def ite_stats(file_name_template):
    """Use the log files to plot an histogram of the number of iteration needed to reach the final result."""
    files = File.search(file_name_template, ext='log')
    ir = []
    chi2_r = []
    cld_t = []
    for file in files:
        with open(file.file, 'r') as f:
            chi2_r_ = 0
            cld_t_ = 0
            for line in f:
                if 'cloud  1 trans' in line:
                    cld_t_ = float(line.split('=')[1].split('+')[0])

                if 'chi2' in line and '(ite.' in line:
                    chi2_r_ = float(line.split('(')[1].replace(')', ''))

                if 'ite. to result' in line:
                    ir.append(np.int(line.split('=')[1]))
                    chi2_r.append(chi2_r_)
                    cld_t.append(cld_t_)

    return np.asarray(ir), np.asarray(chi2_r), np.asarray(cld_t)


def ephemeris_sublon_day_plot(ephemeris):
    """Plot the sub-observer longitude in function of the day of month at some defined periods for a given ephemeris."""
    ephemeris.read_from_jplhorizon()

    param = np.asarray(ephemeris.angular_diameter)

    wh_early_feb = np.where(
        np.logical_and(np.asarray(ephemeris.julian_date) > ephemeris.date2julian('2018-02-01 00:00:00.000'),
                       np.asarray(ephemeris.julian_date) < ephemeris.date2julian('2018-02-16 00:00:00.000')))
    wh_mid_mar = np.where(
        np.logical_and(np.asarray(ephemeris.julian_date) > ephemeris.date2julian('2018-03-15 00:00:00.000'),
                       np.asarray(ephemeris.julian_date) < ephemeris.date2julian('2018-04-01 00:00:00.000')))
    wh_late_jul = np.where(
        np.logical_and(np.asarray(ephemeris.julian_date) > ephemeris.date2julian('2018-07-15 00:00:00.000'),
                       np.asarray(ephemeris.julian_date) < ephemeris.date2julian('2018-08-01 00:00:00.000')))
    wh_late_jun = np.where(
        np.logical_and(np.asarray(ephemeris.julian_date) > ephemeris.date2julian('2018-06-15 00:00:00.000'),
                       np.asarray(ephemeris.julian_date) < ephemeris.date2julian('2018-07-01 00:00:00.000')))
    wh_dates = {'feb_early': wh_early_feb, 'mar_mid': wh_mid_mar, 'jun_late': wh_late_jun, 'jul_late': wh_late_jul}

    for wh_date in wh_dates:
        print(wh_date, np.min(param[wh_dates[wh_date]]))
        print(wh_date, np.max(param[wh_dates[wh_date]]))
    print('all', np.min(param))
    print('all', np.max(param))

    # best obs (max elevation) for given lon
    min_elevation = 52
    target_lon = 7
    lon_bin = 20
    lon_bin2 = 75

    feb_j = ephemeris.date2julian('2018-02-01 00:00:00.000')
    mar_j = ephemeris.date2julian('2018-03-01 00:00:00.000')
    jun_j = ephemeris.date2julian('2018-06-01 00:00:00.000')
    jul_j = ephemeris.date2julian('2018-07-01 00:00:00.000')

    months = {'feb_early': feb_j, 'mar_mid': mar_j, 'jun_late': jun_j, 'jul_late': jul_j}
    subplots = {'feb_early': 221, 'mar_mid': 222, 'jun_late': 223, 'jul_late': 224}

    for wh_date in wh_dates:
        # plt.figure()
        plt.subplot(subplots[wh_date])
        lon = np.asarray(copy.deepcopy(np.asarray(ephemeris.obs_sublon)[wh_dates[wh_date]]))
        lon -= 180
        date = np.asarray(ephemeris.julian_date)[wh_dates[wh_date]] - months[wh_date] + 1

        plt.plot(date, lon, marker='+', linestyle='', color='orange', label=r'elev < 30$^\circ$')
        plt.plot(date[np.where(np.asarray(ephemeris.elevation)[wh_dates[wh_date]] > 30)],
                 lon[np.where(np.asarray(ephemeris.elevation)[wh_dates[wh_date]] > 30)],
                 marker='+', linestyle='', color='b', label=r'elev < 52$^\circ$')
        plt.plot(date[np.where(np.asarray(ephemeris.elevation)[wh_dates[wh_date]] > min_elevation)],
                 lon[np.where(np.asarray(ephemeris.elevation)[wh_dates[wh_date]] > min_elevation)],
                 marker='+', linestyle='', color='g', label=r'elev > 52$^\circ$')
        plt.plot(date, date * 0 + target_lon, color='r', linewidth=0.5)
        plt.plot(date, date * 0 + target_lon - lon_bin, color='r', linestyle='--', linewidth=0.5)
        plt.plot(date, date * 0 + target_lon + lon_bin, color='r', linestyle='--', linewidth=0.5)
        plt.plot(date, date * 0 + target_lon - lon_bin2, color='r', linestyle=':', linewidth=0.5)
        plt.plot(date, date * 0 + target_lon + lon_bin2, color='r', linestyle=':', linewidth=0.5)
        plt.title(wh_date)
        minor_ticks = np.arange(np.ceil(plt.gca().get_xlim()[0]),
                                np.floor(plt.gca().get_xlim()[1]), 1)
        plt.gca().set_xticks(minor_ticks, minor=True)
        plt.ylim([-180, 180])
        plt.xlabel('Day of month')
        plt.ylabel('Sub-observer longitude ($^\circ$W sys III)')
        plt.gca().xaxis.grid(linestyle=':', which='both')
        plt.legend()


def sky_cor_test_fts98():
    """Test to validate the sky correction cancellation method."""
    s = Spectrum('fts98_BF', Spectrum.PATH_OUT)
    sobs = Spectrum('fts98', Spectrum.PATH_IN, find=True)

    sobs.atmosphere = copy.deepcopy(s.atmosphere)
    sobs.atmosphere.sky = Sky('fts98', find=True)
    sobs.atmosphere.viewing_angle = np.rad2deg(np.arccos(1 / 1.30))
    sobs.atmosphere.sol_angle = np.rad2deg(np.arccos(1 / 1.30))
    sobs.atmosphere.clouds[0].p_base = 1.0
    sobs.atmosphere.clouds[0].p_top = 1.0
    sobs.atmosphere.clouds[0].t = 0.198
    sobs.atmosphere.molecules['AsH3'].retrieve = True
    sobs.atmosphere.molecules['CO'].retrieve = True
    sobs.atmosphere.molecules['H2O'].retrieve = True
    sobs.atmosphere.molecules['GeH4'].retrieve = True
    sobs.atmosphere.molecules['NH3'].retrieve = True
    sobs.atmosphere.molecules['PH3'].retrieve = True
    sobs.instrument = Instrument('fts98', find=True)
    s.atmosphere = copy.deepcopy(sobs.atmosphere)

    # run_nrt(s, [sobs], exec_nrt=True, convolve=True, retrieve=True, plot_ap=False)

    sobs.color = 'b'
    sobs.linestyle = ''
    sobs.marker = '+'
    sb = Spectrum('fts98.*Bezard.*_skyconv', Spectrum.PATH_DATA, find=True)
    print(sb.file)
    print(s.file)
    # sky = Atmosphere.Sky('fts98', find=True)
    sbs = copy.deepcopy(sb)
    sbs.radiances *= 0.2

    sobs.label = 'Bezard 2002 data'
    sbs.label = 'Bezard 2002 best fit'
    s.errorbar([sobs, sbs, s], plot_residuals=True, interp_for_plot=True, boundaries='tight')

    list_vmrs = []
    for mol in s.atmosphere.molecules:
        if s.atmosphere.molecules[mol].retrieve:
            list_vmrs.append([s.atmosphere.Molecule.vmr_profiles_ap[mol]])
            s.atmosphere.molecules[mol].vmr_profile = Molecule.VMRProfile(
                s.name + '_' + mol + '_vmr', Molecule.VMRProfile.PATH_OUT, find=True)
            s.atmosphere.molecules[mol].vmr_profile.load()
            s.atmosphere.molecules[mol].vmr_profile.color = 'g'
            s.atmosphere.molecules[mol].vmr_profile.label = 'Best fit'
            s.atmosphere.molecules[mol].vmr_profile.name = f'{mol}'
            list_vmrs[-1].append(s.atmosphere.molecules[mol].vmr_profile)

    s.color = 'g'
    s.label = 'this study'
    wh_bruno = np.where(np.logical_and(s.wavenumbers > 2090, s.wavenumbers < 2170))
    s.wavenumbers = s.wavenumbers[wh_bruno]
    s.radiances = s.radiances[wh_bruno]
    s.errorbar(spectra=[sobs, sbs, s], plot_residuals=True, interp_for_plot=True)


def derivative_test(test_name, mol_to_test, level_to_test, test_factor=0.1, cloud=False, sky=False):
    """
    Test the radiance derivatives in respect with the logarithm of the molecular vmr.
    The spectrum difference should be close to the derivative for a small test factor.
    :param test_name: name of the test, for the outputted files
    :param mol_to_test: molecule of the derivative to test
    :param level_to_test: level of the derivative
    :param test_factor: factor of change in the vmr profile of the tested molecule at the tested level
    :param cloud: if True, a cloud will be added
    :param sky: if True, the sky will be added
    """
    i_mol = None
    for i, mol in enumerate(Atmosphere().molecules):
        if mol == mol_to_test:
            i_mol = i
            break

    if i_mol is None:
        raise NameError(f'molecule \'{mol_to_test}\' not found in database')

    vmr_mol = Molecule.VMRProfile(mol_to_test, Molecule.VMRProfile.PATH_IN, exists=True)
    backup_value = copy.copy(vmr_mol.vmr[level_to_test])
    vmr_mol.vmr[level_to_test] *= test_factor
    print(vmr_mol.vmr[level_to_test], backup_value)
    vmr_mol.save()

    sc_obs = SpectralCube('jup', SpectralCube.PATH_DATA, find=True)
    if cloud:
        sc_obs.spectra[14][49].atmosphere.clouds[0].transmittance = 0.1

    if not sky:
        sc_obs.spectra[14][49].atmosphere.sky = None

    wavenumbers = sc_obs.spectra[14][49].wvn

    sc_obs.spectra[14][49].atmosphere.clouds[0].p_base = 1.0
    sc_obs.spectra[14][49].atmosphere.clouds[0].p_top = 1.0
    s_test = Spectrum.calculate_radiances(wavenumber_lim=wavenumbers, atmosphere=sc_obs.spectra[14][49],
                                          spec_base_name=test_name, output_derivfile=False)

    vmr_mol.vmr[level_to_test] = backup_value
    vmr_mol.save()

    s_ref = Spectrum.calculate_radiances(wavenumber_lim=wavenumbers, atmosphere=sc_obs.spectra[14][49].atmosphere,
                                         spec_base_name=test_name + '_ref', output_derivfile=True)

    der_ref = DerivativeVMR(s_ref.name + 'da', Spectrum.PATH_OUT)
    der_ref.read()

    plt.subplot(211)
    plt.plot(s_ref.wvn, s_ref.radw - s_test.radw, color='b',
             label=f'ref spectrum - test spectrum (factor:{test_factor})')
    plt.plot(der_ref.wavenumbers, der_ref.d_radiances[:, level_to_test, i_mol], color='g',
             label=f'derivative @lvl{level_to_test}, {mol_to_test}')
    plt.plot(der_ref.wavenumbers, der_ref.d_radiances[:, level_to_test, i_mol] * (1 - test_factor), color='grey',
             linestyle=':', label='derivative * (1-factor)')
    plt.legend()

    plt.subplot(212)
    wh_sup0 = np.where(np.abs(der_ref.d_radiances[:, level_to_test, i_mol]) > 0)
    # diff = (der_ref.d_radiance[:, level_to_test, i_mol] - (s_ref.radiance - s_test.radiance)) / \
    #        np.max(np.abs(der_ref.d_radiance[:, level_to_test, i_mol]))
    x = np.asarray(s_ref.radw - s_test.radw)[wh_sup0] / \
        np.asarray(der_ref.d_radiances[:, level_to_test, i_mol])[wh_sup0]
    diff_red = (der_ref.d_radiances[:, level_to_test, i_mol] * (1 - test_factor) - (
            s_ref.radw - s_test.radw)) / np.max(np.abs(der_ref.d_radiances[:, level_to_test, i_mol]))
    plt.plot(der_ref.wavenumbers, diff_red, color='b', label='true diff')
    diff_act = (der_ref.d_radiances[:, level_to_test, i_mol] * np.sign(np.max(x)) * np.max(np.abs(x)) - (
            s_ref.radw - s_test.radw)) / np.max(np.abs(der_ref.d_radiances[:, level_to_test, i_mol]))
    plt.plot(der_ref.wavenumbers, diff_act, color='r', label='actual diff')
    plt.plot(der_ref.wavenumbers[wh_sup0], x, color='orange', label='actual diff w/o correction')
    plt.legend()

    print(f'true difference factor: {1 - test_factor} \n'
          f'actual difference factor: {np.sign(np.max(x)) * np.max(np.abs(x))} \n'
          f'ratio true/actual: {(1 - test_factor) / np.max(x)} \n'
          f'mean residual difference: {np.mean(diff_red)}\n'
          f'mean residual actual diff: {np.mean(diff_act)}')


def plot_vmr_derivatives(spectrum, p_cloud=1.0, t_cloud=(1.0, 0.2, 0.1, 0.05, 0.01, 0.001), compile_nrt=False):
    """Plot the derivatives of the radiance over the VMR of the molecules."""
    spectrum.atmosphere.clouds[0].p_base = p_cloud
    spectrum.atmosphere.clouds[0].p_top = p_cloud

    for ct in t_cloud:
        spectrum.atmosphere.clouds[0].transmittance = ct
        sr = spectrum.retrieve(iteration_max=0, output_derivatives=True, compile_nrt=compile_nrt)
        a = DerivativeVMR(sr.name + 'da', path=DerivativeVMR.PATH_OUT, find=True)
        a.read()

        plt.contourf(a.wavenumbers, a.pressures, np.transpose(a.d_radiances[:, :, 6]), cmap='inferno_r',
                     levels=np.linspace(-1E-8, 0, 21))
        plt.gca().invert_yaxis()
        plt.semilogy()
        plt.ylim([20, 5E-1])
        cbar = plt.colorbar()
        plt.xlabel(r'$\tilde{\nu}$ (cm$^{-1}$)')
        plt.ylabel(r'pressure (bar)')
        cbar.set_label('')
        plt.title(f't cloud = {spectrum.atmosphere.clouds[0].transmittance}')
        plt.savefig(
            f'kernel_vmr_NH3_cloud_t' + f'{spectrum.atmosphere.clouds[0].transmittance:.3f}'.replace(".", ""))
        compile_nrt = False


def spikes_map(sc_ret, sc_obs, wvn_min=2149.0, wvn_max=2149.6, ratio_min=0.4, ratio_max=1.6, cmap='RdBu',
               lat_lon_overlay=False):
    """Map of the ratio of the radiance between observed spectra and retrieved spectra in a specified wavenumber 
    range."""
    # "spike": 2149.0 - 2149.6 cm-1, "mid-lobe dip": 2141.9 - 2142.5 cm-1
    wh_spike_obs = np.where(np.logical_and(sc_obs.spectra[37][33].wavenumbers >= wvn_min,
                                           sc_obs.spectra[37][33].wavenumbers <= wvn_max))
    wh_spike_inv = np.where(np.logical_and(sc_ret.spectra[37][33].wavenumbers >= wvn_min,
                                           sc_ret.spectra[37][33].wavenumbers <= wvn_max))
    spike_map = np.zeros(np.shape(sc_ret.spectra))
    for s_obs in tqdm(np.asarray(sc_obs.spectra).flatten()):
        if s_obs is not None:
            for s_inv in np.asarray(sc_ret.spectra).flatten():
                if s_inv is not None:
                    if s_inv.location == s_obs.location:
                        spike_map[s_obs.location[0], s_obs.location[1]] = np.mean(s_obs.radiances[wh_spike_obs]) / \
                                                                          np.mean(np.interp(
                                                                              s_obs.wavenumbers[wh_spike_obs],
                                                                              s_inv.wavenumbers[wh_spike_inv],
                                                                              s_inv.radiances[wh_spike_inv]))

    if lat_lon_overlay:
        latitudes = sc_ret.get_from_spectra('latitude')
        longitudes = sc_ret.get_from_spectra('longitude')
    else:
        latitudes = None
        longitudes = None

    SpectralCube.plot_map(spike_map, cmap=cmap, vmin=ratio_min, vmax=ratio_max, title=None, fig_name='map_spike',
                          cbar_label=rf'Mean radiance Obs/Ret in [{wvn_min}, {wvn_max}] cm$^{-1}$', latitudes=latitudes,
                          longitudes=longitudes)

    return spike_map


def ratios_map(sc_obs=None, wvn_min1=2141.0, wvn_max1=2141.6, wvn_min2=2149.0, wvn_max2=2149.6, ratio_min=0.6,
               ratio_max=1.4, cmap='plasma', only_where_retrieved=False):
    """Map of the ratio of the radiance of observed spectra between 2 wavenumber ranges."""
    if sc_obs is None:
        sc_obs = SpectralCube('jup', SpectralCube.PATH_DATA, find=True)
    wh_1 = np.where(
        np.logical_and(sc_obs.spectra[37][33].wvn >= wvn_min1, sc_obs.spectra[37][33].wvn <= wvn_max1))
    wh_2 = np.where(
        np.logical_and(sc_obs.spectra[37][33].wvn >= wvn_min2, sc_obs.spectra[37][33].wvn <= wvn_max2))
    ratio_map = np.zeros(np.shape(sc_obs.spectra))
    for s_obs in tqdm(np.asarray(sc_obs.spectra).flatten()):
        if s_obs is not None:
            if only_where_retrieved:
                sc_ret = SpectralCube('spec_cube_NRT_357_cld10_mol_inv_NH3svo', SpectralCube.PATH_OUT, find=True)
                for s_inv in np.asarray(sc_ret.spectra).flatten():
                    if s_inv is not None:
                        if s_inv.location == s_obs.location:
                            ratio_map[s_obs.location[0], s_obs.location[1]] = np.mean(s_obs.radw[wh_2]) / \
                                                                              np.mean(s_obs.radw[wh_1])
            else:
                ratio_map[s_obs.location[0], s_obs.location[1]] = np.mean(s_obs.radw[wh_2]) / \
                                                                  np.mean(s_obs.radw[wh_1])
    SpectralCube.plot_map(ratio_map, cmap=cmap, vmin=ratio_min, vmax=ratio_max, title=None, fig_name='map_ratio',
                          cbar_label=rf'Mean obs rad ratio '
                                     rf'[{wvn_min2}, {wvn_max2}] / [{wvn_min1}, {wvn_max1}] cm$^{-1}$')
    return ratio_map


def mol_by_mol(spec_obs=None, compile_nrt=False, plot=True, add_plot_with_all_molecules=True, add_sky=True,
               convolve=False, instrument=None, wavenumber_step=0.05,
               wavenumbers=None, doppler_shift=0, sky_size_reduction=1,
               atmosphere=None,
               overwrite=True):
    """
    Runs NRT once for each molecules in a priori atmosphere.
    Useful to see molecule lines in a spectrum.
    :param spec_obs: observed spectrum, if not None, all the spectrum parameters are taken from this
    :param compile_nrt: c.f. run_nrt
    :param plot: c.f. run_nrt
    :param instrument: c.f. run_nrt
    :param add_plot_with_all_molecules: if True, a spectrum with all the molecules together is plotted
    :param add_sky: if True and sky is not None in a priori, add the sky in the plots
    :param convolve: c.f. run_nrt
    :param wavenumbers: wavenumbers
    :param wavenumber_step: step between two wavenumbers
    :param doppler_shift: Doppler shift of the spectrum
    :param sky_size_reduction: divide the size of the sky by the given factor, has nbo influence in boundaries
    :param atmosphere: c.f. run_nrt
    :param overwrite: c.f. run_nrt
    :return: all the generated spectra
    """
    if spec_obs is not None:
        if spec_obs.atmosphere is not None and atmosphere is None:
            atmosphere = copy.deepcopy(spec_obs.atmosphere)

        if spec_obs.instrument is not None and instrument is None:
            instrument = copy.deepcopy(spec_obs.instrument)

        if spec_obs.wavenumbers is not None and wavenumbers is None:
            wavenumbers = [spec_obs.wavenumbers[0], spec_obs.wavenumbers[-1]]

    molecules = copy.deepcopy(atmosphere.molecules)
    atm = copy.deepcopy(atmosphere)

    specs = {}
    vmr_factors = {}

    # Run all
    for m in molecules:
        vmr_factors[m] = molecules[m].vmr_factor
        atm.molecules[m].vmr_factor = vmr_factors[m]

    spec_all = Spectrum.calculate_radiances(retrieved_spectrum=spec_obs, wavenumber_lim=wavenumbers,
                                            compile_nrt=compile_nrt, wavenumber_step=wavenumber_step,
                                            convolve=convolve, atmosphere=atm, instrument=instrument,
                                            overwrite=overwrite)

    compile_nrt = False

    for mol in molecules:
        if mol == 'H2' or mol == 'He':
            continue

        for m in molecules:
            if m == 'H2' or m == 'He' or m == mol:
                molecules[m].vmr_factor = vmr_factors[m]
            else:
                molecules[m].vmr_factor = 0

        atm.molecules = copy.deepcopy(molecules)
        specs[mol] = Spectrum.calculate_radiances(retrieved_spectrum=spec_obs, wavenumber_lim=wavenumbers,
                                                  instrument=instrument, compile_nrt=compile_nrt,
                                                  wavenumber_step=wavenumber_step,
                                                  convolve=convolve, atmosphere=atm,
                                                  overwrite=overwrite)

    if plot:
        if add_plot_with_all_molecules:
            plt.plot(spec_all.wavenumbers, spec_all.radiances,
                     label='all molecules', linestyle=':')

        for mol in molecules:
            if mol == 'H2' or mol == 'He':
                continue
            plt.plot(specs[mol].wavenumbers, specs[mol].radiances,
                     color=molecules[mol].color, label=mol)

        if add_sky and atm.sky is not None:
            y_max = plt.gca().get_ylim()[1]
            y_max = np.floor(y_max * 10 ** -np.floor(np.log10(y_max))) * 10 ** np.floor(np.log10(y_max))

            sky_wvn = []
            sky_trans = []
            for i in range(len(atm.sky.wvn)):
                if i % sky_size_reduction == 0:
                    sky_wvn.append(atm.sky.wvn[i])
                    sky_trans.append(atm.sky.trans[i])
            plt.plot(np.asarray(sky_wvn) - doppler_shift, np.asarray(sky_trans) * y_max, color='skyblue', linestyle=':',
                     label=rf'sky transmittance $\times$ {y_max}')

        plt.xlim([np.min(spec_all.wavenumbers), np.max(spec_all.wavenumbers)])
        plt.ylim([0, None])
        plt.xlabel(rf'{Spectrum.XLABEL}')
        plt.ylabel(rf'{Spectrum.YLABEL}')
        plt.legend()
        plt.show()

    return specs


def plot_lat_vmr_profile(spec_cubes, latitude=0, lat_bin=1, cloud_trans_min=0.05,
                         save=False, fig_dir='', fig_name='', fmt='png', fig_size=(4, 4), dpi=80):
    plt.close('all')
    plt.figure(figsize=fig_size)
    lats = stack_arrays(ObjList(spec_cubes).get('latitude_map'))

    s_map = stack_arrays(ObjList(spec_cubes).get('spectrum_map'))
    plots = []

    for i, spec in enumerate(s_map.flatten()):
        if spec is not None and spec != 0:
            if latitude - lat_bin < lats.flatten()[i] < latitude + lat_bin and \
                    spec.atmosphere.clouds[0].transmittance > cloud_trans_min:
                plots.append(spec.atmosphere.molecules['NH3'].vmr_profile.vmr)
                plt.plot(spec.atmosphere.molecules['NH3'].vmr_profile.vmr,
                         spec.atmosphere.molecules['NH3'].vmr_profile.pressure,
                         color='lightgrey')

    if len(plots) == 0:
        raise ValueError(f'no profile found within latitudes {latitude - lat_bin} and {latitude + lat_bin}'
                         f' and with a cloud transmittance > {cloud_trans_min}')

    plt.plot(np.mean(plots, axis=0), spec_cubes[0].atmosphere.molecules['NH3'].vmr_profile.p, color='g')
    plt.plot(np.percentile(plots, axis=0, q=16),
             spec_cubes[0].atmosphere.molecules['NH3'].vmr_profile.p, color='g',
             linestyle=':')
    plt.plot(np.percentile(plots, axis=0, q=84),
             spec_cubes[0].atmosphere.molecules['NH3'].vmr_profile.p, color='g',
             linestyle=':')

    plt.ticklabel_format(useMathText=True)
    plt.gca().get_xaxis().get_major_formatter().set_powerlimits((-2, 2))
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(True)
    plt.gca().invert_yaxis()
    plt.ylim(2E1, 1E-1)
    plt.xlim(0, None)
    plt.semilogy()
    plt.xlabel(r'[NH$_3$]')
    plt.ylabel('Pressure (bar)')
    plt.text(3E-4, 2E-1, rf'latitude = {latitude}$\pm$1$^\circ$E', horizontalalignment='right')

    if save:
        fig_name = fig_dir + fig_name + '.' + fmt
        plt.savefig(fig_name, dpi=dpi, format=fmt)


def plot_multi_specs(spec_cubes, spec_cubes_obs, latitude=0, lat_bin=1, cloud_trans_min=0.05,
                     save=False, f_dir='', f_name='', fmt='png', f_size=(4, 4), dpi=80):
    plt.close('all')
    plt.figure(figsize=f_size)
    lats = stack_arrays(ObjList(spec_cubes).get('latitude_map'))

    spec_map = stack_arrays(ObjList(spec_cubes).get('spec_map'))
    spec_obs_map = stack_arrays(spec_cubes_obs)
    plots = []
    plots_obs = []

    for i, spec in enumerate(spec_obs_map.flatten()):
        if spec is not None and spec != 0:
            if latitude - lat_bin < lats.flatten()[i] < latitude + lat_bin and \
                    spec.atmosphere.clouds[0].t > cloud_trans_min:
                plots_obs.append(spec.radw)
                plt.plot(spec.wvn, spec.radw, color='lightgrey')

    for i, spec in enumerate(spec_map.flatten()):
        if spec is not None and spec != 0:
            if latitude - lat_bin < lats.flatten()[i] < latitude + lat_bin and \
                    spec.atmosphere.clouds[0].t > cloud_trans_min:
                plots.append(spec.radw)

    plt.plot(spec_obs_map[35][35].wvn, np.mean(plots_obs, axis=0), color='b')
    plt.plot(spec_map[35][35].wvn, np.mean(plots, axis=0), color='g')

    plt.ticklabel_format(useMathText=True)
    plt.gca().get_xaxis().get_major_formatter().set_powerlimits((-2, 2))
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(True)
    plt.gca().invert_yaxis()
    plt.ylim(0, None)
    plt.xlabel(r'[NH$_3$]')
    plt.ylabel('Pressure (bar)')
    plt.text(3E-4, 2E-1, r'latitude = 7$\pm$1$^\circ$E', horizontalalignment='right')
    if save:
        f_name = f_dir + f_name + '.' + fmt
        plt.savefig(f_name, dpi=dpi, format=fmt)


def stack_feature(spec_cubes, feature):
    """Same than stack_maps but for SpectralCube features."""
    features = []

    for sc in spec_cubes:
        features.append(sc.get_from_spectra(feature))

    return stack_arrays(features)


def cloud_transmittance_lacis1973ackermann2001(eps_mol_atm, pressure, q_mol, r_eff, gravity, f_rain, rho_mol, g_c, w0,
                                               p0=1.013E5):
    """
    Calculate the cloud transmittance according to Lacis1973 and Ackermann2001 cloud models.
    :param eps_mol_atm: ratio of the molar mass of the molecule over the molar mass of the atmosphere
    :param pressure: (bar) pressure of the cloud base
    :param q_mol: molecular vmr at the base of the cloud
    :param r_eff: (m) area-weighted mean particle effective radius
    :param gravity: (m.s-2) gravity of the planet
    :param f_rain: ratio of the mass-averaged particle sedimentation velocity over convective velocity
    :param rho_mol: density of the molecule
    :param g_c: asymmetry factor of the cloud particles
    :param w0: single scattering albedo of the cloud particles
    :param p0: pressure conversion factor to Pa, by default from bar to Pa
    :return: the cloud transmittance for the given parameters
    """
    tau_ackermann = 3 / 2 * eps_mol_atm * pressure * p0 * q_mol * r_eff / (gravity * 100E-6 * (1 + f_rain) * rho_mol)
    u = ((1 - g_c * w0) / (1 - w0)) ** 0.5
    t = (3 * (1 - w0) * (1 - g_c * w0)) ** 0.5 * tau_ackermann
    tc = 4 * u / ((u + 1) ** 2 * np.exp(t) - (u - 1) ** 2 * np.exp(-t))

    return tc


def plot_spec_and_d_radiance(spectrum, derivative, molecule_number=0,
                             xlim=None, ylim_derivative=None):
    """
    Plot a spectrum radiance and one of its corresponding molecular kernel.
    :param spectrum: the spectrum to plot
    :param derivative: the derivative to plot
    :param molecule_number: the molecular number of the derivative
    :param xlim: c.f. matplotlib
    :param ylim_derivative: c.f. matplotlib
    :return:
    """
    plt.subplot(212)
    plt.contourf(derivative.wavenumbers, derivative.pressures,
                 np.transpose(derivative.d_radiances[:, :, molecule_number]),
                 cmap='inferno_r',
                 levels=np.linspace(np.min(derivative.dradw[:, :, 6]), 0, 19))
    cb = plt.colorbar(orientation='horizontal')
    cb.set_label('drad / dlnNH3 (W.cm-2.sr-1/cm-1)')
    plt.gca().invert_yaxis()
    plt.semilogy()
    plt.xlabel('Wavenumber (cm-1)')
    plt.ylabel('Pressure (bar)')
    if ylim_derivative is not None:
        plt.ylim(ylim_derivative)

    plt.subplot(211)
    plt.plot(spectrum.wavenumbers, spectrum.radiances)
    plt.xlabel('Wavenumber (cm-1)')
    plt.ylabel('Radiance (W.cm-2.sr-1/cm-1)')
    if xlim is not None:
        plt.xlim(xlim)
