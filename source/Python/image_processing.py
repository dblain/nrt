"""
Image processing module. Mainly map projections transforms.
"""
import cv2
import numpy as np
from scipy.ndimage import interpolation
from skimage.transform import resize

from .spectral_cube import SpectralCube


def _rotate(map_x, map_y, angle, x0=0, y0=0, reshape=False):
    """
    Rotate two maps by a given angle. Using scipy.interpolation.rotate gives better results.
    """
    angle = np.deg2rad(angle)

    x_size = np.max(map_x)
    y_size = np.max(map_y)

    map_x_ = map_x - x_size / 2
    map_y_ = map_y - y_size / 2
    if reshape:
        new_map_x = np.zeros(np.shape(map_x_))
        new_map_y = np.zeros(np.shape(map_y_))
        # new_min_x = np.cos(angle) * (np.min(map_x_) - x0) - np.sin(angle) * (np.max(map_y_) - y0) + x0
        # new_max_x = np.cos(angle) * (np.max(map_x_) - x0) - np.sin(angle) * (np.min(map_y_) - y0) + x0
        # new_x_size = np.ceil(new_max_x - new_min_x)
        #
        # new_min_y = np.sin(angle) * (np.min(map_x_) - x0) + np.cos(angle) * (np.min(map_y_) - y0) + y0
        # new_max_y = np.sin(angle) * (np.max(map_x_) - x0) + np.cos(angle) * (np.max(map_y_) - y0) + y0
        # new_y_size = np.ceil(new_max_y - new_min_y)
        #
        # new_map_x = np.zeros((new_y_size, new_x_size))
        # new_map_y = np.zeros((new_y_size, new_x_size))
        #
        # map_x_n = np.zeros((new_y_size, new_x_size))
        # map_y_n = np.zeros((new_y_size, new_x_size))

        # <2D interpolation of map_x and map_y>
    else:
        new_map_x = np.zeros(np.shape(map_x_))
        new_map_y = np.zeros(np.shape(map_y_))

    for i in range(np.size(map_x_, 0)):
        for j in range(np.size(map_x_, 1)):
            new_map_x[i, j] = np.cos(angle) * (map_x_[i, j] - x0) - np.sin(angle) * (map_y_[i, j] - y0) + x0
            new_map_y[i, j] = np.sin(angle) * (map_x_[i, j] - x0) + np.cos(angle) * (map_y_[i, j] - y0) + y0

    new_map_x -= np.min(new_map_x)
    new_map_y -= np.min(new_map_y)
    new_map_x[np.where(new_map_x > np.max(map_x))] = 0
    new_map_y[np.where(new_map_y > np.max(map_y))] = 0

    return new_map_x, new_map_y


def cylindrical2equirectangular(file, graphic2centric=False, **kwargs):
    """
    Transform a cylindrical projected map into an equirectangular projected map.
    The inverse cylindrical projection formulae is assumed to be (0.5*sin(lat))+0.5.
    The destination image is read correctly by cv2.imread().
    :param file: the source image file
    :param graphic2centric: if True, a planetographic to planetocentric transformation is performed
    :return: the transformed image
    """
    # Read source file
    source = cv2.imread(file)
    source_x_size = np.size(source, axis=1)
    source_y_size = np.size(source, axis=0)

    # Calculate destination image file
    lon_px_ratio = 360 / np.size(source, axis=1)
    y_size = int(np.ceil(180 / lon_px_ratio))

    # Initialise latitude space
    lat = np.deg2rad(np.linspace(-90, 90, y_size))

    if graphic2centric:
        lat = np.rad2deg(SpectralCube.graphic2centric(np.deg2rad(lat), **kwargs))

    # Calculate transformation matrix
    y_transform = ((0.5 * np.sin(lat)) + 0.5) * source_y_size

    map_y = np.zeros((y_size, source_x_size))
    for i, new_y in enumerate(y_transform):
        for j in range(source_x_size):
            map_y[i][j] = new_y

    # The x axis has no transformation
    map_x = np.zeros((y_size, source_x_size))
    for i in range(y_size):
        for j in range(source_x_size):
            map_x[i][j] = j

    map_x_32 = map_x.astype('float32')
    map_y_32 = map_y.astype('float32')

    destination = cv2.remap(source, map_x_32, map_y_32, cv2.INTER_CUBIC)
    return destination


def disk2latitude_longitude_maps(disk_x_size, disk_y_size, lon_0=0, lat_0=0, angle=0, order=5, **kwargs):
    """
    Retrieve latitudes and longitudes for each pixels of a deformed, rotated disk.
    :param disk_x_size: (px) x-size of the disk
    :param disk_y_size: (px) y-size of the disk
    :param lon_0: (deg) longitude of the central pixel of the disk
    :param lat_0: (deg) latitude of the central pixel of the disk
    :param angle: (deg) angle of which the disk is rotated
    :param order: order of interpolation during rotation
    :return: longitude and latitude maps, in degrees
    """
    greatest_size = np.max([disk_x_size, disk_y_size])

    longitude_map, latitude_map = inverse_orthographic_projection(size=greatest_size, lon_0=lon_0, lat_0=lat_0,
                                                                  **kwargs)
    longitude_map[np.where(longitude_map != longitude_map)] = 0
    latitude_map[np.where(latitude_map != latitude_map)] = 0

    rotated_longitude = interpolation.rotate(longitude_map, angle, reshape=False, order=order)
    rotated_latitude = interpolation.rotate(latitude_map, angle, reshape=False, order=order)

    new_longitude_map = resize(rotated_longitude, (disk_y_size, disk_x_size))
    new_latitude_map = resize(rotated_latitude, (disk_y_size, disk_x_size))

    new_longitude_map = np.ma.array(np.fliplr(new_longitude_map))
    new_latitude_map = np.ma.array(np.flipud(new_latitude_map))
    new_longitude_map[np.where(new_longitude_map == 0)] = np.nan
    new_latitude_map[np.where(new_latitude_map == 0)] = np.nan

    return new_longitude_map, new_latitude_map


def equirectangular2mollweide(file):
    """
    Transform an equirectangular projected map into a Mollweide projected map.
    It is needed to take the closed-form inverse Mollweide transformation since cv2.remap() work as following:
    >>> destination[x_new, y_new] = source[map_x[i, j], map_y[i, j]]
    The destination image is read correctly by cv2.imread().
    :param file: the source image file
    :return: the transformed image
    """
    # Read source file
    source = cv2.imread(file)
    source_x_size = np.size(source, axis=1)
    source_y_size = np.size(source, axis=0)

    # Initialise new x-y space
    x_new = np.linspace(0, source_x_size-1, source_x_size)
    y_new = np.linspace(0, source_y_size-1, source_y_size)

    # Initialise theta
    theta = np.arcsin((2 * y_new - source_y_size) / source_y_size)

    # Calculate transformation matrix
    lon = np.zeros((source_y_size, source_x_size))
    for i, t in enumerate(theta):
        lon[i, :] = np.pi * ((2 * x_new - source_x_size) / source_x_size) / np.cos(t)

    map_x = np.zeros((source_y_size, source_x_size))
    for i in range(source_y_size):
        for j in range(source_x_size):
            map_x[i][j] = (np.rad2deg(lon[i, j]) + 180) / 360 * source_x_size

    lat = np.arcsin((2*theta + np.sin(2*theta)) / np.pi)
    y_transform = (np.rad2deg(lat) + 90) / 180 * source_y_size

    map_y = np.zeros((source_y_size, source_x_size))
    for i, new_y in enumerate(y_transform):
        for j in range(source_x_size):
            map_y[i][j] = new_y

    map_x_32 = map_x.astype('float32')
    map_y_32 = map_y.astype('float32')

    destination = cv2.remap(source, map_x_32, map_y_32, cv2.INTER_CUBIC)
    return destination


def equirectangular2orthographic(file, lon_0=0, lat_0=0, lon_0_file=0, lat_0_file=0, **kwargs):
    """
    Transform an equirectangular projected map into an orthographic projected map.
    :param file: the source image file
    :param lon_0: (deg) longitude of the central pixel
    :param lat_0: (deg) latitude of the central pixel
    :param lon_0_file: (deg) longitude of the left pixels of the file
    :param lat_0_file: (deg) latitude of the left pixels of the file
    :return: the transformed image
    """
    # Read source file
    source = cv2.imread(file)
    source_y_size = np.size(source, axis=0)

    # Get lon lat
    longitude_map, latitude_map = inverse_orthographic_projection(source_y_size, lon_0=lon_0 + lon_0_file,
                                                                  lat_0=lat_0 + lat_0_file, **kwargs)

    # Get transformation matrix
    map_x = np.zeros((source_y_size, source_y_size))
    for i in range(source_y_size):
        for j in range(source_y_size):
            map_x[i, j] = (longitude_map[i, j] + 90) / 180 * source_y_size

    map_y = np.zeros((source_y_size, source_y_size))
    for i in range(source_y_size):
        for j in range(source_y_size):
            map_y[i, j] = (latitude_map[i, j] + 90) / 180 * source_y_size

    map_x_32 = map_x.astype('float32')
    map_y_32 = map_y.astype('float32')

    destination = cv2.remap(source, map_x_32, map_y_32, cv2.INTER_CUBIC)
    return destination


def inverse_orthographic_projection(size, lon_0=0, lat_0=0, western_longitude=False, north_upward=False):
    """
    Perform the inverse orthographic projection to retrieve latitudes and longitudes of each pixels in a square of a
    given size.
    :param size: size of the square
    :param lon_0: (deg) longitude of the central pixel
    :param lat_0: (deg) latitude of the central pixel
    :param western_longitude: if True, the given longitudes are considered as western longitudes
    :param north_upward: if True, northern latitudes are positives and southern latitudes are negatives
    :return: longitude and latitude maps, in degrees
    """
    # Change coordinates referential
    if western_longitude:
        lon_0 = 360 - lon_0
    else:
        lon_0 = lon_0

    if north_upward:
        lat_0 = - lat_0

    lat_0 = np.deg2rad(lat_0)
    lon_0 = np.deg2rad(lon_0)

    # Initialise new x-y space
    x_new = np.linspace(0, size-1, size)
    y_new = np.linspace(0, size-1, size)

    x = ((2 * x_new - size) / size)
    y = ((2 * y_new - size) / size)

    # Initialise rho and c
    rho = np.zeros((size, size))
    c = np.zeros((size, size))

    for i in range(size):
        for j in range(size):
            rho[i, j] = np.sqrt(x[j]**2 + y[i]**2)
            c[i, j] = np.arcsin(rho[i, j] / np.max(y))

    # Perform inverse transformation
    longitude_map = np.zeros((size, size))
    for i in range(size):
        for j in range(size):
            longitude_map[i, j] = lon_0 + np.arctan2(x[j] * np.sin(c[i, j]),
                                                     rho[i, j] * np.cos(c[i, j]) * np.cos(lat_0) -
                                                     y[i] * np.sin(c[i, j]) * np.sin(lat_0))

    latitude_map = np.zeros((size, size))
    for i in range(size):
        for j in range(size):
            latitude_map[i, j] = np.arcsin(np.cos(c[i, j]) * np.sin(lat_0) + y[i] * np.sin(c[i, j]) * np.cos(lat_0) /
                                           rho[i, j])

    # Finalisation

    longitude_map = np.rad2deg(longitude_map)

    latitude_map = np.rad2deg(latitude_map)

    return longitude_map, latitude_map
