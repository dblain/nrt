"""
SPICE kernel object.
"""
import scipy.constants as cst
import spiceypy as spice

from .files import *


class SpiceMetaKernel(File):
    """
    Spice metakernel object.
    Can contain a list of SPICE kernels files.
    """
    EXT = 'txt'
    PATH = File.PATH_DATA

    def __init__(self, name='', path=PATH, ext=EXT, file='', exists=False, find=False, kernels=None):
        """
        Spice metakernel object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param file: c.f. File
        :param exists: c.f. File
        :param find: c.f. File
        :param kernels: list of SPICE kernel Files
        """
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

        self.kernels = kernels

    def load(self):
        """
        Load the SPICE kernels using SPICE method furnsh.
        """
        spice.furnsh(self.file)

    def save(self):
        """
        Save the SPICE metakernel.
        """
        tab = ''.rjust(4)

        with open(self.file, 'w') as f:
            f.write('\\begindata\n\n')
            f.write(f'{tab}PATH_VALUES = (\'{self.path}\')\n\n')
            f.write(f'{tab}PATH_SYMBOLS = (\'KERNELS\')\n\n')
            f.write(f'{tab}KERNELS_TO_LOAD = (\n')

            for kernel in self.kernels:
                f.write(f'{tab}{tab}\'{os.path.join("$KERNELS", kernel.name + "." + kernel.ext)}\'\n')

            f.write(f'{tab})\n\n')
            f.write('\\begintext')

    @classmethod
    def from_kernel_list(cls, kernel_list, name, path=PATH):
        """
        Initialise a SpiceMetaKernel using a file name list.
        :param kernel_list: list of file names
        :param name: c.f. File
        :param path: c.f. File
        :return: a SpiceMetaKernel
        """
        kernels = []

        for kernel in kernel_list:
            kernel_name, kernel_ext = kernel.rsplit('.', 1)
            kernels.append(File(name=kernel_name, path=path, ext=kernel_ext))

        return cls(name=name, path=path, ext=SpiceMetaKernel.EXT, kernels=kernels)

    @staticmethod
    def date2spice(date):
        """
        Convert an ISO 8601 date into SPICE ephemeris epoch.
        Uses SPICE method str2et.
        :param date: (ISO 8601) date
        :return: (s, J2000) SPICE ephemeris epoch
        """
        return spice.str2et(date)

    @staticmethod
    def get_illumination_angles(target, epoch, observer, surface_coordinates, aberration_correction='CN+S'):
        """
        Get the illumination angles (phase, incidence, emission) of a target relative to an observer.
        Uses SPICE method illum.
        :param target: object observed
        :param epoch: (ISO 8601) time of observation
        :param observer: object observing
        :param surface_coordinates: point of the surface of the target observed
        :param aberration_correction: aberration correction
        :return: (deg) phase, incidence and emission angles of the observed point
        """
        surface_coordinates = spice.srfrec(body=spice.bodn2c(target),
                                           longitude=np.deg2rad(surface_coordinates[1]),
                                           latitude=np.deg2rad(surface_coordinates[0]))

        if isinstance(epoch, str):
            epoch = SpiceMetaKernel.date2spice(date=epoch)

        phase_angle, incidence_angle, emission_angle = spice.illum(target=target, et=epoch,
                                                                   abcorr=aberration_correction, obsrvr=observer,
                                                                   spoint=surface_coordinates)

        return np.rad2deg(phase_angle), np.rad2deg(incidence_angle), np.rad2deg(emission_angle)

    @staticmethod
    def get_observer_surface_point_velocity(target, epoch, observer, intercept_point, intercept_vector,
                                            reference_frame='', aberration_correction='CN+S'):
        """
        Get the radial velocity between an observer and a surface point on a target.
        A positive velocity means that the point is moving to the observer, a negative one that the point is moving from
        the observer.
        :param target: object observed
        :param epoch: (ISO 8601) epoch of observation
        :param observer: observing object
        :param intercept_point: surface intercept point on the target body in the given reference frame
        :param intercept_vector: vector from observer to intercept point in the given reference frame
        :param reference_frame: reference frame of interception, usually the target reference frame
        :param aberration_correction: aberration correction to be applied
        :return: (m.s-1) radial velocity between the observer and the surface point on the target
        """
        if reference_frame == '':
            reference_frame = 'IAU_' + target

        if isinstance(epoch, str):
            ephemeris_epoch = spice.str2et(epoch)
        else:
            ephemeris_epoch = epoch

        # Get velocity of observer relative to target, taking light time into account
        observer_target_velocity = -1 * spice.spkezr(targ=target, et=ephemeris_epoch, ref='J2000',
                                                     abcorr=aberration_correction, obs=observer)[0][3:]

        # Get transformation matrix from target reference frame to J2000 reference frame
        iau_target2j2000 = spice.sxform(instring=reference_frame, tostring='J2000', et=ephemeris_epoch)

        # Get intercept point and vector velocities in J2000 reference frame
        intercept_point_state = np.append(intercept_point, [0, 0, 0])  # velocity is 0 since it is taken in the targetRF
        intercept_vector_state = np.append(intercept_vector, [0, 0, 0])

        intercept_point_velocity_j2000 = np.matmul(iau_target2j2000, intercept_point_state)[3:]
        intercept_vector_position_j2000 = np.matmul(iau_target2j2000, intercept_vector_state)[:3]

        # Get velocity between the intercept point and the observer
        surface_point_velocity = intercept_point_velocity_j2000 - observer_target_velocity

        # Get radial velocity between the intercept point and the observer
        surface_point_observer_velocity = spice.vproj(surface_point_velocity, intercept_vector_position_j2000)

        # Is the surface point moving to or from the observer ?
        intercept_point_observer_velocity_angle = spice.vsep(intercept_vector_position_j2000, observer_target_velocity)

        if intercept_point_observer_velocity_angle <= np.pi / 2:
            direction = 1  # surface point is moving to the observer
        else:
            direction = -1  # surface point is moving from the observer

        return np.linalg.norm(surface_point_observer_velocity) * direction * 1E3  # convert from km/s-1 to m.s-1

    @staticmethod
    def get_observer_target_velocity(target, epoch, observer, reference_frame='J2000', aberration_correction='CN+S'):
        """
        Get the velocity between an observer and a target.
        :param target: object observed
        :param epoch: epoch of observation
        :param observer: observing object
        :param reference_frame: reference frame
        :param aberration_correction: aberration correction
        :return: the velocity between the target and the observer
        """
        target = spice.bodn2c(target)
        observer = spice.bodn2c(observer)

        if isinstance(epoch, str):
            ephemeris_epoch = spice.str2et(epoch)
        else:
            ephemeris_epoch = epoch

        d_light_epoch = spice.spkacs(targ=target, et=ephemeris_epoch, ref=reference_frame, abcorr=aberration_correction,
                                     obs=observer)[2]  # derivative of light time between observer and target

        return d_light_epoch * cst.c

    @staticmethod
    def get_pixel_boresight_latitudinal_coordinates(target, epoch, observer, instrument_frame, instrument_array_shape,
                                                    viewing_px,
                                                    method='ellipsoid', target_frame='', aberration_correction='CN+S'):
        """
        Get the planetocentric latitudes and longitudes of the boresight of a pixel of an instrument.
        The boresight of a pixel is constituted of four points so four latitudes and four longitudes are returned.
        :param target: name of target body
        :param epoch: (ISO8601) epoch of observation
        :param observer: name of observing body
        :param instrument_frame: reference frame of the instrument
        :param instrument_array_shape: shape of the instrument array
        :param viewing_px: pixel of instrument array where to cast the interception ray
        :param method: computation method
        :param target_frame: body-fixed, body centered target body frame
        :param aberration_correction: aberration correction
        :return: pixel boresight latitudes (4-elements tuple), pixel boresight longitudes (4-elements tuple)
        """
        # Maximum number of bounds are limited to 4 for rectangular-shaped FOVs.
        center_latitude, center_longitude, rad, \
            center_intercept_epoch, center_intercept_vector, center_intercept_point = \
            SpiceMetaKernel.get_pixel_latitudinal_coordinates(target=target, epoch=epoch, observer=observer,
                                                              max_bounds=4,
                                                              instrument_frame=instrument_frame,
                                                              instrument_array_shape=instrument_array_shape,
                                                              viewing_pixel=(viewing_px[0],
                                                                             viewing_px[1]),
                                                              method=method, target_frame=target_frame,
                                                              aberration_correction=aberration_correction)

        lower_left_latitude, lower_left_longitude, rad, \
            lower_left_intercept_epoch, lower_left_intercept_vector, lower_left_intercept_point = \
            SpiceMetaKernel.get_pixel_latitudinal_coordinates(target=target, epoch=epoch, observer=observer,
                                                              max_bounds=4,
                                                              instrument_frame=instrument_frame,
                                                              instrument_array_shape=instrument_array_shape,
                                                              viewing_pixel=(viewing_px[0] - 0.5,
                                                                             viewing_px[1] - 0.5),
                                                              method=method, target_frame=target_frame,
                                                              aberration_correction=aberration_correction)

        lower_right_latitude, lower_right_longitude, rad, \
            lower_right_intercept_epoch, lower_right_intercept_vector, lower_right_intercept_point = \
            SpiceMetaKernel.get_pixel_latitudinal_coordinates(target=target, epoch=epoch, observer=observer,
                                                              max_bounds=4,
                                                              instrument_frame=instrument_frame,
                                                              instrument_array_shape=instrument_array_shape,
                                                              viewing_pixel=(viewing_px[0] + 0.5,
                                                                             viewing_px[1] - 0.5),
                                                              method=method, target_frame=target_frame,
                                                              aberration_correction=aberration_correction)
        upper_left_latitude, upper_left_longitude, rad, \
            upper_left_intercept_epoch, upper_left_intercept_vector, upper_left_intercept_point = \
            SpiceMetaKernel.get_pixel_latitudinal_coordinates(target=target, epoch=epoch, observer=observer,
                                                              max_bounds=4,
                                                              instrument_frame=instrument_frame,
                                                              instrument_array_shape=instrument_array_shape,
                                                              viewing_pixel=(viewing_px[0] - 0.5,
                                                                             viewing_px[1] + 0.5),
                                                              method=method, target_frame=target_frame,
                                                              aberration_correction=aberration_correction)
        upper_right_latitude, upper_right_longitude, rad, \
            upper_right_intercept_epoch, upper_right_intercept_vector, upper_right_intercept_point = \
            SpiceMetaKernel.get_pixel_latitudinal_coordinates(target=target, epoch=epoch, observer=observer,
                                                              max_bounds=4,
                                                              instrument_frame=instrument_frame,
                                                              instrument_array_shape=instrument_array_shape,
                                                              viewing_pixel=(viewing_px[0] + 0.5,
                                                                             viewing_px[1] + 0.5),
                                                              method=method, target_frame=target_frame,
                                                              aberration_correction=aberration_correction)

        pixel_boresight_latitudes = np.asarray([
            center_latitude,
            lower_left_latitude, lower_right_latitude,
            upper_left_latitude, upper_right_latitude
        ])
        pixel_boresight_longitudes = np.asarray([
            center_longitude,
            lower_left_longitude, lower_right_longitude,
            upper_left_longitude, upper_right_longitude
        ])

        pixel_intercept_epoch = center_intercept_epoch

        pixel_intercept_vectors = np.asarray([
            center_intercept_vector,
            lower_left_intercept_vector,
            lower_right_intercept_vector,
            upper_left_intercept_vector,
            upper_right_intercept_vector
        ])

        pixel_intercept_point = center_intercept_point

        return pixel_boresight_latitudes, pixel_boresight_longitudes, \
            pixel_intercept_epoch, pixel_intercept_vectors, pixel_intercept_point

    @staticmethod
    def get_pixel_full_intercept_geometry(target, start_time, stop_time, observer, instrument_frame,
                                          instrument_array_shape, viewing_px,
                                          time_threshold=0.001, reject_partial_intercept=False):
        """
        Find by dichotomy the interception latitudes and longitudes of the boresight of a pixel of an instrument between
        two epochs.
        If an interception is found both at start_time and stop_time, no dichotomy is performed.
        If no interception is found, try to find the epoch of first/last interception of the boresight of the pixel.
        If no interception is found at start_time, return nan and None.
        :param target: object observed
        :param start_time: (ISO 8601) time at the beginning of observation
        :param stop_time: (ISO 8601) time at the end of observation
        :param observer: observing object
        :param instrument_frame: instrument frame
        :param instrument_array_shape: shape of the instrument array
        :param viewing_px: viewing pixel of the instrument array
        :param time_threshold: (s) maximum precision of the dichotomy; end the dichotomy below this threshold
        :param reject_partial_intercept: if True, no dichotomy is performed when not finding intercept the first time
        :return: first and last boresight latitudes and longitudes of interception, and first and last epoch of
        interception
        """
        start_time = SpiceMetaKernel.date2spice(start_time)
        stop_time = SpiceMetaKernel.date2spice(stop_time)

        latitude_start_time = None
        longitude_start_time = None
        latitude_stop_time = None
        longitude_stop_time = None

        intercept_epoch = start_time
        d_time = stop_time - start_time
        first_time_intercept = True
        intercept = False
        intercept_not_found = False

        # Find first interception with target
        while not (intercept and d_time < time_threshold):
            latitude_start_time, longitude_start_time, epoch, dist, pts = \
                SpiceMetaKernel.get_pixel_boresight_latitudinal_coordinates(
                    target=target,
                    epoch=intercept_epoch,
                    observer=observer,
                    instrument_frame=instrument_frame,
                    instrument_array_shape=instrument_array_shape,
                    viewing_px=viewing_px
                )

            if np.isnan(latitude_start_time).any() or np.isnan(latitude_start_time).any():
                first_time_intercept = False  # no interception detected at exactly start_time
                intercept = False
                d_time *= 0.5
                if d_time < time_threshold:
                    if not intercept_not_found:
                        d_time = time_threshold
                        intercept_not_found = True
                    else:
                        return np.full((2, 4), np.nan), np.full((2, 4), np.nan), np.asarray([None, None])

                intercept_epoch += d_time
                continue

            if first_time_intercept or reject_partial_intercept:
                break  # interception found exactly at start_time or rejection of partial intercept
            else:
                intercept = True  # interception found at some epoch between start_time and stop_time
                intercept_not_found = False
                d_time *= 0.5
                if d_time > time_threshold:
                    intercept_epoch -= d_time

        intercept_start_time = SpiceMetaKernel.spice2date(intercept_epoch)

        intercept_epoch = stop_time
        d_time = stop_time - start_time
        first_time_intercept = True
        intercept = False

        # Find last interception with target
        while not (intercept and d_time < time_threshold):
            try:
                latitude_stop_time, longitude_stop_time, epoch, dist, pts = \
                    SpiceMetaKernel.get_pixel_boresight_latitudinal_coordinates(
                        target=target,
                        epoch=intercept_epoch,
                        observer=observer,
                        instrument_frame=instrument_frame,
                        instrument_array_shape=instrument_array_shape,
                        viewing_px=viewing_px
                    )
            except spice.support_types.SpiceyError:
                first_time_intercept = False
                intercept = False
                d_time *= 0.5
                if d_time < time_threshold:
                    d_time = time_threshold

                intercept_epoch -= d_time
                continue

            if first_time_intercept or reject_partial_intercept:
                break
            else:
                intercept = True
                d_time *= 0.5
                if d_time > time_threshold:
                    intercept_epoch += d_time

        intercept_stop_time = SpiceMetaKernel.spice2date(intercept_epoch)

        return np.asarray([latitude_start_time, latitude_stop_time]), \
            np.asarray([longitude_start_time, longitude_stop_time]), \
            np.asarray([intercept_start_time, intercept_stop_time])

    @staticmethod
    def get_pixel_intercept(target, epoch, observer, instrument_frame, instrument_array_shape, viewing_pixel,
                            max_bounds=6, method='ellipsoid', target_frame='', aberration_correction='CN+S'):
        """
        Get the surface intercept point between an observer and a target at the given epoch.
        Meta kernel needs to be loaded.
        Raises "SpiceyError: Spice returns not found for function: sincpt" probably when no intercept is found.
        This means that, at the given epoch, the viewing pixel of the instrument does not fully intercept
        the target. Sightly changing the epoch might gives acceptable results.
        :param target: object observed
        :param epoch: (ISO 8601) epoch of observation
        :param observer: observing object
        :param instrument_frame: instrument frame
        :param instrument_array_shape: shape of the instrument array
        :param max_bounds: maximum number of 3D vectors that can be returned for FOV
        :param viewing_pixel: pixel of instrument array where to cast the interception ray
        :param method: computation method
        :param target_frame: body-fixed, body centered target body frame
        :param aberration_correction: aberration correction
        :return: surface interception point (3-element array), intercept epoch and vector from observer to intercept
        point.
        """
        if target_frame == '':
            target_frame = 'IAU_' + target

        if isinstance(epoch, str):
            ephemeris_epoch = spice.str2et(epoch)
        else:
            ephemeris_epoch = epoch

        instrument_reference_number = spice.bodn2c(instrument_frame)
        fov_shape, reference_frame, view_direction, bounds_number, bounds = \
            spice.getfov(instrument_reference_number, max_bounds)

        fov_size_x = np.max(bounds[:, 0]) - np.min(bounds[:, 0])
        fov_size_y = np.max(bounds[:, 1]) - np.min(bounds[:, 1])

        pixel_fov_size_x = fov_size_x / instrument_array_shape[0]
        pixel_fov_size_y = fov_size_y / instrument_array_shape[1]

        pixel_fov_sizes = [pixel_fov_size_x, pixel_fov_size_y]
        viewing_ray = np.array([0., 0., np.max(bounds[:, 2])])

        for i, px_number in enumerate([instrument_array_shape[0], instrument_array_shape[1]]):
            if viewing_pixel[i] > px_number - 0.5 or viewing_pixel[i] < -0.5:
                raise IndexError(f'viewing pixel {viewing_pixel} is out of instrument array for axis {i} with size '
                                 f'{px_number} (pixel count begin at 0)')

            viewing_ray[i] = np.max(bounds[:, i]) - (viewing_pixel[i] + 0.5) * pixel_fov_sizes[i]

        surface_intercept_point, intercept_epoch, observer_intercept_vector = \
            spice.sincpt(method=method, target=target, et=ephemeris_epoch, fixref=target_frame,
                         abcorr=aberration_correction, obsrvr=observer, dref=instrument_frame, dvec=viewing_ray)

        return surface_intercept_point, intercept_epoch, observer_intercept_vector

    @staticmethod
    def get_pixel_geometry(target, epoch, observer, instrument_frame,
                           instrument_array_shape, viewing_px):
        """
        Return the latitudes and longitudes of the boresight of a pixel on a target.
        :param target: object observed
        :param epoch: (ISO 8601) time of observation
        :param observer: observing object
        :param instrument_frame: instrument frame
        :param instrument_array_shape: shape of the instrument array
        :param viewing_px: viewing pixel of the instrument array
        :return: latitudes and longitudes of the boresight
        """
        epoch = SpiceMetaKernel.date2spice(epoch)

        latitudes, longitudes, intercept_epoch, vectors, intercept_point = \
            SpiceMetaKernel.get_pixel_boresight_latitudinal_coordinates(
                target=target,
                epoch=epoch,
                observer=observer,
                instrument_frame=instrument_frame,
                instrument_array_shape=instrument_array_shape,
                viewing_px=viewing_px
            )

        return latitudes, longitudes, intercept_epoch, vectors, intercept_point

    @staticmethod
    def get_pixel_latitudinal_coordinates(target, epoch, observer, instrument_frame, instrument_array_shape,
                                          viewing_pixel, max_bounds=6,
                                          method='ellipsoid', target_frame='', aberration_correction='CN+S'):
        """
        Get the planetocentric latitudes and longitudes of the center of a pixel of an instrument.
        If no intercept is found, return NaN
        :param target: object observed
        :param epoch: (ISO 8601) epoch of observation
        :param observer: observing object
        :param instrument_frame: instrument frame
        :param instrument_array_shape: shape of the intrument array
        :param viewing_pixel: pixel of instrument array where to cast the interception ray
        :param max_bounds: maximum number of 3D vectors that can be returned for FOV
        :param method: computation method
        :param target_frame: body-fixed, body centered target body frame
        :param aberration_correction: aberration correction
        :return: pixel planetocentric latitudes and longitudes, distance from target center to target surface intercept
        point, epoch of interception, observer to surface intercept point vector
        """
        try:
            intercept_point, intercept_epoch, observer_intercept_vector = \
                SpiceMetaKernel.get_pixel_intercept(target=target, epoch=epoch, observer=observer,
                                                    max_bounds=max_bounds, instrument_frame=instrument_frame,
                                                    instrument_array_shape=instrument_array_shape,
                                                    viewing_pixel=viewing_pixel,
                                                    method=method, target_frame=target_frame,
                                                    aberration_correction=aberration_correction)

            radius, longitude, latitude = spice.reclat(intercept_point)

            latitude = np.rad2deg(latitude)
            longitude = np.rad2deg(longitude)
        except spice.support_types.SpiceyError as error_message:
            if str(error_message) == 'Spice returns not found for function: sincpt':
                latitude = np.nan
                longitude = np.nan
                radius = np.nan
                intercept_epoch = np.nan
                intercept_point = np.full(3, np.nan)
                observer_intercept_vector = np.full(3, np.nan)
            else:
                raise spice.support_types.SpiceyError(str(error_message))

        return latitude, longitude, radius, intercept_epoch, observer_intercept_vector, intercept_point

    @staticmethod
    def get_observer_point_distance(target, epoch, observer, surface_point, aberration_correction='CN+S'):
        """
        Return the distance between the surface point of a target and an observer.
        :param target:
        :param epoch:
        :param observer:
        :param surface_point: 3D position vector expressed in the observer reference frame
        :param aberration_correction:
        :return:
        """
        observer_target_distance = spice.spkpos(
            targ=target,
            et=epoch,  # center intercept epoch
            ref='IAU_' + observer,
            abcorr=aberration_correction,
            obs=observer
        )[0]

        return np.linalg.norm(surface_point - observer_target_distance)

    @staticmethod
    def spice2date(spice_ephemeris_epoch):
        """
        Convert a SPICE ephemeris epoch in seconds into an ISO 8601 date.
        Uses the SPICE method et2utc.
        :param spice_ephemeris_epoch: (s) SPICE ephemeris epoch
        :return: (ISO 8601) date
        """
        return spice.et2utc(spice_ephemeris_epoch, 'ISOC', 3)
