"""
Spectrum-related objects and functions.
"""
import copy
import os
from sys import float_info
from warnings import warn

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

from .astra import Star, Planet
from .atmosphere import Atmosphere, Molecule, Sky
from .files import File, Figure, DataFile, PickleFile, save_figure
from .interface import run_nrt
from .math_ import chi2_reduced, chi2
from .utils import info
from .version import compact_version


def wavelength2wavenumber(wavelength):
    """Convert a wavelength in m to a wavenumber in cm-1."""
    return 1 / (wavelength * 100)


def wavenumber2wavelength(wavenumber):
    """Convert a wavenumber in cm-1 into a wavelength in m."""
    return 1 / (wavenumber * 100)


def radiance_um2radiance_wvn(radiance, wavelength):
    """
    Convert radiance from W.m-2.sr-1/um to W.cm-2.sr-1/cm-1 at a given wavelength.
    Steps:
        1 cm-2 = 1E4 m-2 => 1 W.cm-2.sr-1/um = 1E-4 W.m-2.sr-1/um
        1 cm-1 = 1E-4 um-1 => 1 W.cm-2.sr-1/cm = 1E4 * 1E-4 W.m-2.sr-1/um
        L(cm-1) = L(cm) * l(cm)^2 => 1 W.cm-2.sr-1/cm-1 = l(cm)^2 * W.m-2.sr-1/um
    :param radiance: radiance (W.m-2.sr-1/um)
    :param wavelength: wavelength (m)
    :return:radiance (W.cm-2.sr-1/cm-1)
    """
    return radiance * (wavelength * 1E2) ** 2 * 1E4


def radiance_wvn2radiance_um(radiance, wavenumber):
    """
    Convert radiance from W.cm-2.sr-1/cm-1 to W.cm-2.sr-1.um-1 at a given wavenumber.
    Steps:
        L(m) = L(cm-1) * wvn(m-1)**2 => 1 W.cm-2.sr-1.m-1 = wvn(m-1)**2 * W.cm-2.sr-1/cm-1
        1 m = 1E6 um => 1 W.cm-2.sr-1.um-1 = 1E-6 W.cm-2.sr-1.m-1
    :param radiance: radiance (W.cm-2.sr-1/cm-1)
    :param wavenumber: wavenumber (cm-1)
    :return:radiance (W.cm-2.sr-1/um)
    """
    return radiance * (wavenumber * 1E1) ** 2 * 1E-6


def radiance_wvn2radiance_niri(radiance, wavenumber):
    """
    Convert radiance from W.cm-2.sr-1/cm-1 to W.m-2.arcsec-2.um-1 at a given wavenumber.
    The Gemini Near-Infrared Imager (NIRI) is the primary near-infrared  (NIR) imaging instrument at the Gemini
    North telescope.
    :param radiance: radiance (W.cm-2.sr-1/cm-1)
    :param wavenumber: wavenumber (cm-1)
    :return:radiance (W.m-2.arcsec-2.um-1)
    """
    return radiance_wvn2radiance_um(radiance, wavenumber) / 1E-2 ** 2 / 42545170296.1522


class DerivativeVMR(File):
    def __init__(self, name, path, ext='dat', exists=False, find=False, label='', color=None, linestyle='-', marker=''):
        """
        Radiance derivative in respect with logarithm of molecular VMR object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param label: c.f. File
        :param color: c.f. File
        :param linestyle: c.f. File
        :param marker: c.f. File
        """
        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        self.label = label
        self.color = color
        self.linestyle = linestyle
        self.marker = marker
        self.wavenumbers = []
        self.d_radiances = [[[]]]
        self.mol_name = []
        self.pressures = []

    def read(self):
        """
        Read the derivative file.
        """
        with open(self.file, 'r') as f:
            line = f.readline()
            line = line.strip()
            columns = line.split()
            wvn_min = float(columns[0])
            n_wavenumber = int(columns[1])
            step = float(columns[2])
            wvn_max = wvn_min + (n_wavenumber - 1) * step
            wavenumber = np.linspace(wvn_min, wvn_max, n_wavenumber)

            n_mol = int(columns[3])
            n_level = int(columns[4])
            mol_name = []
            p = np.zeros(n_level)
            d_radiance = np.zeros((n_wavenumber, n_level, n_mol))
            for l in range(0, n_mol):
                line = f.readline()
                line = line.strip()
                mol_name.append(line)
                for k in range(0, n_level):
                    line = f.readline()
                    line = line.strip()
                    p[k] = float(line)
                    for j in range(0, n_wavenumber):
                        line = f.readline()
                        line = line.strip()
                        try:
                            d_radiance[j, k, l] = float(line)
                        except ValueError:
                            d_radiance[j, k, l] = 0.0
            self.wavenumbers = wavenumber
            self.d_radiances = d_radiance
            self.mol_name = mol_name
            self.pressures = np.asarray(p)

    def plot(self, mol, p, wavenumber,
             eps=False, cmap='inferno_r', levels=None, xlim=None, ylim=None, plot_pressure_max_sensibility=True,
             save=False, size=18, nickname='', aspect=6.5 / 5, resolution=720, dpi=100):
        """Plot the derivative."""
        plt.figure(figsize=((resolution * aspect) / dpi, resolution / dpi))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)

        if np.size(mol) == 1 and np.size(p) > 1 and np.size(wavenumber) == 1:
            wh_mol = np.where(mol == self.mol_name)
            if np.size(wh_mol) == 0:
                wh_mol = mol

            k = np.where(np.logical_and(self.pressures >= np.min(p), self.pressures <= np.max(p)))
            if np.size(k) == 0:
                k = p

            j = np.where(np.logical_and(self.wavenumbers >= wavenumber, self.wavenumbers <= wavenumber))
            if np.size(j) == 0:
                j = wavenumber
            if np.size(j) >= 2:
                j = np.where(self.wavenumbers == min(self.wavenumbers, key=lambda x: abs(x - wavenumber)))
            # ylim = self.pressures[np.where(self.d_radiance[j, k, l][0] > 0)]
            plt.plot(np.asarray(self.d_radiances)[j, k, wh_mol], self.pressures[k],
                     c=self.color, ls=self.linestyle, marker=self.marker)
            plt.ylim(1E-1, 20)
            plt.gca().invert_yaxis()
            plt.yscale('log')
            plt.title('dI/dln(q) for ' + self.mol_name[wh_mol][0] + f' @ {self.wavenumbers[j]} cm-1')
            plt.xlabel('dI/dln(q) [J.s-1.cm-2.sr-1/cm-1]')
            plt.ylabel('Pressure [bar]')

        if np.size(mol) == 1 and np.size(p) > 1 and np.size(wavenumber) > 1:
            plt.contourf(self.wavenumbers, self.pressures, np.transpose(self.d_radiances[:, :, mol]), cmap=cmap,
                         levels=levels)
            plt.semilogy()

            d_radiance = np.transpose(self.d_radiances[:, :, mol])
            wh = []
            for i in range(np.size(self.wavenumbers)):
                whi = np.where(abs(d_radiance[:, i]) == np.max(abs(d_radiance[:, i])))
                if np.size(whi) > 1:
                    whi = np.asarray([0])
                wh.append(int(whi[0]))
            wh = np.asarray(wh)

            pp = p[wh]
            # pp = pp[:, 0]
            if plot_pressure_max_sensibility:
                plt.plot(self.wavenumbers, pp, c='w',
                         label='Pressure of max sensibility')

            plt.yscale('log')
            if xlim is not None:
                plt.xlim(xlim)
            if ylim is not None:
                plt.ylim(ylim)

            plt.xlabel(r'Wavenumber (cm$^{-1}$)')
            plt.ylabel('Pressure (bar)')

            ax = plt.gca()
            tl = ax.get_xticklines()
            for line in tl:
                line.set_color('w')
            tl = ax.get_yticklines()
            for line in tl:
                line.set_color('w')

            ax.tick_params(which='both', color='w')
            # ax.get_yticklines(color='k')

            cbar_format = ticker.ScalarFormatter(useMathText=True, useOffset=True)
            cbar_format.set_powerlimits((-3, 3))
            cbar = plt.colorbar(format=cbar_format)

            cbar.ax.tick_params(labelsize=size)
            cbar.ax.yaxis.get_offset_text().set(size=size)

            plt.gca().invert_yaxis()

            # plt.legend(loc=2, prop={'size': 12 * size})
            # legend.get_frame().set_facecolor('k')
            # for text in legend.get_texts():
            #     text.set_color('w')
            for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(size)
            cbar.set_label(r'$\partial L_{th}/\partial \ln c_m$ ' + f'({Spectrum.RADIANCE_UNITS})',
                           size=size)
            cbar.ax.tick_params(labelsize=size)

        if save:
            fig_name = self.name + "_" + nickname
            print(f'Saving figure \'{fig_name}\'...')
            fig_dir = os.path.join(Figure.PATH, 'fig_TEXES', 'drad', compact_version())
            if not os.path.isdir(fig_dir):
                os.makedirs(fig_dir)
            plt.savefig(
                os.path.join(fig_dir, fig_name + '.png'),
                dpi=80, format='png')
            if eps:
                plt.savefig(
                    os.path.join(Figure.PATH, 'fig_TEXES', 'kernels', compact_version(),
                                 fig_name + '.eps'), dpi=80, format='eps')


class DerivativeCloudTransmittance(File):
    def __init__(self, name, path, ext='dat', exists=False, find=False, label='', color=None, linestyle='-', marker=''):
        """
        Radiance derivative in respect with total cloud transmittance object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param label: c.f. File
        :param color: c.f. File
        :param linestyle: c.f. File
        :param marker: c.f. File
        """
        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        self.name = name
        self.path = path + name + ext

        self.label = label
        self.color = color
        self.linestyle = linestyle
        self.marker = marker
        self.wavenumbers = []
        self.d_radiances = [[[]]]
        self.layer = []
        self.pressures = []

    def read(self):
        """
        Read the file.
        """
        with open(self.file, 'r') as f:
            line = f.readline()
            line = line.strip()
            columns = line.split()
            wvn_min = float(columns[0])
            n_wavenumber = int(columns[1])
            step = float(columns[2])
            wvn_max = wvn_min + (n_wavenumber - 1) * step
            wavenumber = np.linspace(wvn_min, wvn_max, n_wavenumber)

            n_cloud = int(columns[3])
            n_level = int(columns[4])
            layer = []
            p = np.zeros(n_level)
            d_radiance = np.zeros((n_wavenumber, n_level, n_cloud))
            for l in range(0, n_cloud):
                line = f.readline()
                line = line.strip()
                layer.append(int(line))
                for k in range(0, n_level):
                    line = f.readline()
                    line = line.strip()
                    p[k] = float(line)
                    for j in range(0, n_wavenumber):
                        line = f.readline()
                        line = line.strip()
                        d_radiance[j, k, l] = float(line)
            self.wavenumbers = wavenumber
            self.d_radiances = d_radiance
            self.layer = np.asarray(layer)
            self.pressures = np.asarray(p)


class Instrument(PickleFile):
    DEFAULT_NAME = 'new_instrument'
    DEFAULT_HOST = 'unknown_host'
    PATH = os.path.join(File.PATH_IN, 'instruments', '')
    EXT = PickleFile.EXT

    class InstrumentDataFile(DataFile):
        DEFAULT_NAME = 'new_instrument'
        PATH = os.path.join(File.PATH_IN, 'instruments', '')
        EXT = DataFile.EXT

        def __init__(self, name=DEFAULT_NAME, path=PATH, ext=EXT, exists=False, find=False,
                     lsf_wavenumbers=0.0, lsf_fwhm=None, lsf_shape=None):
            """
            Data File of an Instrument.
            Used in interface with NRT.
            :param name: c.f. DataFile
            :param path: c.f. DataFile
            :param ext: c.f. DataFile
            :param exists: c.f. DataFile
            :param find: c.f. DataFile
            :param lsf_wavenumbers: (cm-1) wavenumbers of the instrument Line Spread Functions
            :param lsf_fwhm: (cm-1) instrument Line Spread Functions Full Width Half Maximum
            """
            super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

            self.lsf_wavenumbers = lsf_wavenumbers
            self.lsf_fwhm = lsf_fwhm
            self.lsf_shape = lsf_shape

        def load(self):
            self.lsf_wavenumbers, self.lsf_fwhm = self.read()

        def save(self):
            data = [self.DataColumn(self.lsf_wavenumbers), self.DataColumn(self.lsf_fwhm)]
            self.write(data, title='w(cm-1) lsf_fwhm(cm-1)', parameters='{' + f'\'lsf_shape\': {self.lsf_shape}' + '}')

    def __init__(self, name=DEFAULT_NAME, path=PATH, ext=EXT, exists=False, find=False,
                 host_name=DEFAULT_HOST, array_shape=None, pixel_resolution=None,
                 lsf_wavenumbers=None, lsf_fwhm=0.0, lsf_shape=None, data_file=None):
        """
        Define an instrument characteristics.
        :param name: name of the instrument
        :param path: path of the file
        :param ext: extension of the file
        :param exists: c.f. File
        :param find: c.f. File
        :param host_name: name of the instrument host (telescope, spacecraft, ...)
        :param array_shape: shape of the instrument array
        :param pixel_resolution: (arcsec) pixel-projected angular resolution of the instrument
        :param lsf_wavenumbers: (cm-1) wavenumbers of the instrument line spread functions full width half maximum
        :param lsf_fwhm: (cm-1) full width half maximum of the instrument function
        :param lsf_shape: shape of the lsf of the instrument
        :param data_file: data file of the instrument
        """
        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        self.host_name = host_name
        self.array_shape = array_shape
        self.pixel_resolution = pixel_resolution
        self.lsf_wavenumbers = lsf_wavenumbers
        self.lsf_fwhm = lsf_fwhm
        self.lsf_shape = lsf_shape

        if data_file is None:
            self.data_file = self.InstrumentDataFile(
                name=self.name,
                path=self.path,
                lsf_shape=self.lsf_shape,
                lsf_wavenumbers=self.lsf_wavenumbers,
                lsf_fwhm=self.lsf_fwhm
            )
        else:
            self.data_file = data_file

    def update_data_file(self):
        """
        Update the data file.
        """
        self.data_file.name = self.name
        self.data_file.lsf_shape = self.lsf_shape
        self.data_file.lsf_wavenumbers = self.lsf_wavenumbers
        self.data_file.lsf_fwhm = self.lsf_fwhm
        self.data_file.save()

    @staticmethod
    def res2fwhm(wavenumber, resolution_power):
        """
        Resolution power to lsf Full Width Half Maximum.
        :param wavenumber: wavenumber (cm-1)
        :param resolution_power: resolution power
        :return: lsf Full Width Half Maximum (cm-1)
        """
        return wavenumber / (resolution_power + 1)

    @staticmethod
    def res_power2wvn_res(res_power, wavenumber):
        """
        Convert a resolution power at a reference wavenumber into a wavenumber resolution.
        :param res_power: resolution power
        :param wavenumber: (cm-1) reference wavenumber
        :return: (cm-1) wavenumber resolution
        """
        lmb = wavenumber2wavelength(wavenumber)
        lmb += lmb / res_power
        return wavenumber - wavelength2wavenumber(lmb)


class Noise(DataFile):
    PATH = os.path.join(File.PATH_IN, 'noises', '')
    EXT = 'dat'

    def __init__(self, name, path=PATH, wavenumbers=None, radiances=None, exists=False, find=False):
        """
        Represents the standard deviation of the error on the radiance measurement.
        :param name: c.f. File
        :param path: c.f. File
        :param wavenumbers: measurement error at the given wavenumber
        :param radiances: measurement error in radiance
        :param exists: c.f. File
        :param find: c.f. File
        """
        super().__init__(name=name, path=path, ext=self.EXT, exists=exists, find=find)

        if wavenumbers is None:
            self.wavenumbers = np.array([])
        else:
            self.wavenumbers = np.asarray(wavenumbers)
        if radiances is None:
            self.radiances = np.array([])
        else:
            self.radiances = np.asarray(radiances)

    def load(self):
        """
        Load noise from the Noise DataFile.
        """
        self.wavenumbers, self.radiances = self.read()

    def save(self):
        """
        Save the Noise into a DataFile.
        """
        data = [self.DataColumn(self.wavenumbers), self.DataColumn(self.radiances, force_exp_notation=True)]
        self.write(data, title='w(cm-1) err(W.m-2.sr-1.cm)')

    @classmethod
    def from_data(cls, name, path=PATH):
        """
        Initialise a Noise from a .dat file.
        :param name: c.f. File
        :param path: c.f. File
        :return: initialised Noise
        """
        new_noise = cls(name=name, path=path, find=True)
        new_noise.load()

        return new_noise


class Target(Planet, PickleFile):
    DEFAULT_NAME = 'new_target'
    PATH = os.path.join(File.PATH_IN, 'targets', '')
    EXT = PickleFile.EXT

    DEFAULT_TARGET_FILE = os.path.join(PATH, 'default_target.cfg')

    # Load default atmosphere
    DEFAULT = None
    if DEFAULT is None:
        try:
            with open(DEFAULT_TARGET_FILE, 'r') as f:
                for line in f:
                    DEFAULT = PickleFile(name=line, path=PATH, find=True)
        except FileNotFoundError or NotADirectoryError:
            DEFAULT = None
            info(f'file \'{DEFAULT_TARGET_FILE}\' not found, no default target will be used')

    class LightSource(Star, PickleFile):
        DEFAULT_NAME = 'new_light_source'
        PATH = os.path.join(File.PATH_IN, 'targets', 'light_sources', '')
        EXT = PickleFile.EXT

        def __init__(self, name=DEFAULT_NAME, path=PATH, ext=EXT, exists=False, find=False,
                     radius=0.0, effective_temperature=0.0):
            """
            Light source object.
            :param name: c.f. File
            :param path: c.f. File
            :param ext: c.f. File
            :param exists: c.f. File
            :param find: c.f. File
            :param radius: (m) c.f. Star
            :param effective_temperature: (K) c.f. Star
            """
            Star.__init__(self, radius=radius, effective_temperature=effective_temperature)
            PickleFile.__init__(self, name=name, path=path, ext=ext, exists=exists, find=find)

        @classmethod
        def from_pkl(cls, name, path=PATH):
            """Load the target from a .pkl file."""
            return cls.loader(File(name=name, path=path, ext='pkl', find=True).file)

    def __init__(self, name=DEFAULT_NAME, path=PATH, ext=EXT,
                 mass=0.0, radius=0.0, equatorial_radius=0.0, polar_radius=0.0, atmosphere=None, light_source=None,
                 light_source_range=np.nan, latitude=np.nan, longitude=np.nan,
                 incidence_angle=np.nan, emission_angle=np.nan, phase_angle=np.nan):
        """
        Target object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param mass: (kg) c.f. Planet
        :param radius: (m) c.f. Planet
        :param equatorial_radius: (m) c.f. Planet
        :param polar_radius: (m) c.f. Planet
        :param atmosphere: a priori atmosphere of the target
        :param light_source: light source illuminating the target
        :param light_source_range: (m) distance between the center of the light source and the target
        :param latitude: (deg) latitude from which the spectrum come from
        :param longitude: (deg) longitude from which the spectrum come from
        :param incidence_angle: (deg) angle between the surface normal and the vector from the surf. to the light source
        :param emission_angle: (deg) angle between the surface normal and the vector from the surf. to the observer
        :param phase_angle: (deg) angle between the vectors from the surface to the observer and from the surface to the
                           light source.
        """
        Planet.__init__(self, mass=mass, radius=radius, equatorial_radius=equatorial_radius, polar_radius=polar_radius)
        PickleFile.__init__(self, name=name, path=path, ext=ext)

        if Target.DEFAULT is not None:
            local_args = locals()
            default_target = Target.from_pkl(Target.DEFAULT.name)
            for arg in local_args:
                if not local_args[arg]:
                    setattr(self, arg, copy.deepcopy(getattr(default_target, arg)))
                else:
                    setattr(self, arg, local_args[arg])
        else:
            if light_source is None:
                light_source = Target.LightSource(name=self.name + '_light_source')

            if atmosphere is None:
                self.atmosphere = Atmosphere(name=self.name + '_atmosphere')
            else:
                self.atmosphere = copy.deepcopy(atmosphere)

            self.light_source = light_source
            self.light_source_range = light_source_range
            self.latitude = latitude
            self.longitude = longitude
            self.incidence_angle = incidence_angle
            self.emission_angle = emission_angle
            self.phase_angle = phase_angle

    def use_as_default(self):
        Target.DEFAULT = self
        self.save()

        with open(self.DEFAULT_TARGET_FILE, 'w') as f:
            f.write(f'{self.name}')

    @classmethod
    def from_pkl(cls, name, path=PATH):
        """Load the target from a .pkl file."""
        return cls.loader(File(name=name, path=path, ext='pkl', find=True).file)


class Spectrum(DataFile):
    """Spectrum object."""
    PATH_IN = os.path.join(File.PATH_IN, 'spectra', '')
    PATH_OUT = os.path.join(File.PATH_OUT, 'spectra', '')
    EXT = DataFile.EXT
    BASE_NAME = 'NRT' + compact_version()
    X_UNITS = r'cm$^{-1}$'
    RADIANCE_UNITS = r'W$\cdot$m$^{-2}\cdot$sr$^{-1}$/' + f'{X_UNITS}'
    XLABEL = rf'Wavenumber ({X_UNITS})'
    YLABEL = rf'Radiance ({RADIANCE_UNITS})'

    def __init__(self, name, path='', exists=False, find=False,
                 wavenumbers=None, radiances=None, noise=None, target=None, atmosphere=None, instrument=None,
                 doppler_shift=0.0, goodness_of_fit=None, location=None,
                 start_time='', stop_time='', reference='',
                 label='', color='k', linestyle='-', marker=''):
        """
        :param name: c.f. File
        :param path: c.f. File
        :param exists: c.f. File
        :param find: c.f. File
        :param wavenumbers: wavenumber of the spectrum
        :param radiances: radiance in wavenumber of the spectrum
        :param noise: standard deviation of the noise of the spectrum
        :param target: target from which the is issuing spectrum
        :param atmosphere: atmosphere from which the spectrum is the result
        :param instrument: instrument from which the spectrum has been taken
        :param doppler_shift: doppler shift of the spectrum
        :param goodness_of_fit: dict of chi2 of the current Spectrum (value) in respect with an indicated Spectrum (key)
        :param location: x-y location of the spectrum (only meaningful in spectral cube)
        :param start_time: time from which the spectrum has begun to be taken
        :param stop_time: time from which the spectrum has ended to be taken
        :param reference: id of the spectrum / id of the spectral cube of the spectrum
        :param label: c.f. File
        :param color: c.f. File
        :param linestyle: c.f. File
        :param marker: c.f. File
        """
        super().__init__(name=name, path=path, ext=Spectrum.EXT, exists=exists, find=find)

        if wavenumbers is None:  # wavenumber array
            self.wavenumbers = np.array([])
        else:
            self.wavenumbers = np.asarray(wavenumbers)

        if radiances is None:  # radiance in wavenumber array
            self.radiances = np.array([])
        else:
            self.radiances = np.asarray(radiances)

        if noise is None:
            self.noise = Noise('noise_' + self.name)
            if not os.path.isfile(self.noise.file):
                self.noise = None
        else:
            if isinstance(noise, Noise):
                self.noise = noise
            elif len(self.wavenumbers) == len(noise):
                self.noise = Noise('noise_' + self.name, wavenumbers=self.wavenumbers, radiances=noise)
            else:
                warn(f'cannot generate \'{"noise_" + self.name}\': '
                     f'incompatible size of noise array ({len(noise)}) and wavenumber array ({len(self.wavenumbers)})',
                     Warning)

        if target is None:
            self.target = Target(name='target_' + self.name)
        else:
            self.target = target

        if atmosphere is None:
            if target is None:
                self.atmosphere = Atmosphere(name='atm_' + self.name)
            else:
                self.atmosphere = copy.deepcopy(self.target.atmosphere)
        else:
            self.atmosphere = copy.deepcopy(atmosphere)  # copy is necessary to keep the original cloud_config

        self.instrument = instrument
        self.doppler_shift = doppler_shift

        if goodness_of_fit is None:
            self.chi2 = {}
        elif isinstance(goodness_of_fit, dict):
            self.chi2 = goodness_of_fit
        else:
            raise TypeError('chi2 must be a dictionary')

        self.location = location

        self.start_time = start_time
        self.stop_time = stop_time
        self.reference = reference

        self.label = label
        self.color = color
        self.linestyle = linestyle
        self.marker = marker

        if os.path.isfile(path + name + 'da' + Spectrum.EXT):
            self.d_vmr = DerivativeVMR(name + 'da', path, color=color, linestyle=linestyle, marker=marker)
        else:
            self.d_vmr = None

        if os.path.isfile(path + name + 'dt' + Spectrum.EXT):
            self.d_cloud_transmittance = DerivativeCloudTransmittance(name + 'dt', path,
                                                                      color=color, linestyle=linestyle, marker=marker)
        else:
            self.d_cloud_transmittance = None

    def auto_label(self):
        """
        Auto generate a label based on the Spectrum metadata.
        """
        if self.instrument is not None or self.start_time != '' or self.reference != '' or self.location is not None:
            self.label = ''
            metadata = [self.instrument.name, self.start_time, self.reference, self.location2str(self.location)]
            for meta in metadata:
                if meta != '' and meta is not None:
                    if self.label == '':
                        self.label += meta
                    else:
                        self.label += ' ' + meta
        else:
            self.label = self.name

    def doppler_shift_fx(self):
        """
        Correct a Spectrum from the Doppler shift effect. Does not change the original Spectrum wavenumbers.
        """
        wavenumber_doppler = self.doppler_shift + self.wavenumbers
        wh = np.where(np.logical_and(self.wavenumbers >= np.min(wavenumber_doppler),
                                     self.wavenumbers <= np.max(wavenumber_doppler)))
        radiances_doppler = np.zeros(np.size(self.radiances))
        radiances_doppler[wh] = np.interp(self.wavenumbers[wh], wavenumber_doppler, self.radiances)
        self.radiances = radiances_doppler

    def get_fig_dir(self, retrieved=False, default_fig_dir=Figure.PATH):
        """
        Generate a figure directory name based on the Spectrum Atmosphere and Instrument.
        :param retrieved: if True, the Spectrum is considered retrieved (this change the output).
        :param default_fig_dir: default figure directory
        :return: a figure directory name
        """
        fig_dir = default_fig_dir

        if self.instrument is not None:
            fig_dir = os.path.join(fig_dir, self.instrument.name)

        at_least_one_molecule_retrieved = False
        if self.atmosphere is not None and retrieved:
            if self.atmosphere.clouds[0].transmittance < 1:
                fig_dir = os.path.join(fig_dir, 'retrievals')
            for molecule in self.atmosphere.molecules:
                if self.atmosphere.molecules[molecule].retrieve:
                    at_least_one_molecule_retrieved = True
                    break

        if self.instrument is not None:
            if self.instrument.name in self.name and '_xy' in self.name:
                fig_dir = os.path.join(
                    fig_dir, self.name.split(self.instrument.name + '_')[1].split('_xy')[0])

                if self.atmosphere is not None:
                    fig_dir += f'_cloud' \
                        f'{str(self.atmosphere.clouds[0].pressure_base).replace(".0", "").replace(".", "")}' \
                        f'bar'

                    if self.atmosphere.clouds[0].transmittance < 1 and not at_least_one_molecule_retrieved:
                        fig_dir += '_cloud-only'
            else:
                fig_dir = os.path.join('divers')

        return fig_dir

    def load(self):
        """
        Read the radiance, the wavenumber and eventually the cloud configuration of a spectrum.
        """
        self.wavenumbers, self.radiances = self.read()

    def retrieve(self, overwrite=True, **kwargs):
        """
        Retrieve the spectrum.
        :param overwrite: If True, and if a file with the same name as the generated output file already exists, then
        overwrite the old file
        :param kwargs: c.f. run_nrt
        :return: the retrieved Spectrum
        """
        # run retrieval
        best_fit_spectrum = self.__run_wrap(retrieved_spectrum=self, convolve=True, retrieve=True, overwrite=overwrite,
                                            **kwargs)

        # get retrieved VMR profiles if any
        for molecule in self.atmosphere.molecules:
            if self.atmosphere.molecules[molecule].retrieve:
                best_fit_spectrum.atmosphere.molecules[molecule].vmr_profile = Molecule.VMRProfile(
                    name=best_fit_spectrum.name + '_' + molecule + '_', path=Molecule.VMRProfile.PATH_OUT,
                    find=True)
                best_fit_spectrum.atmosphere.molecules[molecule].vmr_profile.load()

        # get chi2
        if os.path.isfile(self.noise.file):
            self.noise.load()
        else:
            self.noise.radiances = 0.1 * np.max(self.radiances)

        best_fit_spectrum.chi2[self] = chi2_reduced(self.radiances,
                                                    np.interp(self.wavenumbers, best_fit_spectrum.wavenumbers,
                                                              best_fit_spectrum.radiances),
                                                    self.noise.radiances) / (np.size(self.radiances) - 1)

        # update label to match retrieved atmosphere
        best_fit_spectrum.label = self._generate_label(atmosphere=best_fit_spectrum.atmosphere,
                                                       instrument=best_fit_spectrum.instrument, head='best fit')

        best_fit_spectrum.target.latitude = self.target.latitude
        best_fit_spectrum.target.longitude = self.target.longitude
        best_fit_spectrum.location = self.location
        best_fit_spectrum.start_time = self.start_time
        best_fit_spectrum.reference = self.reference

        return best_fit_spectrum

    def save(self):
        """
        Save the Spectrum into a DataFile.
        """
        data = [self.DataColumn(self.wavenumbers),
                self.DataColumn(self.radiances, force_exp_notation=True)]
        self.write(data=data, title='w(cm-1) r(W.m-2.sr-1.cm)')

    @classmethod
    def auto_name(cls, path=PATH_OUT, find=False,
                  atmosphere=None, instrument=None,
                  spec_obs=None, spec_base_name='NRT' + compact_version(), spec_nickname='',
                  label='', color='k', **kwargs):
        """
        Return an auto-named Spectrum with the given parameters.
        :param path: c.f. File
        :param find: c.f. File
        :param atmosphere: atmosphere of the spectrum
        :param instrument: can be an Instrument or a string for an existing Instrument name
        :param spec_obs: observed spectrum (for retrieving)
        :param spec_base_name: base name of the spectrum
        :param spec_nickname: addition to the spectrum name
        :param label: c.f. Spectrum
        :param color: c.f. Spectrum
        :return: An auto-named Spectrum.
        """
        if isinstance(spec_obs, Spectrum):
            return cls.from_spectrum(spectrum=spec_obs, path=path, find=find,
                                     base_name=spec_base_name, nickname=spec_nickname,
                                     base_label=label, color=color, **kwargs)
        else:
            return cls.from_physics(atmosphere=atmosphere, instrument=instrument, **kwargs)

    @classmethod
    def calculate_radiances(cls, wavenumber_lim, overwrite=True, **kwargs):
        """
        Generate a spectrum and calculate its radiances for the given parameters.
        :param wavenumber_lim: set the wavenumber limits of the spectrum
        :param overwrite: if True and if a file of the same name of the outputted spectrum file exists, overwrite the
        former.
        :param kwargs: c.f. run_nrt
        :return: the synthetic Spectrum
        """
        if np.size(wavenumber_lim) < 1:
            raise TypeError(f'\'wavenumbers\' object ({wavenumber_lim}) is not subscriptable')
        elif np.size(wavenumber_lim) > 2:
            raise ValueError(f'too many values to unpack (expected 2)')

        new_spectrum = cls.__run_wrap(wavenumber_min=wavenumber_lim[0], wavenumber_max=wavenumber_lim[-1],
                                      retrieve=False, overwrite=overwrite, **kwargs)

        return new_spectrum

    @classmethod
    def from_physics(cls, atmosphere=None, instrument=None, path=PATH_OUT, find=False,
                     base_name='NRT' + compact_version(), nickname='sy',
                     base_label='synthetic', color='k', **kwargs):
        """
        Return an auto-named Spectrum based on atmospheric and instrument parameters.
        Useful to name any new Spectrum.
        :param atmosphere: Atmosphere from which to take parameters
        :param instrument: Instrument from which to take parameters
        :param path: c.f. File
        :param find: c.f. File
        :param base_name:
        :param nickname:
        :param base_label:
        :param color:
        :param kwargs:
        :return: a Spectrum
        """
        if nickname != '' and nickname[0] != '_':
            nickname = '_' + nickname

        spec_name = base_name

        if isinstance(atmosphere, Atmosphere):
            if atmosphere.clouds[0].transmittance < 1:
                spec_name += \
                    f'_cld{atmosphere.clouds[0].transmittance}t' \
                    f'{atmosphere.clouds[0].pressure_base}b'.replace('.0', '').replace('.', '')
        if isinstance(instrument, Instrument):
            if instrument.lsf_fwhm > 0.0:
                spec_name += '_cnvFWHM%.0E' % instrument.lsf_fwhm

        label = cls._generate_label(atmosphere=atmosphere, instrument=instrument, head=base_label)

        spec_name += nickname
        spec_name = spec_name.replace('+', '')

        return cls(name=spec_name, path=path, find=find,
                   atmosphere=atmosphere, instrument=instrument, label=label, color=color, **kwargs)

    @classmethod
    def from_spectrum(cls, spectrum, path=PATH_OUT, find=False,
                      base_name='NRT' + compact_version(), nickname='BF',
                      base_label='best fit', color='g', **kwargs):
        """
        Return an auto-named Spectrum based on another Spectrum parameters. Also copy the given Spectrum atmosphere,
        instrument, image location and space-time locations, if any.
        Useful to name a Spectrum retrieved from an observed spectrum.
        :param path: c.f. File
        :param find: c.f. File
        :param spectrum: Spectrum from which to take the parameters
        :param base_name: base name of the spectrum
        :param nickname: addition to the spectrum name
        :param base_label: c.f. Spectrum
        :param color: c.f. Spectrum
        :return: A Spectrum
        """
        if nickname != '' and nickname[0] != '_':
            nickname = '_' + nickname

        if isinstance(spectrum, Spectrum):
            spec_obs_name = 'spectrum'

            if spectrum.location is not None:
                spec_obs_location = Spectrum.location2str(spectrum.location)
                spec_location = copy.deepcopy(spectrum.location)
            else:
                spec_obs_location = ''
                spec_location = None

            target = copy.deepcopy(spectrum.target)
            atmosphere = copy.deepcopy(spectrum.atmosphere)

            instrument = copy.deepcopy(spectrum.instrument)
            if spectrum.instrument is not None:
                spec_obs_name = spectrum.instrument.name

            label = cls._generate_label(atmosphere=atmosphere, instrument=instrument, head=base_label)

            if spectrum.start_time is not '':
                spec_obs_name += '_' + '_'.join(
                    spectrum.start_time.split('.')[0].replace(':', '').replace('-', '').split('T'))

            if spec_obs_location is not '':
                spec_obs_name += '_' + spec_obs_location

            spec_name = base_name + '_' + spec_obs_name

            spec_name += nickname
            spec_name = spec_name.replace('+', '')

            return cls(name=spec_name, path=path, find=find,
                       target=target, atmosphere=atmosphere, instrument=instrument,
                       location=spec_location,
                       start_time=spectrum.start_time,
                       label=label, color=color, **kwargs)
        else:
            raise TypeError(f'spectrum {spectrum.__class__} is not an instance of Spectrum')

    @staticmethod
    def __run_wrap(retrieved_spectrum=None, path=PATH_OUT, find=False, noise_file=None, output_spectrum=None,
                   target=None, atmosphere=None, instrument=None, doppler_shift=None, overwrite=True, verbose=True,
                   base_name='NRT' + compact_version(), base_label='', nickname='', **kwargs):
        """
        Wrapper for function run_nrt().
        Manage default cases.
        :param retrieved_spectrum: c.f. run_nrt
        :param noise_file: c.f. run_nrt
        :param output_spectrum: c.f. run_nrt
        :param target: c.f. run_nrt
        :param atmosphere: c.f. run_nrt
        :param instrument: c.f. run_nrt
        :param doppler_shift: c.f. run_nrt
        :param kwargs: c.f. run_nrt
        :return: a Spectrum with its radiance calculated by NRT according to the specified parameters
        """
        if target is None:
            if retrieved_spectrum is not None:
                target = copy.deepcopy(retrieved_spectrum.target)
            else:
                target = Target()

        if atmosphere is None:
            if retrieved_spectrum is not None:
                atmosphere = copy.deepcopy(retrieved_spectrum.atmosphere)
            else:
                atmosphere = Atmosphere()

        if instrument is None:
            if retrieved_spectrum is not None:
                instrument = copy.deepcopy(retrieved_spectrum.instrument)
            else:
                instrument = Instrument()

        if doppler_shift is None:
            if retrieved_spectrum is not None:
                doppler_shift = copy.deepcopy(retrieved_spectrum.doppler_shift)
            else:
                doppler_shift = 0.0

        if retrieved_spectrum is not None:
            if os.path.isfile(retrieved_spectrum.file):
                retrieved_spectrum.load()
            else:
                retrieved_spectrum.save()

            if noise_file is None:
                if retrieved_spectrum.noise is None:
                    retrieved_spectrum.noise = Noise('noise_' + retrieved_spectrum.name,
                                                     wavenumbers=retrieved_spectrum.wavenumbers,
                                                     radiances=np.max(retrieved_spectrum.radiances) / 10)
                else:
                    noise_file = retrieved_spectrum.noise.file.rsplit(os.path.sep)[-1]

        if output_spectrum is None:
            if base_label == '':
                base_label = base_name

            if retrieved_spectrum is not None:
                output_spectrum = Spectrum.from_spectrum(spectrum=retrieved_spectrum, path=path, find=find,
                                                         base_name=base_name, base_label=base_label, nickname=nickname)
            else:
                output_spectrum = Spectrum.from_physics(atmosphere=atmosphere, instrument=instrument, path=path,
                                                        find=find, base_name=base_name, base_label=base_label,
                                                        nickname=nickname)

        if os.path.isfile(output_spectrum.file) and not overwrite:
            if verbose:
                info(f'skipping run: spectrum file \'{output_spectrum.file}\' already exists')
        else:
            run_nrt(retrieved_spectrum=retrieved_spectrum, noise_file=noise_file, output_spectrum=output_spectrum,
                    target=target, atmosphere=atmosphere, instrument=instrument,
                    doppler_shift=doppler_shift,
                    path_vmr=Atmosphere.PATH_IN_VMR,
                    path_lines=Molecule.Lines.PATH_IN,
                    path_molecule_parameters=Atmosphere.PATH_IN_MOL,
                    path_absorption_cross_sections=Molecule.PATH_ABSORPTION_CROSS_SECTIONS,
                    path_sky=Sky.PATH,
                    path_retrieved_spectrum=Spectrum.PATH_IN,
                    path_noise=Noise.PATH,
                    path_instrument=instrument.data_file.path,
                    path_temperature_profile=Atmosphere.PATH_IN_TEMP,
                    path_output_spectrum=Spectrum.PATH_OUT,
                    path_output_vmr=Molecule.VMRProfile.PATH_OUT,
                    **kwargs)

        output_spectrum.load()
        return output_spectrum

    @staticmethod
    def _generate_label(atmosphere=None, instrument=None, head='', tail=''):
        """
        Generate a spectrum label from physical parameters.
        :param atmosphere: Atmosphere to take into account
        :param instrument: Instrument to take into account
        :param head: head of the label
        :param tail: tail of the label
        :return: a label
        """
        label = head

        if isinstance(atmosphere, Atmosphere):
            if atmosphere.clouds[0].transmittance < 1:
                label += f' cloud {atmosphere.clouds[0].transmittance} t @ {atmosphere.clouds[0].pressure_base} bar'

        if isinstance(instrument, Instrument):
            if hasattr(instrument.lsf_fwhm, '__iter__'):
                label += rf' sliding lsf$'
            else:
                if instrument.lsf_fwhm > 0.0:
                    label += rf' lsfFWHM={instrument.lsf_fwhm} cm$^{-1}$'

        label += tail

        return label

    @staticmethod
    def _get_common_wavenumbers(spectra, mode='tight'):
        wvn_step0 = 0
        wvn_cmp = []
        if mode == 'tight':
            wvn_max0 = float_info.max
            wvn_min0 = -1
        else:
            wvn_max0 = -1
            wvn_min0 = float_info.max

        # Boundaries and comparison wavenumber range management
        for spec in spectra:
            wvn_size = np.size(spec.wavenumbers)
            wvn_max = np.max(spec.wavenumbers)
            wvn_min = np.min(spec.wavenumbers)
            wvn_step = (wvn_max - wvn_min) / wvn_size

            if mode == 'tight':
                # Set as boundaries the minimal wavenumber range among all spectra
                if wvn_max < wvn_max0:
                    wvn_max0 = wvn_max

                if wvn_min0 < wvn_min:
                    wvn_min0 = wvn_min

                if wvn_step > wvn_step0:
                    wvn_step0 = wvn_step
                    wvn_cmp = spec.wavenumbers
            elif mode == 'lax':
                # Set as boundaries the maximum wavenumber range
                if wvn_step > wvn_step0:
                    wvn_step0 = wvn_step

                if spec == spectra[0]:
                    wvn_cmp = spec.wavenumbers
                    wvn_min0 = wvn_min
                    wvn_max0 = wvn_max
                else:
                    if wvn_min < wvn_cmp[0]:
                        wvn_cmp = np.append(spec.wavenumbers[np.where(spec.wavenumbers < wvn_cmp[0])], wvn_cmp)
                        wvn_min0 = wvn_min

                    if wvn_cmp[-1] < wvn_max:
                        wvn_cmp = np.append(wvn_cmp, spec.wavenumbers[np.where(spec.wavenumbers > wvn_cmp[-1])])
                        wvn_max0 = wvn_max
            else:
                # Same as lax, but rewrite the wavenumber array instead of using the values from the spectra
                if hasattr(mode, '__iter__') and np.size(mode) == 2:
                    wvn_min0 = mode[0]
                    wvn_max0 = mode[1]
                else:
                    if wvn_max > wvn_max0:
                        wvn_max0 = wvn_max

                    if wvn_min < wvn_min0:
                        wvn_min0 = wvn_min

                if wvn_step > wvn_step0:
                    wvn_step0 = wvn_step

                wvn_cmp = np.arange(wvn_min0, wvn_max0, wvn_step0)
                wvn_cmp = np.append(wvn_cmp, wvn_max0)

            if wvn_min0 >= wvn_max0:
                raise ValueError(
                    'No shared wavenumber intervals between spectra: min=%g, max=%g' % (wvn_min0, wvn_max0))

            wh = np.where(np.logical_and(wvn_cmp >= wvn_min0, wvn_cmp <= wvn_max0))
            wvn_cmp = wvn_cmp[wh]

        # Add breaks where gap between two points is too wide
        wh_break = np.where(np.abs(np.diff(wvn_cmp)) >= 2 * wvn_step0)

        for _break in wh_break:
            _break += 1
            wvn_cmp = np.insert(wvn_cmp, _break, np.nan)

        wh_not_nan = np.where(np.logical_not(np.isnan(wvn_cmp)))

        return wvn_cmp, wvn_step0, wh_not_nan

    @staticmethod
    def _plot_residual(spectra, wavenumber_grid, wh_not_nan, line_widths, size, sigma_points_number=8):
        radiances0 = np.array([])
        stderr0 = np.array([])

        for spec in spectra:
            if spec == spectra[0]:  # take the first spectrum as reference
                error_std = 1
                radiances0 = np.interp(wavenumber_grid, spec.wavenumbers, spec.radiances)

                try:
                    if np.size(spec.noise.radiances) == np.size(spec.wavenumbers):
                        stderr0 = spec.noise.radiances
                    else:
                        stderr0 = np.ones(np.size(spec.wavenumbers)) * np.max(radiances0) * error_std
                except AttributeError:
                    stderr0 = np.ones(np.size(spec.wavenumbers)) * np.max(radiances0) * error_std

                gnu = wavenumber_grid
                stderr0 = np.asarray(stderr0[wh_not_nan])

                if sigma_points_number <= 0 or sigma_points_number > np.size(gnu):
                    gnu_std = gnu
                else:
                    gnu_std_ = np.array_split(gnu, sigma_points_number - 1)
                    gnu_std = []

                    for gnu_ in gnu_std_:
                        gnu_std.append(gnu_[0])

                    gnu_std.append(gnu_std_[-1][-1])
                    gnu_std = np.asarray(gnu_std)
                    stderr0_ = np.array_split(stderr0, sigma_points_number - 1)
                    stderr0 = []

                    for std_ in stderr0_:
                        stderr0.append(std_[0])

                    stderr0.append(stderr0_[-1][-1])
                    stderr0 = np.asarray(stderr0)

                error_symbol = r'$\sigma$'

                plt.plot(gnu, np.zeros(np.size(gnu)), c=(0.5, 0.5, 0.5), ls=':')
                plt.plot(gnu_std, np.zeros(np.size(gnu_std)) + stderr0, c=(1.0, 0.5, 0.5), ls=':')
                plt.plot(gnu_std, np.zeros(np.size(gnu_std)) - stderr0, c=(1.0, 0.5, 0.5), ls=':')
                plt.plot(gnu_std, np.zeros(np.size(gnu_std)) + 3 * stderr0, c=(1.0, 0.5, 0.5), ls=':')
                plt.plot(gnu_std, np.zeros(np.size(gnu_std)) - 3 * stderr0, c=(1.0, 0.5, 0.5), ls=':')
                plt.plot(gnu_std, np.zeros(np.size(gnu_std)) + 5 * stderr0, c=(1.0, 0.5, 0.5), ls=':')
                plt.plot(gnu_std, np.zeros(np.size(gnu_std)) - 5 * stderr0, c=(1.0, 0.5, 0.5), ls=':')

                plt.text(np.min(wavenumber_grid[wh_not_nan]), (stderr0[0]), f'{error_std}' + error_symbol,
                         ha='left', va='bottom', color=(1.0, 0.5, 0.5), style='italic', size=size)
                plt.text(np.min(wavenumber_grid[wh_not_nan]), (3 * stderr0[0]), f'{3 * error_std}' + error_symbol,
                         ha='left', va='bottom', color=(1.0, 0.5, 0.5), style='italic', size=size)
                plt.text(np.min(wavenumber_grid[wh_not_nan]), (5 * stderr0[0]), f'{5 * error_std}' + error_symbol,
                         ha='left', va='bottom', color=(1.0, 0.5, 0.5), style='italic', size=size)

            if np.size(spectra) > 1 and spec != spectra[0]:
                radiances = np.interp(wavenumber_grid, spec.wavenumbers, spec.radiances)
                wh_sup0 = np.where(radiances > 0)
                gnu = wavenumber_grid
                diff = (radiances - radiances0)

                if np.std(stderr0) < np.max(stderr0) * 1E-15:
                    spec.chi2[spectra[0]] = chi2(radiances0[wh_sup0], radiances[wh_sup0])

                spec_chi2_name = list(spec.chi2.keys())[0].name

                if spec_chi2_name == spectra[0].name:
                    spec_chi2 = list(spec.chi2.values())[0]
                    plt.plot(gnu, diff, label='with ' + spec.label + rf' ($\chi^2/n$ = {spec_chi2:.2e})',
                             c=spec.color, lw=line_widths)
                else:
                    raise KeyError(f'chi2 of Spectrum \'{spec.name}\' is given for Spectrum \'{spec_chi2_name}\', '
                                   f'not \'{spectra[0].name}\'')

                plt.xlabel(Spectrum.XLABEL)
                plt.ylabel(fr'Difference ({Spectrum.RADIANCE_UNITS})')
                plt.ylim(-np.max(6 * stderr0), np.max(6 * stderr0))
                plt.xlim(np.min(wavenumber_grid[wh_not_nan]), np.max(wavenumber_grid[wh_not_nan]))
                plt.legend(loc=3, prop={'size': size})
                plt.ticklabel_format(useOffset=False)
                ax = plt.gca()

                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                              ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(size)

    @staticmethod
    def _plot_spectra(spectra, err_lines_widths, line_widths, marker_sizes, size, marker_edge_widths,
                      sigmas, wavenumber_grid, wh_not_nan, wavenumber_step,
                      interp_for_plot=True, normalised=False, plot_sky=False):
        # Spectra comparison
        for spec in spectra:
            try:
                if np.size(spec.noise.radiances) > 0:
                    if np.size(spec.noise.radiances) == 1:
                        y_err = np.ones(np.size(spec.wavenumbers)) * spec.noise.radiances
                    else:
                        y_err = spec.noise.radiances
                    line_width = err_lines_widths
                else:
                    y_err = np.zeros(np.size(spec.wavenumbers))
                    line_width = line_widths
            except AttributeError:
                y_err = np.zeros(np.size(spec.wavenumbers))
                line_width = line_widths

            if np.max(spec.wavenumbers) < np.max(wavenumber_grid[wh_not_nan]):
                spec.wavenumbers = np.append(spec.wavenumbers, np.max(spec.wavenumbers) + wavenumber_step)
                spec.wavenumbers = np.append(spec.wavenumbers, np.max(wavenumber_grid[wh_not_nan]))
                spec.radiances = np.append(spec.radiances, [np.nan, np.nan])
                y_err = np.append(y_err, [np.nan, np.nan])

            if np.min(spec.wavenumbers) > np.min(wavenumber_grid[wh_not_nan]):
                spec.wavenumbers = np.concatenate(([np.min(wavenumber_grid[wh_not_nan]),
                                                    np.min(spec.wavenumbers) - wavenumber_step],
                                                   spec.wavenumbers))
                spec.radiances = np.concatenate(([np.nan, np.nan], spec.radiances))
                y_err = np.concatenate(([np.nan, np.nan], y_err))

            wh = np.where(spec.radiances > 0)
            radiance = spec.radiances[wh]
            gnu = spec.wavenumbers[wh]
            y_err = np.array(y_err)[wh]
            y_err *= sigmas  # <x> sigmas

            if interp_for_plot:
                radiance = np.interp(wavenumber_grid, spec.wavenumbers, spec.radiances)
                y_err = np.interp(wavenumber_grid, gnu, y_err)
                gnu = wavenumber_grid

            if normalised:
                radiance /= np.max(radiance)

            plt.errorbar(gnu, radiance, yerr=[y_err, y_err], label=spec.label,
                         c=spec.color, ls=spec.linestyle, marker=spec.marker,
                         lw=line_width, ms=marker_sizes, mew=marker_edge_widths)

            plt.xlabel(Spectrum.XLABEL)

            if not normalised:
                plt.ylabel(Spectrum.YLABEL)
            else:
                plt.ylabel('Relative radiance')

            plt.xlim(np.min(wavenumber_grid[wh_not_nan]), np.max(wavenumber_grid[wh_not_nan]))
            plt.ylim(np.max(radiance) / 1000, None)

        ax = plt.gca()
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                      ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(size)

        if plot_sky and spectra[0].atmosphere.sky is not None:
            ax2 = plt.gca().twinx()

            if not spectra[0].atmosphere.sky.convolved and spectra[0].instrument is not None:
                d_wavenumber = \
                    (spectra[0].atmosphere.sky.wavenumbers[-1] - spectra[0].atmosphere.sky.wavenumbers[0]) / \
                    np.size(spectra[0].atmosphere.sky.wavenumbers)
                length = np.ceil(10 * spectra[0].instrument.lsf_fwhm / d_wavenumber) + 2
                x = np.linspace(-length / 2, length / 2, length) * d_wavenumber
                v = np.exp(-2 * (x / spectra[0].instrument.lsf_fwhm) ** 2)
                v /= np.sum(v)
                sky = np.convolve(spectra[0].atmosphere.sky.transmittances, v, mode='same')
            else:
                sky = spectra[0].atmosphere.sky.transmittances

            ax2.plot(spectra[0].atmosphere.sky.wavenumbers + spectra[0].doppler_shift, sky,
                     label=spectra[0].label + ' sky',
                     c='skyblue', ls='--', lw=line_widths, ms=marker_sizes, mew=marker_edge_widths)

            plt.xlabel(Spectrum.XLABEL)
            plt.ylabel('Sky transmittance')
            plt.ylim(0, None)

        ax = plt.gca()
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                      ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(size)

    @staticmethod
    def errorbar(spectra, plot_vmr_profiles=False, plot_vmr_apriori=False,
                 boundaries='tight', plot_residuals=False, sigma_points_number=8,
                 normalised=False, interp_for_plot=False, plot_sky=False,
                 save=False, fig_name=None, fig_dir=Figure.PATH,
                 sigmas=1, size=12, line_widths=1, err_lines_widths=1, marker_sizes=6,
                 marker_edge_widths=1, fmt='png'):
        """
        Plot a figure representing all the spectra in specs.
        :param spectra: spectra to plot
        :param plot_vmr_profiles: if True, the profiles of each molecule of each spectrum are represented
                                        side-by-side
        :param plot_vmr_apriori: if True, the a a priori VMR profiles are plotted
        :param boundaries: None|'tight'|'lax'|[min, max], method to constrain the wavenumber range
        :param plot_residuals: if True, display the difference bw the first spectrum in specs and all the other spectra
        :param sigma_points_number: number of points of the sigmas lines in the residuals plot
        :param normalised: display normalised radiance instead of the true radiance
        :param interp_for_plot: if True, the spectra will be interpolated with the first spectrum for the plot
        :param plot_sky: if True, add the sky of the first spectrum to the figure
        :param save: if True, the figure will be saved instead of showed
        :param fig_name: the name of the figure
        :param fig_dir: the directory in which the figure will be saved
        :param sigmas: size in sigmas of the y-errorbar in the spectrum plot
        :param size: specify the size of the elements in the figure
        :param line_widths: specify the width of the lines of the figure
        :param err_lines_widths: specify the width of the error bars of the figure
        :param marker_sizes: specify the size of the marker of the figure
        :param marker_edge_widths: specify the size of the edges of the marker of the figure
        :param fmt: format of the figure
        """
        if not hasattr(spectra, '__iter__'):
            spectra = [spectra]

        # Grid initialisation
        if plot_vmr_profiles:
            # Build vmr profiles list
            molecular_vmr_profiles = []

            for mol_name in spectra[0].atmosphere.molecules:
                if spectra[0].atmosphere.molecules[mol_name].retrieve:
                    profiles = []
                    start_spec = 0

                    if plot_vmr_apriori:
                        profile_ap = Molecule.VMRProfile(name='^' + mol_name + '$',
                                                         path=Molecule.VMRProfile.PATH_IN, find=True)

                        profiles.append(profile_ap)
                        if profile_ap.file == spectra[0].atmosphere.molecules[mol_name].vmr_profile.file:
                            start_spec = 1

                    for spec in spectra[start_spec:]:
                        if mol_name in spec.atmosphere.molecules:
                            spec.atmosphere.molecules[mol_name].vmr_profile.label = spec.label
                            spec.atmosphere.molecules[mol_name].vmr_profile.color = spec.color
                            spec.atmosphere.molecules[mol_name].vmr_profile.name = mol_name

                            profiles.append(spec.atmosphere.molecules[mol_name].vmr_profile)

                    molecular_vmr_profiles.append(profiles)

            # Build grid accordingly
            if len(molecular_vmr_profiles) == 0:
                molecular_vmr_profiles = None
                sp_n_cols = 1
            else:
                if not hasattr(molecular_vmr_profiles, '__iter__'):
                    molecular_vmr_profiles = [molecular_vmr_profiles]
                    if not hasattr(molecular_vmr_profiles[0], '__iter__'):
                        molecular_vmr_profiles[0] = [molecular_vmr_profiles[0]]

                for i, vmr_profiles in enumerate(molecular_vmr_profiles):
                    if not hasattr(vmr_profiles, '__iter__'):
                        molecular_vmr_profiles[i] = [vmr_profiles]

                sp_n_cols = 2
        else:
            molecular_vmr_profiles = None
            sp_n_cols = 1

        x_grid_diff = 2
        if plot_residuals and len(spectra) > 1:
            sp_n_rows = 3
        else:
            plot_residuals = False
            sp_n_rows = 1

        wvn_cmp, wvn_step0, wh_not_nan = Spectrum._get_common_wavenumbers(spectra, mode=boundaries)

        # Plot spectra
        fig = plt.figure()

        ax1 = plt.subplot2grid((sp_n_rows, sp_n_cols), (0, 0), rowspan=2)
        Spectrum._plot_spectra(spectra, err_lines_widths, line_widths, marker_sizes, size, marker_edge_widths,
                               sigmas, wvn_cmp, wh_not_nan, wvn_step0,
                               interp_for_plot=interp_for_plot, normalised=normalised, plot_sky=plot_sky)
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)

        # Plot residuals
        if plot_residuals:
            plt.subplot2grid((sp_n_rows, sp_n_cols), (x_grid_diff, 0), sharex=ax1)

            Spectrum._plot_residual(spectra=spectra, wavenumber_grid=wvn_cmp, wh_not_nan=wh_not_nan,
                                    line_widths=line_widths, size=size, sigma_points_number=sigma_points_number)

            fig.subplots_adjust(hspace=0)
            plt.setp(ax1.get_xticklabels(), visible=False)
            ax1.set_xlabel('')
            plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0), useMathText=True)

        # Plot vmr profiles
        # Loop over all the molecules
        if molecular_vmr_profiles is not None:
            sp_n_rows = 1
            sp_n_cols = 2 * len(molecular_vmr_profiles)
            sp_plot_number = sp_n_cols - len(molecular_vmr_profiles)
            ax0 = plt.subplot2grid((sp_n_rows, sp_n_cols), (0, sp_plot_number))

            # Loop over all the vmr profiles of one molecule
            # Search vmr boundaries
            for vmr_profiles in molecular_vmr_profiles:
                if vmr_profiles is not molecular_vmr_profiles[0]:
                    ax = plt.subplot2grid((sp_n_rows, sp_n_cols), (0, sp_plot_number))
                else:
                    ax = ax0

                sp_plot_number += 1
                x_min = float_info.max
                x_max = -1

                for vmr_profile in vmr_profiles:
                    if not isinstance(vmr_profile, Molecule.VMRProfile):
                        continue

                    if np.size(vmr_profile.vmr) > 1:
                        vmr_profile.load()

                    min_loc = np.min(
                        (vmr_profile.vmr * (1 - np.asarray(vmr_profile.vmr_uncertainty_factor)))
                        [np.where(vmr_profile.pressures >= 1E-1)])
                    max_loc = np.max(
                        (vmr_profile.vmr * (1 + np.asarray(vmr_profile.vmr_uncertainty_factor)))
                        [np.where(vmr_profile.pressures >= 1E-1)])
                    if min_loc < x_min:
                        x_min = min_loc

                        if 'NH3' in vmr_profile.name:
                            x_min = np.min(
                                (vmr_profile.vmr * (1 - np.asarray(vmr_profile.vmr_uncertainty_factor)))
                                [np.where(vmr_profile.pressures >= 7.2E-1)])

                        if 'PH3' in vmr_profile.name:
                            x_min = np.min(
                                (vmr_profile.vmr * (1 - np.asarray(vmr_profile.vmr_uncertainty_factor)))
                                [np.where(vmr_profile.pressures >= 8E-1)])

                    if max_loc > x_max:
                        x_max = max_loc

                # Plot all the vmr profiles of one molecule
                for vmr_profile in vmr_profiles:
                    if not isinstance(vmr_profile, Molecule.VMRProfile):
                        continue

                    if np.all(np.asarray(vmr_profile.vmr_uncertainty_factor) == 0):
                        capsize = 0
                    else:
                        capsize = 5

                    plt.errorbar(vmr_profile.vmr, vmr_profile.pressures,
                                 xerr=[vmr_profile.vmr * vmr_profile.vmr_uncertainty_factor,
                                       vmr_profile.vmr * vmr_profile.vmr_uncertainty_factor],
                                 capsize=capsize, label=vmr_profile.label,
                                 c=vmr_profile.color, ls=vmr_profile.linestyle, marker=vmr_profile.marker,
                                 lw=line_widths)

                    plt.xlim(x_min, x_max)

                plt.xscale('log')
                plt.legend(loc=1, prop={'size': size})

                plt.ylim(1E-1, 2E1)
                ax.invert_yaxis()
                plt.yscale('log')

                if ax == ax0:
                    # plt.ylabel(Atmosphere.Molecule.VMRProfile.YLABEL)
                    pass
                else:
                    ax.set_yticklabels([])

                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label, ax.yaxis.offsetText,
                              ax.xaxis.offsetText] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(size)

        plt.subplots_adjust(bottom=0.07, left=0.07, right=0.95, top=0.95)

        if save:
            if fig_name is not None:
                fig_name = fig_name
            else:
                fig_name = spectra[0].name + '_' + spectra[-1].name + '.' + fmt

            save_figure(fig_dir=fig_dir, fig_name=fig_name, fmt=fmt)
        else:
            plt.show()

    @staticmethod
    def location2str(location):
        """
        Convert a spectrum location (typically a list of length 2) into a string. Used for auto naming.
        :param location: indexes of a Spectrum into a 1-D or 2-D array
        :return: a string representing the indexes
        """
        if location is None:
            location = ''
        elif np.size(location) == 2:
            location = 'xy%d-%d' % (location[0], location[1])
        else:
            location = str(location)
        return location
