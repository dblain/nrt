"""
Basic file objects.
"""
import ast
import collections
import copy
import os
import pickle
import re
import ssl
import urllib.request
from warnings import warn

import matplotlib.image
import matplotlib.ticker
import matplotlib.pyplot as plt
import numpy as np

from .utils import auto_convert, info, join_if_between, ObjList


def save_figure(fig_dir, fig_name, fmt, width=24, height=13.5, dpi=80):
    """
    Standard way to save a figure.
    :param fig_dir: directory where to save the figure
    :param fig_name: name of the figure
    :param fmt: format and extension of the figure
    :param width: (in) width of the figure
    :param height: (in) height of the figure
    :param dpi: dots per inches of the figure
    """
    if not os.path.isdir(fig_dir):
        os.makedirs(fig_dir)

    fig = plt.gcf()
    fig.set_size_inches(width, height)
    plt.savefig(os.path.join(fig_dir, fig_name), dpi=dpi, fmt=fmt)
    print('Figure saved in file %s', os.path.join(fig_dir, fig_name))
    plt.close()


class File:
    """Superclass for all kind of files."""

    PATH = os.path.abspath(os.path.curdir)
    PATH_IN = os.path.join(PATH, 'inputs', '')
    PATH_OUT = os.path.join(PATH, 'outputs', '')
    PATH_DATA = os.path.join(PATH, 'data', '')
    PATH_BIN = os.path.join(PATH, 'bin', '')
    EXT = ''

    def __init__(self, name='', path='', ext='', file='', exists=False, find=False):
        """
        Either parameter 'file' or 'name' is required.
        :param name: the name of the file, without the extension and the directory path
        :param path: the path of the directory containing the file, equivalent of __path__ for python modules
        :param ext: the extension of the file
        :param file: the complete pathname of the file, equivalent of __file__ for python modules
        :param exists: if True, the file is considered to exists, return an error if the file does not exists
        :param find: if True and if name is not empty, try to find a file with "name" in its name. See File._find
        """
        if '.' in ext:  # remove dot
            self.ext = ext[1:]

        if file is not '':
            if os.path.isfile(file):
                self.file = file
                self.update_parameters()
            elif exists or find:
                raise FileNotFoundError(f'no such file: \'{self.file}\'')
        elif name is not '':
            self.name = name

            if path is '' or ext is '':
                if path is '':
                    self.path = self.PATH
                else:
                    self.path = path

                if ext is '':
                    self.ext = self.EXT
                else:
                    self.ext = ext

                self.update_file()
            else:
                if os.path.isdir(path):
                    self.path = path
                    self.name = name
                    self.ext = ext

                    self.update_file()
                else:
                    raise NotADirectoryError(f'no such directory: \'{path}\'')

            if find:
                self.file, self.path, self.name, self.ext = self._find(self.name, self.path, self.ext)
            elif exists and not os.path.isfile(self.file):
                raise FileNotFoundError(f'no such file: \'{self.file}\'')
        else:
            raise TypeError('__init__() missing 1 required argument: \'file\' or \'name\'')

    def _find(self, name, path='', ext=''):
        """
        Find the file in the path <path> with pattern <name> in its name.
        Raise an error if more than one file is found, or if no file is found.
        Argument "name" can simply be the name of the file, or can use regex metacharacters to create a pattern.
        :param name: string, which can use regex metacharacters, contained in the researched file name
        :param path: path of the directory containing the researched file, subdirectories will also be searched
        :param ext: extension of the file
        :return: pathname ('file'), path, name and extension ('ext') of the researched file
        """
        if not os.path.isdir(path):
            raise NotADirectoryError(f'no such directory: \'{path}\'')

        if ext is '':
            ext = self.EXT

        result = []
        for root, dirs, files in os.walk(path):
            for file in files:
                f_path, f_name, f_ext = self._split(file)
                if re.match(r'.*' + name + r'.*', f_name) and re.match(ext + r'$', f_ext):
                    result.append(os.path.join(root, file))
                    if len(result) > 1:
                        if len(result) == 2:
                            print(f'Matched file: {result[0]}')
                        print(f'Matched file: {os.path.join(root, file)}')

        if len(result) > 1:
            raise FileExistsError(f'more than one file with name \'{name}\' found in directory \'{path}\'')
        elif len(result) == 1:
            file = result[0]
            if not os.path.isfile(file):
                raise IsADirectoryError('\'{file}\' is a directory, not a file')
            split = list(File._split(file))
            split.insert(0, file)
            return split
        else:
            if ext is '':
                raise FileNotFoundError(f'no file with name \'{name}\' found in directory \'{path}\'')
            else:
                raise FileNotFoundError(f'no .{ext} file with name \'{name}\' found in directory \'{path}\'')

    def update_file(self):
        """
        Update file string from path, name and ext.
        """
        self.file = os.path.join(self.path, self.name) + '.' + self.ext

    def update_parameters(self):
        """
        Update path, name and ext strings from file.
        """
        self.path, self.name, self.ext = self._split(self.file)

    @classmethod
    def download_from(cls, url, pattern='.*', path=PATH, ext=EXT, overwrite=True):
        """
        Download files with a specific extension following a given pattern from an url.
        :param url: url of the files
        :param pattern: regex pattern in the name of the file
        :param path: path where to download the files
        :param ext: extension of the file
        :param overwrite: if True, overwrite files if they already exists
        :return:
        """
        warn(f'creating unverified context to bypass certificate requirements ! '
             f'Be sure you trust the host of your url: {url}', Warning)
        cont = input('Do you really trust this url ? (Y/N)')

        if 'Y' in cont:
            context = ssl._create_unverified_context()
        else:
            print('Trying to use standard certificate requirements...')
            context = ssl._create_default_https_context()

        if re.match(rf'.*.{ext}$', url) or not re.match(rf'.*/$', url):
            file_name = url.rsplit("/", 1)[1] + '.' + ext
            downloaded_file = cls(name=file_name.rsplit('.', 1)[0], path=path, ext=ext, exists=False)

            if overwrite:
                print(f'Downloading from {url} to {downloaded_file.file}')
                with open(downloaded_file.file, 'wb') as f:
                    f.write(urllib.request.urlopen(url, context=context).read())
            else:
                print(f'File \'{downloaded_file.file}\' already exists; skipping download')

            return downloaded_file
        else:
            source = urllib.request.urlopen(url, context=context).read()

            pattern = pattern.replace('.', '[a-zA-Z0-9_]')

            pattern = rf'({pattern}\.{ext})'
            if isinstance(source, bytes):
                pattern = bytes(pattern, 'utf-8')

            files = set(re.findall(pattern, source, re.M))
            downloaded_files = []

            for file in files:
                if isinstance(file, bytes):
                    file = str(file, 'utf-8')

                remote = url + file
                downloaded_file = cls(name=file.rsplit('.')[0], path=path, ext=ext, exists=False)

                if overwrite:
                    print(f'Downloading from {remote} to {downloaded_file.file}')
                    with open(downloaded_file.file, 'wb') as f:
                        f.write(urllib.request.urlopen(remote, context=context).read())
                else:
                    print(f'File \'{downloaded_file.file}\' already exists; skipping download')

                downloaded_files.append(downloaded_file)

            return downloaded_files

    @classmethod
    def search(cls, name, path=PATH, ext=EXT):
        """
        Search any file in the path <path> with pattern <name> in its name.
        Argument "name" can simply be the name of the file, or can use regex metacharacters to create a pattern.
        :param name: string, which can use regex metacharacters, contained in the researched file name
        :param path: path of the directory containing the researched file
        :param ext: extension of the file
        :return: a list of NRTFiles containing all the matched files
        """
        result = ObjList()
        for file in File._search(name, path=path, ext=ext):
            result.append(cls(file=file))
        return result

    @staticmethod
    def _search(name, path=PATH, ext=EXT):
        """
        Search any file in the path <path> with pattern <name> in its name.
        Argument "name" can simply be the name of the file, or can use regex metacharacters to create a pattern.
        :param name: string, which can use regex metacharacters, contained in the researched file name
        :param path: path of the directory containing the researched file
        :param ext: extension of the file
        :return: a list of string containing all the matched files
        """
        if not os.path.isdir(path):
            print(f'No such directory: \'{path}\'')
            return []

        result = []
        for root, dirs, files in os.walk(path):
            for filename in files:
                if re.match(r'.*' + name + '.*' + ext + '$', filename):
                    file = os.path.join(root, filename)
                    if os.path.isfile(file):
                        result.append(file)

        return result

    @staticmethod
    def _split(file):
        """
        Return the path, the name and the extension of a file.
        :param file: file to be split
        :return: path, name and ext of the file
        """
        path, tail = os.path.split(file)
        if '.' in tail:
            name, ext = tail.rsplit('.', 1)
        else:
            name = tail
            ext = ''
        return path, name, ext


class BibTexFile(File):
    """File containing a LaTeX bibliography."""
    EXT = 'bib'
    STANDARD_FIELD_ORDER = ['title', 'journal', 'booktitle', 'publisher', 'editor', 'institution', 'school',
                            'organization', 'howpublished', 'address', 'volume', 'number', 'series', 'edition',
                            'chapter', 'pages', 'year', 'month', 'type', 'note', 'issn', 'doi', 'url', 'author']

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def read(self):
        """
        Read the file.
        :return: dictionary containing all the references and their fields.
        """
        references = {}
        reference_label = None

        with open(self.file, 'r', encoding='utf-8') as f:
            for line in f:
                if line.strip() != '':
                    if line.strip()[0] == '@' and reference_label is None:
                        reference_type, reference_label = line.strip().split('@')[1].replace(',', '').split('{')
                        references[reference_label] = {'entry type': reference_type.lower()}
                    elif '=' in line and reference_label is not None:
                        field, value = line.split('=', 1)

                        field = field.strip().lower()
                        value = value.strip()

                        if value[-1] == ',':
                            value = value[:-1]

                        if field == 'author':
                            while value[-1] != '\"' and value[-1] != '}':
                                value += f.readline().strip()

                                if value[-1] == ',':
                                    value = value[:-1]
                                value = value.replace('\n', '')

                            value = value.replace('\"', '').replace('{', '').replace('}', '').replace('\n', '').\
                                replace('\t', '')
                            value = re.split(r'\.and | and.', value)

                        if not isinstance(value, list):
                            value = value.replace('\"', '').replace('{', '').replace('}', '').replace('\n', '').\
                                replace('\t', '')

                        references[reference_label][field] = value
                    elif line.strip() == '}':
                        reference_label = None

        return references

    def write(self, references, clean=False):
        """
        Write the given references in the file.
        The references will be classed in alphabetic order and the field in STANDARD_FIELD_ORDER's order.
        :param references: dictionary containing references to write and their fields.
        :param clean: if True, uniformise the references and the fields
        """
        if clean:
            references = self.clean(references)

        ordered_references = collections.OrderedDict(sorted(references.items(), key=lambda t: t[0]))

        with open(self.file, 'w', encoding='utf-8') as f:
            for reference in ordered_references:
                reference_dict = copy.deepcopy(ordered_references[reference])
                entry_type = reference_dict.pop('entry type')
                f.write(f'@{entry_type}' + '{' + reference.replace(' ', '') + ',\n')

                for standard_field in BibTexFile.STANDARD_FIELD_ORDER:
                    if standard_field in reference_dict:
                        f.write(standard_field + ' = {' + f'{reference_dict.pop(standard_field)}' + '},\n')

                other_fields = collections.OrderedDict(sorted(reference_dict.items(), key=lambda t: t[0]))
                for other_field in other_fields:
                    f.write(other_field + ' = {' + f'{reference_dict[other_field]}' + '},\n')

                f.write('}\n\n')

    @staticmethod
    def _check_and_clean_first_name(first_name, author, reference):
        """
        Check if the string given as first name can be a first name, then keep only the initials.
        :param first_name: string to check and clean
        :param author: full name of the author (first name + last name)
        :param reference: reference containing the author
        :return: only the initials of first_name
        """
        cleaned_first_name = []

        if not 1 <= np.size(first_name) <= 3 and first_name[3] != '':
            raise NameError(f'ambiguous author name \'{author}\' '
                            f'in reference \'{reference}\': too much or too few initials (first name: {first_name})')
        else:
            if len(first_name) > 2 and ('.' not in first_name and '~' not in first_name) and \
                    isinstance(first_name, str):
                first_name = first_name.strip()
                initials = []

                if '-' in first_name:
                    split_first_name = first_name.split('-')
                    for first_name_ in split_first_name:
                        initials.append(first_name_[0])

                    initials = '.-'.join(initials).upper()
                else:
                    initials = first_name[0]

                cleaned_first_name.append(initials + '.')
            else:
                for initial in first_name:
                    initial = initial.strip()

                    if (len(initial) > 2 and ('-' not in initial and '~' not in initial)) or len(initial) > 3:
                        raise NameError(f'ambiguous author name \'{author}\' '
                                        f'in reference \'{reference}\': non-initials in first name '
                                        f'(initial: {initial})')
                    elif len(initial.strip()) > 0:
                        i = initial.replace('~', '') + '.'
                        i = i.strip()
                        cleaned_first_name.append(i)

        cleaned_first_name = '~'.join(cleaned_first_name)
        return cleaned_first_name.strip()

    @staticmethod
    def clean(references):
        """
        Clean the references so that reference label is always AuthorYear, and only keep the initials of author's first
        name.
        :param references: references to clean
        :return: cleaned references
        """
        cleaned_references = {}

        for reference in references:
            if 'author' in references[reference]:
                authors = []

                for author in references[reference]['author']:
                    if ',' in author and '.' in author:
                        names = author.split(',', 1)
                        last_name = 'unknown'
                        first_name = 'unknown'

                        for name in names:
                            name = name.strip()

                            if '.' in name:
                                first_name = name.split('.')
                                first_name = BibTexFile._check_and_clean_first_name(first_name, author, reference)
                            else:
                                last_name = name
                    elif '.' in author:
                        first_name, last_name = author.rsplit('.', 1)
                        first_name = first_name.split('.')
                        first_name = BibTexFile._check_and_clean_first_name(first_name, author, reference)
                    elif ',' in author:
                        last_name, first_name = author.split(',', 1)
                        first_name = BibTexFile._check_and_clean_first_name(first_name, author, reference)
                    else:
                        raise NameError(f'ambiguous author name \'{author}\' in reference \'{reference}\', '
                                        f'author list: {references[reference]["author"]}')

                    authors.append(', '.join([last_name.strip(), first_name.strip()]))

                if 'year' in references[reference]:
                    cleaned_reference = authors[0].split(',')[0] + references[reference]['year']
                    cleaned_references[cleaned_reference] = copy.deepcopy(references[reference])
                else:
                    cleaned_reference = copy.deepcopy(reference)
                    cleaned_references[cleaned_reference] = copy.deepcopy(references[reference])
                    warn(f'unable to find field \'year\' for reference {reference}', Warning)

                cleaned_references[cleaned_reference]['author'] = ' and '.join(authors)
            else:
                cleaned_reference = copy.deepcopy(reference)

            for field in cleaned_references[cleaned_reference]:
                cleaned_references[cleaned_reference][field] = \
                    cleaned_references[cleaned_reference][field].replace('\\textquoteright', '`')

        return cleaned_references


class CellSheetFile(File):
    """File containing a Cell Sheet."""
    EXT = 'csv'

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def read(self):
        """
        Read a cell sheet file with a header.
        :returns dictionary containing the csv data
        """
        with open(self.file, 'r') as f:
            header = f.readline().split(',')
            data = []
            for line in f:
                line = line.strip()
                columns = join_if_between(line.split(','), '"')
                data.append(np.empty(np.size(header), dtype=object))

                for i in range(len(columns)):
                    data[-1][i] = auto_convert(columns[i])

        data = np.asarray(data)
        dictionary = {}
        for i, attr in enumerate(header):
            dictionary[attr] = data[:, i]

        return dictionary

    def write(self, object_list, option='w'):
        """
        Write a cell sheet file.
        """
        with open(self.file, option) as f:
            for key in object_list[0].__dict__:
                f.write(f'{key} ')

            for o in object_list:
                f.write('\n')
                for attr in o.__dict__:
                    w = getattr(o, attr)
                    if w == '':
                        w = '-'
                    f.write(f'{w} ')

    @staticmethod
    def _header_to_dict(header, data):
        """

        :param header:
        :param data:
        :return:
        """
        dictionary = {}
        for i, attr in enumerate(header):
            dictionary[attr] = data[i]
        return dictionary


class DataFile(File):
    """File containing ASCII-formatted data with a header and organised in columns."""
    EXT = 'dat'

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def read(self, return_header=False, set_parameters=True):
        """
        Read a data file with a header and n data columns.
        :param return_header: header of the file
        :param set_parameters: set the parameters read
        :returns data, [header]: the data columns and the header
        """
        with open(self.file, 'r') as f:
            header = f.readline()
            data = [np.array([])]
            for line in f:
                line = line.strip()
                columns = line.split()

                for j in range(len(columns)):
                    if len(data) < j + 1:
                        data.append(np.array([]))

                    data[j] = np.append(data[j], [float(columns[j])])

        if len(header) > 36 and set_parameters:
            try:
                parameters = ast.literal_eval(str(header[36:]))
                for parameter in parameters:
                    if ObjList.hassubattr(self, parameter):
                        ObjList.setsubattr(self, parameter, parameters[parameter])
                    else:
                        warn(f'type object \'{type(self).__name__}\' has no subattribute \'{parameter}\'', Warning)
            except ValueError:
                warn(f'parameters found in file \'{self.file}\', but not in an exploitable format', Warning)
                print(str(header[36:]))
            except SyntaxError:
                info(f'ignoring header of file \'{self.file}\' from column 36: no valid syntax detected')

        if return_header:
            return data, header
        else:
            return data

    def write(self, data, title='', parameters=None):
        """
        Write a data file with a header containing a title, the number of elements and comments, and n columns of data.
        :param data: list of data columns
        :param title: description of the data
        :param parameters: extra parameters
        """
        if parameters is None:
            parameters = ''

        if not hasattr(data, '__iter__'):
            data = [data]

        if data[0].force_exp_notation:
            exp = ['E']
        else:
            exp = ['']

        if len(data) > 1:
            for column in data[1:]:
                if not hasattr(column.data, '__iter__'):
                    column.data = [column.data]

                if not hasattr(data[0].data, '__iter__'):
                    data[0].data = [data[0].data]

                if len(column.data) != len(data[0].data):
                    raise ValueError(f'columns must have the same length, '
                                     f'but have lengths {len(data[0].data)} and {len(column.data)}')
                if column.force_exp_notation:
                    exp.append('E')
                else:
                    exp.append('')

        with open(self.file, 'w') as f:
            f.write(f' {title:24.24} {len(data[0].data):9} {parameters}'.rstrip())
            for i in range(len(data[0].data)):
                f.write(f'\n {data[0].data[i]:{data[0].width}.{data[0].precision}{exp[0]}}')
                for j, column in enumerate(data[1:]):
                    f.write(f' {column.data[i]:{column.width}.{column.precision}{exp[j+1]}}')

    class DataColumn:
        """Used in write_data."""

        def __init__(self, data, width=0, precision=12, force_exp_notation=False):
            """
            :param data: data to write
            :param width: format width
            :param precision: format precision
            :param force_exp_notation: force exponential notation
            """
            self.data = data
            self.width = width
            self.precision = precision
            self.force_exp_notation = force_exp_notation


class Figure(File):
    """Figure file."""
    PATH = os.path.join(File.PATH, 'figures', '')
    EXT = 'png'
    DEFAULT_SIZE = 12

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def show(self):
        """Show the figure."""
        img = matplotlib.image.imread(self.file)
        plt.figure()
        plt.imshow(img)

    @staticmethod
    def _points2inches(points):
        """
        Convert a length in points units to inches units.
        :param points: (pt) length to convert
        :return: (in) converted length
        """
        return points / 72.27

    @staticmethod
    def get_figsize_from_width(width=452, aspect=16 / 9):
        """
        Get standard size of a figure.
        :param width: (pt) width of the figure; default value is LaTeX \textwidth for a two columns A4 10pt article
        :param aspect: width to height ratio of the figure; default value is default screen aspect ratio
        :return: size of the figure
        """
        width = Figure._points2inches(width)
        height = width / aspect

        return width, height

    @staticmethod
    def get_figsize_from_resolution(resolution=1080, dpi=300, aspect=16 / 9):
        """
        Get standard size of a figure.
        :param resolution: (px) resolution of the figure; default is FHD.
        :param dpi: dot per inches of the figure; default value is what is generally recommended for color figures
        :param aspect: width to height ratio of the figure; default value is default screen aspect ratio
        :return: size of the figure
        """

        return (resolution * aspect) / dpi, resolution / dpi

    @staticmethod
    def plot_nice_colorbar(sst, vmin=None, vmax=None, norm=None,
                           size=DEFAULT_SIZE, cbar_label='', power_min=-3, power_max=3):
        cbar_format = matplotlib.ticker.ScalarFormatter(useMathText=True, useOffset=True)
        cbar_format.set_powerlimits((power_min, power_max))

        if vmin is None or vmax is None:
            cbar = plt.colorbar(sst, format=cbar_format, ticks=matplotlib.ticker.MaxNLocator(), fraction=0.05)
        elif vmin < 0.03 * vmax and norm is not None:
            pow_min = int(np.floor(np.log10(vmin))) + 1
            pow_max = int(np.floor(np.log10(vmax)))

            sub_pow_min = np.int(np.ceil(vmin / np.exp(int(np.floor(np.log10(vmin))) * np.log(10))))
            sub_pow_max = np.int(np.floor(vmax / np.exp(int(np.floor(np.log10(vmax))) * np.log(10))))
            ticks = [sub_pow_min * np.exp((pow_min - 1) * np.log(10))] + list(
                np.exp((np.asarray(list(range(np.abs(pow_max - pow_min) + 1))) + pow_min) * np.log(10))) + [
                        sub_pow_max * np.exp(pow_max * np.log(10))]

            minorticks = []
            for pow_range in range(np.abs(pow_max - pow_min) + 2):
                if pow_range == 0:
                    sub_pow_min = np.int(np.floor(vmin / np.exp(int(np.floor(np.log10(vmin))) * np.log(10)))) + 1
                    sub_pow_range = 10 - sub_pow_min
                elif pow_range == np.abs(pow_max - pow_min) + 1:
                    sub_pow_min = 2
                    sub_pow_max = np.int(np.floor(vmax / np.exp(int(np.floor(np.log10(vmax))) * np.log(10))))
                    sub_pow_range = sub_pow_max - 1
                else:
                    sub_pow_min = 2
                    sub_pow_range = 8

                for minor in range(sub_pow_range):
                    minorticks.append((minor + sub_pow_min) * np.exp((pow_range + pow_min - 1) * np.log(10)))

            minorticks = sst.norm(minorticks)
            cbar = plt.colorbar(sst, format=cbar_format, ticks=ticks, fraction=0.025)
            cbar.ax.yaxis.set_ticks(minorticks, minor=True)
        else:
            cbar = plt.colorbar(sst, format=cbar_format, ticks=matplotlib.ticker.MaxNLocator(), fraction=0.05)

        cbar.ax.tick_params(labelsize=size)
        cbar.ax.yaxis.offsetText.set(size=size)
        cbar.set_label(cbar_label, size=size)

    @staticmethod
    def resize_labels(ax, size=DEFAULT_SIZE):
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(12 * size)

    @staticmethod
    def save_fig(directory='', name='', prefix='', main='', suffix='', dpi=300, fmt=EXT):
        if directory == '':
            directory = os.path.join(Figure.PATH, 'divers', prefix + 's')

        if name == '' and isinstance(main, str):
            if prefix != '':
                name = prefix + '_'

            name += main.replace('.', '_').replace('[', '-').replace(']', '')
        elif name == '':
            name = prefix + '_'

        if suffix != '':
            if name[-1] == '_':
                name += suffix
            else:
                name += '_' + suffix

        if not os.path.isdir(directory):
            os.makedirs(directory)

        plt.savefig(os.path.join(directory, name + '.' + fmt), dpi=dpi, format=fmt)
        print(f'Figure saved in \'{os.path.join(directory, name + "." + fmt)}\'')


class FortranUnformattedFile(File):
    """Fortran unformatted binary file."""
    def __init__(self, name='', path='', ext='', file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def read(self, data_type=np.float32, count=-1, sep=''):
        """
        Read a Fortran unformatted file. See numpy.fromfile for more details.
        :param data_type: data type of the returned array
        :param count: number of items to read. -1 means all items (i.e., the complete file)
        :param sep: separator between items if file is a text file
        :return: a 1-D data array
        """
        return np.fromfile(self.file, dtype=data_type, count=count, sep=sep)


class NameListFile(File):
    """File containing a Fortran NameList."""
    EXT = 'nml'

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def read(self):
        """
        Read a Fortran namelist file.
        :return: dictionary of dictionaries containing the variable names and their values as NMLLines
        """
        name_lists = {}
        namelist_name = None

        with open(self.file, 'r') as f:
            for line in f:
                if line.strip() != '':
                    if line.strip()[0] == '&' and namelist_name is None:
                        namelist_name = line.strip().split('&')[1]
                        name_lists[namelist_name] = {}
                    elif '=' in line and namelist_name is not None:
                        name, nml_line = line.split('=', 1)

                        if '!' in nml_line:
                            nml_line, comment = nml_line.rsplit('!', 1)
                        else:
                            comment = ''

                        name = name.strip()
                        nml_line = nml_line.strip()
                        comment = comment.strip()

                        if '\'' not in nml_line and '\"' not in nml_line:
                            nml_line = list(nml_line.split(','))
                            for i, value in enumerate(nml_line):
                                value = value.strip()
                                try:
                                    if value == 'True':
                                        nml_line[i] = True
                                        continue
                                    elif value == 'False':
                                        nml_line[i] = False
                                        continue
                                    elif '.' not in value:
                                        try:
                                            nml_line[i] = int(value)
                                            continue
                                        except ValueError:
                                            pass
                                    nml_line[i] = float(value)
                                except ValueError:
                                    pass
                        else:
                            nml_line = list(nml_line.replace('\'', '').replace('\"', '').replace(' ', '').split(','))

                        name_lists[namelist_name][name] = self.NameListLine(value=nml_line, comment=comment)
                    elif line.strip() == '/':
                        namelist_name = None
        return name_lists

    def write(self, name_lists):
        """
        Write a Fortran namelist.
        Parameter 'name_lists' must be a dictionary of dictionaries containing the variable names as keys and their
        values. The values can be anything but using a NameListLine offer the possibility to write a comment.
        Example:
            Input:
                file = 'path/to/file.nml'
                nml = {'LIST1': {'var1': 2.3, 'array1': NameListLine([1, 2, 3]), 'array2': [4, 5, 6, 7.2]},
                       'LIST2': {'array3': NameListLine(['a', 'b'], comment='this is a comment')}}
                write_nml(file, nml)
            Output (in 'file'):
                &LIST1
                    var1 = 2.3
                    array1 = 1, 2, 3
                    array2 = 4, 5, 6, 7.2
                \

                &LIST2
                    array3 = 'a', 'b' ! this is a comment
                \
        :param name_lists: dictionary of dictionaries containing the variable names and their values
        :type name_lists: dict
        """
        tab = ''.rjust(4)

        with open(self.file, 'w') as f:
            for namelist_name in name_lists:
                f.write(f'&{namelist_name}\n')
                namelist = name_lists[namelist_name]

                if isinstance(namelist, dict):
                    for name in namelist:
                        if isinstance(namelist[name], self.NameListLine):
                            if not isinstance(namelist[name].value, list):
                                namelist[name].value = [namelist[name].value]

                            f.write(f'{tab}{name} = {str(namelist[name].value)[1:-1]}')

                            if namelist[name].comment != '':
                                f.write(f' ! {namelist[name].comment}')
                        else:
                            if not isinstance(namelist[name], list):
                                namelist[name] = [namelist[name]]

                            f.write(f'{tab}{name} = {str(namelist[name])[1:-1]}')
                        f.write('\n')
                else:
                    raise TypeError(f'namelist \'{namelist}\' is not a Dictionary')
                f.write('/\n\n')

    class NameListLine:
        def __init__(self, value, comment=''):
            """
            Used in write_nml.
            :param value: value of the variable to be written in the namelist.
            :param comment: comment about the variable to be written in the namelist.
            """
            self.value = value
            self.comment = comment


class PDS3LabelFile(File):
    """File using Nasa's PDS3 (Planetary Data Science) formatting."""
    EXT = 'LBL'

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def read(self):
        """
        Read a PDS3-formatted file.
        :return: dictionary containing the variables name and their value
        """
        dict_labels = {}

        with open(self.file, 'r') as f:
            for line in f:
                if line.strip() != '':
                    if '=' in line:
                        key, value = line.split('=', 1)
                        key = key.strip()
                        key = key.lower()
                        value = value.split('<', 1)[0].strip()

                        if value.replace('\"', '') == '':
                            next_line = f.readline().strip()
                            if next_line != '':
                                if '{' in next_line and '}' not in next_line:
                                    value = []
                                    while '}' not in next_line:
                                        value.append(
                                            next_line.rsplit(',', 1)[0].strip().replace('{', '').replace('}', ''))
                                        next_line = f.readline()
                                elif next_line.count('\"') == 1:
                                    value += next_line
                                    while '\"' not in next_line.strip()[-1]:
                                        next_line = f.readline().strip()
                                        value = ' '.join([value, next_line])

                                    value.strip()
                                else:
                                    value = next_line
                        elif '(' in value and ')' not in value:
                            value = [value.split(',', 1)[0].split('(', 1)[-1]]
                            next_line = f.readline()
                            while ')' not in next_line:
                                value.append(next_line.rsplit(',', 1)[0].split('<')[0].strip())
                                next_line = f.readline()

                            value.append(next_line.rsplit(',', 1)[0].split('<')[0].split(')', 1)[0].strip())
                        elif key == 'object' or key == 'end_object':
                            continue

                        if np.size(value) < 2:
                            value = [value]

                        for i, val in enumerate(value):
                            try:
                                if val.replace('\"', '').strip() == 'True':
                                    value[i] = True
                                    continue
                                elif val.replace('\"', '').strip() == 'False':
                                    value[i] = False
                                    continue
                                elif val.replace('\"', '').strip() == 'N/A':
                                    value[i] = None
                                    continue
                                elif '.' not in val:
                                    try:
                                        value[i] = int(val)
                                        continue
                                    except ValueError:
                                        pass

                                value[i] = float(val)
                            except ValueError:
                                value[i] = val.replace('\"', '').strip()

                        if np.size(value) < 2:
                            value = value[0]

                        dict_labels[key] = value

        return dict_labels


class PickleFile(File):
    """File containing Pickle-formatted data."""
    EXT = 'pkl'

    def __init__(self, name='', path='', ext=EXT, file='', exists=False, find=False):
        super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

    def load(self):
        """Load the file."""
        with open(self.file, 'rb') as file:
            tmp_obj = pickle.load(file)
        self.__dict__.update(tmp_obj.__dict__)

    def save(self):
        """Save the file."""
        with open(self.file, 'wb') as file:
            pickle.dump(self, file, -1)

    @classmethod
    def loader(cls, file):
        """File loader. The file does not need to be initialised."""
        with open(file, 'rb') as f:
            return pickle.load(f)
