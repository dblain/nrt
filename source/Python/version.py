"""
Definition of the version number.
"""
__version__ = '7.0.0'


def minor_version():
    return __version__.rsplit('.', 1)[0] + '.x'


def major_version():
    return __version__.rsplit('.', 2)[0] + '.x.x'


def compact_version():
    return __version__.replace('.', '')
