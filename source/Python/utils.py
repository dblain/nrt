"""
Useful functions and objects.
"""
import re
import sys

import numpy as np


def auto_convert(string):
    """
    Try to convert a string into the most adapted format (None, boolean, int,  float). If it fails, return the given
    string.
    :param string: string to convert to None, boolean, in or float
    :return: converted string or original string
    """
    for conversion_method in (boolify, int, float):
        try:
            return conversion_method(string)
        except (ValueError, TypeError):
            pass
    return string


def boolify(string):
    """
    Convert a string into a None or boolean.
    Return an error if the string is not recognized as a boolean.
    :param string: string to convert
    :return: None or boolean
    """
    if string == '' or re.match(r'[iI]nf$', string) or re.match(r'[nN]an$', string) or re.match(r'[nN]one$', string):
        return None
    if re.match(r'[tT]rue$', string) or re.match(r'[yY]es$', string) or re.match(r'T$', string):
        return True
    if re.match(r'[fF]alse$', string) or re.match(r'[nN]o$', string) or re.match(r'F$', string):
        return False
    raise ValueError(f'string \'{string}\' could not be converted into a boolean')


def info(*args, **kwargs):
    """
    Displays informations in the console stderr channel.
    """
    print('Info:', *args, file=sys.stderr, **kwargs)


def join_if_between(list_of_string, unifier_begin, unifier_end=None, keep_unifiers=False):
    """
    Join the strings in a list of string between a given unifier.
    Example:
    >>> join_if_between(['pineapple', '"foo', 'bar', 'spam', 'egg"', 'sausages'], '"')
        ['pineapple', ['foo', 'bar', 'spam', 'egg'], 'sausages']
    >>> join_if_between(['vector = ', '(4', '2', '1', '3)'], '\(', '\)')
        ['vector = ', ['4', '2', '1', '3']]
    :param list_of_string: list of string
    :param unifier_begin: first unifier (i.e. '\[')
    :param unifier_end: last unifier (i.e. '\]'); if None, is took as the same as unifier_begin (i.e. '"')
    :param keep_unifiers: if True, keep the unifiers in the result
    :return: list of list of string and string
    """

    if unifier_end is None:
        unifier_end = unifier_begin

    unifiers_begin = [string for string in list_of_string if re.match(rf'^{unifier_begin}.*', string)]
    unifiers_end = [string for string in list_of_string if re.match(rf'.*{unifier_end}$', string)]

    if len(unifiers_begin) == len(unifiers_end):
        for i in range(len(unifiers_begin)):
            index = list_of_string.index(unifiers_begin[i])
            list_of_string[index:list_of_string.index(unifiers_end[i]) + 1] = \
                [list_of_string[index:list_of_string.index(unifiers_end[i]) + 1]]

            if not keep_unifiers:
                list_of_string[index][0] = list_of_string[index][0].replace(unifier_begin.replace('\\', ''), '').strip()
                list_of_string[index][-1] = list_of_string[index][-1].replace(unifier_end.replace('\\', ''), '').strip()

            for j in range(len(list_of_string[index])):
                list_of_string[index][j] = list_of_string[index][j].strip()

    return list_of_string


def linarange(start, step, num):
    """
    Return evenly spaced numbers from a specified values.
    Returns num evenly spaced samples, calculated from start and spaced by step.
    :param start: the starting value of the sequence
    :param step: spacing between values
    :param num: number of samples to generate
    :return: num evenly spaced samples, calculated from start and spaced by step
    """
    return np.arange(num) * step + start


def stack_arrays(arrays, axis=0, fill_value=np.nan):
    """
    Stack maps along the specified axis.
    If the maps does not have the same shape, rows and columns of fill_value will be added.
    Useful to stack maps to be projected using basemap.
    :param arrays: list of nD arrays to stack
    :param axis: axis used for stacking
    :param fill_value: fill value of the stacked array
    :return: nD array containing the stacked maps
    """
    shapes = []
    for m in arrays:
        shapes.append(np.shape(m))

    for i, m in enumerate(arrays):
        shape_array_to_stack = list(np.shape(arrays[i]))

        for dim in range(np.size(shape_array_to_stack)):
            shape_array_to_stack = list(np.shape(arrays[i]))
            shape_array_to_stack[dim] = np.max(shapes, axis=0)[dim] - shape_array_to_stack[int(dim)]

            if shape_array_to_stack[dim] > 0:
                arrays[i] = np.concatenate((arrays[i], np.full(shape_array_to_stack, fill_value)), axis=dim)

    stacked_maps = np.concatenate(arrays, axis=axis)
    stacked_maps = np.ma.masked_invalid(stacked_maps)
    stacked_maps = np.ma.masked_values(stacked_maps, fill_value)

    return np.ma.array(stacked_maps)


class ObjList(list):
    """A list to have an easy access to objects attributes and methods."""

    def call(self, method, *args, **kwargs):
        """
        Call the given method name with its arguments for all the objects in the ObjList.
        :param method: a method of an object inside the ObjList to call
        :param args: arguments of the method
        :param kwargs: keyword arguments of the method
        :return: list of the results given by the method
        """
        return [obj.__getattribute__(method)(*args, **kwargs) for obj in self]

    def get(self, attribute):
        """
        Return the list of the values of the given attribute name of all the objects in the ObjList.
        :param attribute: name of an attribute of an object inside the ObjList
        :return: list of the values of the attribute
        """
        return [obj.__getattribute__(attribute) for obj in self]

    def getsubattr_list(self, attribute):
        """
        Return the list of the values of the given subattribute name of all the objects in the ObjList.
        :param attribute: name of a subattribute of an object inside the ObjList
        :return: list of the values of the subattribute
        """
        return [ObjList.getsubattr(obj, attribute) for obj in self]

    def set(self, attribute, value):
        """
        Change the value of the given attribute name of all the objects in the ObjList.
        :param attribute: name of a subattribute of an object inside the ObjList
        :param value: new value of the subattribute
        """
        if hasattr(value, '__iter__'):
            if len(value) == len(self):
                for i, obj in enumerate(self):
                    obj.__setattr__(attribute, value[i])
        else:
            for obj in self:
                obj.__setattr__(attribute, value)

    @staticmethod
    def getsubattr(obj, attribute):
        """
        Same that the built-in function getattr(), but works for any nested object inside obj.
        For example, getsubattr(x, 'y.z[3].aa[dict_key].ab.foobar') is equivalent to x.y.z[3].aa[dict_key].ab.foobar.
        :param obj: any object
        :param attribute: any attribute or attribute of a nested object
        :return: the attribute value
        """
        current_obj = obj
        attributes = attribute.split('.')
        for attr in attributes:
            if '[' in attr and ']' in attr:
                attr, attr_index = attr.split('[')
                try:
                    attr_index = int(attr_index[:-1])
                except ValueError:
                    attr_index = attr_index[:-1]

                if current_obj is not None:
                    current_obj = getattr(current_obj, attr)[attr_index]
            else:
                if current_obj is not None:
                    current_obj = getattr(current_obj, attr)
        return current_obj

    @staticmethod
    def hassubattr(obj, name_with_dots):
        """
        Same that the built-in function hasattr(), but works for any nested object inside obj.
        :param obj: any object
        :param name_with_dots: any attribute or attribute of a nested object
        :return: True if the attribute exists, False else
        """
        try:
            ObjList.getsubattr(obj, name_with_dots)
        except AttributeError:
            return False
        return True

    @staticmethod
    def setsubattr(obj, name_with_dots, value):
        """
        Same that the built-in function setattr(), but works for any nested object inside obj.
        :param obj: any object
        :param name_with_dots: any attribute or attribute of a nested object
        :param value: value of the attribute or attribute of a nested object
        """
        attributes = name_with_dots.rsplit('.', 1)
        if len(attributes) == 2:
            setattr(ObjList.getsubattr(obj, attributes[0]), attributes[-1], value)
        elif len(attributes) == 1:
            setattr(obj, attributes[0], value)
        else:
            raise RuntimeError('this should not be possible')
