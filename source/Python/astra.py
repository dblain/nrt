"""
Astra objects.
"""
import re

import numpy as np

from scipy import constants as cst
from .utils import ObjList
from .files import CellSheetFile

M_sol = 1.9884E30  # (kg) mass of the Sun (USNO 2014 Astronomical Constants)
R_sol = 6.957E8  # (m) radius of the Sun (IAU 2015, Archinal et al. 2018)
T_sol = 5772  # (K) effective temperature of the Sun (IAU 2015, Archinal et al. 2018)
L_sol = 3.828E26  # (W) nominal solar luminosity (IAU 2015)

M_jup = 1.898187E27  # (kg) mass of Jupiter (https://ssd.jpl.nasa.gov/?planet_phys_par, 12/2018)
R_jup = 6.9911E7  # (m) radius of Jupiter (IAU 2015, Archinal et al. 2018)

M_earth = 5.97237E24  # (kg) mass of Earth (https://ssd.jpl.nasa.gov/?planet_phys_par, 12/2018)
R_earth = 6.3710084E6  # (km) radius of Earth (IAU 2015, Archinal et al. 2018)

M_Bol = 3.0128E28  # (W) absolute bolometric magnitude (IAU 2015, resolution B2)


class Star:
    def __init__(self, name='', mass=0.0, radius=0.0, effective_temperature=0.0, luminosity=0.0,
                 spectral_type='', luminosity_type='', metallicity=0.0, spectrum=None,
                 magnitude_v=0.0, magnitude_i=0.0, magnitude_j=0.0, magnitude_h=0.0, magnitude_k=0.0, age=0.0,
                 right_ascension=0.0, declination=0.0, distance=0.0,
                 has_disc=False, has_magnetic_field=False, alternate_names='', planets=None,
                 mass_err=(0.0, 0.0), radius_err=(0.0, 0.0), effective_temperature_err=(0.0, 0.0),
                 age_err=(0.0, 0.0), distance_err=(0.0, 0.0), metallicity_err=(0.0, 0.0)):
        """
        Star object.
        :param name: name of the star
        :param mass: (kg) star mass
        :param radius: (m) star radius
        :param effective_temperature: effective stellar temperature
        :param luminosity: (W) Luminosity of the star
        :param spectral_type: stellar spectral type (M, K, G, F, A, B, O)
        :param luminosity_type: stellar luminosity type (I, II, III, IV, V, ...)
        :param metallicity: decimal logarithm of the massive elements ("metals") to hydrogen ratio in solar units
        :param spectrum: Spectrum of the star
        :param magnitude_v: apparent magnitude in the V band
        :param magnitude_i: apparent magnitude in the I band
        :param magnitude_j: apparent magnitude in the J band
        :param magnitude_h: apparent magnitude in the H band
        :param magnitude_k: apparent magnitude in the K band
        :param age: (s) stellar age
        :param right_ascension: (hh:mm:ss) right_ascension of the star
        :param declination: (hh:mm:ss) declination of the star
        :param distance: distance of the star to the observer
        :param has_disc: (direct imaging or IR excess) disc detected
        :param has_magnetic_field: stellar magnetic field detected
        :param alternate_names: alternate names of the star
        :param planets: planets in stellar system
        :param mass_err: mass error min and max
        :param radius_err: radius error min and max
        :param effective_temperature_err: temperature error min and max
        :param age_err: age error min and max
        :param distance_err: distance error min and max
        :param metallicity_err: metallicity error min and max
        """
        self.age = age
        self.age_err = age_err
        self.alternate_names = alternate_names
        self.declination = declination
        self.distance = distance
        self.distance_err = distance_err
        self.effective_temperature = effective_temperature
        self.effective_temperature_err = effective_temperature_err
        self.has_disc = has_disc
        self.has_magnetic_field = has_magnetic_field
        self.luminosity = luminosity
        self.luminosity_type = luminosity_type
        self.magnitude_h = magnitude_h
        self.magnitude_i = magnitude_i
        self.magnitude_j = magnitude_j
        self.magnitude_k = magnitude_k
        self.magnitude_v = magnitude_v
        self.mass = mass
        self.mass_err = mass_err
        self.metallicity = metallicity
        self.metallicity_err = metallicity_err
        self.name = name
        self.radius = radius
        self.radius_err = radius_err
        self.right_ascension = right_ascension
        self.spectral_type = spectral_type
        self.spectrum = spectrum

        if planets is None:
            planets = ObjList([])

        self.planets = planets

    @classmethod
    def from_exoplanet_eu_csv(cls, csv_file):
        """
        Initialise Stars from an exoplanet.eu Cell Sheet File.
        :param csv_file: exoplanet.eu cell sheet file
        :type csv_file: CellSheetFile
        """
        regex_split = '([A-HJ-UW-Z][A-HJ-UW-Z]?[0-9]?([.][0-9][0-9]?)?)'  # identify spectral type

        data_dict = csv_file.read()
        stars = []

        for i in range(len(data_dict['# name'])):
            if data_dict['star_sp_type'][i] is not None and data_dict['star_sp_type'][i] is not False and \
                    np.size(re.split(regex_split, data_dict['star_sp_type'][i])) == 4:
                spectral_type = re.split(regex_split, data_dict['star_sp_type'][i])[1].strip()

                if spectral_type == '':
                    spectral_type = 'V'

                luminosity_type = re.split(regex_split, data_dict['star_sp_type'][i])[-1].strip()
            else:
                spectral_type = data_dict['star_sp_type'][i]
                luminosity_type = None

            stars.append(
                cls(
                    name=data_dict['star_name'][i],
                    right_ascension=float(np.asarray(data_dict['ra'][i], dtype=float)),
                    declination=float(np.asarray(data_dict['dec'][i], dtype=float)),
                    magnitude_v=float(np.asarray(data_dict['mag_v'][i], dtype=float)),
                    magnitude_i=float(np.asarray(data_dict['mag_i'][i], dtype=float)),
                    magnitude_j=float(np.asarray(data_dict['mag_j'][i], dtype=float)),
                    magnitude_h=float(np.asarray(data_dict['mag_h'][i], dtype=float)),
                    magnitude_k=float(np.asarray(data_dict['mag_k'][i], dtype=float)),
                    distance=float(np.asarray(data_dict['star_distance'][i], dtype=float)) * cst.parsec,
                    distance_err=(
                        float(np.asarray(data_dict['star_distance_error_min'][i], dtype=float)) * cst.parsec,
                        float(np.asarray(data_dict['star_distance_error_min'][i], dtype=float)) * cst.parsec),
                    metallicity=float(np.asarray(data_dict['star_metallicity'][i], dtype=float)),
                    metallicity_err=(float(np.asarray(data_dict['star_metallicity_error_min'][i], dtype=float)),
                                     float(np.asarray(data_dict['star_metallicity_error_max'][i], dtype=float))),
                    mass=float(np.asarray(data_dict['star_mass'][i], dtype=float)) * M_sol,
                    mass_err=(
                        float(np.asarray(data_dict['star_mass_error_min'][i], dtype=float)) * M_sol,
                        float(np.asarray(data_dict['star_mass_error_max'][i], dtype=float)) * M_sol),
                    radius=float(np.asarray(data_dict['star_radius'][i], dtype=float)) * R_sol,
                    radius_err=(float(np.asarray(data_dict['star_radius_error_min'][i], dtype=float)) * R_sol,
                                float(np.asarray(data_dict['star_radius_error_max'][i], dtype=float)) * R_sol),
                    spectral_type=spectral_type,
                    luminosity_type=luminosity_type,
                    age=float(np.asarray(data_dict['star_age'][i], dtype=float)) * cst.year * 1E9,
                    age_err=(float(np.asarray(data_dict['star_age_error_min'][i], dtype=float)) * cst.year * 1E9,
                             float(np.asarray(data_dict['star_age_error_max'][i], dtype=float)) * cst.year * 1E9),
                    effective_temperature=float(np.asarray(data_dict['star_teff'][i], dtype=float)),
                    effective_temperature_err=(float(np.asarray(data_dict['star_teff_error_min'][i], dtype=float)),
                                               float(np.asarray(data_dict['star_teff_error_max'][i], dtype=float))),
                    has_disc=data_dict['star_detected_disc'][i],
                    has_magnetic_field=data_dict['star_magnetic_field'][i]
                )
            )

        return stars


class Planet:
    def __init__(self, name='', mass=0.0, radius=0.0, equatorial_radius=0.0, polar_radius=0.0,
                 semi_major_axis=0.0, eccentricity=0.0, inclination=0.0,
                 sideral_orbit_period=0.0, sideral_rotation_period=0.0,
                 surface_temperature=0.0, geometric_albedo=0.0, surface_gravity=0.0,
                 mass_err=tuple(), radius_err=tuple(), equatorial_radius_err=tuple(), polar_radius_err=tuple(),
                 semi_major_axis_err=tuple(), eccentricity_err=tuple(), inclination_err=tuple(),
                 sideral_orbit_period_err=tuple(), sideral_rotation_period_err=tuple(),
                 geometric_albedo_err=tuple(),
                 atmosphere=None, discovery_parameters=None):
        """
        Planet object.
        :param name: name of the planet
        :param mass: (kg) mass of the planet
        :param radius: (m) radius of the planet
        :param equatorial_radius: (m) equatorial radius of the planet
        :param polar_radius: (m) polar radius of the planet
        :param sideral_orbit_period: (s) orbital period of the planet
        :param sideral_rotation_period: (s) sideral rotation period of the planet
        :param semi_major_axis: (m) semi-major axis of the planet orbit
        :param eccentricity: eccentricity of the planet orbit from 0, circular orbit, to almost 1, very elongated orbit
        :param inclination: (deg) inclination of planet orbit, angle between the planet orbit and the sky plane
        :param surface_temperature: (K) planetary equilibrium temperature
        :param geometric_albedo: geometric albedo
        :param surface_gravity: (m.s-2) surface gravity
        :param mass_err: (kg) mass of the planet error min and max
        :param radius_err: (m) radius error min and max
        :param sideral_orbit_period_err: (s) orbital period of the planet error min and max
        :param semi_major_axis_err: (m) semi major axis of the planet error min and max
        :param sideral_rotation_period_err: (s) sideral rotation period of the planet erro min and max
        :param eccentricity_err: eccentricity of the planet error min and max
        :param inclination_err: (deg) inclination of the planet error min and max
        :param geometric_albedo_err: geometric albedo error min and max
        :param discovery_parameters:
        """
        self.atmosphere = atmosphere
        self.eccentricity = eccentricity
        self.eccentricity_err = eccentricity_err
        self.equatorial_radius = equatorial_radius
        self.equatorial_radius_err = equatorial_radius_err
        self.geometric_albedo = geometric_albedo
        self.geometric_albedo_err = geometric_albedo_err
        self.inclination = inclination
        self.inclination_err = inclination_err
        self.mass = mass
        self.mass_err = mass_err
        self.name = name
        self.polar_radius = polar_radius
        self.polar_radius_err = polar_radius_err
        self.radius = radius
        self.radius_err = radius_err
        self.semi_major_axis = semi_major_axis
        self.semi_major_axis_err = semi_major_axis_err
        self.sideral_orbit_period = sideral_orbit_period
        self.sideral_orbit_period_err = sideral_orbit_period_err
        self.sideral_rotation_period = sideral_rotation_period
        self.sideral_rotation_period_err = sideral_rotation_period_err
        self.surface_gravity = surface_gravity
        self.surface_temperature = surface_temperature

        if discovery_parameters is None:
            self._get_discovery_parameters()

        self.discovery_parameters = discovery_parameters

    @classmethod
    def from_exoplanet_eu_csv(cls, csv_file):
        """
        Initialise planets from an exoplanet.eu cell sheet file.
        :param csv_file: exoplanet.eu cell sheet file
        :type csv_file: CellSheetFile
        """

        data_dict = csv_file.read()
        planets = []
        discovery_parameters = cls._get_discovery_parameters()

        for i in range(len(data_dict['# name'])):
            discovery_parameters['status'] = \
                data_dict['planet_status'][i]
            discovery_parameters['mass * sin(i)'] = \
                float(np.asarray(data_dict['mass_sini'][i], dtype=float)) * M_jup
            discovery_parameters['mass * sin(i) error'] = \
                (float(np.asarray(data_dict['mass_sini_error_min'][i], dtype=float) * M_jup),
                 float(np.asarray(data_dict['mass_sini_error_max'][i], dtype=float)) * M_jup)
            discovery_parameters['angular_distance'] = \
                np.rad2deg(float(data_dict['angular_distance'][i] * cst.arcsec))
            discovery_parameters['year of discovery'] = \
                float(data_dict['discovered'][i])
            discovery_parameters['update date'] = \
                data_dict['updated'][i]
            discovery_parameters['argument of periastron'] = \
                float(np.asarray(data_dict['omega'][i], dtype=float))
            discovery_parameters['argument of periastron error'] = \
                (float(np.asarray(data_dict['omega_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['omega_error_max'][i], dtype=float)),)
            discovery_parameters['periastron epoch'] = \
                float(np.asarray(data_dict['tperi'][i], dtype=float))
            discovery_parameters['periastron epoch error'] = \
                (float(np.asarray(data_dict['tperi_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['tperi_error_max'][i], dtype=float)))
            discovery_parameters['conjunction epoch'] = \
                float(np.asarray(data_dict['tconj'][i], dtype=float))
            discovery_parameters['conjunction epoch error'] = \
                (float(np.asarray(data_dict['tconj_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['tconj_error_max'][i], dtype=float)))
            discovery_parameters['primary transit epoch'] = \
                float(np.asarray(data_dict['tzero_tr'][i], dtype=float))
            discovery_parameters['primary transit epoch error'] = \
                (float(np.asarray(data_dict['tzero_tr_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['tzero_tr_error_max'][i], dtype=float)))
            discovery_parameters['secondary transit epoch'] = \
                float(np.asarray(data_dict['tzero_tr_sec'][i], dtype=float))
            discovery_parameters['secondary transit epoch error'] = \
                (float(np.asarray(data_dict['tzero_tr_sec_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['tzero_tr_sec_error_max'][i], dtype=float)))
            discovery_parameters['lambda angle'] = \
                float(np.asarray(data_dict['lambda_angle'][i], dtype=float))
            discovery_parameters['lambda angle error'] = \
                (float(np.asarray(data_dict['lambda_angle_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['lambda_angle_error_max'][i], dtype=float)))
            discovery_parameters['impact parameter'] = \
                float(np.asarray(data_dict['impact_parameter'][i], dtype=float))
            discovery_parameters['impact parameter error'] = \
                (float(np.asarray(data_dict['impact_parameter_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['impact_parameter_error_max'][i], dtype=float)))
            discovery_parameters['zero radial speed epoch'] = \
                float(np.asarray(data_dict['tzero_vr'][i], dtype=float))
            discovery_parameters['zero radial speed epoch error'] = \
                (float(np.asarray(data_dict['tzero_vr_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['tzero_vr_error_max'][i], dtype=float)))
            discovery_parameters['velocity semi amplitude'] = \
                float(np.asarray(data_dict['k'][i], dtype=float))
            discovery_parameters['velocity semi amplitude error'] = \
                (float(np.asarray(data_dict['k_error_min'][i], dtype=float)),
                 float(np.asarray(data_dict['k_error_max'][i], dtype=float)))
            discovery_parameters['calculated temperature'] = \
                float(np.asarray(data_dict['temp_calculated'][i], dtype=float))
            discovery_parameters['measured temperature'] = \
                float(np.asarray(data_dict['temp_measured'][i], dtype=float))
            discovery_parameters['alternate names'] = \
                data_dict['alternate_names'][i]
            discovery_parameters['detected atmospheric molecules'] = \
                data_dict['molecules'][i]
            discovery_parameters['hottest point longitude'] = \
                float(np.asarray(data_dict['hot_point_lon'][i], dtype=float))
            discovery_parameters['publication status'] = \
                data_dict['publication'][i]
            discovery_parameters['detection method'] = \
                data_dict['detection_type'][i]
            discovery_parameters['mass measurement method'] = \
                data_dict['mass_detection_type'][i]
            discovery_parameters['radius measurement method'] = \
                data_dict['radius_detection_type'][i]

            planets.append(
                cls(
                    name=data_dict['# name'][i],
                    mass=float(np.asarray(data_dict['mass'][i], dtype=float)) * M_jup,
                    mass_err=(float(np.asarray(data_dict['mass_error_min'][i], dtype=float)) * M_jup,
                              float(np.asarray(data_dict['mass_error_max'][i], dtype=float)) * M_jup),
                    radius=float(np.asarray(data_dict['radius'][i], dtype=float)) * R_jup,
                    radius_err=(float(np.asarray(data_dict['radius_error_min'][i], dtype=float) * R_jup),
                                float(np.asarray(data_dict['radius_error_max'][i], dtype=float)) * R_jup),
                    sideral_orbit_period=float(np.asarray(data_dict['orbital_period'][i], dtype=float) * cst.day),
                    sideral_orbit_period_err=(
                        float(np.asarray(data_dict['orbital_period_error_min'][i], dtype=float) * cst.day),
                        float(np.asarray(data_dict['orbital_period_error_max'][i], dtype=float)) * cst.day),
                    semi_major_axis=float(np.asarray(data_dict['semi_major_axis'][i], dtype=float) * cst.au),
                    semi_major_axis_err=(
                        float(np.asarray(data_dict['semi_major_axis_error_min'][i], dtype=float) * cst.au),
                        float(np.asarray(data_dict['semi_major_axis_error_max'][i], dtype=float)) * cst.au),
                    eccentricity=float(np.asarray(data_dict['eccentricity'][i], dtype=float)),
                    eccentricity_err=(float(np.asarray(data_dict['eccentricity_error_min'][i], dtype=float)),
                                      float(np.asarray(data_dict['eccentricity_error_max'][i], dtype=float))),
                    inclination=float(np.asarray(data_dict['inclination'][i], dtype=float)),
                    inclination_err=(float(np.asarray(data_dict['inclination_error_min'][i], dtype=float)),
                                     float(np.asarray(data_dict['inclination_error_max'][i], dtype=float))),
                    geometric_albedo=float(np.asarray(data_dict['geometric_albedo'][i], dtype=float)),
                    geometric_albedo_err=(float(np.asarray(data_dict['geometric_albedo_error_min'][i], dtype=float)),
                                          float(np.asarray(data_dict['geometric_albedo_error_max'][i], dtype=float))),
                    surface_gravity=np.exp(float(np.asarray(data_dict['log_g'][i], dtype=float))) * cst.g,
                    discovery_parameters=discovery_parameters
                )
            )

        return planets

    @staticmethod
    def _get_discovery_parameters():
        discovery_parameters = {
            'status': None,
            'mass * sin(i)': None,
            'mass * sin(i) error': tuple(),
            'angular distance': None,
            'year of discovery': None,
            'update date': None,
            'argument of periastron': None,
            'argument of periastron error': tuple(),
            'periastron epoch': None,
            'periastron epoch error': tuple(),
            'conjunction epoch': None,
            'conjunction epoch error': tuple(),
            'primary transit epoch': None,
            'primary transit epoch error': tuple(),
            'secondary transit epoch': None,
            'secondary transit epoch error': tuple(),
            'lambda angle': None,
            'lambda angle error': tuple(),
            'impact parameter': None,
            'impact parameter error': tuple(),
            'zero radial speed epoch': None,
            'zero radial speed epoch error': tuple(),
            'velocity semi amplitude': None,
            'velocity semi amplitude error': tuple(),
            'measured temperature': None,
            'calculated temperature': None,
            'hottest point longitude': None,
            'publication status': '',
            'detection method': '',
            'mass measurement method': '',
            'radius measurement method': '',
            'alternate names': '',
            'detected atmospheric molecules': [],
        }

        return discovery_parameters


def luminosity2bolometric_magnitude(luminosity):
    """
    Convert luminosity to bolometric magnitude.
    :param luminosity: (W) luminosity to be converted
    :return: equivalent bolometric magnitude
    """
    return -2.5 * np.log(luminosity / M_Bol)


def bolometric_magnitude2luminosity(bolometric_magnitude):
    """
    Convert bolometric magnitude to luminosity.
    :param bolometric_magnitude: bolometric magnitude to be converted
    :return: (W) equivalent luminosity
    """
    return M_Bol * np.exp(-bolometric_magnitude / 2.5)


def get_bulk_density(mass, radius):
    """
    Return the bulk density of an astrum from its mass and radius.
    :param mass: (kg) mass of the astrum
    :param radius: (m) radius of the astrum
    :return: (kg.m-3) bulk density of the astrum
    """
    return mass / (4/3 * np.pi * radius**3)


def get_escape_velocity(mass, radius):
    """
    Return the bulk density of an astrum from its mass and radius.
    :param mass: (kg) mass of the astrum
    :param radius: (m) radius of the astrum
    :return: (m.s-1) escape velocity of the astrum
    """
    return np.sqrt(2 * cst.G * mass / radius)


def get_luminosity(radius, temperature):
    """
    Return the luminosity of an astrum from its radius and temperature
    :param radius: (m) radius of the object
    :param temperature: (K) temperature of the object
    :return: (W) luminosity of the object
    """
    return 4 * np.pi * radius**2 * cst.Stefan_Boltzmann * temperature**4


def get_surface_gravity(mass, radius):
    """
    Return the surface gravity of an astrum from its mass and radius.
    :param mass: (kg) mass of the astrum
    :param radius: (m) radius of the astrum
    :return: (m.s-2) surface gravity of the astrum
    """
    return cst.G * mass / radius**2
