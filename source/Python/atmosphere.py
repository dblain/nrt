"""
Atmosphere-related objects.
"""
import copy
import os
import re
import struct
from warnings import warn

import numpy as np

from .files import DataFile, PickleFile, File, NameListFile
from .math_ import interpx
from .utils import info


class Cloud:
    """Cloud object. Contains the cloud parameters."""

    def __init__(self, pressure_base, pressure_top, reflectance=0.0, transmittance=1.0):
        """
        :param pressure_base: pressure of the base of the cloud
        :param pressure_top: pressure of the top of the cloud
        :param reflectance: reflectance of the cloud
        :param transmittance: transmittance of the cloud
        """
        self.pressure_base = pressure_base
        self.pressure_top = pressure_top
        self.reflectance = reflectance
        self.transmittance = transmittance


class Molecule:
    """Molecule object. Contains the molecule parameters."""

    class Lines(File):
        """Lines file. Contains the lines parameters following GEISA 2015 format."""
        PATH_DATA = os.path.join(File.PATH_DATA, 'lines', '')
        PATH_DATA_GEISA = os.path.join(PATH_DATA, 'GEISA', '')
        PATH_IN = os.path.join(File.PATH_IN, 'molecules', 'lines', '')
        EXT = 'dat'
        UNDEFINED_VALUES = \
            (b'-0.999999', b'-9.9999D-01', b'-.9999', b'-0.9999', b'*', b'*', b'*', b'*', b'-.99', b'-99', b'-99', b'*',
             b'-9', b'0', b'-9.999E-1', b'-9.9999', b'0', b'-0.9999',
             b'-0.999999', b'-9.9999E-1', b'-0.9999', b'-0.99', b'0', b'-0.9999', b'-9.9999',
             b'-0.99', b'-0.99', b'0', b'0', b'-0.99', b'-0.99')  # GEISA undefined values from A to F'

        def __init__(self, name, path=PATH_IN, ext=EXT, file='', exists=True, find=False):
            """
            Line parameters file, GEISA 2015 format, c.f. http://www.pole-ether.fr/etherTypo/?id=1734.
            :param name: c.f. File
            :param path: c.f. File
            :param ext: c.f. File
            :param file: c.f. File
            :param exists: c.f. File
            :param find: c.f. File
            """
            super().__init__(name=name, path=path, ext=ext, file=file, exists=exists, find=find)

            self.A_ = []  # (cm-1) wavenumber of the line
            self.B_ = []  # (cm-1/(molecule.cm-2)) intensity of the line at 296 K
            self.C_ = []  # (cm-1.atm-1) air broadening pressure halfwidth at 296 K, 1 atm and for Earth's atmosphere
            self.D_ = []  # (cm-1) energy of the lower transition level
            self.E1 = []  # upper state vibrational identification
            self.E2 = []  # lower state vibrational identification
            self.E3 = []  # upper state rotational identification
            self.E4 = []  # lower state rotational identification
            self.F_ = []  # temperature dependence coefficient of the air broadening halfwidth for Earth's atmosphere
            self.G_ = []  # identification code for isotopologue as in GEISA
            self.I_ = []  # identification code for molecule as in GEISA
            self.J_ = []  # internal GEISA code for the data identification
            self.K_ = []  # molecule number as in HITRAN
            self.L_ = []  # isotopologue number (1=most abundant, 2= second...etc) as in HITRAN
            self.M_ = []  # Einstein A-coefficient
            self.N_ = []  # (cm-1.atm-1) self broadening pressure halfwidth at 296K, 1 atm and for Earth's atm.
            self.O_ = []  # (cm-1.atm-1) air pressure shift of the line transition at 296K, 1 atm and for Earth's atm.
            self.R_ = []  # temperature dependence coefficient of the air pressure shift for Earth's atmosphere
            self.AA = []  # (cm-1) estimated accuracy on the line position
            self.BB = []  # (cm-1/(molecule.cm-2)) estimated accuracy on the intensity of the line
            self.CC = []  # (cm-1.atm-1) estimated accuracy on the air collision halfwidth
            self.FF = []  # estimated accuracy on the temperature dependence coefficient of the air broadening halfwidth
            self.NN = []  # (cm-1.atm-1) estimated accuracy on the self broadened at 296 K, 1 atm and for Earth's atm.
            self.OO = []  # (cm-1.atm-1) estimated accuracy on the air pressure shift of the line transition at 296 K
            self.RR = []  # estimated accuracy on the temperature dependence coefficient of the air pressure shift
            self.S_ = []  # temperature dependence coefficient of the self broadening halfwidth
            self.SS = []  # estimated accuracy on the temperature dependence coefficient of the self broadening HWHM
            self.T_ = []  # (cm-1.atm-1) self pressure shift of the line transition at 296 K
            self.TT = []  # (cm-1.atm-1) estimated accuracy on the self pressure shift of the line transition at 296 K
            self.U_ = []  # temperature dependence coefficient of the self pressure shift
            self.UU = []  # estimated accuracy on the temperature dependence coefficient of the self pressure shift

            self.lines = []  # lines informations in GEISA 2015 format

            self.J_upper = []  # J rotational upper level (diatomic and polyatomic molecules)
            self.J_lower = []  # J rotational lower level (diatomic and polyatomic molecules)
            self.K_upper = []  # K rotational upper level (oblates and prolates polyatomic molecules only)
            self.K_lower = []  # K rotational lower level (oblates and prolates polyatomic molecules only)

        def _merge_with(self, ref_lines):
            """
            Merge two line files into one.
            The Lines object self will be merged with the ref_lines Lines object. Only lines from ref_lines outside
            of the wavenumber range of self will be added to self.
            :param ref_lines: Lines object to merge
            """
            if isinstance(ref_lines, self.__class__):
                ref_lines.read()

                for i in range(len(ref_lines.A_)):
                    if ref_lines.A_[i] < self.A_[0]:
                        self.lines.insert(i, ref_lines.lines[i])
                    elif ref_lines.A_[i] > self.A_[-1]:
                        self.lines.append(ref_lines.lines[i])
            else:
                raise TypeError('expected \'%s\', got \'%s\' instead' % (self.__class__, ref_lines.__class__))

        def _params2line(self):
            """Convert the lines parameters into a string in GEISA 2015 format."""
            self.lines = []

            for i in range(len(self.A_)):
                a_ = bytes(str('%12.6f' % self.A_[i]), 'utf-8')
                b_ = bytes(str('%11.4E' % self.B_[i]).replace('E', 'D'), 'utf-8')

                if self.C_[i] >= 0:
                    c_ = bytes(str('%6.4f' % self.C_[i]), 'utf-8')
                else:
                    c_ = bytes(str('%7.4f' % self.C_[i]).replace('0', ''), 'utf-8')

                d_ = bytes(str('%10.4f' % self.D_[i]), 'utf-8')
                e1 = bytes('%25s' % str(self.E1[i]), 'utf-8')
                e2 = bytes('%25s' % str(self.E2[i]), 'utf-8')
                e3 = bytes('%15s' % str(self.E3[i]), 'utf-8')
                e4 = bytes('%15s' % str(self.E4[i]), 'utf-8')

                if self.F_[i] >= 0:
                    f_ = bytes(str('%4.2f' % self.F_[i]), 'utf-8')
                else:
                    f_ = bytes(str('%5.2f' % self.F_[i]).replace('0', ''), 'utf-8')

                g_ = bytes(str('%3d' % self.G_[i]), 'utf-8')
                i_ = bytes(str('%3d' % self.I_[i]), 'utf-8')
                j_ = bytes('%3s' % str(self.J_[i]), 'utf-8')
                k_ = bytes(str('%2d' % self.K_[i]), 'utf-8')
                l_ = bytes(str('%1d' % self.L_[i]), 'utf-8')
                m_ = bytes(str('%10.3E' % self.M_[i]), 'utf-8')
                n_ = bytes(str('%7.4f' % self.N_[i]), 'utf-8')
                o_ = bytes(str('%9.6f' % self.O_[i]), 'utf-8')

                if self.R_[i] >= 0:
                    r_ = bytes(str('%6.4f' % self.R_[i]), 'utf-8')
                else:
                    r_ = bytes(str('%7.4f' % self.R_[i]).replace('0', ''), 'utf-8')

                aa = bytes(str('%10.6f' % self.AA[i]), 'utf-8')
                bb = bytes(str('%11.4E' % self.BB[i]).replace('E', 'D'), 'utf-8')

                if self.CC[i] >= 0:
                    cc = bytes(str('%6.4f' % self.CC[i]), 'utf-8')
                else:
                    cc = bytes(str('%7.4f' % self.CC[i]).replace('0', ''), 'utf-8')

                if self.FF[i] >= 0:
                    ff = bytes(str('%4.2f' % self.FF[i]), 'utf-8')
                else:
                    ff = bytes(str('%5.2f' % self.FF[i]).replace('0', ''), 'utf-8')

                oo = bytes(str('%9.6f' % self.OO[i]), 'utf-8')

                if self.RR[i] >= 0:
                    rr = bytes(str('%6.4f' % self.RR[i]), 'utf-8')
                else:
                    rr = bytes(str('%7.4f' % self.RR[i]).replace('0', ''), 'utf-8')

                nn = bytes(str('%7.4f' % self.NN[i]), 'utf-8')

                if self.S_[i] >= 0:
                    s_ = bytes(str('%4.2f' % self.S_[i]), 'utf-8')
                else:
                    s_ = bytes(str('%4.2f' % self.S_[i]).replace('0', ''), 'utf-8')

                if self.SS[i] >= 0:
                    ss = bytes(str('%4.2f' % self.SS[i]), 'utf-8')
                else:
                    ss = bytes(str('%5.2f' % self.SS[i]).replace('0', ''), 'utf-8')

                if self.T_[i] >= 0:
                    t_ = bytes(str('%8.6f' % self.T_[i]), 'utf-8')
                else:
                    t_ = bytes(str('%8.6f' % 0), 'utf-8')

                if self.TT[i] >= 0:
                    tt = bytes(str('%8.6f' % self.TT[i]), 'utf-8')
                else:
                    tt = bytes(str('%8.6f' % 0), 'utf-8')

                if self.U_[i] >= 0:
                    u_ = bytes(str('%4.2f' % self.U_[i]), 'utf-8')
                else:
                    u_ = bytes(str('%5.2f' % self.U_[i]).replace('0', ''), 'utf-8')

                if self.UU[i] >= 0:
                    uu = bytes(str('%4.2f' % self.UU[i]), 'utf-8')
                else:
                    uu = bytes(str('%5.2f' % self.UU[i]).replace('0', ''), 'utf-8')

                i = struct.pack('12s11s6s10s25s25s15s15s4s3s3s3s2s1s10s7s9s6s10s11s6s4s9s6s7s4s4s8s8s4s4s1s',
                                a_, b_, c_, d_, e1, e2, e3, e4, f_, g_, i_, j_, k_, l_, m_, n_, o_, r_,
                                aa, bb, cc, ff, oo, rr, nn, s_, ss, t_, tt, u_, uu, bytes('\n', 'utf-8'))
                self.lines.append(i)

        def air2h2he(self, q_h2=0.864, q_he=0.136):
            """
            Change air-broadened lines in H2-He-broadened lines for some molecules.
            :param q_h2: mixing ration of H2 in atmosphere (default: Atreya 2003 for Jupiter's atmosphere)
            :param q_he: mixing ration of He in atmosphere (default: Atreya 2003 for Jupiter's atmosphere)
            """
            print(f'Changing values of C and F of file \'{self.file}\' from air to {q_h2} H2 and {q_he} He...')

            if 'CO' in self.file or 'co' in self.file:
                self.co_h2he(q_h2=q_h2, q_he=q_he)
            elif 'CH3D' in self.file or 'ch3d' in self.file:
                self.ch3d_h2he(q_h2=q_h2, q_he=q_he)
            elif 'H2O' in self.file or 'h2o' in self.file:
                self.h2o_h2he()
            elif 'NH3' in self.file or 'nh3' in self.file:
                self.nh3_h2he(q_h2=q_h2, q_he=q_he)
            elif 'PH3' in self.file or 'ph3' in self.file:
                self.ph3_h2he(q_h2=q_h2, q_he=q_he)
            else:
                info('No H2-He data for the file \'%s\'' % self.file)

        def ch3d_h2he(self, q_h2, q_he):
            """
            Replace the broadening pressure HWHM (C) of the CH3D lines by Boussin's thesis values for H2 and He in
            the V2 band.
            Replace the temperature coefficient (F) of the CH3D lines by the best value according to Boussin's
            thesis.
            N.B. The broadening and the temperature coefficient range of J values investigated in Boussin's works
            are J <= 15 and K <= 15 but this value can get up to 25 in GEISA 2015 (1800-2200 cm-1). We chosen to
            extrapolate the formulae, because we believe the extrapolated results are better than the values for
            air.
            For the Q branch, we chosen to take the mean values of the P and R branches.
            Reference temperature is 296 K.
            :param q_h2: mixing ratio of H2 in the planet atmosphere.
            :param q_he: mixing ratio of He in0 the planet atmosphere.
            """

            # Constants
            a_h2_p = [0.072, -1.1E-3, 0, 1.5E-5,
                      -4.2E-5]  # (cm-1.atm-1) parameters for CH3D-H2 pressure broadening (a, b, c, d, e)
            a_h2_r = [0.074, -1.4E-3, 0, 6E-6, -3E-5]  # (cm-1.atm-1) parameters for CH3D-H2 pressure broadening
            a_h2_q = list(
                np.mean([a_h2_p, a_h2_r], axis=0))  # (cm-1.atm-1) parameters for CH3D-H2 pressure broadening

            a_he_p = [0.0471, -9.4E-3, 0, 1.3E-5, -3.7E-5]  # (cm-1.atm-1) parameters for CH3D-He pressure broadening
            a_he_r = [0.0484, -1.1E-3, 0, 8E-6, -3E-5]  # (cm-1.atm-1) parameters for CH3D-He pressure broadening
            a_he_q = list(
                np.mean([a_he_p, a_he_r], axis=0))  # (cm-1.atm-1) params for CH3D-He pressure broadening

            n_h2 = 0.46  # weighted average of the temperature exponent for CH3D-H2, Boussin's thesis
            n_he = 0.28  # weighted average of the temperature exponent for CH3D-He, Boussin's thesis

            j_max = max(self.J_lower)
            k_max = max(self.K_lower)

            # Info variables
            line_changed = 0
            broadening_diffs = []

            gam_p = self._find_param_boussin(a_h2_p, a_he_p, j_max, k_max, q_h2, q_he)
            gam_q = self._find_param_boussin(a_h2_q, a_he_q, j_max, k_max, q_h2, q_he)
            gam_r = self._find_param_boussin(a_h2_r, a_he_r, j_max, k_max, q_h2, q_he)

            n = q_h2 * n_h2 + q_he * n_he

            # Identification of P, Q and R branches
            for i in range(len(self.A_)):
                if self.K_upper[i] == self.K_lower[i]:
                    # Replace the pressure broadening and the temperature exponent
                    if self.J_upper[i] == self.J_lower[i] - 1:
                        line_changed += 1
                        c = copy.deepcopy(self.C_[i])

                        self.C_[i] = gam_p[self.J_lower[i], self.K_upper[i]]

                        broadening_diffs.append(abs(self.C_[i] - c) / c)

                    elif self.J_upper[i] == self.J_lower[i]:
                        line_changed += 1
                        c = copy.deepcopy(self.C_[i])

                        self.C_[i] = gam_q[self.J_lower[i], self.K_upper[i]]

                        broadening_diffs.append(abs(self.C_[i] - c) / c)

                    elif self.J_upper[i] == self.J_lower[i] + 1:
                        line_changed += 1
                        c = copy.deepcopy(self.C_[i])

                        self.C_[i] = gam_r[self.J_lower[i], self.K_upper[i]]

                        broadening_diffs.append(abs(self.C_[i] - c) / c)

                self.F_[i] = n

            self._params2line()  # update lines
            self._display_info(line_changed, len(self.A_), broadening_diffs)

        def co_h2he(self, q_h2, q_he):
            """
            Replace the broadening pressure HWHM (C) of the CO lines by Bouanich1972 and 1973's values for
            respectively He and H2.
            Replace the temperature exponent (F) by Varanasi1987's values for H2.
            N.B. The broadening range of J values investigated in Bounaich's works is J <= 20.
            There is no influence of K because CO is a diatomic molecule.
            The values for H2 and He are copied from table 2 of their respective articles.
            Reference temperature is 300 K (296 K for GEISA 2015).
            We chosen to apply the H2 temperature exponent for both H2 and He broadening.
            :param q_h2: mixing ratio of H2 in the planet atmosphere.
            :param q_he: mixing ratio of He in the planet atmosphere.
            """

            # Constants
            gamma0_h2 = [0.0806, 0.0745, 0.0729, 0.0723, 0.0719, 0.0716, 0.0713, 0.071, 0.0707, 0.0704, 0.0702,
                         0.0699, 0.0697, 0.0695, 0.0692, 0.069, 0.0687, 0.0685, 0.0682, 0.0677,
                         0.067]  # (cm-1.atm-1) CO-H2 @ 300 K
            gamma0_he = [0.0485, 0.0416, 0.047, 0.0465, 0.0463, 0.0462, 0.0462, 0.0466, 0.0469, 0.0467, 0.0465,
                         0.0464, 0.0464, 0.0463, 0.0463, 0.0462, 0.0462, 0.0461, 0.0461, 0.046,
                         0.046]  # (cm-1.atm-1) CO-He @ 300 K

            n_h2 = 0.747  # temperature exponent for CO-H2, Varanasi 1987

            temp_ref = 300  # (K) reference temperature of the article
            temp0 = 296  # (K) reference temperature of the geisa database

            # Information variables
            line_changed = 0
            broadening_diffs = []

            # Identification of P, Q and R bands
            for i in range(len(self.A_)):
                if self.K_upper[i] == self.K_lower[i]:
                    if (self.J_upper[i] == self.J_lower[i] - 1
                        or self.J_upper[i] == self.J_lower[i]
                        or self.J_upper[i] == self.J_lower[i] + 1) \
                            and self.J_lower[i] <= 20:
                        # Replace the pressure broadening and the temperature exponent
                        line_changed += 1
                        c = copy.deepcopy(self.C_[i])

                        self.C_[i] = \
                            q_h2 * gamma0_h2[self.J_lower[i]] * (temp_ref / temp0) ** n_h2 + \
                            q_he * gamma0_he[self.J_lower[i]] * (temp_ref / temp0) ** n_h2
                        broadening_diffs.append(abs(self.C_[i] - c) / c)

                        self.F_[i] = n_h2

            self._params2line()  # update lines
            self._display_info(line_changed, len(self.A_), broadening_diffs)

        def h2o_h2he(self):
            """
            Multiply the broadening pressure HWHM (C) of the H2O lines by 0.79, a good approximation for H2
            (c.f Bezard2002, Langlois1994).
            """

            self.C_ = np.asarray(self.C_) * 0.79
            self._params2line()  # update lines

        def nh3_h2he(self, q_h2, q_he):
            """
            Replace the broadening pressure HWHM (C) of the NH3 lines by Brown1994's values for H2 and He.
            Replace the temperature exponent (F) by Brown1994's value.
            N.B. The broadening range of J values investigated in Bounaich's works is J <= 10, and for K <= 10.
            Reference temperature is 296 K.
            We chosen to apply the H2 temperature exponent for both H2 and He broadening.
            :param q_h2: mixing ratio of H2 in the planet atmosphere.
            :param q_he: mixing ratio of He in the planet atmosphere.
            """

            # Constants
            a_h2 = [0.09839, -0.01071, 0.00728, 0.000521, -0.000490]
            a_he = [0.0364, -0.00309, 0.00193, 0.000240, 0 - 0.00025]
            n_h2 = 0.73  # temperature exponent for NH3-H2

            # Information variables
            line_changed = 0
            broadening_diffs = []

            # Identification of P, Q and R bands
            for i in range(len(self.A_)):
                if self.K_upper[i] == self.K_lower[i]:
                    if (self.J_upper[i] == self.J_lower[i] - 1
                        or self.J_upper[i] == self.J_lower[i]
                        or self.J_upper[i] == self.J_lower[i] + 1) \
                            and 1 <= self.J_lower[i] <= 10 and self.K_lower[i] <= 10:
                        # Replace the pressure broadening and the temperature exponent
                        line_changed += 1
                        c = copy.deepcopy(self.C_[i])

                        j = self.J_lower[i]
                        k = self.K_lower[i]
                        gamma0_h2 = a_h2[0] + a_h2[1] * j + a_h2[2] * k + a_h2[3] * j ** 2 + a_h2[4] * j * k
                        gamma0_he = a_he[0] + a_he[1] * j + a_he[2] * k + a_he[3] * j ** 2 + a_he[4] * j * k

                        self.C_[i] = q_h2 * gamma0_h2 + q_he * gamma0_he

                        if np.abs(c) > 0:
                            broadening_diffs.append(abs(self.C_[i] - c) / c)
                        else:
                            broadening_diffs.append(0)

                        self.F_[i] = n_h2

            self._params2line()  # update lines
            self._display_info(line_changed, len(self.A_), broadening_diffs)

        def ph3_h2he(self, q_h2, q_he):
            """
            Replace the broadening pressure HWHM (C) of the lines by Levy1993's values for H2 and He.
            Replace the temperature coefficient (F) of the lines  by Levy1994's value for H2 and He.
            The broadening and the temperature coefficient range of J values investigated in Levy's works are
            respectively J <= 15 and J <= 13, but this value can get up to 17 in GEISA 2015 (1800-2200 cm-1). We
            chosen to extrapolate the formulae, because we believe the extrapolated results are better than the
            values for air.
            Values are for "room temperature", i.e. 296 K.
            :param q_h2: mixing ratio of H2 in the planet atmosphere.
            :param q_he: mixing ratio of He in the planet atmosphere.
            """

            # Constants
            a_h2 = 0.1078  # (cm-1.atm-1) parameter for PH3-H2 pressure broadening (Levy et al., 1993)
            b_h2 = -0.0014  # (cm-1.atm-1) parameter for PH3-H2 pressure broadening (Levy et al., 1993)

            a_he = 0.0618  # (cm-1.atm-1) parameter for PH3-He pressure broadening, (Levy et al., 1993)
            b_he = -0.0012  # (cm-1.atm-1) parameter for PH3-He pressure broadening, (Levy et al., 1993)

            n_h2 = 0.732  # weighted average of the temperature exponent for PH3-H2, (Levy et al., 1994)
            n_he = 0.303  # weighted average of the temperature exponent for PH3-He, (Levy et al., 1994)

            # Info variables
            line_changed = 0
            broadening_diffs = []

            # Initialisation
            j_max = np.max(self.J_lower)
            gamma = np.array([])  # pressure broadening HWHM at 1 atm and reference temperature

            # Calculate the pressure broadening coefficient for the given H2-He atmosphere
            for j in range(j_max + 1):
                gamma_h2 = a_h2 + b_h2 * j
                gamma_he = a_he + b_he * j
                gamma = np.append(gamma, [q_h2 * gamma_h2 + q_he * gamma_he])

            n = q_h2 * n_h2 + q_he * n_he

            # Change only P, Q and R bands, because we have no informations about the behaviour of other bands
            for i in range(len(self.A_)):
                if self.K_upper[i] == self.K_lower[i]:
                    if self.J_upper[i] == self.J_lower[i] - 1 \
                            or self.J_upper[i] == self.J_lower[i] \
                            or self.J_upper[i] == self.J_lower[i] + 1:
                        # Replace the pressure broadening and the temperature exponent
                        line_changed += 1
                        c = copy.deepcopy(self.C_[i])
                        if c == 0:
                            c = 1E-4

                        self.C_[i] = gamma[self.J_lower[i]]

                        broadening_diffs.append(abs(self.C_[i] - c) / c)

                        self.F_[i] = n

            self._params2line()  # update lines
            self._display_info(line_changed, len(self.A_), broadening_diffs)

        def read(self):
            """
            Read Lines in GEISA 2015 format, c.f. http://www.pole-ether.fr/etherTypo/?id=1734
            """
            with open(self.file, 'rb') as f:
                for line in f:
                    try:
                        (a, b, c, d, e1, e2, e3, e4, f, g, i, j, k, l, m, n, o, r,
                         aa, bb, cc, ff, oo, rr, nn, s, ss, t, tt, u, uu) = \
                            struct.unpack('12s11s6s10s25s25s15s15s4s3s3s3s2s1s10s7s9s6s10s11s6s4s9s6s7s4s4s8s8s4s4sx',
                                          line)
                    except struct.error:
                        try:
                            (a, b, c, d, e1, e2, e3, e4, f) = self._read_a2f(line)
                            (g, i, j, k, l, m, n, o, r, aa, bb, cc, ff, oo, rr, nn, s, ss, t, tt, u, uu) = \
                                self.UNDEFINED_VALUES[9:]
                        except struct.error:
                            raise struct.error('GEISA 2015 format unpack requires a buffer of either 123 (A-F) or '
                                               '253 (A-U\') bytes')

                    self.A_.append(float(a))
                    self.B_.append(float(str(b, 'utf-8').replace('D', 'E')))
                    self.C_.append(float(c))
                    self.D_.append(float(d))
                    self.E1.append(str(e1, 'utf-8'))
                    self.E2.append(str(e2, 'utf-8'))
                    self.E3.append(str(e3, 'utf-8'))
                    self.E4.append(str(e4, 'utf-8'))
                    self.F_.append(float(f))
                    self.G_.append(int(g))
                    self.I_.append(int(i))
                    self.J_.append(str(j, 'utf-8'))
                    self.K_.append(int(k))
                    self.L_.append(int(l))
                    self.M_.append(float(m))
                    self.N_.append(float(n))
                    self.O_.append(float(o))
                    self.R_.append(float(r))
                    self.AA.append(float(aa))
                    self.BB.append(float(str(bb, 'utf-8').replace('D', 'E')))
                    self.CC.append(float(cc))
                    self.FF.append(float(ff))
                    self.OO.append(float(oo))
                    self.RR.append(float(rr))
                    self.NN.append(float(nn))
                    self.S_.append(float(s))
                    self.SS.append(float(ss))
                    self.T_.append(float(t))
                    self.TT.append(float(tt))
                    self.U_.append(float(u))
                    self.UU.append(float(uu))

                    if str(e3, 'utf-8').strip() != '' and str(e3, 'utf-8').strip() != '*' \
                            and str(e4, 'utf-8').strip() != '*':
                        str_e3 = re.sub(r'[A-Za-z-+]', '', str(e3, 'utf-8').strip())
                        str_e4 = re.sub(r'[A-Za-z-+]', '', str(e4, 'utf-8').strip())

                        try:
                            (j_u, k_u) = str_e3.split()[0:2]
                            self.J_upper.append(int(j_u))
                        except ValueError:
                            (j_u, k_u) = ('*', '*')
                            self.J_upper.append('*')

                        (j_l, k_l) = str_e4.split()[0:2]

                        if '*' in k_u:
                            self.K_upper.append('*')
                        else:
                            self.K_upper.append(int(k_u))

                        if '*' in j_l:
                            self.J_lower.append('*')
                        else:
                            self.J_lower.append(int(j_l))

                        self.K_lower.append(int(k_l))
                    elif 'P' in str(e4, 'utf-8') or 'R' in str(e4, 'utf-8'):
                        (band, j_l) = struct.unpack('12s3s', e4)
                        band = str(band, 'utf-8').strip()
                        if band == 'P':
                            self.J_lower.append(int(str(j_l, 'utf-8')))
                            self.J_upper.append(int(str(j_l, 'utf-8')) - 1)
                        elif band == 'R':
                            self.J_lower.append(int(str(j_l, 'utf-8')))
                            self.J_upper.append(int(str(j_l, 'utf-8')) + 1)
                        self.K_lower.append(0)
                        self.K_upper.append(0)
                    else:
                        self.K_lower.append(-1)
                        self.K_upper.append('*')
                        self.J_lower.append(-1)
                        self.J_upper.append('*')

            self._params2line()

        def write(self):
            """Write the line file."""
            with open(self.file, 'w') as f:
                for line in self.lines:
                    f.write(str(line, 'utf-8').replace('\x00', ' '))

        @classmethod
        def from_wavenumbers(cls, name, wavenumbers):
            """
            Create empty Lines from wavenumbers.
            :param name:
            :param wavenumbers:
            :return:
            """

            new_lines = cls(name, exists=False)

            (b, c, d, e1, e2, e3, e4, f, g, i, j,
             k, l, m, n, o, r,
             aa, bb, cc, ff, oo, rr, nn, s, ss, t, tt, u, uu) = cls.UNDEFINED_VALUES[1:]

            for wavenumber in wavenumbers:
                new_lines.A_.append(wavenumber)
                new_lines.B_.append(float(str(b, 'utf-8').replace('D', 'E')))
                new_lines.C_.append(float(c))
                new_lines.D_.append(float(d))
                new_lines.E1.append(str(e1, 'utf-8'))
                new_lines.E2.append(str(e2, 'utf-8'))
                new_lines.E3.append(str(e3, 'utf-8'))
                new_lines.E4.append(str(e4, 'utf-8'))
                new_lines.F_.append(float(f))
                new_lines.G_.append(int(g))
                new_lines.I_.append(int(i))
                new_lines.J_.append(str(j, 'utf-8'))
                new_lines.K_.append(int(k))
                new_lines.L_.append(int(l))
                new_lines.M_.append(float(m))
                new_lines.N_.append(float(n))
                new_lines.O_.append(float(o))
                new_lines.R_.append(float(r))
                new_lines.AA.append(float(aa))
                new_lines.BB.append(float(str(bb, 'utf-8').replace('D', 'E')))
                new_lines.CC.append(float(cc))
                new_lines.FF.append(float(ff))
                new_lines.OO.append(float(oo))
                new_lines.RR.append(float(rr))
                new_lines.NN.append(float(nn))
                new_lines.S_.append(float(s))
                new_lines.SS.append(float(ss))
                new_lines.T_.append(float(t))
                new_lines.TT.append(float(tt))
                new_lines.U_.append(float(u))
                new_lines.UU.append(float(uu))

            new_lines._params2line()

            return new_lines

        @classmethod
        def from_isotopes_mix(cls, name, isotopes_lines, isotopic_ratios):
            """

            :param name:
            :param isotopes_lines:
            :param isotopic_ratios:
            :return:
            """
            if np.size(isotopes_lines) != np.size(isotopic_ratios):
                raise ValueError(f'isotopes lines and isotopic ratios must have the same lengths, '
                                 f'but have lengths {np.size(isotopes_lines)} and {np.size(isotopic_ratios)}')

            if not 1 - 1E-9 < np.sum(isotopic_ratios) < 1 + 1E-9:
                raise ValueError(f'the sum of the isotopic ratios must be 1, but is {np.sum(isotopic_ratios)}')

            new_lines = cls(name, exists=False)
            new_lines.lines = np.asarray([])

            for i, isotope_lines in enumerate(isotopes_lines):
                isotope_lines.B_ *= isotopic_ratios[i]
                cls._params2line(isotope_lines)
                new_lines.lines = np.concatenate((new_lines.lines, isotope_lines.lines))

            new_lines.lines = np.sort(new_lines.lines)

            for line in new_lines.lines:
                (a, b, c, d, e1, e2, e3, e4, f, g, i, j, k, l, m, n, o, r,
                 aa, bb, cc, ff, oo, rr, nn, s, ss, t, tt, u, uu) = \
                    struct.unpack('12s11s6s10s25s25s15s15s4s3s3s3s2s1s10s7s9s6s10s11s6s4s9s6s7s4s4s8s8s4s4sx', line)

                new_lines.A_.append(float(a))
                new_lines.B_.append(float(str(b, 'utf-8').replace('D', 'E')))
                new_lines.C_.append(float(c))
                new_lines.D_.append(float(d))
                new_lines.E1.append(str(e1, 'utf-8'))
                new_lines.E2.append(str(e2, 'utf-8'))
                new_lines.E3.append(str(e3, 'utf-8'))
                new_lines.E4.append(str(e4, 'utf-8'))
                new_lines.F_.append(float(f))
                new_lines.G_.append(int(g))
                new_lines.I_.append(int(i))
                new_lines.J_.append(str(j, 'utf-8'))
                new_lines.K_.append(int(k))
                new_lines.L_.append(int(l))
                new_lines.M_.append(float(m))
                new_lines.N_.append(float(n))
                new_lines.O_.append(float(o))
                new_lines.R_.append(float(r))
                new_lines.AA.append(float(aa))
                new_lines.BB.append(float(str(bb, 'utf-8').replace('D', 'E')))
                new_lines.CC.append(float(cc))
                new_lines.FF.append(float(ff))
                new_lines.OO.append(float(oo))
                new_lines.RR.append(float(rr))
                new_lines.NN.append(float(nn))
                new_lines.S_.append(float(s))
                new_lines.SS.append(float(ss))
                new_lines.T_.append(float(t))
                new_lines.TT.append(float(tt))
                new_lines.U_.append(float(u))
                new_lines.UU.append(float(uu))

            return new_lines

        @staticmethod
        def _display_info(line_changed, length, broadening_diffs):
            """
            Display some informations about the lines corrections.
            Used to indicate changes in lines file when changing the atmosphere composition.
            :param line_changed: number of the modified line
            :param length: total number of lines in the file
            :param broadening_diffs: ratio between new air broadening and air broadening in Earth's atmosphere
            """
            broadening_diffs = np.asarray(broadening_diffs)
            print('line changed: %d/%d' % (line_changed, length))
            print('broadening: nb of rel. diff. > 1E-2: %d' % np.size(
                broadening_diffs[np.where(broadening_diffs > 1E-2)]))
            print('broadening: min rel. diff.: %g' % np.min(broadening_diffs))
            print('broadening: max rel. diff.: %g' % np.max(broadening_diffs))
            print('broadening: mean rel. diff.: %g' % np.mean(broadening_diffs))
            print('broadening: rel. diff. std: %g' % np.std(broadening_diffs))

        @staticmethod
        def _find_param_boussin(a_h2, a_he, j_max, k_max, q_h2, q_he):
            """
            Find the broadening coefficient and the temperature exponent for CH3D in a mixture of H2 and He,
            according to Boussin 1999's formulae.
            :param a_h2: (cm-1.atm-1) params for CH3D-H2 pressure broadening (a, b, c, d, e), must be of length 5
            :param a_he: (cm-1.atm-1) params for CH3D-He pressure broadening (a, b, c, d, e), must be of length 5
            :param j_max: maximum quantum number J to consider
            :param k_max: maximum quantum number K to consider
            :param q_h2: mixing ratio of H2 in the planet atmosphere.
            :param q_he: mixing ratio of He in the planet atmosphere.
            :return: pressure broadening at 1 atm and reference temperature and best corresponding TDCABHWHM
            """
            if len(a_h2) is not 5 or len(a_he) is not 5:
                raise TypeError('\'a_h2\' and \'a_he\' arguments are expected to be of length 5')

            gamma = np.zeros((j_max + 1, k_max + 1))  # pressure broadening at 1 atm and reference temperature

            for K in range(k_max + 1):
                for J in range(j_max + 1):
                    gamma_h2 = a_h2[0] + a_h2[1] * J + a_h2[2] * K + a_h2[3] * J ** 2 + a_h2[4] * J * K
                    gamma_he = a_he[0] + a_he[1] * J + a_he[2] * K + a_he[3] * J ** 2 + a_he[4] * J * K
                    gamma[J, K] = q_h2 * gamma_h2 + q_he * gamma_he

            return gamma

        @staticmethod
        def _read_a2f(line):
            """
            Read Lines in GEISA 2015 format, from parameter A to F, c.f. http://www.pole-ether.fr/etherTypo/?id=1734
            :param line: GEISA 2015 formatted line to read
            :return: unpacked parameters A to F
            """
            (a, b, c, d, e1, e2, e3, e4, f) = \
                struct.unpack('12s11s6s10s25s25s15s15s4s',
                              line[:123])

            return a, b, c, d, e1, e2, e3, e4, f

    class VMRProfile(DataFile):
        """Volume Mixing Ratio profile data file. Contains VMR informations."""
        PATH_IN = os.path.join(File.PATH_IN, 'atmospheres', 'vmr_profiles', '')
        PATH_OUT = os.path.join(File.PATH_OUT, 'vmr_profiles', '')
        EXT = DataFile.EXT
        YUNITS = 'bar'
        YLABEL = f'Pressure ({YUNITS})'

        def __init__(self, name, path, ext=EXT, exists=False, find=False, label='', color='k', linestyle='-',
                     marker='',
                     pressures=None, vmr=None, vmr_uncertainty_factor=None, vmr_factor=1.):
            """
            Volume Mixing Ratio profile object.
            :param name: c.f. File
            :param path: c.f. File
            :param ext: c.f. File
            :param exists: c.f. File
            :param find: c.f. File
            :param label: c.f. File
            :param color: c.f. File
            :param linestyle: c.f. File
            :param marker: c.f. File
            :param pressures: pressure of the VMR profile
            :param vmr: Volume Mixing Ratio
            :param vmr_uncertainty_factor: uncertainty on VMR (errorbars)
            :param vmr_factor: VMR factor multiplier (for test/debug)
            """
            import os

            super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

            if pressures is None:
                self.pressures = np.array([])
            else:
                self.pressures = pressures

            if vmr is None:
                self.vmr = np.array([])
            else:
                self.vmr = vmr * vmr_factor
                if len(self.vmr) <= 1:
                    self.vmr = np.array([self.vmr])

            if vmr_uncertainty_factor is None:
                self.vmr_uncertainty_factor = np.array([])
            else:
                self.vmr_uncertainty_factor = vmr_uncertainty_factor
                if len(self.vmr_uncertainty_factor) <= 1:
                    self.vmr_uncertainty_factor = np.array([self.vmr_uncertainty_factor])

            self.vmr_factor = vmr_factor

            if label == '':
                self.label = name.replace('^', '').replace('$', '')
            else:
                self.label = label
            self.color = color
            self.linestyle = linestyle
            self.marker = marker

            if os.path.isfile(self.file) and find:
                self.load()

        def load(self):
            """Read the VMR profile file and load the parameters."""
            data = self.read()
            self.pressures = data[0]
            self.vmr = data[1]
            if len(data) >= 3:
                self.vmr_uncertainty_factor = data[2]
            else:
                self.vmr_uncertainty_factor = np.zeros(np.size(self.pressures))

        def ppmv2vmr(self):
            """Convert VMR from Part Per Million Volume to Volume Mixing Ratio."""
            self.vmr /= 1E6

        def save(self):
            """Save the VMR profile file."""
            data = [self.DataColumn(self.pressures, force_exp_notation=True, precision=2),
                    self.DataColumn(self.vmr, force_exp_notation=True, precision=2)]
            self.write(data, title=f'pressure(bar) [{self.name}]')

        def vmr2ppmv(self):
            """Convert VMR from Volume Mixing Ratio to Part Per Million Volume."""
            self.vmr *= 1E6

    PATH_IN = os.path.join(File.PATH_IN, 'molecules')
    PATH_ABSORPTION_CROSS_SECTIONS = os.path.join(PATH_IN, 'absorption_cross_sections', '')
    PATH_LINES = Lines.PATH_IN
    PATH_PARAM = os.path.join(PATH_IN, 'parameters', '')
    PATH_VMR = VMRProfile.PATH_IN
    PARAM_NAMELIST_NAME = 'molecule_parameters'
    PT_PARAM_NAMELIST_NAME = 'post_treatment_parameters'

    def __init__(self, name, rotational_partition_exponent=0.0, molar_mass=0.0, cutoff=0.0, n_vibrational_band=0,
                 vibrational_bands_wavenumber=0.0, vibrational_bands_degeneracy=0,
                 vmr_profile=None, lines=None, retrieve=False, vmr_factor=1.0, is_continuum=False,
                 color='k'):
        """
        Contains all the molecular parameters.
        :param name: name of the molecule
        :param rotational_partition_exponent: rotational partition exponent of the molecule (~1.0 if linear, ~3/2 else)
        :param molar_mass: (g.mol-1) molar mass of the molecule
        :param cutoff: (cm-1) distance from the center of the line where the sublorentzian profile replace the voigt
        :param n_vibrational_band: number of vibrational bands
        :param vibrational_bands_wavenumber: (cm-1) vibrational bands wavenumber
        :param vibrational_bands_degeneracy: vibrational bands degeneracy
        :param vmr_profile: VMR profile of the molecule
        :param lines: lines parameters of the molecule
        :param retrieve: if True, the molecule will be retrieved
        :param vmr_factor: VMR factor of the molecule (2 means 2 times the a priori VMR)
        :param is_continuum: if True, the molecule is considered part of a continuum
        :param color: color of the molecule plot
        :type vibrational_bands_wavenumber: np.array
        :type vibrational_bands_degeneracy: np.array
        :type vmr_profile: self.VMRProfile
        """
        self.name = name
        self.rotational_partition_exponent = rotational_partition_exponent
        self.molar_mass = molar_mass
        self.cutoff = cutoff
        self.n_vibrational_band = n_vibrational_band

        self.vibrational_bands_wavenumber = vibrational_bands_wavenumber
        self.vibrational_bands_degeneracy = vibrational_bands_degeneracy

        if n_vibrational_band > 0:
            if not hasattr(vibrational_bands_wavenumber, '__iter__'):
                vibrational_bands_wavenumber = np.asarray([vibrational_bands_wavenumber])
            if len(vibrational_bands_wavenumber) < n_vibrational_band:
                self.vibrational_bands_wavenumber = np.concatenate((vibrational_bands_wavenumber,
                                                                    np.zeros(8 - len(vibrational_bands_wavenumber))))

            if not hasattr(vibrational_bands_degeneracy, '__iter__'):
                vibrational_bands_degeneracy = np.asarray([vibrational_bands_degeneracy])
            if len(vibrational_bands_degeneracy) < n_vibrational_band:
                self.vibrational_bands_degeneracy = np.concatenate((vibrational_bands_degeneracy,
                                                                    np.zeros(8 - len(vibrational_bands_degeneracy))))

        self.lines = lines
        self.vmr_factor = vmr_factor
        self.retrieve = retrieve
        self.is_continuum = is_continuum
        self.color = color

        self.vmr_profile = vmr_profile

    def load(self):
        """Load the parameters and VMR profile of a molecule, but not the lines file which is generally heavy."""
        self.load_parameters_from_nml()
        self.load_vmr_profile()

    def load_all(self):
        """Load the parameters, lines, and VMR profile of a molecule."""
        self.load_parameters_from_nml()
        self.load_vmr_profile()
        self.load_lines()

    def load_lines(self):
        """
        Get molecule lines file from a GEISA-formatted file.
        """
        try:
            self.lines = Molecule.Lines(self.name)
        except FileNotFoundError:
            warn(f'molecule line file \'{self.name}.{Molecule.Lines.EXT}\' not found while loading lines', Warning)
            print(f'Download the line file for this molecule from the GEISA database (parameters A to F) and put it '
                  f'into the \'{self.Lines.PATH_IN}\' directory')

    def load_vmr_profile(self):
        """
        Load molecule VMR profile from a data file.
        """
        try:
            self.vmr_profile = self.VMRProfile(f'^{self.name}$', self.VMRProfile.PATH_IN, find=True)
        except FileNotFoundError:
            warn(f'molecule VMR profile file \'{self.name}.{Molecule.VMRProfile.EXT}\' '
                 'not found while loading VMR profiles', Warning)
            print(f'Generate an VMR profile for this molecule with the exact same grid as your atmosphere '
                  f'temperature profile and put it into the \'{self.VMRProfile.PATH_IN}\' directory')

    def load_parameters_from_nml(self):
        """
        Load molecule parameters from a Fortran namelist.
        """
        file = NameListFile(path=self.PATH_PARAM, name=self.name)

        try:
            name_lists = file.read()
        except FileNotFoundError:
            warn(f'molecule parameters file \'{file}\' not found while loading parameters', Warning)
            print(f'Initialise the parameters of this molecule then save them using \'Molecule.save_parameters()\'')
            return

        parameters = name_lists[self.PARAM_NAMELIST_NAME]
        if self.PT_PARAM_NAMELIST_NAME in name_lists:
            parameters.update(name_lists[self.PT_PARAM_NAMELIST_NAME])

        for parameter in parameters:
            if hasattr(self, parameter):
                if len(parameters[parameter].value) == 1:
                    value = parameters[parameter].value[0]
                else:
                    value = parameters[parameter].value

                if parameter == 'name' and self.name != value:
                    raise NameError(f'tried to read parameters of Molecule \'{self.name}\' in file \'{file.file}\' '
                                    f'specified for Molecule \'{value}\'')

                setattr(self, parameter, value)
            else:
                warn(f'parameter \'{parameter}\' found in file \'{file.file}\' is not a Molecule attribute', Warning)

    def save_parameters(self):
        """
        Save molecule parameters in a Fortran namelist.
        N.B. Since namelist names are hardcoded in Fortran, the more elegant way of naming the namelist after
        the molecule name (and removing the parameter 'name') is not practically possible.
        """
        namelist = {self.PARAM_NAMELIST_NAME: {
            'name': NameListFile.NameListLine(value=self.name, comment='name of the molecule'),
            'molar_mass': NameListFile.NameListLine(value=self.molar_mass, comment='(g.mol-1) molar mass'),
            'rotational_partition_exponent': NameListFile.NameListLine(value=self.rotational_partition_exponent,
                                                                       comment='rotational partition exponent'),
            'cutoff': NameListFile.NameListLine(value=self.cutoff, comment='(cm-1) sublorentzian cutoff'),
            'n_vibrational_band': NameListFile.NameListLine(value=self.n_vibrational_band,
                                                            comment='number of vibrational band'),
            'vibrational_bands_wavenumber': NameListFile.NameListLine(value=self.vibrational_bands_wavenumber,
                                                                      comment='(cm-1) vibrational bands wavenumber'),
            'vibrational_bands_degeneracy': NameListFile.NameListLine(value=self.vibrational_bands_degeneracy,
                                                                      comment='vibrational bands degeneracies')
        },
            self.PT_PARAM_NAMELIST_NAME: {
                'color': NameListFile.NameListLine(value=self.color, comment='line/marker color for plots')
            }}

        NameListFile(path=self.PATH_PARAM, name=self.name).write(namelist)

    def vmr_constant2vmr_profile(self, vmr, pressures):
        """
        Convert a float into a VMR profile for the given pressures.
        :param vmr: value of the VMR profile at all the pressures
        :param pressures: (bar) pressures of the VMR profile
        """
        if vmr is None:
            raise TypeError('Cannot convert instance \'NoneType\' into VMR')
        elif vmr <= 0.0:
            raise ValueError('Volume mixing ratio must be > 0')

        self.vmr_profile = self.VMRProfile(name=self.name, path=self.VMRProfile.PATH_IN,
                                           pressures=pressures, vmr=np.ones(np.size(pressures)) * vmr)
        self.vmr_profile.save()
        self.vmr_profile.load()

    def get_saturation_profile(self, pressures, temperatures):
        if self.name == 'H2O':
            saturation_pressures = []

            for temperature in temperatures:
                saturation_pressures.append(self.get_h2o_saturation_pressure(temperature))

            saturation_pressures = np.asarray(saturation_pressures)
        elif self.name == 'AsH3':
            saturation_pressures = []

            for temperature in temperatures:
                saturation_pressures.append(self.get_ash3_saturation_pressure(temperature))

            saturation_pressures = np.asarray(saturation_pressures)
        elif self.name == 'NH3':
            saturation_pressures = []

            for temperature in temperatures:
                saturation_pressures.append(self.get_nh3_saturation_pressure(temperature))

            saturation_pressures = np.asarray(saturation_pressures)
        elif self.name == 'PH3':
            saturation_pressures = []

            for temperature in temperatures:
                saturation_pressures.append(self.get_ph3_saturation_pressure(temperature))

            saturation_pressures = np.asarray(saturation_pressures)
        else:
            warn(f'no saturation profile model available for molecule \'{self.name}\'', Warning)

            return

        saturation_profile = saturation_pressures / pressures
        saturation_profile[np.where(saturation_profile > 1.0)] = 1.0

        return saturation_profile

    @staticmethod
    def get_ash3_saturation_pressure(temperature):
        """
        Calculate the AsH3 pressure of saturation.
        Sources: Fray & Schmitt 2009, Wagner and Pruss 1993
        :param temperature: (K) temperature
        :return: (bar) AsH3 saturation pressure
        """
        temperature_triple_point = 156.23  # (K) temperature of AsH3 triple point (Fray & Schmitt 2009)
        pressure_triple_point = 2.98E-2  # (bar) pressure of AsH3 triple point (Fray & Schmitt 2009)

        temperature_critical_point = 373.1  # (K) temperature of NH3 critical point (Lide 2006)
        pressure_critical_point = 113.5  # (bar) pressure of NH3 critical point (Lide 2006)

        if temperature <= temperature_triple_point:  # semi-empirical sublimation pressure
            # Coefficients from Fray and Schmitt 2009
            # No data were collected below 136.15 K, so P_sat below this temperature is extrapolated from F&S formula
            a = [1.176E1, -2.382E3, 0, 0, 0, 0, 0]
            saturation_pressure = 0

            for i, ai in enumerate(a):
                saturation_pressure += ai / temperature ** i

            saturation_pressure = np.exp(saturation_pressure)
        elif temperature <= temperature_critical_point:  # interpolated data of condensation pressure
            # Stull 1947 has data for AsH3 up to 220 K, but I could not access the article
            temperatures_ref = [temperature_triple_point, temperature_critical_point]  # (K) more detailed in Stull 1947
            saturation_pressures_ref = [pressure_triple_point, pressure_critical_point]  # (bar) idem

            # Log-interpolation
            saturation_pressure = np.exp(
                np.interp(np.log(temperature), np.log(temperatures_ref), np.log(saturation_pressures_ref))
            )
        else:  # species behave like a super-critical fluid
            saturation_pressure = pressure_critical_point

        return saturation_pressure

    @staticmethod
    def get_h2o_saturation_pressure(temperature):
        """
        Calculate the H2O pressure of saturation.
        Sources: Fray & Schmitt 2009, Flatau et al. 1992.
        :param temperature: (K) temperature
        :return: (bar) H2O saturation pressure
        """
        temperature_triple_point = 273.16  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
        pressure_triple_point = 6.11657E-3  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

        temperature_critical_point = 647.096  # (K) temperature of H2O critical point (IAPWS 2011)
        pressure_critical_point = 220.64  # (bar) pressure of H2O critical point (IAPWS 2011)

        if temperature <= temperature_triple_point:  # semi-empirical sublimation pressure
            # Coefficients from Feistel and Wagner 2007
            e = [20.996967, 3.724375, -13.920548, 29.698877, -40.197239, 29.788048, -9.130510]
            eta = 0

            for i, ei in enumerate(e):
                eta += ei * (temperature / temperature_triple_point) ** i

            saturation_pressure = pressure_triple_point * \
                np.exp(3 / 2 * np.log(temperature / temperature_triple_point) +
                       (1 - temperature_triple_point / temperature) * eta)
        elif temperature <= temperature_critical_point:  # semi-empirical condensation pressure
            # Coefficients from Wagner and Pruss 1993 (IAPWS 1992)
            # This formula misses the triple point by less than 0.08 percent
            a = [-7.85951783, 1.84408259, -11.7866497, 22.6807411, -15.9618719, 1.80122502]
            tau = 1 - temperature / temperature_critical_point

            saturation_pressure = pressure_critical_point * np.exp(temperature_critical_point / temperature * (
                a[0] * tau +
                a[1] * tau**1.5 +
                a[2] * tau**3 +
                a[3] * tau**3.5 +
                a[4] * tau**4 +
                a[5] * tau**7.5
            ))
        else:  # species behave like a super-critical fluid
            saturation_pressure = pressure_critical_point

        return saturation_pressure

    @staticmethod
    def get_nh3_saturation_pressure(temperature):
        """
        From Fray and Schmitt 2009
        :param temperature: (K) temperature
        :return: (bar) NH3 saturation pressure
        """
        temperature_triple_point = 195.41  # (K) temperature of NH3 triple point (Fray & Schmitt 2009)
        pressure_triple_point = 0.0609  # (bar) pressure of NH3 triple point (Fray & Schmitt 2009)

        temperature_critical_point = 405.5  # (K) temperature of NH3 critical point (Lide 2006)
        pressure_critical_point = 113.5  # (bar) pressure of NH3 critical point (Lide 2006)

        if temperature <= temperature_triple_point:  # semi-empirical sublimation pressure
            # Coefficients from Fray and Schmitt 2009
            if temperature < 15:
                warn('semi-empirical relation (Fray and Schmitt 2009) not valid below 15 K, extrapolating anyway',
                     Warning)

            a = [1.596E1, -3.537E3, -3.310E4, 1.742E6, -2.995E7, 0, 0]
            saturation_pressure = 0

            for i, ai in enumerate(a):
                saturation_pressure += ai / temperature ** i

            saturation_pressure = np.exp(saturation_pressure)
        elif temperature <= temperature_critical_point:  # interpolated data of condensation pressure
            # Data from The CRC Handbook of Chemistry and Physics (Lide 2006)
            temperatures_ref = np.arange(200.0, 300.1, 5.0)  # (K) no data above 300 K
            saturation_pressures_ref = np.asarray([
                8.7,
                12.6,
                17.9,
                24.9,
                34.1,
                45.9,
                60.8,
                79.6,
                103,
                131,
                165,
                207,
                256,
                313,
                381,
                460,
                552,
                655,
                774,
                909,
                1062
            ]) * 1E-2  # (bar)

            # Add triple point to data to smooth the curve
            temperatures_ref = np.append(temperature_triple_point, temperatures_ref)
            saturation_pressures_ref = np.append(pressure_triple_point, saturation_pressures_ref)

            # Add critical point to data for temperatures > 300 K
            temperatures_ref = np.append(temperatures_ref, temperature_critical_point)
            saturation_pressures_ref = np.append(saturation_pressures_ref, pressure_critical_point)

            # Log-interpolation
            saturation_pressure = np.exp(
                np.interp(np.log(temperature), np.log(temperatures_ref), np.log(saturation_pressures_ref))
            )
        else:  # species behave like a super-critical fluid
            saturation_pressure = pressure_critical_point

        return saturation_pressure

    @staticmethod
    def get_ph3_saturation_pressure(temperature):
        """
        From Fray and Schmitt 2009
        :param temperature: (K) temperature
        :return: (bar) PH3 saturation pressure
        """
        temperature_triple_point = 139.41  # (K) temperature of PH3 triple point (Fray & Schmitt 2009)
        pressure_triple_point = 3.6E-2  # (bar) pressure of PH3 triple point (Fray & Schmitt 2009)

        temperature_critical_point = 324.5  # (K) temperature of PH3 critical point
        pressure_critical_point = 65.4  # (bar) pressure of PH3 critical point

        if temperature < 110:
            warn('no data (Lide 2006) below 110 K, returning minimum known value (1 mbar)', Warning)
            saturation_pressure = 1E-3
        elif temperature <= temperature_critical_point:  # interpolated data of condensation pressure
            # Data from The CRC Handbook of Chemistry and Physics (Lide 2006)
            if temperature < temperature_triple_point:
                warn('data (Lide 2006) below triple point temperature are extrapolated from condensation vapor pressure'
                     '(c.f. Fray & Schmitt 2009)',
                     Warning)
            elif temperature > 300:
                warn('data (Lide 2006) not available above 300 K, extrapolating anyway...', Warning)

            temperatures_ref = np.arange(110.0, 300.1, 5.0)  # (K) no data below 105 K
            saturation_pressures_ref = np.asarray([
                0.1,
                0.2,
                0.4,
                0.7,
                1.3,
                2.3,
                3.9,
                6.2,
                9.6,
                14.5,
                21.1,
                30.0,
                41.6,
                56.6,
                75.6,
                99.2,
                128,
                163,
                205,
                254,
                312,
                379,
                456,
                544,
                644,
                756,
                881,
                1019,
                1172,
                1341,
                1525,
                1725,
                1942,
                2176,
                2428,
                2699,
                2987,
                3295,
                3621
            ]) * 1E-2  # (bar)

            # Add triple point to data to smooth the curve
            temperatures_ref_ = np.append(np.arange(110.0, temperature_triple_point, 5.0), temperature_triple_point)
            saturation_pressures_ref_ = np.append(
                saturation_pressures_ref[np.where(temperatures_ref < temperature_triple_point)], pressure_triple_point
            )

            saturation_pressures_ref = np.append(
                saturation_pressures_ref_,
                saturation_pressures_ref[np.where(temperatures_ref > temperature_triple_point)]
            )
            temperatures_ref = np.append(
                temperatures_ref_, temperatures_ref[np.where(temperatures_ref > temperature_triple_point)]
            )

            # Add critical point to data for temperatures > 300 K
            temperatures_ref = np.append(temperatures_ref, temperature_critical_point)
            saturation_pressures_ref = np.append(saturation_pressures_ref, pressure_critical_point)

            # Log-interpolation
            saturation_pressure = np.exp(
                np.interp(np.log(temperature), np.log(temperatures_ref), np.log(saturation_pressures_ref))
            )
        else:  # species behave like a super-critical fluid
            saturation_pressure = pressure_critical_point

        return saturation_pressure

    @staticmethod
    def get_vmr_from_scale_height(vmr_ref, pressure_ref, pressure, scale_factor=1):
        """
        Get a Volume Mixing Ratio at a given pressure for a given scale height.
        Equation comes from:
            P = P0 * exp(-z/H)
        V the VMR will vary as:
            V = V0 * exp(-z/kH)
        Where k is the scale factor.
        :param vmr_ref: volume mixing ratio at reference pressure p0
        :param pressure_ref: (bar) reference pressure
        :param pressure: pressure at which to calculate the volume mixing ratio
        :param scale_factor: scale height factor; higher values means slower decrease; negative value means increasing
        :return:
        """
        return vmr_ref * np.exp(np.log(pressure / pressure_ref) / scale_factor)


class Sky(DataFile):
    """Sky data file. Contains sky (Earth's atmosphere transmittance) informations."""
    PATH = os.path.join(File.PATH, 'inputs', 'skies', '')
    EXT = DataFile.EXT

    def __init__(self, name, path=PATH, exists=False, find=False,
                 wavenumbers=None, transmittances=None):
        """
        Sky object.
        :param name: c.f. File
        :param path: c.f. File
        :param exists: c.f. File
        :param find: c.f. File
        :param wavenumbers: (cm-1) wavenumber
        :param transmittances: sky transmittance
        """
        super().__init__(name=name, path=path, ext=self.EXT, exists=exists, find=find)

        if wavenumbers is None:
            self.wavenumbers = np.array([])
        else:
            self.wavenumbers = np.asarray(wavenumbers)
        if transmittances is None:
            self.transmittances = np.array([])
        else:
            self.transmittances = np.asarray(transmittances)

    def load(self):
        """Read the sky file."""
        self.wavenumbers, self.transmittances = self.read()

    def save(self):
        """Write the sky file."""
        data = [self.DataColumn(self.wavenumbers, precision=10), self.DataColumn(self.transmittances)]
        self.write(data, title=f'w(cm-1) transmittance')


class TemperatureProfile(DataFile):
    PATH_IN = os.path.join(File.PATH_IN, 'atmospheres', 'temperature_profiles', '')
    EXT = DataFile.EXT

    def __init__(self, name, path=PATH_IN, ext=EXT, exists=False, find=False, label='', color='k', linestyle='-',
                 marker='', pressures=None, temperatures=None):
        """
        Temperature profile object.
        :param name: c.f. File
        :param path: c.f. File
        :param ext: c.f. File
        :param exists: c.f. File
        :param find: c.f. File
        :param label: c.f. File
        :param color: c.f. File
        :param linestyle: c.f. File
        :param marker: c.f. File
        :param pressures: pressure (bar)
        :param temperatures: temperature (K)
        """

        super().__init__(name=name, path=path, ext=ext, exists=exists, find=find)

        if pressures is None:
            self.pressures = np.array([])
        else:
            self.pressures = pressures

        if temperatures is None:
            self.temperatures = np.array([])
        else:
            self.temperatures = temperatures
            if np.size(self.temperatures) <= 1:
                self.temperatures = np.asarray([self.temperatures])

        if label == '':
            self.label = name
        else:
            self.label = label
        self.color = color
        self.linestyle = linestyle
        self.marker = marker

    def load(self):
        """Read the temperature profile file and load the parameters."""
        self.pressures, self.temperatures = self.read()

    def save(self):
        """Write the temperature profile file."""
        data = [self.DataColumn(self.pressures, force_exp_notation=True, precision=3),
                self.DataColumn(self.temperatures)]
        self.write(data, title=f'Jupiter pressure(bar) T(K)')


class Atmosphere(PickleFile):
    """Atmosphere object. Contains atmospheric informations."""
    PATH_DATA = os.path.join(File.PATH_DATA, 'atmospheres', '')
    PATH_DATA_TEMP = os.path.join(File.PATH_DATA, 'temperature_profiles', '')
    PATH_DATA_VMR = os.path.join(File.PATH_DATA, 'vmr_profiles', '')
    PATH_IN = os.path.join(File.PATH_IN, 'atmospheres', '')
    PATH_IN_MOL = Molecule.PATH_PARAM
    PATH_IN_CROSS_SECTIONS = Molecule.PATH_ABSORPTION_CROSS_SECTIONS
    PATH_IN_TEMP = TemperatureProfile.PATH_IN
    PATH_IN_VMR = Molecule.VMRProfile.PATH_IN
    EXT = PickleFile.EXT

    DEFAULT_NAME = 'new_atmosphere'
    DEFAULT_ATM_FILE = os.path.join(PATH_IN, 'default_atmosphere.cfg')

    # Load default atmosphere
    DEFAULT = None
    if DEFAULT is None:
        try:
            with open(DEFAULT_ATM_FILE, 'r') as f:
                for line in f:
                    DEFAULT = PickleFile(name=line, path=PATH_IN, find=True)
        except FileNotFoundError or NotADirectoryError:
            DEFAULT = None
            info(f'file \'{DEFAULT_ATM_FILE}\' not found, no default atmosphere will be used')

    def __init__(self, name=DEFAULT_NAME, path=PATH_IN, ext=EXT,
                 pressure_min=0, pressure_max=0, molecules=None, clouds=None, sky=None,
                 temperature_profile=None):
        """
        Contains all the atmospheric parameters.
        :param pressure_min: (bar) minimum pressure
        :param pressure_max: (bar) maximum pressure
        :param molecules: molecules in the atmosphere
        :param clouds: clouds in the atmosphere
        :param sky: telluric transmittance
        :param temperature_profile: temperature profile T(pressure) of the atmosphere
        """
        super().__init__(name=name, path=path, ext=ext)

        if Atmosphere.DEFAULT is not None:
            local_args = locals()
            default_atmosphere = Atmosphere.from_pkl(Atmosphere.DEFAULT.name)
            for arg in local_args:
                if not local_args[arg]:
                    setattr(self, arg, copy.deepcopy(getattr(default_atmosphere, arg)))
                else:
                    setattr(self, arg, local_args[arg])
        else:
            self.pressure_min = pressure_min
            self.pressure_max = pressure_max
            self.molecules = molecules

            if clouds is None:
                self.clouds = [Cloud(pressure_base=1.0, pressure_top=1.0, transmittance=1)]
            else:
                self.clouds = clouds

            if sky is None:
                self.sky = Sky('sky_' + name)
                if not os.path.isfile(self.sky.file):
                    self.sky = None
            else:
                self.sky = sky

            if temperature_profile is None:
                self.temperature_profile = TemperatureProfile('temperature_profile')
                if not os.path.isfile(self.temperature_profile.file):
                    self.temperature_profile = None
            else:
                self.temperature_profile = temperature_profile

    def _interpol_vmr_profile(self, path_in=PATH_DATA_VMR, path_out=PATH_IN_VMR, temperature_profile=None,
                              molecules=None):
        """
        Interpolate or extrapolate VMR profiles from a file to match with the pressure(T) file,
        and output a new file.
        :param path_in:
        :param path_out:
        :param temperature_profile:
        :param molecules:
        :type molecules: dict
        """

        if temperature_profile is None:
            if self.temperature_profile is None:
                temperature_profile = DataFile(file=os.path.join(self.PATH_IN_TEMP, 'temperature_profile.dat'))
            else:
                temperature_profile = self.temperature_profile

        if molecules is None:
            molecules = self.molecules

        p_atm = temperature_profile.read(set_parameters=False)[0]

        for molecule in molecules:
            mol_file = DataFile(name=molecule + '_vmr', path=path_in)

            if not os.path.isfile(mol_file.file):
                if molecules[molecule].vmr_profile is not None:
                    warn(f'VMR profile file \'{mol_file.file}\' does not exist', Warning)
                    continue
                elif molecules[molecule].vmr > 0:
                    p = [-1]
                    a = molecules[molecule].vmr
                else:
                    continue
            else:
                # read source VMR profile file
                p, a = mol_file.read(set_parameters=False)

            if len(p) == 1:
                a = np.ones(np.size(p_atm)) * a
            else:
                if not np.all(np.diff(p) > 0):
                    p = p[::-1]
                    a = a[::-1]
                if not np.all(np.diff(p) > 0):
                    raise ValueError(f'pressure array of file \'{mol_file}\' is not ordered')
                if not np.all(np.diff(p_atm) > 0):
                    p_atm = p_atm[::-1]
                if not np.all(np.diff(p_atm) > 0):
                    raise ValueError(f'pressure array of file \'{mol_file}\' is not ordered')

                a = np.exp(interpx(np.log(p_atm), np.log(p), np.log(a)))

                # Put back in right order
                p_atm = p_atm[::-1]
                a = a[::-1]

                for k in range(np.size(a)):
                    if a[k] < 1.17549435E-38:  # tiny(float4)
                        a[k] = 1.17549435E-38

            p = p_atm

            out_file = DataFile(file=os.path.join(path_out, molecule + '.dat'))
            data = [DataFile.DataColumn(p, precision=2, force_exp_notation=True),
                    DataFile.DataColumn(a, precision=2, force_exp_notation=True)]
            out_file.write(data, title=f'Jupiter p(bar) [{molecule}]')

    def _interpol_temperature_profile(self, temperature_profile_in=None, temperature_profile_out=None,
                                      pressure_min=None, pressure_max=None, n_level=0):
        """
        Extrapolate the atmospheric profile from a file to match p_min and p_max specifications, and output a new file.
        If the extrapolation is needed, the function will add extra layers spaced with the first (or last) step in the
        original file until the desired extrema is reached. The extrapolation is linear.
        :param temperature_profile_in: temperature profile to interpolate
        :param temperature_profile_out: interpolated temperature profile
        :param pressure_min: (bar) minimum pressure
        :param pressure_max: (bar) maximum pressure
        :param n_level: number of level in new file
        """

        if temperature_profile_in is None:
            temperature_profile_in = TemperatureProfile('temperature_profile', path=self.PATH_DATA_TEMP, find=True)
        elif isinstance(temperature_profile_in, str):
            temperature_profile_in = TemperatureProfile(temperature_profile_in, path=self.PATH_DATA_TEMP, find=True)

        if temperature_profile_out is None:
            temperature_profile_out = TemperatureProfile('temperature_profile', path=self.PATH_IN_TEMP, find=True)
        elif isinstance(temperature_profile_in, str):
            temperature_profile_out = TemperatureProfile(temperature_profile_out, path=self.PATH_DATA_TEMP, find=True)

        if pressure_min is None:
            pressure_min = self.pressure_min

        if pressure_max is None:
            pressure_max = self.pressure_max

        temperature_profile_in.load()
        p = temperature_profile_in.pressures
        t = temperature_profile_in.temperatures

        if n_level > 0:
            if not np.all(np.diff(p) > 0):
                p = p[::-1]
                t = t[::-1]
            if not np.all(np.diff(p) > 0):
                raise ValueError(f'pressure array of file \'{temperature_profile_in}\' is not ordered')

            new_p = np.exp(np.linspace(np.log(pressure_min), np.log(pressure_max), n_level))
            new_t = np.exp(interpx(np.log(new_p), np.log(p), np.log(t)))

            # Put back in right order
            p = new_p[::-1]
            t = new_t[::-1]
        else:
            # extrapolation/truncating of pressure and T if needed
            if p[0] < p[-1]:
                p = p[::-1]
                t = t[::-1]

            if pressure_max > p[0]:
                # Extend pressure
                dp_log10 = np.log10(p[0]) - np.log10(p[1])
                px = np.exp(np.arange(np.log10(p[0]) + dp_log10, np.log10(pressure_max), dp_log10) * np.log(10))
                px = np.concatenate([[pressure_max], px[::-1]])
                p = np.concatenate([px, p])

                # Extend T
                dt_log10 = np.log10(t[0]) - np.log10(t[1])
                tx_log10 = np.zeros(np.size(px))
                tx_log10[0] = np.log10(t[0]) + dt_log10
                if np.size(px) >= 2:
                    for i in range(np.size(px) - 2):
                        tx_log10[i + 1] = tx_log10[i] + dt_log10
                    tx_log10 = tx_log10[::-1]
                    tx_log10[0] = tx_log10[1] + dt_log10 * (np.log10(p[0]) - np.log10(p[1])) / dp_log10
                else:
                    tx_log10[0] = tx_log10[0] + dt_log10 * (np.log10(p[0]) - np.log10(p[1])) / dp_log10
                t = np.concatenate([np.exp(tx_log10 * np.log(10)), t])
            elif pressure_max < p[0]:
                # Truncate pressure
                dp_log10 = np.log10(p[0]) - np.log10(p[1])
                i_x = np.max(np.where(p > pressure_max))
                p = p[i_x:]
                p[0] = pressure_max

                # Truncate T
                t = t[i_x:]
                dt_log10 = np.log10(t[0]) - np.log10(t[1])
                t[0] = np.exp((np.log10(t[1]) + dt_log10 * (np.log10(p[0]) - np.log10(p[1])) / dp_log10) * np.log(10))

            if pressure_min < p[-1]:
                # Extend pressure
                dp_log10 = np.log10(p[-1]) - np.log10(p[-2])
                px = np.exp(np.arange(np.log10(p[-1]) + dp_log10, np.log10(pressure_min), dp_log10) * np.log(10))
                px = np.concatenate([px, [pressure_min]])
                p = np.concatenate([p, px])

                # Extend T
                dt_log10 = np.log10(t[-1]) - np.log10(t[-2])
                tx_log10 = np.zeros(np.size(px))
                tx_log10[0] = np.log10(t[-1]) + dt_log10
                if np.size(px) >= 2:
                    for i in range(np.size(px) - 2):
                        tx_log10[i + 1] = tx_log10[i] + dt_log10
                    tx_log10[-1] = tx_log10[-2] + dt_log10 * (np.log10(p[-1]) - np.log10(p[-2])) / dp_log10
                else:
                    tx_log10[-1] = tx_log10[-1] + dt_log10 * (np.log10(p[-1]) - np.log10(p[-2])) / dp_log10
                t = np.concatenate([t, np.exp(tx_log10 * np.log(10))])
            else:
                if pressure_min > p[-1]:
                    # Truncate pressure
                    dp_log10 = np.log10(p[-1]) - np.log10(p[-2])
                    i_x = np.min(np.where(p < pressure_min))
                    p = p[:i_x + 1]
                    p[-1] = pressure_min

                    # Truncate T
                    t = t[:i_x + 1]
                    dt_log10 = np.log10(t[-1]) - np.log10(t[-2])
                    t[-1] = np.exp(
                        (np.log10(t[-2]) + dt_log10 * (np.log10(p[-1]) - np.log10(p[-2])) / dp_log10) * np.log(10))

        data = [TemperatureProfile.DataColumn(p, precision=2, force_exp_notation=True),
                TemperatureProfile.DataColumn(t, precision=4)]
        temperature_profile_out.write(data, title='Jupiter pressure(bar) T(K)')

        self.temperature_profile = temperature_profile_out

    def init_all(self, air2h2he=False):
        """
        Initialise all the atmospheric files.
        :param air2h2he: if True and if H2 and He in molecules, convert air lines to H2-He lines
        """
        if self.temperature_profile is None:
            self._interpol_temperature_profile()
        elif self.temperature_profile.path == self.PATH_DATA_TEMP or not os.path.isfile(
                self.temperature_profile.file):
            self._interpol_temperature_profile()

        self.temperature_profile.load()

        if self.molecules is not None:
            for mol in self.molecules:
                self.molecules[mol].save_parameters()
                self.molecules[mol].load_vmr_profile()

            if 'H2' in self.molecules and 'He' in self.molecules and air2h2he:
                print('H2-He atmosphere detected, converting air lines to H2-He lines')
                for molecule in self.molecules:
                    if self.molecules[molecule].lines is not None:
                        molecule_data = copy.deepcopy(self.molecules[molecule])
                        molecule_data.lines.file = \
                            molecule_data.lines.PATH_DATA + molecule_data.name + molecule_data.lines.EXT
                        molecule_data.lines.load()
                        molecule_data.lines.air2h2he()
                        molecule_data.lines.save(
                            file=self.molecules[molecule].lines.PATH_IN +
                            self.molecules[molecule].lines.name + self.molecules[molecule].lines.EXT)

    def get_saturation_profiles(self):
        saturation_profiles = {}
        for molecule in self.molecules:
            saturation_profiles[molecule] = self.molecules[molecule].get_saturation_profile(
                self.temperature_profile.pressures,
                self.temperature_profile.temperatures
            )

        return saturation_profiles

    def use_as_default(self):
        Atmosphere.DEFAULT = self
        self.save()

        with open(self.DEFAULT_ATM_FILE, 'w') as f:
            f.write(f'{self.name}')

    @classmethod
    def from_pkl(cls, name, path=PATH_IN):
        """Load the atmosphere from a .pkl file."""
        return cls.loader(File(name=name, path=path, ext='pkl', find=True).file)

    @staticmethod
    def airmass2angle(airmass):
        """Convert airmass into angle (degree)"""
        return np.rad2deg(np.arccos(1 / airmass))

    @staticmethod
    def sub_coordinates2angle(sub_latitude, sub_longitude, latitude, longitude):
        latitude = np.deg2rad(latitude)
        sub_latitude = np.deg2rad(sub_latitude)
        longitude_difference = np.deg2rad(abs(longitude - sub_longitude))
        angle = np.rad2deg(np.arccos(np.sin(latitude) * np.sin(sub_latitude) +
                                     np.cos(latitude) * np.cos(sub_latitude) * np.cos(longitude_difference)))
        return angle
