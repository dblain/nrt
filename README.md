# NRT

## Synopsis
NRT is a software able to perform simulation and analysis of atmospheric
spectra. The core of the program is parallelised for better performances.
NRT is capable of: 
  * calculating synthetic spectra line-by-line from parameters given by the 
user. Molecules can be added at will;
  * retrieving molecular abundance and cloud transmittance from an observed 
spectrum and given a priori with a retrieving method;
  * retrieving entire spectral cubes and make maps of a given feature.

## Motivation
NRT is written with universality in mind, it is meant to be versatile and 
flexible. It is written in Python and Fortran, and should be easy to
understand, maintain and modify.

## Installation
The installation process is slightly more complicated on Windows than on Linux, 
mainly because of the many required dependencies.

### Installing from source
1. Before installing the program, you need to install its dependencies:
    * [gfortran](https://gcc.gnu.org/wiki/GFortran)
    * [OpenMPI 4.0.0 or later](https://www.open-mpi.org/)
    * [Python 3.7.1 or later](https://www.python.org/)
2. Download the tar.gz archive in the "dist" directory, then extract
its content anywhere you want. There should be all the needed files and
directories.
3. Open a terminal window and put yourself in the NRT directory.
4. Execute the command below. This will install all the dependencies.
    * On Linux: ```sudo pip3 install .```
    * On Windows: ```python -m pip3 install .```
    
    You can add the ```--upgrade``` flag to automatically update Python libraries.
5. **[Optional]** You can remove the "PKG-INFO" file as well as all the "empty.txt"
files. You can remove them automatically by opening a python3 console then
executing the following script in the NRT directory:

    ```python
    from setup import remove_empty_files
    remove_empty_files()
    ```

### Note on Windows
Installing numpy and scipy on Windows is not trivial. Unofficial installation packages and installation instructions can be found [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/). Check as well the [matplotlib dependencies](https://matplotlib.org/users/installing.html).
Installing [Cygwin](https://www.cygwin.com/) is also **strongly** recommended.

### MacOSX
NRT was **not** tested on MacOSX.

## Wiki
For more informations, tutorials and documentations, visit the 
[NRT wiki](https://gitlab.obspm.fr/dblain/nrt/wikis/home).
