"""
Example of minimal script to initialise all the elements required to retrieve a spectrum in NRT.
This script in its current state uses the same inputs as in the NRT wiki example. You may modify this file to suit your
needs, for example by changing the molecules or the instrument properties.
You may launch this script only the first time you use the software. After execution you can run NRT and retrieve
spectra, and you can safely remove this file. Using this file to initialise NRT is not mandatory.
For more informations or details, visit the NRT getting started page on the NRT wiki.

Instructions: in a terminal, put yourself into the NRT directory, then type (on Linux):
    ```python3 quick_init.py```

Important:
    - it is assumed that the temperature profile file, the abundance profile files and the lines files already exists;
    - lines files must follow the GEISA format, with at least parameters A to F;
    - VMR profiles must have exactly the same pressure grid than the temperature profile;
    - VMR profile files and temperature profile file must follow the DataFile format (described in the NRT wiki).
"""
from nrt.atmosphere import *
from nrt.interface import mpicompile_nrt
from nrt.spectrum import *

# Starting message
print('\nInitialising NRT (Info messages can safely be ignored)...\n')

# Atmosphere composition
molecules_in_atmosphere = {
    'N2': Molecule(
          name='N2',  # name of the molecule, use the exact same name you gave to the line file
          rotational_partition_exponent=1.0,  # rotational partition exponent of the molecule
          molar_mass=28.0134,  # (g.mol-1) molar mass of the molecule
          cutoff=35,  # (cm-1) Lorentzian cutoff distance
    ),
    'O2': Molecule(
          name='O2',
          rotational_partition_exponent=1.0,
          molar_mass=31.9988,
          cutoff=35,
    ),
    'H2O': Molecule(
           name='H2O',
           rotational_partition_exponent=1.5,
           molar_mass=18.0153,
           cutoff=35,
    )
}

# Initialise the temperature profile
temperature_profile = TemperatureProfile('my_t_profile', find=True)
temperature_profile.load()

# Initialise the atmosphere
atmosphere = Atmosphere(
    name='my_atmosphere',
    pressure_min=1E-3,  # (bar) minimum pressure of the atmosphere
    pressure_max=1.0,  # (bar) maximum pressure of the atmosphere
    molecules=molecules_in_atmosphere,
    temperature_profile=temperature_profile
)

# Generate an instrument
instrument = Instrument(
    name='my_instrument',
    lsf_fwhm=3.0,  # Full Width Half Maximum of the instrument Point Spread Function
    lsf_shape='gaussian'  # shape of the instrument Point Spread Function
)

# Generate a light source
light_source = Target.LightSource(
    name='sol',
    radius=695700.0E3,  # (m) radius of the Sun (IAU 2015)
    effective_temperature=5772.0  # (K) effective temperature of the Sun (IAU 2015)
)

# Generate a target
target = Target(
    name='default_target',
    mass=1E24,  # (kg) mass of the target
    equatorial_radius=1E3,  # (m) equatorial radius of the target
    polar_radius=1E3,  # (m) polar radius of the target
    light_source=light_source,  # light source illuminating the target
    light_source_range=150E9,  # (m) distance between the target and the light source
    latitude=0.0,  # (deg)
    longitude=0.0,  # (deg)
    incidence_angle=0.0,  # (deg) angle between the light source and the normal of the surface
    emission_angle=0.0  # (deg) angle between the observer and the normal of the surface
)

# Save the parameters of all the molecules in atmosphere
atmosphere.init_all()

# Save current atmosphere as default
atmosphere.use_as_default()

# Save the instrument
instrument.save()

# Save the light source
light_source.save()

# Save the current target as default
target.use_as_default()

# Compile NRT
mpicompile_nrt()

# Done without error
print('\nNRT successfully initialised !\n')
