"""
Setup file for distribution and installation.
"""
import os
from distutils.command.sdist import sdist
from warnings import warn

from setuptools import setup

from source.Python import version

software_structure = ('bin', 'data', 'figures',
                      {'inputs': ({'atmospheres': ('vmr_profiles', 'temperature_profiles')},
                                  'instruments',
                                  {'molecules': ('absorption_cross_sections', 'lines', 'parameters')},
                                  'noises', 'skies', 'spectra',
                                  {'targets': 'light_sources'})},
                      {'outputs': ('spectra', 'spectral_cubes', 'vmr_profiles')},
                      {'source': ('Fortran', 'Python')})  # list of all the software directories

empty_files = []  # list of all the empty files used to create the structure


class __CustomSDistCommand(sdist):
    """Custom sdist command that check if all the directories are correctly defined, add empty files to build the
    structure properly and clean the build files."""

    def run(self):
        for directory in software_structure:
            check_dirs_from_dict(directory, add_empty_file=True)

        sdist.run(self)

        if os.path.isfile('MANIFEST'):
            print('removing manifest file \'MANIFEST\'')
            os.remove('MANIFEST')

        print('removing empty files')
        remove_empty_files()


def _fill_empty_files():
    """Automatically determine the empty files paths. Needed for the structure to be built properly."""
    global empty_files

    for directory in software_structure:
        main_directories = check_dirs_from_dict(directory, check=False)

        if isinstance(main_directories, list):
            for main_directory in main_directories:
                empty_files.append(main_directory)
        else:
            empty_files.append(main_directories)

    for i, dirs in enumerate(empty_files):
        empty_files[i] = os.path.join(dirs, 'empty.txt')


def _read_readme():
    """Read the README.md file to add it to the 'long_description' of the setup."""
    return open(os.path.join(os.path.dirname(__file__), 'README.md')).read()


def check_dirs_from_dict(directory, path='', make=False, add_empty_file=False, check=True):
    """
    Return recursively a list of paths from a dictionary.
    Example: {'foo': 'bar', 'bid', {'ule': 'foobar'}} will return ['foo/bar', 'foo/bid', 'foo/ule/foobar'].
    :param directory: dictionary or string
    :param path: path of 'directory' if it is a subdirectory
    :param make: if True, make the directory if it does not exists
    :param add_empty_file: if True, add empty files in the directories
    :param check: if True, info the user if a directory does not exists
    :return: list of string containing the paths of the subdirectories
    """
    if isinstance(directory, dict):
        if not os.path.isdir(os.path.join(path, list(directory.keys())[0])):
            if make:
                os.makedirs(os.path.join(path, list(directory.keys())[0]))
            elif check:
                warn(f'directory \'{os.path.join(path, list(directory.keys())[0])}\' does not exists', Warning)

        path = os.path.join(path, list(directory.keys())[0])

        if isinstance(directory[list(directory.keys())[0]], tuple):
            subdirectories = []
            for subdirectory in directory[list(directory.keys())[0]]:
                sub_subdirectories = check_dirs_from_dict(
                    subdirectory, path, make=make, add_empty_file=add_empty_file, check=check)
                if isinstance(sub_subdirectories, list):
                    for sub_subdirectory in sub_subdirectories:
                        subdirectories.append(sub_subdirectory)
                else:
                    subdirectories.append(sub_subdirectories)
        else:
            subdirectories = check_dirs_from_dict(
                directory[list(directory.keys())[0]], path, make=make, add_empty_file=add_empty_file, check=check)

        return subdirectories
    elif not os.path.isdir(os.path.join(path, directory)):
        if make:
            os.makedirs(os.path.join(path, directory))
        elif check:
            warn(f'directory \'{os.path.join(path, directory)}\' does not exists', Warning)
    else:
        if add_empty_file:
            open(os.path.join(path, directory, 'empty.txt'), 'a').close()

        return os.path.join(path, directory)


def remove_empty_files():
    if not empty_files:
        _fill_empty_files()

    for file in empty_files:
        if os.path.isfile(file):
            os.remove(file)
    print('empty files removed')


if __name__ == '__main__':
    _fill_empty_files()

    setup(
        name='NRT',
        version=version.__version__,
        author='Doriann Blain',
        author_email='doriann.blain@gmail.com',
        description='A parallelised line-by-line radiative transfer code coupled with a non-linear inversion method.',
        license='MIT',
        keywords='spectroscopy line-by-line radiative transfer non-linear inversion retrieving atmosphere',
        url='https://gitlab.obspm.fr/dblain/nrt',
        packages=['nrt'],
        package_dir={'nrt': 'source/Python'},
        data_files=[('metadata', ['README.md', 'LICENSE.md', 'CHANGELOG.md']),
                    ('empty_files', empty_files),
                    ('quick init file', ['quick_init.py']),
                    ('mpi_files', ['source/Fortran/module_math.f90',
                                   'source/Fortran/module_mpi_utils.f90',
                                   'source/Fortran/module_nrt_retrieval.f90',
                                   'source/Fortran/module_nrt_IO.f90',
                                   'source/Fortran/module_nrt_parameters.f90',
                                   'source/Fortran/module_nrt_physics.f90',
                                   'source/Fortran/nrt.f90'])],
        long_description=_read_readme(),
        classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Environment :: Console',
            'Intended Audience :: Science/Research',
            'License :: MIT',
            'Natural Language:: English',
            'Operating System :: Microsoft',
            'Operating System :: POSIX',
            'Programming Language :: Fortran',
            'Programming Language :: Fortran :: OpenMPI',
            'Programming Language :: Python :: 3.7',
            'Programming Language :: Python :: 3 :: Only',
            'Topic :: Scientific/Engineering :: Atmospheric Science',
        ],
        install_requires=[
            'cartopy',
            'jdcal',
            'matplotlib',
            'numpy',
            'opencv-python',
            'PyQt5',
            'scikit-image',
            'scipy',
            'spiceypy',
            'tqdm'
        ],
        cmdclass=dict(sdist=__CustomSDistCommand),
    )
